﻿#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QCoreApplication>
#include <QApplication>
#include <QFont>
#include <QDebug>
#include <QResource>
#include <QDir>
#include <QFontDatabase>

#include "src/cpp/hqmlmodeltoolbar.h"
#include "src/cpp/hmotioncontroller.h"
#include "src/cpp/hqmlmodelio.h"
#include "src/cpp/hqmlmodelmotion.h"
#include "src/cpp/hqmlmodelcamera.h"
#include "src/cpp/hqmlmodelsettingcoord.h"
#include "src/cpp/hqmlmodelsettingflex.h"
#include "src/cpp/hqmlmodelCapture.h"
#include "src/cpp/hqmlmodelflow.h"
#include "src/cpp/hqmlmodelDispenseActionSetting.h"
#include "src/cpp/hqmlmodelDispensePatternSetting.h"
#include "src/cpp/hqmlmodeldatavisual.h"
#include "src/cpp/hqmlmodelgluecontroller.h"
#include "src/cpp/hqmlcameraparameter.h"
#include "src/cpp/hqmlPatternRecognition.h"
#include "src/cpp/hqmlLoading.h"
#include "src/cpp/hqmlLoadingDispensingSetting.h"
#include "src/cpp/hqmlLoadingCoordSetting.h"

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication  app(argc, argv);
    HMotionController::GetInstance()->openController();
    HMotionController::GetInstance()->startForceThread();
    app.setFont(QFont("Microsoft YaHei UI", 9));
    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/resource/icons/editorFont.ttf") == -1) {
        qWarning() << "Failed to load fontello.ttf";
        return -1;
    }
    QQmlApplicationEngine engine;

    qmlRegisterType<HQmlModelToolbar>("HQmlModelToolbar", 1, 0, "HQmlModelToolbar");
    qmlRegisterType<HQmlModelIO>("HQmlModelIO", 1, 0, "HQmlModelIO");
    qmlRegisterType<HQmlModelMotion>("HQmlModelMotion", 1, 0, "HQmlModelMotion");
    qmlRegisterType<HQmlModelCamera>("HQmlModelCamera", 1, 0, "HQmlModelCamera");
    qmlRegisterType<HQmlModelSettingCoord>("HQmlModelSettingCoord", 1, 0, "HQmlModelSettingCoord");
    qmlRegisterType<HQmlModelSettingFlex>("HQmlModelSettingFlex", 1, 0, "HQmlModelSettingFlex");
    qmlRegisterType<HQmlModelCapture>("HQmlModelCapture", 1, 0, "HQmlModelCapture");
    qmlRegisterType<HQmlModelDispenseActionSettings>("HQmlModelDispenseActionSettings", 1, 0, "HQmlModelDispenseActionSettings");
    qmlRegisterType<HQmlModelDispensePatternSettings>("HQmlModelDispensePatternSettings", 1, 0, "HQmlModelDispensePatternSettings");
    qmlRegisterType<HQmlModelFlow>("HQmlModelFlow", 1, 0, "HQmlModelFlow");
    qmlRegisterType<HQmlModelDataVisual>("HQmlModelDataVisual", 1, 0, "HQmlModelDataVisual");
    qmlRegisterType<HQmlModelGlueController>("HQmlModelGlue", 1, 0, "HQmlModelGlue");
    qmlRegisterType<HQmlCameraParameter>("HQmlCameraParameter", 1, 0, "HQmlCameraParameter");
    qmlRegisterType<HQmlPatternRecognition>("HQmlPatternRecognition", 1, 0, "HQmlPatternRecognition");
    qmlRegisterType<HQmlLoading>("HQmlLoading", 1, 0, "HQmlLoading");
    qmlRegisterType<HQmlLoadingDispensingSetting>("HQmlLoadingDispensingSetting", 1, 0, "HQmlLoadingDispensingSetting");
    qmlRegisterType<HQmlLoadingCoordSetting>("HQmlLoadingCoordSetting", 1, 0, "HQmlLoadingCoordSetting");
    //engine.rootContext()->setContextProperty("modBusInstance",HModbus::GetInstance());

    engine.load(QUrl(QStringLiteral("qrc:/src/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    int rcode = app.exec();
    HMotionController::GetInstance()->Release();
    return rcode;
}



