# ChipPaster
This software is used to control the gantry system, made by the CoreTech company at Wuxi, JiangSu province, China. The gantry system is used to automatically assembly the HGTD modules and also load the modules to the detector units. In order to achieve this automation assembly goal, this software has been integrated the high precision picking, placing and gluing function.
## Prerequisites
Developing or compiling this software you need this software:
1. ACS control software
2. qt
3. vs2019
## Compilation
The full compilation proceduces are listed below:
1. Install the ACS software. You can go to the ASC official website download the software [SPiiPlus ADK Suite v3.11.01](https://acsmotioncontrol.com/engineering-resources-downloads/?cat=spiiplus-adk-suite)(may need the account) or just download from this [link](https://ihepbox.ihep.ac.cn/ihepbox/index.php/s/tVF9uWwDTOgq23b). After you download this file, you just unzip and install it directly.
2. Install the Qt development environment. Go to the [Qt download webpage](https://www.qt.io/download)-->**Downloads for open source users**-->**Go open source**-->**Download the Qt Online Installer**. Fill your personal information to get the open source Qt. Choose the options below when intalling the Qt software:
![Qt software options](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/Qt-option1.png)
![Qt software options](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/Qt-option2.png)
3. Install the vs2019. Go to the VS official website to download the vs2019 and intall it. Don't forget to install the C++ windows package. Below is the screenshot of the Chinese version of the C++ windows package.
![C++ windows package](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/vs2019-C%2B%2Bwindows.png) 
4. Copy the lib files. Copy the [libs](https://ihepbox.ihep.ac.cn/ihepbox/index.php/s/vN2WgXk5j40yWiE)(**unzip**) to the same directory of ChipPaster.
5. Compile the source code. Don't forget choosing the 64-bit compiler.
![64-bit compiler](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/Qt-64-bit-compiler.png)
6. Copy the dll files. Copy the file libs\OpenCv\opencv4.1.1\x64\bin\opencv_world411d.dll to the folder build-ChipPaster-Desktop_Qt_5_15_0_MSVC2019_64bit-Debug\debug.
If you finish all the above steps, the compilation of the control software is done.

**tips**
1. If you want to run the software in the release mode, you need to copy all the previous dll files in the [release folder](https://ihepbox.ihep.ac.cn/ihepbox/index.php/s/ktHXE920fj4vQ5i) to the current release folder. If you still need some dll files, you can go to the Qt installation directory (C:\Qt\5.15.0\msvc2019_64\bin) to copy the files to the release folder.
2. Don't forget rerun the qmake when you add new files to the project.
## Coding Instructions
This software is based on C++ and Qt Quick, the code are all written using C++ and qml. The C++ code is the core code and qml is the GUI code.  
The C++ source codes are in the folder src/cpp, and the qml source codes are in src/qml. The File description is below:
### 1. C++ code description
* [DispenseAction.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/DispenseAction.h)/[DispenseAction.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/DispenseAction.cpp): Definition of the dispense action including dot dispense or line dispense action.
* [DispensePattern.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/DispensePattern.h)/[DispensePattern.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/DispensePattern.cpp): Definition of the dispense pattern which is a set of many dispense actions.
* [grapimageprovider.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/grapimageprovider.h)/[grapimageprovider.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/grapimageprovider.cpp):
* [hmathfunctions.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hmathfunctions.h): Definition of some math functions which might be used in some place
* [hmotioncontroller.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hmotioncontroller.h)/[hmotioncontroller.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hmotioncontroller.cpp): The motion cotroller is defined here and many funtions that can be called by qml also defined here.
* [hnetworkcommond.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hnetworkcommond.h)/[hnetworkcommond.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hnetworkcommond.cpp): Some basic network commands are defined here. Fuctions here also can be called by qml.
* [hqmlmodelcamera.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelcamera.h)/[hqmlmodelcamera.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelcamera.cpp): The associated code for the camera
* [hqmlmodelCapture.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelCapture.h)/[hqmlmodelCapture.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelCapture.cpp): Definition of the capture action that the gantry header sucks the vacumn spreader and bare modules or flex via the vacumn spreader, can be called by the qml.
* [hqmlmodeldatavisual.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodeldatavisual.h)/[hqmlmodeldatavisual.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodeldatavisual.cpp): For the motion position visualization, called by the qml
* [hqmlmodelDispenseActionSetting.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelDispenseActionSetting.h)/[hqmlmodelDispenseActionSetting.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelDispenseActionSetting.cpp): Definition of the settings of dispense actions, used to connect the GUI and the C++ data structure.
* [hqmlmodelDispensePatternSetting.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelDispensePatternSetting.h)/[hqmlmodelDispensePatternSetting.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelDispensePatternSetting.cpp): Dispense pattern settings are defined here, connecting the qml GUI and the c++ data structure.
* [hqmlmodelflow.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelflow.h)/[hqmlmodelflow.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelflow.cpp): Implementation of the workflow for picking, placing and gluing.
* [hqmlmodelgluecontroller.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelgluecontroller.h)/[hqmlmodelgluecontroller.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelgluecontroller.cpp): Dispense controller code for setting the dispense controller and sending commands for dispensing and so on. More details you can just directly read the source code.
* [hqmlmodelio.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelio.h)/[hqmlmodelio.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelio.cpp): The associated code for the ACS IO control, also called by the qml
* [hqmlmodelmotion.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelmotion.h)/[hqmlmodelmotion.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelmotion.cpp): The motion controll code for the gantry movement (X,Y,Z,T), can be called by the qml.
* [hqmlmodelsettingcoord.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelsettingcoord.h)/[hqmlmodelsettingcoord.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodelsettingcoord.cpp): For the coordination settings, can be used for the transformation calculation between the camera coordination and the gantry table coordination.
* [hqmlmodeltoolbar.h](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodeltoolbar.h)/[hqmlmodeltoolbar.cpp](https://github.com/zenghaowhu/ChipPaster/blob/main/src/cpp/hqmlmodeltoolbar.cpp): Functions used in the toolbar are here.

### 2. qml code description
* [main.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/main.qml): The main windows of this software, which has 7 parts showing below:
![GUI-mian](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-main.png)
1. **Main menu**: The settings of the camera, coordination, despendse action and despense pattern are here. The corresponding qml file is [HMainMenuBar.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMainMenuBar.qml). 
2. **Main tool bar**: Including the suck, release, start and stop button. The correspoding qml file is [HMainToolBar.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMainToolBar.qml). 
3. **Motion panel**: The basic motion control panel and the corresponding qml file is [HMotionPanel.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMotionPanel.qml). 
4. **IO panel**: The ACS controller IOs can be swith on/off in this panel. The corresponding qml file is [HIOPanel.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HIOPanel.qml). 
5. **Workflow setting panel**: The settings for all the tools used in the assembly proccedures. Corresponding qml file is [HPanelFlowSettings.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HPanelFlowSettings.qml). 
6. **Data visualization panel**: The panel for data display. Corresponding qml file is [HDataVisual.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDataVisual.qml). 
7. **Glue controller panel**: The panel for the glue despensing settings. Corresponding qml file is [HQmlModelGlueController.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HQmlModelGlueController.qml). 
More details of each panel will introduced in their qml file part.
* [HMainMenuBar.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMainMenuBar.qml): The code for main menu bar, including 4 menus: File, Settings, View and Help. The Settings menu contains [Camera](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCamera.qml), [Grab And Release Parameters](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCapture.qml), [Coordinate Associations](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCoord.qml) and Gluing Settings ([Dispense Action Settings](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDispenseActionSetting.qml) and [Dispense Pattern Settings](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDispensePatternSetting.qml)). A screen shot of the Setting menu is below:
![GUI-menu-Settings](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-menu-Settings.png)
* [HMainToolBar.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMainToolBar.qml): The qml code for four functional button: 
 1. **Suck**: Move the gantry head to suck something using current Grab and Release Parameters
 2. **Release**: Move the gantry head to release the tool or sensor using current Grab and Release Paramenters.
 3. **Start**: Start the workfolw, the parameters of which are in the workflow setting panel.
 4. **Stop**: Stop the workflow.
* [HMotionPanel.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HMotionPanel.qml): The qml code for the basic motion control and motion parameter display for X, Y, Z and T.
* [HIOPanel.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HIOPanel.qml): IO control panel code is here, including 32 one-bit logic switch and force display of the pressure sensor.
* [HPanelFlowSettings.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HPanelFlowSettings.qml): The qml code for the workflow settings, including tools settings, chip settings and panel settings. One screenshot of this panel is below:
![GUI-panel-workflow](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-panel-workflow.png)
* [HDataVisual.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDataVisual.qml): The qml code for the axis value and force value display. One screenshot of this UI is below:
![GUI-panel-digitalscope](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-panel-digitalscope.png)
* [HQmlModelGlueController.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HQmlModelGlueController.qml): The qml code for the dispensing controller. You can control the dispense machine remotely by RS232 port in this panel. The UI of the Glue Controller is below:
![GUI-panel-gluecontroller](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-panel-gluecontroller.png)
* [HSettingCamera.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCamera.qml): The qml code for the camera setting, which including the IP, port and debugging command. One screenshot of the camera setting GUI is showed below:
![GUI-cameraSetting](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-cameraSetting.png)
* [HSettingCapture.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCapture.qml): The qml code for the parameters setting of one suck or release action. You can add and delete the grab and release settings here and all the parameters will be automatically stored in the configure files. The GUI of this capture setting is below:
![GUI-CaptureSetting](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-CaptureSetting.png)
* [HSettingCoord.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HSettingCoord.qml): The qml code for the coordination transformation setting, some are out of date. The GUI is showed below:
![GUI-CoordinationSetting](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-CoordinationSetting.png)
* [HDispenseActionSetting.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDispenseActionSetting.qml): The qml code for setting the dispensing action parameters, and one can add or delete dispensing actions in this window. All the parameters will be automatically stored in the config file. The GUI of this dispensing action setting window is showed below:
![GUI-DispensingActionSetting](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-DispensingActionSetting.png):
* [HDispensePatternSetting.qml](https://github.com/zenghaowhu/ChipPaster/blob/main/src/qml/HDispensePatternSetting.qml): The qml code for setting the dispensing pattern, which is the collection of many dispensing actions. You can add, delete and run the dispensing patterns by clicking the buttons in this window. The GUI of the Dispensing Pattern Setting is showed below:
![GUI-DispensingPatternSetting](https://github.com/zenghaowhu/ChipPaster/blob/main/img-folder/GUI-DispensingPatternSetting.png)