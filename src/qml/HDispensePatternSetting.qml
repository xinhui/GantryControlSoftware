import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.1


import HQmlModelDispensePatternSettings 1.0

Window {

    id: window
    title: "Dispense Pattern Setting"
    width: 600
    height: 450
    minimumWidth: 600
    maximumWidth: 600
    minimumHeight: 450
    maximumHeight: 450
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    //property var configControls: [lineEditwaitingz, lineEditgluingz, lineEditmovingspeed, lineEditmovingacc, lineEditgluingspeed,
    //                            lineEditgluingacc, lineEditgluingtime, lineEditgluingpressure, lineEditx1, lineEdity1, lineEditx2, lineEdity2, lineEditprotectionforce]

    property ListModel patternmodel: ListModel {}
    //property ListModel choosemodel: ListModel {}

    function initUI() {
        modelPattern.updateUI()
        comboBoxActions.model = modelPattern.getActionNames()
    }

    function reindex()
    {
        for (var i = 0; i < patternmodel.count; i++)
        {
            patternmodel.setProperty(i, "index",i)
        }
    }

    HQmlModelDispensePatternSettings {
        id: modelPattern
        onUpdateControlData: {
            chooseEvent.enabled = false
            choose.model = patternNames
            choose.currentIndex = currentPatternIndex
            chooseEvent.enabled = true
            dispensemode.currentIndex = currentPatternType
            patternmodel.clear()
            for (var k = 0; k < currentActionNames.length; ++k) {
                patternmodel.append({"index":k,"name":currentActionNames[k]})
                //patternmodel.setProperty(k, "name", currentActionNames[k])
            }

            //dispensemode.currentIndex = cfgDatas[0]
            //for (var k = 0; k < configControls.length; ++k) {
            //    configControls[k].text = cfgDatas[k + 1]
            //}
            textX.text = patternOffset[0]
            textY.text = patternOffset[1]
            textT.text = patternOffset[3]
        }

        onUpdateGluingOffset: {
            textX.text = _strGluingOffset[0]
            textY.text = _strGluingOffset[1]
            textT.text = _strGluingOffset[3]
        }

    }

    Connections{
        target: modelDispensePattern
        onUpdateGluingOffset: {
            textX.text = _strGluingOffset[0]
            textY.text = _strGluingOffset[1]
            textT.text = _strGluingOffset[3]
        }
    }

    Component.onCompleted: {
        initUI()
    }

    GridLayout {
        id: gridLayout1
        height: 755
        anchors.fill: parent
        rowSpacing: 20
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 260
        anchors.topMargin: 10
        columns: 4

        Label {
            id: label10
            width: 49
            height: 12
            text: qsTr("Pattern Name:")
        }

        ComboBox {
            id: choose
            width: 279
            height: 40
            Layout.fillWidth: true
            editable: true
            selectTextByMouse: true
            //model: choosemodel
            Connections {
                id: chooseEvent
                enabled: false
                function onCurrentIndexChanged() {
                    modelPattern.setCurrentPattern(choose.currentIndex)
                }
            }
        }

        MyButton {
            id: myButton
            Layout.preferredWidth: 120
            Layout.preferredHeight: 40
            myRadius: 0
            buttonText: "new"
            onMyButttonLeftclicked: {
                modelPattern.addPattern(choose.editText,dispensemode.currentIndex)
            }
        }

        MyButton {
            id: myButton1
            Layout.preferredHeight: 40
            Layout.preferredWidth: 120
            myRadius: 0
            buttonText: "delete"
            isEnable: choose.model.length !== 0
            onMyButttonLeftclicked: {
                modelPattern.removePattern(choose.currentIndex)
            }
        }

        GroupBox {
            id: groupBox
            width: 700
            height: 261
            Layout.fillWidth: true
            Layout.columnSpan: 4

            GridLayout {
                id: gridLayout
                anchors.fill: parent
                rows: 8
                columns: 4

                Label {
                    id: label7
                    text: qsTr("pattern type:")
                }

                ComboBox {
                    id: dispensemode
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    currentIndex: 0
                    model: ["dot","line"]
                    onCurrentTextChanged: {
                        //printtext.text = currentIndex
                        //modelPattern.setConfigData(0,currentIndex)
                    }
                }

                ComboBox {
                    id: comboBoxActions
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    currentIndex: 0
                    model: ["action1", "action2", "action3", "action4"]
                    //model: modelPattern.getActionNames()
                    onCurrentTextChanged: {

                    }
                    onHighlighted: {
                        this.model = modelPattern.getActionNames()
                    }
                }

                MyButton {
                    id: buttonAddAction
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 120
                    myRadius: 0
                    buttonText: "Add Action"
                    onMyButttonLeftclicked: {
                        modelPattern.appendActionToCurrentPattern(comboBoxActions.currentText)
                    }
                }

                /*Rectangle {
                    id: rectangle1
                    width: 200
                    height: 200
                    color: "#ffffff"
                    Layout.columnSpan: 2
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    Text {
                        id: printtext
                        x: 0
                        y: 0
                        width: 230
                        height: 30
                        text: qsTr("for testing")
                    }
                }*/

            }
        }


        Rectangle {
            id: rectangle2
            width: 200
            height: 200
            color: "#00ffffff"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 4
        }
    }

    TableView {
            id: tableView
            y: 146
            height: 200

            anchors.top: groupBox.bottom
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.left: parent.left
            anchors.right: parent.right
            //anchors.bottom: parent.bottom
            antialiasing: true
            highlightOnFocus: true
            selectionMode: SelectionMode.SingleSelection

            TableViewColumn {
                id: firstcol
                role: "index"
                title: qsTr("index")
                movable: false
                resizable: false
                delegate: Rectangle {
                    color: "gray"
                    border.width: 1
                    //border.color: "palevioletred"
                    antialiasing: true

                    Text {
                        id: textIndex
                        anchors.verticalCenter: parent.verticalCenter
                        //anchors.fill: parent
                        anchors.horizontalCenter: parent.horizontalCenter
                        //anchors.leftMargin: 50
                        //font.family: "Calibri"
                        color: "black"
                        text: styleData.value.toString()
                        font.pixelSize: parent.height * 0.35
                    }
                }
                width: removeIndex.width
            }

            TableViewColumn {
                id: col1
                role: "name"
                title: qsTr("Action Name")
                movable: false
                resizable: false
                width: (tableView.viewport.width - firstcol.width)
                delegate: Rectangle {
                    color: "mistyrose"
                    //border.width: 1
                    //border.color: "palevioletred"
                    border.width: parent.activeFocus ? 2 : 1
                    border.color: parent.activeFocus ? "red" : "black"
                    antialiasing: true

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        //anchors.fill: parent
                        anchors.horizontalCenter: parent.horizontalCenter
                        //anchors.leftMargin: 50
                        //font.family: "Calibri"
                        color: "black"
                        text: styleData.value.toString()
                        font.pixelSize: parent.height * 0.35
                    }
                }
                /*delegate: Text {
                    //readOnly: true
                    //selectByMouse: true
                    anchors.fill: parent
                    //font.family: "Calibri"
                    text: styleData.value
                    color: "black"
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pixelSize: parent.height * 0.35
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor
                        acceptedButtons: Qt.NoButton
                    }
                    background: Rectangle {
                        anchors.fill: parent
                        color: "mistyrose"
                        border.width: parent.activeFocus ? 2 : 1
                        border.color: parent.activeFocus ? "red" : "black"
                    }

                }*/
            }



            model: patternmodel



            headerDelegate: Rectangle {
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color: "gainsboro"
                    }
                    GradientStop {
                        position: 1.0
                        color: "darkgrey"
                    }
                }

                color: styleData.selected ? "blue" : "darkgray"
                height: 50

                border.color: "black"
                border.width: 1
                radius: 0
                Text {
                    //font.family: "Calibri"
                    anchors.centerIn: parent
                    text: styleData.value
                    font.pixelSize: parent.height * 0.35
                }
            }
            rowDelegate: Component {
                Rectangle {
                    color: "wheat"
                    height: 30 //(table.height-42)/tablemode.count
                }
            }

            style: TableViewStyle {
                highlightedTextColor: "#00CCFE"
                backgroundColor: "wheat"
            }
        }

    SpinBox {
        id: removeIndex
        //Layout.preferredHeight: -1
        //Layout.fillWidth: true
        anchors.top: tableView.bottom
        anchors.topMargin: 5
        anchors.leftMargin: 10
        anchors.left: parent.left
        editable: true
        from: 0
        to: patternmodel.count > 0 ? patternmodel.count -1 : 0
        onValueChanged: {
            //model.setConfigData(8, value)
        }
    }

    MyButton {
        id: buttonRemoveOneAction
        height: removeIndex.height
        width: removeIndex.width
        anchors.top: tableView.bottom
        anchors.topMargin: 5
        anchors.left: removeIndex.right
        anchors.leftMargin: 10
        isEnable: patternmodel.count > 0
        myRadius: 0
        buttonText: "Remove Action"
        onMyButttonLeftclicked: {
            modelPattern.removeActionFromCurrentPattern(removeIndex.value)
            //patternmodel.remove(removeIndex.value)
            //reindex()
            //firstcol.delegate.textIndex.text = firstcol.styleData.row
        }
    }

    MyButton {
        id: buttonRunPattern
        height: removeIndex.height
        width: removeIndex.width
        anchors.top: tableView.bottom
        anchors.topMargin: 5
        anchors.right: buttonClearActions.left
        anchors.rightMargin: 5
        myRadius: 10
        buttonText: "Run Pattern"
        onMyButttonLeftclicked: {
            modelPattern.runCurrentPattern()
        }
    }

    MyButton {
        id: buttonClearActions
        height: removeIndex.height
        width: removeIndex.width
        anchors.top: tableView.bottom
        anchors.topMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 10
        myRadius: 10
        buttonText: "Clear Actions"
        onMyButttonLeftclicked: {
            //patternmodel.clear()
            modelPattern.clearCurrentActions()
        }
    }

    GridLayout{
        id: gridLayout2
        height: 50
        anchors.fill: parent
        anchors.topMargin: 390
        rowSpacing: 20
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        columns: 5

        Label {
            text: qsTr("offset(XYT):")
            verticalAlignment: Qt.AlignVCenter
            horizontalAlignment: Qt.AlignLeft
        }
        TextField {
            id: textX
            selectByMouse: true
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            onTextChanged: {
                model.setControlData(36, text)
            }
        }
        TextField {
            id: textY
            selectByMouse: true
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            readOnly: true
            onTextChanged: {
                model.setControlData(37, text)
            }
        }
        TextField {
            id: textT
            selectByMouse: true
            Layout.fillWidth: true
            Layout.preferredHeight: 35
            readOnly: true
            onTextChanged: {
                model.setControlData(38, text)
            }
        }
        MyButton {
            imgRes: "qrc:/resource/icons/location.svg"
            Layout.preferredHeight: 35
            Layout.preferredWidth: 35
            rightButtonEnable: false
            myRadius: 2
            onMyButttonLeftclicked: {
            }
        }

    }

}
