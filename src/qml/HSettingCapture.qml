﻿import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelCapture 1.0

Window {

    id: window

    width: 600
    height: 350
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var configControls: [lineEdit1, lineEdit2, lineEdit3, lineEdit4, lineEdit5, lineEdit6, lineEdit7, lineEdit8]

    function initUI() {
        model.updateUI()
    }

    HQmlModelCapture {
        id: model
        onUpdateControlData: {
            chooseEvent.enabled = false
            choose.model = cfgNames
            choose.currentIndex = currentCfg
            chooseEvent.enabled = true

            typeOfCapture.currentIndex = cfgDatas[0]
            output_Gantry.value = cfgDatas[1]
            output_vc_release.value = cfgDatas[2]
            for (var k = 0; k < configControls.length; ++k) {
                configControls[k].text = cfgDatas[k + 3]
            }
        }
    }

    Component.onCompleted: {
        initUI()
    }

    GridLayout {
        id: gridLayout1
        height: 755
        anchors.fill: parent
        rowSpacing: 20
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        columns: 4

        Label {
            id: label10
            width: 49
            height: 12
            text: qsTr("choose")
        }

        ComboBox {
            id: choose
            width: 279
            height: 40
            Layout.fillWidth: true
            editable: true
            selectTextByMouse: true

            Connections {
                id: chooseEvent
                enabled: false
                function onCurrentIndexChanged() {
                    model.setCurrentConfig(choose.currentIndex)
                }
            }
        }

        MyButton {
            id: myButton
            Layout.preferredWidth: 120
            Layout.preferredHeight: 40
            myRadius: 0
            buttonText: "new"
            onMyButttonLeftclicked: {
                model.addConfig(choose.editText)
            }
        }

        MyButton {
            id: myButton1
            Layout.preferredHeight: 40
            Layout.preferredWidth: 120
            myRadius: 0
            isEnable: choose.model.length !== 0
            buttonText: "delete"
            onMyButttonLeftclicked: {
                model.removeConfig(choose.currentIndex)
            }
        }

        GroupBox {
            id: groupBox
            width: 700
            height: 261
            Layout.fillWidth: true
            Layout.columnSpan: 4

            GridLayout {
                id: gridLayout
                anchors.fill: parent
                rows: 6
                columns: 4

                Label {
                    id: label_Type
                    text: qsTr("Type")
                }
                ComboBox {
                    id: typeOfCapture
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    model: ["Tooling","Target"]
                    onCurrentTextChanged: {
                        model.setConfigData(0,currentIndex)
                    }
                }

                Label {
                    id: label_output_inner
                    text: qsTr("output (Gantry)")
                }

                SpinBox {
                    id: output_Gantry
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    editable: true
                    to: 31
                    onValueChanged: {
                        model.setConfigData(1, value)
                    }
                }

                Label {
                    id: label
                    x: 22
                    y: 109
                    width: 30
                    height: 7
                    text: qsTr("wait pos")
                }

                TextField {
                    id: lineEdit1
                    x: 58
                    y: 93
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(3, text)
                    }
                }

                Label {
                    id: label_vc_release
                    text: qsTr("output (VC)")
                }

                SpinBox {
                    id: output_vc_release
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    editable: true
                    to: 31
                    onValueChanged: {
                        model.setConfigData(2, value)
                    }
                }

                Label {
                    id: label1
                    x: 264
                    y: 107
                    text: qsTr("fast speed(mm/s)")
                }

                TextField {
                    id: lineEdit2
                    x: 311
                    y: 93
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(4, text)
                    }
                }

                Label {
                    id: label2
                    x: 14
                    y: 56
                    width: 38
                    height: 12
                    text: qsTr("fast acc(mm/ss)")
                }

                TextField {
                    id: lineEdit3
                    x: 58
                    y: 42
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(5, text)
                    }
                }

                Label {
                    id: label3
                    x: 22
                    y: 168
                    width: 30
                    height: 7
                    text: qsTr("search pos1(mm)")
                }

                TextField {
                    id: lineEdit4
                    x: 58
                    y: 152
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(6, text)
                    }
                }

                Label {
                    id: label5
                    x: 22
                    y: 221
                    width: 30
                    height: 7
                    text: qsTr("search pos2(mm)")
                }

                TextField {
                    id: lineEdit5
                    x: 311
                    y: 152
                    width: 200
                    height: 40
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(7, text)
                    }
                }

                Label {
                    id: label6
                    x: 266
                    y: 260
                    text: qsTr("search speed(mm/s)")
                }

                TextField {
                    id: lineEdit6
                    x: 58
                    y: 205
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(8, text)
                    }
                }

                Label {
                    id: label8
                    x: 22
                    y: 260
                    text: qsTr("search acc(mm/ss)")
                }

                TextField {
                    id: lineEdit7
                    x: 311
                    y: 205
                    Layout.fillWidth: true
                     selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(9, text)
                    }
                }

                Label {
                    id: label4
                    x: 266
                    y: 219
                    text: "search force(g)"
                }

                TextField {
                    id: lineEdit8
                    x: 58
                    y: 251
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(10, text)
                    }
                }
            }
        }

        Rectangle {
            id: rectangle2
            width: 200
            height: 200
            color: "#00ffffff"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 4
        }
    }
}
