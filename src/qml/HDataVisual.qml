import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelDataVisual 1.0
import "greater/"

Item {
    id: item1
    width: 800
    height: 600

    HQmlModelDataVisual {
        id: model
    }

    Timer {
        id: timer
        interval: model.getInterval()
        repeat: true
        property real time: 0
        property var sourceChecks: [source1, source2, source3, source4, source5]
        onTriggered: {

            var lastC = -1
            for (var k = 4; k > -1; --k) {
                if (sourceChecks[k].checked) {
                    lastC = k
                    break
                }
            }

            var datas = model.getDatas()
            if (source1.checked)
                chart.processData(0, time, datas[0], lastC == 0)
            if (source2.checked)
                chart.processData(1, time, datas[1], lastC == 1)
            if (source3.checked)
                chart.processData(2, time, datas[2], lastC == 2)
            if (source4.checked)
                chart.processData(3, time, datas[3], lastC == 3)
            if (source5.checked)
                chart.processData(4, time, datas[4], lastC == 4)
            time += interval
        }

        onRunningChanged: {
            if (running) {
                axisXMin.enabled = false
                axisXMax.enabled = false
            } else {
                axisXMin.enabled = true
                axisXMax.enabled = true
            }
        }
    }

    Rectangle {
        id: rectangle
        width: 289
        color: "#ffffff"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        anchors.leftMargin: 1
        anchors.topMargin: 1

        GroupBox {
            id: groupBox
            height: 262
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 2
            anchors.leftMargin: 2
            anchors.topMargin: 2
            title: qsTr("Data Source")

            GridLayout {
                id: gridLayout1
                anchors.fill: parent
                columns: 2

                Rectangle {
                    id: color1
                    width: 200
                    height: 200
                    color: "#36dada"
                    Layout.preferredWidth: 26
                    Layout.preferredHeight: 26
                }

                CheckBox {
                    id: source1
                    text: qsTr("Position-X(mm)")
                    Layout.fillWidth: true
                    checked: model.getSource(0)
                    onCheckedChanged: {
                        model.setSource(0, checked)
                        chart.setSourceVisiable(0, checked)
                    }
                }

                Rectangle {
                    id: color2
                    width: 200
                    height: 200
                    color: "#1f65d8"
                    Layout.preferredHeight: 26
                    Layout.preferredWidth: 26
                }

                CheckBox {
                    id: source2
                    text: qsTr("Position-Y(mm)")
                    Layout.fillWidth: true
                    checked: model.getSource(1)
                    onCheckedChanged: {
                        model.setSource(1, checked)
                        chart.setSourceVisiable(1, checked)
                    }
                }

                Rectangle {
                    id: color3
                    width: 200
                    height: 200
                    color: "#58e71c"
                    Layout.preferredHeight: 26
                    Layout.preferredWidth: 26
                }

                CheckBox {
                    id: source3
                    text: qsTr("Position-Z(mm)")
                    Layout.fillWidth: true
                    checked: model.getSource(2)
                    onCheckedChanged: {
                        model.setSource(2, checked)
                        chart.setSourceVisiable(2, checked)
                    }
                }

                Rectangle {
                    id: color4
                    width: 200
                    height: 200
                    color: "#f4d914"
                    Layout.preferredHeight: 26
                    Layout.preferredWidth: 26
                }

                CheckBox {
                    id: source4
                    text: qsTr("Position-T(deg)")
                    Layout.fillWidth: true
                    checked: model.getSource(3)
                    onCheckedChanged: {
                        model.setSource(3, checked)
                        chart.setSourceVisiable(3, checked)
                    }
                }

                Rectangle {
                    id: color5
                    width: 200
                    height: 200
                    color: "#e50d0d"
                    Layout.preferredHeight: 26
                    Layout.preferredWidth: 26
                }

                CheckBox {
                    id: source5
                    text: qsTr("Force(g)")
                    Layout.fillWidth: true
                    checked: model.getSource(4)
                    onCheckedChanged: {
                        model.setSource(4, checked)
                        chart.setSourceVisiable(4, checked)
                    }
                }

                Rectangle {
                    id: rectangle2
                    width: 200
                    height: 200
                    color: "#ffffff"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                }
            }
        }

        GroupBox {
            id: groupBox1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: groupBox.bottom
            anchors.bottom: parent.bottom
            anchors.rightMargin: 2
            anchors.leftMargin: 2
            anchors.bottomMargin: 2
            anchors.topMargin: 5
            title: qsTr("Setting")

            GridLayout {
                id: gridLayout
                anchors.fill: parent
                columns: 3

                Label {
                    id: label
                    text: qsTr("Interval(ms):")
                }

                SpinBox {
                    id: spinBox
                    Layout.columnSpan: 2
                    value: model.getInterval()
                    editable: true
                    to: 1000000
                    from: 1
                    Layout.fillWidth: true
                    onValueChanged: {
                        model.setInterval(value)
                        timer.interval = value
                    }
                }

                Label {
                    id: label1
                    text: qsTr("AxisY-Max:")
                }

                TextField {
                    id: textField
                    Layout.fillWidth: true
                    selectByMouse: true
                    text: model.getMaxAxisY()
                    onAccepted: {
                        model.setMaxAxisY(text)
                        chart.setYMax(Number(text))
                    }
                }

                CheckBox {
                    id: checkBox5
                    text: qsTr("Auto")
                    Layout.rowSpan: 2
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    checked: model.getAutoAxisY()
                    onCheckedChanged: {
                        model.setAutoAxisY(checked)
                        chart.isAutoY = checked
                    }
                }

                Label {
                    id: label2
                    text: qsTr("AxisY-Min:")
                }

                TextField {
                    id: textField1
                    Layout.fillWidth: true
                    selectByMouse: true
                    text: model.getMinAxisY()
                    onAccepted: {
                        model.setMinAxisY(text)
                        chart.setYMin(Number(text))
                    }
                }

                Label {
                    id: label3
                    text: qsTr("AxisX-Min(ms):")
                }

                TextField {
                    id: axisXMin
                    Layout.fillWidth: true
                    selectByMouse: true
                    text: model.getMinAxisX()
                    onAccepted: {
                        model.setMinAxisX(text)
                        chart.setXMin(Number(text))
                    }
                }

                CheckBox {
                    id: checkBox6
                    text: qsTr("Roll")
                    Layout.rowSpan: 2
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    checked: model.getScrollAxisX()
                    onCheckedChanged: {
                        model.setScrollAxisX(checked)
                        chart.isScrolX = checked
                    }
                }

                Label {
                    id: label4
                    text: qsTr("AxisX-Max(ms):")
                }

                TextField {
                    id: axisXMax
                    text: model.getMaxAxisX()
                    selectByMouse: true
                    Layout.fillWidth: true
                    onAccepted: {
                        model.setMaxAxisX(text)
                        chart.setXMax(Number(text))
                    }
                }

                CheckBox {
                    id: checkBox
                    text: qsTr("Use OpenGL")
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.columnSpan: 3
                    onCheckedChanged: {
                        chart.setOGL(checked)
                    }
                }

                Rectangle {
                    id: rectangle1
                    width: 200
                    height: 200
                    color: "#ffffff"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 3
                }
            }
        }
    }

    Rectangle {
        id: rectangle3
        color: "#ffffff"
        anchors.left: rectangle.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: 0

        Rectangle {
            id: toolBar
            anchors.left: parent.left
            anchors.leftMargin: 1
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 5
            height: 40
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#eee"
                }
                GradientStop {
                    position: 0.5
                    color: "#ccc"
                }
            }
            Flow {
                anchors.fill: parent

                Row {
                    id: row
                    height: parent.height
                    spacing: 0

                    MyButton {
                        height: parent.height
                        width: parent.height
                        myRadius: 0
                        buttonBorderWidth: 0
                        imgRes: "qrc:/resource/icons/start.svg"
                        onMyButttonLeftclicked: {
                            timer.start()
                        }
                    }
                    MyButton {
                        height: parent.height
                        width: parent.height
                        myRadius: 0
                        buttonBorderWidth: 0
                        imgRes: "qrc:/resource/icons/stop.svg"
                        onMyButttonLeftclicked: {
                            timer.stop()
                        }
                    }
                    MyButton {
                        height: parent.height
                        width: parent.height
                        myRadius: 0
                        buttonBorderWidth: 0
                        imgRes: "qrc:/resource/icons/sweep.svg"
                        onMyButttonLeftclicked: {
                            timer.stop()
                            timer.time = 0
                            chart.clearAllData()
                            chart.setXMin(model.getMinAxisX())
                            chart.setXMax(model.getMaxAxisX())
                            chart.setYMin(model.getMinAxisY())
                            chart.setYMax(model.getMaxAxisY())
                        }
                    }
                    MyButton {
                        height: parent.height
                        width: parent.height
                        myRadius: 0
                        buttonBorderWidth: 0
                        imgRes: "qrc:/resource/icons/new.svg"
                    }
                    MyButton {
                        height: parent.height
                        width: parent.height
                        myRadius: 0
                        buttonBorderWidth: 0
                        imgRes: "qrc:/resource/icons/new.svg"
                    }
                }
            }
        }

        HChartCommon {
            id: chart
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: toolBar.bottom
            anchors.bottom: parent.bottom
            anchors.rightMargin: -8
            anchors.leftMargin: -7
            anchors.bottomMargin: -8
            anchors.topMargin: -8
            isAutoY: model.getAutoAxisY()
            isScrolX: model.getScrollAxisX()

            visibles: [model.getSource(0), model.getSource(1), model.getSource(
                    2), model.getSource(3), model.getSource(4)]
            colors: [color1.color, color2.color, color3.color, color4.color, color5.color]

            Component.onCompleted: {
                chart.init()
                setXMin(model.getMinAxisX())
                setXMax(model.getMaxAxisX())
                setYMin(model.getMinAxisY())
                setYMax(model.getMaxAxisY())
            }
        }
    }
}
