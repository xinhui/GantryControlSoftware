import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import HQmlModelCamera 1.0
import HQmlModelSettingCoord 1.0
import HQmlModelGlue 1.0

Item {
    width: 1200
    height: 800

    HQmlModelFlow {
        id: model
        onUpdateUI: {
            isComboxOk = false
            var grabNames = model.getCaptureNames()
            var patternNames = model.getPatternNames()
            for (var k = 0; k < cameraAlis.length; ++k) {
                cameraAlis[k].value = alins[k]
            }

            for (var d = 0; d < grabs.length; ++d) {
                grabs[d].model = grabNames
                grabs[d].currentIndex = combxIndexs[d]
            }
            for (var m = 0; m < locationPoss.length; ++m) {
                locationPoss[m].text = texts[m]
            }
            for (var i = 0; i < patterns.length; ++i)
            {
                patterns[i].model = patternNames
                patterns[i].currentIndex = patternIndexs[i]
            }
            for (var j = 0; j < accvels.length; j++)
            {
                accvels[j].text = accVels[j]
            }

            isComboxOk = true
        }
    }

    Component.onCompleted: {
        model.updataCtrl()
    }

    GroupBox {
        id: toolLocGroup
        x: 13
        y: 0
        width: 675
        height: 232
        title: qsTr("Location-Tools")

        GridLayout {
            id: gridLayout
            anchors.fill: parent
            columns: 7

            Label {
                id: label11
                width: 132
                height: 32
                text: qsTr("GluingToolPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos0
                x: 158
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(0, text)
                }
            }

            TextField {
                id: pos1
                x: 306
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(1, text)
                }
            }

            TextField {
                id: pos2
                x: 454
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(2, text)
                }
            }

            SpinBox {
                id: ali0
                Layout.minimumWidth: 120
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(0, value)
                }
            }

            MyButton {
                id: myButton01
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(0)
                }
            }

            MyButton {
                id: myButton02
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(0)
                }
            }

            Label {
                id: label12
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("BareModuleTool(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos3
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(3, text)
                }
            }

            TextField {
                id: pos4
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(4, text)
                }
            }

            TextField {
                id: pos5
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(5, text)
                }
            }

            SpinBox {
                id: ali1
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(1, value)
                }
            }

            MyButton {
                id: myButton03
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(1)
                }
            }

            MyButton {
                id: myButton04
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(1)
                }
            }

            Label {
                id: label13
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("FlexToolPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos6
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(6, text)
                }
            }

            TextField {
                id: pos7
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(7, text)
                }
            }

            TextField {
                id: pos8
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(8, text)
                }
            }

            SpinBox {
                id: ali2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(2, value)
                }
            }

            MyButton {
                id: myButton05
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(2)
                }
            }

            MyButton {
                id: myButton06
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(2)
                }
            }

            Label {
                id: label14
                width: 132
                height: 32
                text: qsTr("Gluing Tool G&R")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab0
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(0, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(0)
                    isComboxOk = true
                }
            }

            Label {
                id: label15
                width: 132
                height: 32
                text: qsTr("BareMod tool G&R")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            ComboBox {
                id: grab1
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(1, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(1)
                    isComboxOk = true
                }
            }
            Rectangle {
                id: rectangle11
                width: 20
                height: 200
                //color: "#ffffff"
                Layout.columnSpan: 1
                Layout.preferredHeight: 35
                //Layout.fillWidth: true
            }
            Label {
                id: label16
                width: 132
                height: 32
                text: qsTr("Flex tool G&R")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab2
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(2, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(2)
                    isComboxOk = true
                }
            }
        }
    }

    GroupBox {
        id: motionParaGroup
        x: 707
        y: 0
        width: 386
        height: 212
        title: qsTr("Motion Parameters")

        GridLayout {
            id: gridLayout3
            anchors.fill: parent
            Label {
                id: label7
                text: qsTr("AccVel-X")
            }

            TextField {
                id: accvel1
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(0, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(1, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label8
                text: qsTr("AccVel-Y")
            }

            TextField {
                id: accvel3
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(2, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel4
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(3, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label9
                text: qsTr("AccVel-Z")
            }

            TextField {
                id: accvel5
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(4, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel6
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(5, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label10
                text: qsTr("AccVel-T")
            }

            TextField {
                id: accvel7
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(6, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel8
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(7, text)
                }
                placeholderText: qsTr("VEL")
            }
            columns: 3
        }
    }

    GroupBox {
        id: moduleFlexLocGroup
        x: 13
        y: 238
        width: 675
        height: 195
        title: qsTr("Location-BareModule-Flex")
        GridLayout {
            id: gridLayout1
            anchors.fill: parent
            Label {
                id: label21
                width: 132
                height: 32
                text: qsTr("BareModulePos1(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos9
                x: 158
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(9, text)
                }
            }

            TextField {
                id: pos10
                x: 306
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(10, text)
                }
            }

            TextField {
                id: pos11
                x: 454
                y: 359
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(11, text)
                }
            }

            SpinBox {
                id: ali3
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                Layout.minimumWidth: 120
                onValueChanged: {
                    model.setCameraAli(3, value)
                }
            }

            MyButton {
                id: myButton11
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(3)
                }
            }

            MyButton {
                id: myButton12
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(3)
                }
            }

            Label {
                id: label22
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("BareModulePos2(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos12
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(12, text)
                }
            }

            TextField {
                id: pos13
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(13, text)
                }
            }

            TextField {
                id: pos14
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(14, text)
                }
            }

            SpinBox {
                id: ali4
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                onValueChanged: {
                    model.setCameraAli(4, value)
                }
            }

            MyButton {
                id: myButton13
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(4)
                }
            }

            MyButton {
                id: myButton14
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(4)
                }
            }

            Label {
                id: label23
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("FlexPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos15
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(15, text)
                }
            }

            TextField {
                id: pos16
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(16, text)
                }
            }

            TextField {
                id: pos17
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(17, text)
                }
            }

            SpinBox {
                id: ali5
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                onValueChanged: {
                    model.setCameraAli(5, value)
                }
            }

            MyButton {
                id: myButton15
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(5)
                }
            }

            MyButton {
                id: myButton16
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(5)
                }
            }

            Label {
                id: label24
                width: 132
                height: 32
                text: qsTr("Bare Module Grab")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab3
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(3, currentIndex)
                    }
                }

                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(3)
                    isComboxOk = true
                }
            }
            Label {
                id: label25
                width: 132
                height: 32
                text: qsTr("Flex Grab")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab4
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(4, currentIndex)
                    }
                }

                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(4)
                    isComboxOk = true
                }
            }
            columns: 7
        }
    }

    GroupBox {
        id: chunckLocGroup
        x: 13
        y: 439
        width: 675
        height: 185
        title: qsTr("Location-Chunck")
        GridLayout {
            id: gridLayout2
            anchors.fill: parent
            anchors.leftMargin: 0
            anchors.topMargin: -6
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            Label {
                id: label31
                width: 132
                height: 32
                text: qsTr("MarkPos1(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos18
                x: 158
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(18, text)
                }
            }

            TextField {
                id: pos19
                x: 306
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(19, text)
                }
            }

            TextField {
                id: pos20
                x: 454
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(20, text)
                }
            }

            SpinBox {
                id: ali6
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.minimumWidth: 120
                onValueChanged: {
                    model.setCameraAli(6, value)
                }
            }

            MyButton {
                id: myButton21
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(6)
                }
            }

            MyButton {
                id: myButton22
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(6)
                }
            }

            Label {
                id: label32
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("MarkPos2(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: pos21
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setLocationPos(21, text)
                }
            }

            TextField {
                id: pos22
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setLocationPos(22, text)
                }
            }

            TextField {
                id: pos23
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setLocationPos(23, text)
                }
            }

            SpinBox {
                id: ali7
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(7, value)
                }
            }

            MyButton {
                id: myButton23
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(7)
                }
            }

            MyButton {
                id: myButton24
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(7)
                }
            }

            Label {
                id: label33
                width: 132
                height: 32
                text: qsTr("Bare Module Release")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab5
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(5, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(5)
                    isComboxOk = true
                }
            }
            Label {
                id: label34
                width: 132
                height: 32
                text: qsTr("Flex Release")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: grab6
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(6, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(6)
                    isComboxOk = true
                }
            }
            Rectangle {
                id: rectangle1
                width: 30
                height: 200
                //color: "#ffffff"
                Layout.columnSpan: 1
                Layout.preferredHeight: 35
                //Layout.fillWidth: true
            }

            Label {
                id: label35
                width: 132
                height: 32
                text: qsTr("Pattern1")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: dispensepattern1
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPattern(0,currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getPatternNames()
                    currentIndex = model.getPattern(0)
                    isComboxOk = true
                }
            }
            /*Rectangle {
                id: rectangle2
                width: 200
                height: 200
                //color: "#ffffff"
                Layout.columnSpan: 4
                Layout.preferredHeight: 35
                //Layout.fillWidth: true
            }*/
            Label {
                id: label36
                width: 132
                height: 32
                text: qsTr("Pattern2")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }

            ComboBox {
                id: dispensepattern2
                width: 190
                height: 32
                Layout.columnSpan: 2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPattern(1,currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getPatternNames()
                    currentIndex = model.getPattern(1)
                    isComboxOk = true
                }
            }
            columns: 7
        }
    }
}
