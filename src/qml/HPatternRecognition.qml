import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlPatternRecognition 1.0

Item {
    width: 1200;
    height: 800;

    property var sensor1Points: [x0S1, y0S1, x1S1, y1S1, x2S1, y2S1, x3S1, y3S1];
    property var sensor1Locate: [xcS1, ycS1, rotS1];
    property var sensor1Focus;

    property var sensor2Points: [x0S2, y0S2, x1S2, y1S2, x2S2, y2S2, x3S2, y3S2];
    property var sensor2Locate: [xcS2, ycS2, rotS2];
    property var sensor2Focus;

    property var flexHVs: [x_HV1, y_HV1, x_HV2, y_HV2];
    property var flexLocate: [xcFlex, ycFlex, rotFlex];
    property var flexFocus;

    property var moduleHVs: [x_moduleHV1, y_moduleHV1, x_moduleHV2, y_moduleHV2];
    property var moduleLocate: [xcModule, ycModule, rotModule];
    property var moduleFocus;


    HQmlPatternRecognition {
        id: model
        onUpdateUI: {

            for( var i = 0; i < 8; ++i){
                if(true){
                    sensor1Points[i].text = _strSensor1[i];
                    sensor2Points[i].text = _strSensor2[i];
                }
            }

            for( var j = 0; j < 4; ++j){
                if(true){
                    flexHVs[j].text = _strFlex[j];
                    moduleHVs[j].text = _strModule[j];
                }
            }

            for( var k = 0 ; k < 3; ++k){
                if(true){
                    sensor1Locate[k].text = _strCentreSensor1[k];
                    sensor2Locate[k].text = _strCentreSensor2[k];
                    flexLocate[k].text = _strCentreFlex[k];
                    moduleLocate[k].text = _strCentreModule[k];
                }
            }

            sensor1Focus.text = _strSensor1Focus;

            flexFocus.text = _strFlexFocus;

            moduleFocus.text = _strModuleFocus;
        }

    }

    Component.onCompleted: {
        model.updateCentreInfo();
    }


    GroupBox {
        id: focus
        x: 13
        y: 0
        width: 737
        height: 80
        title: qsTr("Focus")

        GridLayout {
            id: gridfocus
            anchors.fill: parent
            columns: 8
// focus on sensor
            Label {
                id: lable_SensorFocus
                text: qsTr("FocusHeight(Sensor1)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: sensor1Focus
                Layout.preferredWidth: 92
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z (Sensor)")
            }

            MyButton {
                id: locate_SensorFocus
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setFocusHeight("Sensor1")
                }
            }

            MyButton {
                id: move_SensorFocus
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToFocus("Sensor1")
                }
            }
// Focus on flex
            Label {
                id: lable_FlexFocus
                text: qsTr("FocusHeight(Flex)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: flexFocus
                Layout.preferredWidth: 92
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z (Flex)")
            }

            MyButton {
                id: locate_FlexFocus
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setFocusHeight("Flex")
                }
            }

            MyButton {
                id: move_FlexFocus
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToFocus("Flex")
                }
            }

        }
    }

    GroupBox {
        id: moduleLocation
        x: 760
        y: 0
        width: 342
        height: 630
        title: qsTr("Module")

        GridLayout {
            id: module_gridlayout
            anchors.fill: parent
            columns: 3
            rows: 10

            MyButton {
                id: module_buttonLocate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Module", 0)
                }
            }

            MyButton {
                id: module_buttonMove
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Module", 0)
                }
            }

            MyButton {
                id: module_buttonRecognize
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.preferredWidth: 35
                Layout.column: 2
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeFlex(3, "Module");
                }
            }


            Label {
                id: label_moduleHV1
                Layout.column: 0
                Layout.row: 1
                text: qsTr("HV1 pos:")
            }

            TextField {
                id: x_moduleHV1
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setPointX("Module", 0, text)
                }
            }

            TextField {
                id: y_moduleHV1
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setPointY("Module", 0, text)
                }
            }

            Label {
                id: label_moduleHV2
                Layout.column: 0
                Layout.row: 2
                text: qsTr("HV2 pos:")
            }

            TextField {
                id: x_moduleHV2
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 2
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setPointX("Module", 1, text)
                }
            }

            TextField {
                id: y_moduleHV2
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 2
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setPointY("Module", 1, text)
                }
            }

            Label {
                id: label_module_centre
                Layout.column: 0
                Layout.row: 3
                text: qsTr("module pos:")
            }

            TextField {
                id: xcModule
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 3
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Xc")
            }

            TextField {
                id: ycModule
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 3
                selectByMouse: true
                readOnly:true
                placeholderText: qsTr("Yc")
            }

            Label {
                id: label_module_rotAngle
                Layout.column: 0
                Layout.row: 4
                text: qsTr("Rotation:")
            }

            TextField {
                id: rotModule
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 4
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("rotation")
            }

            Label{
                id: label_module_focus
                Layout.column: 0
                Layout.row: 5
                text: qsTr("Focus Module:")
            }

            TextField {
                id: moduleFocus
                Layout.column: 1
                Layout.row: 5

                Layout.preferredWidth: 92
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z (Module)")
            }

            MyButton {
                id: locate_ModuleFocus
                Layout.column: 0
                Layout.row: 6
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setFocusHeight("Module")
                }
            }

            MyButton {
                id: move_ModuleFocus
                Layout.column: 1
                Layout.row: 6
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToFocus("Module")
                }
            }

        }
    }

    GroupBox {
        id: sensor1
        x: 13
        y: 80
        width: 360
        height: 360
        title: qsTr("Sensor1")

        GridLayout {
            id: sensor1_gridlayout
            anchors.fill: parent
            columns: 8
            rows: 8

       //------------------------Top---------------------------//
            MyButton {
                id: buttonS1Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor1", 0)
                }
            }

            MyButton {
                id: buttonS1Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor1", 0)
                }
            }

            MyButton {
                id: buttonS1Recognize
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.preferredWidth: 35
                Layout.column: 2
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor1", 0)
                }
            }

            MyButton {
                id: buttonS1Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 3
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor1", 0)
                }
            }

            MyButton {
                id: buttonS2Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 4
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor1", 1)
                }
            }

            MyButton {
                id: buttonS2Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 5
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor1", 1)
                }
            }

            MyButton {
                id: buttonS2Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 6
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor1", 1)
                }
            }

            MyButton {
                id: buttonS2Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 7
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor1", 1)
                }
            }

            TextField {
                id: x0S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X1")
                onEditingFinished: {
                    model.setPointX("Sensor1", 0, text);
                }
            }

            TextField {
                id: y0S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y1")
                onEditingFinished: {
                    model.setPointY("Sensor1", 0, text);
                }
            }


            TextField {
                id: x1S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X2")
                onEditingFinished: {
                    model.setPointX("Sensor1", 1, text)
                }
            }

            TextField {
                id: y1S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 6
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y2")
                onEditingFinished: {
                    model.setPointY("Sensor1", 1, text)
                }
            }
//------------------------Bottom---------------------------//
            MyButton {
                id: buttonS3Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor1", 2)
                }
            }

            MyButton {
                id: buttonS3Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor1", 2)
                }
            }

            MyButton {
                id: buttonS3Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 2
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor1", 2)
                }
            }

            MyButton {
                id: buttonS3Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 3
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor1", 2)
                }
            }

            MyButton {
                id: buttonS4Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 4
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor1", 3)
                }
            }

            MyButton {
                id: buttonS4Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 5
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor1", 3)
                }
            }

            MyButton {
                id: buttonS4Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 6
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor1", 3)
                }
            }

            MyButton {
                id: buttonS4Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 7
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor1", 3)
                }
            }

            TextField {
                id: x2S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X3")
                onEditingFinished: {
                    model.setPointX("Sensor1", 2, text)
                }
            }

            TextField {
                id: y2S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y3")
                onEditingFinished: {
                    model.setPointY("Sensor1", 2, text)
                }
            }


            TextField {
                id: x3S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X4")
                onEditingFinished: {
                    model.setPointX("Sensor1", 3, text)
                }
            }

            TextField {
                id: y3S1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 6
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y4")
                onEditingFinished: {
                    model.setPointY("Sensor1", 3, text)
                }
            }

            Label {
                id: label_centre_sensor1
                Layout.column: 0
                Layout.row: 2
                Layout.columnSpan: 2
                text: qsTr("Centre:")
            }

            TextField {
                id: xcS1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 2
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Xc")
            }

            TextField {
                id: ycS1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 2
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Yc")
            }

            Label {
                id: label_rotAngle_sensor1
                Layout.column: 2
                Layout.row: 3
                Layout.columnSpan: 2
                text: qsTr("Rotation Angle:")
            }

            TextField {
                id: rotS1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 3
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("rotation")
            }

        }
    }

    GroupBox {
        id: sensor2
        x: 390
        y: 80
        width: 360
        height: 360
        title: qsTr("Sensor2")
        GridLayout {
            id: sensor2_gridlayout
            anchors.fill: parent
            columns: 8
            rows: 8

       //------------------------Top---------------------------//
            MyButton {
                id: sensor2_buttonS1Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor2", 0)
                }
            }

            MyButton {
                id: sensor2_buttonS1Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor2", 0)
                }
            }

            MyButton {
                id: sensor2_buttonS1Recognize
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.preferredWidth: 35
                Layout.column: 2
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor2", 0)
                }
            }

            MyButton {
                id: sensor2_buttonS1Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 3
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor2", 0)
                }
            }

            MyButton {
                id: sensor2_buttonS2Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 4
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor2", 1)
                }
            }

            MyButton {
                id: sensor2_buttonS2Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 5
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor2", 1)
                }
            }

            MyButton {
                id: sensor2_buttonS2Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 6
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor2", 1)
                }
            }

            MyButton {
                id: sensor2_buttonS2Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 7
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor2", 1)
                }
            }

            TextField {
                id: x0S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X1")
                onEditingFinished: {
                    model.setPointX("Sensor2", 0, text);
                }
            }

            TextField {
                id: y0S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y1")
                onEditingFinished: {
                    model.setPointY("Sensor2", 0, text);
                }
            }


            TextField {
                id: x1S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X2")
                onEditingFinished: {
                    model.setPointX("Sensor2", 1, text)
                }
            }

            TextField {
                id: y1S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 6
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y2")
                onEditingFinished: {
                    model.setPointY("Sensor2", 1, text)
                }
            }
//------------------------Bottom---------------------------//
            MyButton {
                id: sensor2_buttonS3Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor2", 2)
                }
            }

            MyButton {
                id: sensor2_buttonS3Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor2", 2)
                }
            }

            MyButton {
                id: sensor2_buttonS3Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 2
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor2", 2)
                }
            }

            MyButton {
                id: sensor2_buttonS3Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 3
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor2", 2)
                }
            }

            MyButton {
                id: sensor2_buttonS4Locate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 4
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Sensor2", 3)
                }
            }

            MyButton {
                id: sensor2_buttonS4Move
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 5
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Sensor2", 3)
                }
            }

            MyButton {
                id: sensor2_buttonS4Recognize
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.column: 6
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeCorner("Sensor2", 3)
                }
            }

            MyButton {
                id: sensor2_buttonS4Select
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 7
                Layout.row: 6
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.selectPointInPicture("Sensor2", 3)
                }
            }

            TextField {
                id: x2S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X3")
                onEditingFinished: {
                    model.setPointX("Sensor2", 2, text)
                }
            }

            TextField {
                id: y2S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y3")
                onEditingFinished: {
                    model.setPointY("Sensor2", 2, text)
                }
            }


            TextField {
                id: x3S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("X4")
                onEditingFinished: {
                    model.setPointX("Sensor2", 3, text)
                }
            }

            TextField {
                id: y3S2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 6
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                placeholderText: qsTr("Y4")
                onEditingFinished: {
                    model.setPointY("Sensor2", 3, text)
                }
            }

            Label {
                id: label_centre_sensor2
                Layout.column: 0
                Layout.row: 2
                Layout.columnSpan: 2
                text: qsTr("Centre:")
            }

            TextField {
                id: xcS2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 2
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Xc")
            }

            TextField {
                id: ycS2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 2
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Yc")
            }

            Label {
                id: label_rotAngle_sensor2
                Layout.column: 2
                Layout.row: 3
                Layout.columnSpan: 2
                text: qsTr("Rotation Angle:")
            }

            TextField {
                id: rotS2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 3
                Layout.columnSpan: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("rotation")
            }

        }
    }

    GroupBox {
        id: flex
        x: 13
        y: 440
        width: 737
        height: 190
        title: qsTr("Flex HV")
        GridLayout {
            id: flex_gridlayout
            anchors.fill: parent
            columns: 6
            rows: 3

            MyButton {
                id: flex_buttonLocate
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.column: 0
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setSearchPoint("Flex", 0)
                }
            }

            MyButton {
                id: flex_buttonMove
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.column: 1
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY("Flex", 0)
                }
            }

            MyButton {
                id: flex_buttonRecognize
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                Layout.preferredWidth: 35
                Layout.column: 2
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.recognizeFlex(3, "Flex");
                }
            }

            MyButton {
                id: flex_buttonSelect
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/compose.svg"
                Layout.preferredWidth: 35
                Layout.column: 3
                Layout.row: 0
                myRadius: 0
                onMyButttonLeftclicked: {

                }
            }


            Label {
                id: label_HV1
                Layout.column: 0
                Layout.row: 1
                text: qsTr("HV1 pos:")
            }

            TextField {
                id: x_HV1
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setPointX("Flex", 0, text)
                }
            }

            TextField {
                id: y_HV1
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setPointY("Flex", 0, text)
                }
            }

            Label {
                id: label_HV2
                Layout.column: 3
                Layout.row: 1
                text: qsTr("HV2 pos:")
            }

            TextField {
                id: x_HV2
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setPointX("Flex", 1, text)
                }
            }

            TextField {
                id: y_HV2
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 5
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setPointY("Flex", 1, text)
                }
            }

            Label {
                id: label_flex_centre
                Layout.column: 0
                Layout.row: 2
                text: qsTr("flex pos:")
            }

            TextField {
                id: xcFlex
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 1
                Layout.row: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Xc")
            }

            TextField {
                id: ycFlex
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 2
                selectByMouse: true
                readOnly:true
                placeholderText: qsTr("Yc")
            }

            Label {
                id: label_flex_rotAngle
                Layout.column: 3
                Layout.row: 2
                text: qsTr("Rotation:")
            }

            TextField {
                id: rotFlex
                Layout.preferredWidth: 120
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("rotation")
            }

        }
    }
}

