import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import HQmlCameraParameter 1.0

ApplicationWindow {
    id: window
    width: 1080
    height: 650
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    title: qsTr("Camera Parameter Calibration")

    property var totalPhotoNumber: tPN;
    property var focusHeight: zFH;
    property var scanRectangle: [xSR0, ySR0, xSR1, ySR1, xSR2, ySR2, xSR3, ySR3];
    property var chessBoardPattern: [cbH, cbW, xReso, yReso];
    property var cameraAlgoris: [cA0, cA1, cA2, cA3, cA4, cA5, cA6, cA7, cA8, cA9];
    property var cameraAli: cA;
    property var cameraMatrix: [cM11, cM12, cM13, cM21, cM22, cM23, cM31, cM32, cM33];
    property var distCoefficients: [k1, k2, p1, p2, k3];
    property var cameraOffset: [cPOx, cPOy];
    property var posInGantry: [pIGx, pIGy];
    property var posInPicture: [pIPx, pIPy];

    function initControls() {
        ipAddresses.text = model.getHost()
        endMarks.model = model.getEndMarks()
        endMarks.currentIndex = model.getEndMark()
        port.value = model.getPort()
        endMarkEvent.enabled = true
        portEvent.enabled = true
    }

    HQmlCameraParameter {
        id: model;
        onUpdateAllCameraData: {
            tPN.text = num;
            zFH.text = zFocus;
            cA.text = cameraAli;
            for (var i = 0; i < scanRectangle.length; i++){
                scanRectangle[i].text = scanRect[i];
            }

            for (var j = 0; j < chessBoardPattern.length; j++){
                chessBoardPattern[j].text = chessboardPattern[j];
            }

            for (var n = 0; n < cameraOffset.length; n++){
                cameraOffset[n].text = cameraPosOffset[n];
            }

            for (var q = 0; q < posInPicture.length; q++){
                posInPicture[q].text = positionInPicture[q];
            }

            for (var k = 0; k < cameraAlgoris.length; k++){
                cameraAlgoris[k].value = alis[k];
            }

            for (var l = 0; l < cameraMatrix.length; l++){
                cameraMatrix[l].text = cameraMats[l];
            }

            for (var p = 0; p < posInGantry.length; p++){
                posInGantry[p].text = positionInGantry[p];
            }
        }
        onComputeCompleted: {
            model.isEnable = true;
        }
        onUpdateAllCameraMatrixData: {
            for (var l = 0; l < cameraMatrix.length; l++){
                cameraMatrix[l].text = cameraMats[l];
            }
        }
        onUpdateAllPIGData: {
            for (var p = 0; p < posInGantry.length; p++){
                posInGantry[p].text = positionInGantry[p];
            }
        }
    }



    Component.onCompleted: {
        model.updateCameraData()
    }

    GroupBox {
        id: cameraCaliGroup
        x: 13
        y: 0
        width: 340
        height: 74
        title: qsTr("Camera Calibration")
//-----------------set focus height-------------------------//
        GridLayout {
            id: gridfocus
            anchors.fill: parent
            columns: 4

            Label {
                id: lableZFocusHeight
                width: 142
                height: 32
                text: qsTr("FocusHeight(Z)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: zFH
                x: 158
                y: 359
                width: 92
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setFocusHeight(text)
                }
            }

            MyButton {
                id: zFHLocation
                x: 306
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    var pos = model.getAxisFPOS()
                    zFH.text = pos[2]
                }
            }

            MyButton {
                id: zFHMove
                x: 344
                y: 359
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveZ(zFH.text)
                }
            }
        }
    }

    GroupBox {
        id: scanRectNum
        x: 13
        y: 78
        width: 600
        height: 322
        title: qsTr("Scan Area Setting")

        GridLayout {
            id: gridscanNum
            anchors.fill: parent
            columns: 8
            rows: 6
//----------------set scan number-----------------------//
            Label {
                id: pictureNumber
                width: 142
                height: 32
                text: qsTr("Picture Number")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: tPN
                x: 4
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("Num")
                onEditingFinished: {
                    model.setScanNum(text)
                }
            }
//---------------------Set CameraAlgoris---------------//
            Label {
                id: cameraAlgoris
                width: 382
                height: 32
                Layout.column: 4
                Layout.row: 0
                text: qsTr("Camera Algoris")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: cA
                x: 24
                y: 140
                width: 142
                height: 32
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                selectByMouse: true
                placeholderText: qsTr("Ali")
                onEditingFinished: {
                    model.setCameraAli(text);
                }
            }
//--------------------set scan rectangle--------------------//
            Label {
                id: labelScanRectangle
                x: 158
                y: 140
                width: 132
                height: 32
                text: qsTr("ScanRectangle")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                Layout.column: 0
                Layout.row: 1
            }

            TextField {
                id: xSR1
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                Layout.column: 0
                Layout.row: 2
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setScanRangeData(1, "x", text)
                }
            }

            TextField {
                id: ySR1
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setScanRangeData(1, "y", text)
                }
            }

            MyButton {
                id: buttonSR1Locate
                x: 612
                y: 140
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    var pos = model.getAxisFPOS()
                    xSR1.text = pos[0]
                    ySR1.text = pos[1]
                    model.setScanRangeData(1, "x", xSR1.text)
                    model.setScanRangeData(1, "y", ySR1.text)
                }
            }

            MyButton {
                id: buttonSR1Move
                x: 612
                y: 450
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY(xSR1.text, ySR1.text)
                }
            }

            TextField {
                id: xSR2
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setScanRangeData(2, "x", text)
                }
            }

            TextField {
                id: ySR2
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setScanRangeData(2, "y", text)
                }
            }

            MyButton {
                id: buttonSR2Locate
                x: 612
                y: 140
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    var pos = model.getAxisFPOS()
                    xSR2.text = pos[0]
                    ySR2.text = pos[1]
                    model.setScanRangeData(2, "x", xSR2.text)
                    model.setScanRangeData(2, "y", ySR2.text)
                }
            }

            MyButton {
                id: buttonSR2Move
                x: 612
                y: 450
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY(xSR2.text, ySR2.text)
                }
            }

            TextField {
                id: xSR3
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.column: 0
                Layout.row: 3
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setScanRangeData(3, "x", text)
                }
            }

            TextField {
                id: ySR3
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setScanRangeData(3, "y", text)
                }
            }

            MyButton {
                id: buttonSR3Locate
                x: 612
                y: 140
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    var pos = model.getAxisFPOS()
                    xSR3.text = pos[0]
                    ySR3.text = pos[1]
                    model.setScanRangeData(3, "x", xSR3.text)
                    model.setScanRangeData(3, "y", ySR3.text)
                }
            }

            MyButton {
                id: buttonSR3Move
                x: 612
                y: 450
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY(xSR3.text, ySR3.text)
                }
            }

            TextField {
                id: xSR0
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setScanRangeData(0, "x", text)
                }
            }

            TextField {
                id: ySR0
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setScanRangeData(0, "y", text)
                }
            }

            MyButton {
                id: buttonSR0Locate
                x: 612
                y: 140
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    var pos = model.getAxisFPOS()
                    xSR0.text = pos[0]
                    ySR0.text = pos[1]
                    model.setScanRangeData(0, "x", xSR0.text)
                    model.setScanRangeData(0, "y", ySR0.text)
                }
            }

            MyButton {
                id: buttonSR0Move
                x: 612
                y: 450
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY(xSR0.text, ySR0.text)
                }
            }
            //----------------Chessboard Setting-------------------//
            Label {
                id: labelChessboard
                x: 158
                y: 140
                width: 132
                height: 32
                text: qsTr("ChessboardPat")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                Layout.column: 0
                Layout.row: 4
            }
            TextField {
                id: cbH
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.preferredWidth: 100
                Layout.column: 0
                Layout.row: 5
                selectByMouse: true
                placeholderText: qsTr("Rows")
                onEditingFinished: {
                    model.setChessboardPattern("row", "y", text)
                }
            }

            TextField {
                id: cbW
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Cols")
                onEditingFinished: {
                    model.setChessboardPattern("col", "x", text)
                }
            }
            Label {
                id: labelChessboardPattern
                x: 158
                y: 140
                width: 132
                height: 32
                text: qsTr("Dimension: ")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                Layout.column: 4
                Layout.row: 4
            }
            TextField {
                id: xReso
                x: 158
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 4
                Layout.row: 5
                selectByMouse: true
                placeholderText: qsTr("Width/(mm/unit)")
                onEditingFinished: {
                    model.setChessboardPattern("dimension", "x", text)
                }
            }

            TextField {
                id: yReso
                x: 306
                y: 140
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 5
                Layout.row: 5
                selectByMouse: true
                placeholderText: qsTr("Height/(mm/unit)")
                onEditingFinished: {
                    model.setChessboardPattern("dimension", "y", text)
                }
            }
        }
    }

    GroupBox {
        id: cameraMatGroup
        x: 13
        y: 410
        width: 400
        height: 200
        title: qsTr("Matrix Compute")

//-----------------set focus height-------------------------//
        GridLayout {
            id: gridCameraMat
            anchors.fill: parent
            columns: 4

            Label {
                id: lableCameraMat
                width: 142
                height: 32
                text: qsTr("Camera Matrix")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            MyButton {
                id: computeCameraMat
                x: 360
                y: 0
                width: 128
                height: 32
                text: "Compute"
                Layout.preferredHeight: 35
                Layout.preferredWidth: 70
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.computeInstrisincParMatrix()
                    model.updateCameraMatrixData()
                }
            }
            TextField {
                id: cM11
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 0
                Layout.row: 1
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M11")
            }
            TextField {
                id: cM12
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 1
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M12")
            }
            TextField {
                id: cM13
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 1
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M13")
            }
            TextField {
                id: cM21
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 0
                Layout.row: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M21")
            }
            TextField {
                id: cM22
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M22")
            }
            TextField {
                id: cM23
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 2
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M23")
            }
            TextField {
                id: cM31
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 0
                Layout.row: 3
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M31")
            }
            TextField {
                id: cM32
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 3
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M32")
            }
            TextField {
                id: cM33
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 3
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("M33")
            }


        }
    }

    GroupBox {
        id: cameraOff
        x: 620
        y: 13
        width: 400
        height: 400

        GridLayout {
            id: gridCameraOffset
            anchors.fill: parent
            columns: 4

            Label {
                id: lableCameraOffset
                width: 142
                height: 32
                Layout.column: 0
                Layout.row: 0
                text: qsTr("Camera Offset")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: cPOx
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 0
                selectByMouse: true
                placeholderText: qsTr("X_Del")
                onEditingFinished: {
                    model.setCameraOffset("x", text)
                }
            }
            TextField {
                id: cPOy
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 0
                selectByMouse: true
                placeholderText: qsTr("Y_Del")
                onEditingFinished: {
                    model.setCameraOffset("y", text)
                }
            }

            Label {
                id: lablePositionInPicture
                width: 142
                height: 32
                Layout.column: 0
                Layout.row: 1
                text: qsTr("Position In Picture")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: pIPx
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("u")
                onEditingFinished: {
                    model.setPositionInPicture("x", text)
                }
            }
            TextField {
                id: pIPy
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 1
                selectByMouse: true
                placeholderText: qsTr("v")
                onEditingFinished: {
                    model.setPositionInPicture("y", text)
                }
            }

            Label {
                id: lablepositionInGantry
                width: 142
                height: 32
                Layout.column: 0
                Layout.row: 3
                text: qsTr("Position In Gantry")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }
            TextField {
                id: pIGx
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 3
                readOnly: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onTextChanged: {
                    model.setPositionInGantry("x", text)
                }
            }
            TextField {
                id: pIGy
                x: 306
                y: 140
                width: 64
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.column: 2
                Layout.row: 3
                readOnly: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onTextChanged: {
                    model.setPositionInGantry("y", text)
                }
            }
            MyButton {
                id: buttonPIGMove
                x: 612
                y: 450
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveXY(pIGx.text, pIGy.text)
                }
            }
            MyButton {
                id: transPicToGantry
                x: 360
                y: 0
                width: 128
                height: 32
                text: "TransPictoGantry"
                Layout.column: 0
                Layout.row: 2
                Layout.preferredHeight: 35
                Layout.preferredWidth: 135
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.transPictureToGantry()
                    model.updatePIGData()
                }
            }
        }
    }



}

