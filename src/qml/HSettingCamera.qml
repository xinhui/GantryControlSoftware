﻿import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import HQmlModelCamera 1.0

ApplicationWindow {
    id: window
    width: 600
    height: 720
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    title: qsTr("Camera Setting")

    function initControls() {
        ipAddresses.text = model.getHost()
        endMarks.model = model.getEndMarks()
        endMarks.currentIndex = model.getEndMark()
        port.value = model.getPort()
        endMarkEvent.enabled = true
        portEvent.enabled = true
    }

    HQmlModelCamera {
        id: model

        property int msgLines: 0

        onErrorOccurd: {
            messages.append("<font color=\"red\">" + str + "</font>")
            msgLines++
            if (msgLines > 20000) {
                messages.clear()
            }
        }

        onCmdResponsed: {
            messages.append("<font color=\"black\">" + str + "</font>")
            msgLines++
            if (msgLines > 20000) {
                messages.clear()
            }
        }
    }

    Component.onCompleted: {
        initControls()
    }

    GroupBox {
        id: groupBox
        height: 185
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.topMargin: 5
        title: qsTr("Settings")

        GridLayout {
            id: gridLayout
            anchors.fill: parent
            columns: 3

            Label {
                id: label
                text: qsTr("IP: ")
            }

            TextField {
                id: ipAddresses
                selectByMouse: true
                Layout.preferredHeight: 35
                Layout.fillWidth: true

                onTextEdited: {
                    model.setHost(text)
                }
            }

            Rectangle {
                id: rectangle
                width: 200
                height: 200
                color: "#00000000"
                Layout.maximumWidth: 120
                Layout.minimumWidth: 120
                Layout.preferredHeight: 35
            }

            Label {
                id: label1
                text: qsTr("Port: ")
            }

            SpinBox {
                id: port
                editable: true
                from: 1
                to: 65535
                Layout.fillWidth: true

                Connections {
                    id: portEvent
                    enabled: false
                    function onValueChanged() {
                        model.setPort(port.value)
                    }
                }
            }
            MyButton {
                id: myButton
                buttonText: qsTr("connect")
                width: 111
                height: 43
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: 120
                myRadius: 2
                Layout.preferredHeight: 35

                onMyButttonLeftclicked: {
                    model.createConnection()
                }
            }

            Label {
                id: label2
                text: qsTr("End Mark: ")
            }

            ComboBox {
                id: endMarks
                Layout.fillWidth: true

                Connections {
                    id: endMarkEvent
                    enabled: false
                    function onCurrentIndexChanged() {
                        model.setEndMark(endMarks.currentIndex)
                    }
                }
            }
        }
    }

    GroupBox {
        id: groupBox2
        width: 200
        height: 71
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: groupBox.bottom
        anchors.topMargin: 10
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        title: qsTr("Commond")

        TextField {
            id: commond
            height: 35
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.right: myButton1.left
            anchors.leftMargin: 5
            anchors.rightMargin: 20
            selectByMouse: true
        }

        MyButton {
            id: myButton1
            width: 160
            height: 35
            myRadius: 2
            buttonText: qsTr("send")
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenterOffset: 0

            onMyButttonLeftclicked: {
                model.sendCommond(commond.text)
            }
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: groupBox2.bottom
        anchors.bottom: parent.bottom
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 10
        border.width: 1
        border.color: "forestgreen"
        color: "oldlace"

        Flickable {
            id: flickable
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick

            TextArea.flickable: TextArea {
                id: messages
                readOnly: true
                textFormat: Qt.RichText
                wrapMode: TextArea.Wrap
                font.pointSize: 12
                focus: true
                selectByMouse: true
                MouseArea {
                    x: flickable.visibleArea.xPosition * parent.width
                    y: flickable.visibleArea.yPosition * parent.height
                    width: flickable.visibleArea.widthRatio * parent.width
                    height: flickable.visibleArea.heightRatio * parent.height

                    cursorShape: Qt.IBeamCursor
                    acceptedButtons: Qt.RightButton
                    onClicked: {
                        contextMenu.x = mouseX
                        contextMenu.y = mouseY
                        contextMenu.open()
                    }
                }

                background: Rectangle {
                    color: "transparent"
                }
            }

            Menu {
                id: contextMenu

                MenuItem {
                    text: qsTr("Copy")
                    enabled: messages.selectedText
                    onTriggered: messages.copy()
                }

                MenuSeparator {}

                MenuItem {
                    text: qsTr("Clear")
                    onTriggered: messages.clear()
                }
            }

            ScrollBar.vertical: ScrollBar {
                id: fScrollbar
                policy: ScrollBar.AsNeeded
            }
        }
    }
}
