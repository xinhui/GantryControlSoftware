import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelDispenseActionSettings 1.0

Window {

    id: window
    title: "Dispense Action Setting"
    width: 600
    height: 400
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var configControls: [lineEditwaitingz, lineEditgluingz, lineEditmovingspeed, lineEditmovingacc, lineEditgluingspeed,
                                lineEditgluingacc, lineEditgluingtime, lineEditgluingpressure, lineEditx1, lineEdity1, lineEditx2, lineEdity2, lineEditprotectionforce]

    function initUI() {
        model.updateUI()
    }

    HQmlModelDispenseActionSettings {
        id: model
        onUpdateControlData: {
            chooseEvent.enabled = false
            choose.model = cfgNames
            choose.currentIndex = currentCfg
            chooseEvent.enabled = true

            dispensemode.currentIndex = cfgDatas[0]
            for (var k = 0; k < configControls.length; ++k) {
                configControls[k].text = cfgDatas[k + 1]
            }
            lineEditTouchDown.text = touchDownZ
        }
    }

    Component.onCompleted: {
        initUI()
    }

    GridLayout {
        id: gridLayout1
        height: 755
        anchors.fill: parent
        rowSpacing: 20
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        columns: 4

        Label {
            id: label10
            width: 49
            height: 12
            text: qsTr("Action Name:")
        }

        ComboBox {
            id: choose
            width: 279
            height: 40
            Layout.fillWidth: true
            editable: true
            selectTextByMouse: true

            Connections {
                id: chooseEvent
                enabled: false
                function onCurrentIndexChanged() {
                    model.setCurrentConfig(choose.currentIndex)
                }
            }
        }

        MyButton {
            id: myButton
            Layout.preferredWidth: 120
            Layout.preferredHeight: 40
            myRadius: 0
            buttonText: "new"
            onMyButttonLeftclicked: {
                model.addConfig(choose.editText)
            }
        }

        MyButton {
            id: myButton1
            Layout.preferredHeight: 40
            Layout.preferredWidth: 120
            myRadius: 0
            isEnable: choose.model.length !== 0
            buttonText: "delete"
            onMyButttonLeftclicked: {
                model.removeConfig(choose.currentIndex)
            }
        }

        GroupBox {
            id: groupBox
            width: 700
            height: 261
            Layout.fillWidth: true
            Layout.columnSpan: 4

            GridLayout {
                id: gridLayout
                anchors.fill: parent
                rows: 8
                columns: 4

                Label {
                    id: label7
                    text: qsTr("dispense mode:")
                }

                ComboBox {
                    id: dispensemode
                    Layout.preferredWidth: 120
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    currentIndex: 0
                    model: ["Timed","Steady"]
                    onCurrentTextChanged: {
                        //printtext.text = currentIndex
                        model.setConfigData(0,currentIndex)
                    }
                }

                Label {
                    id: label0
                    width: 30
                    height: 7
                    text: qsTr("touch-down z (mm):")
                }

                TextField {
                    id: lineEditTouchDown
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")

                }

                Label {
                    id: label
                    width: 30
                    height: 7
                    text: qsTr("waiting z (mm):")
                }

                TextField {
                    id: lineEditwaitingz
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(1, text)
                    }
                }


                Label {
                    id: label1
                    text: qsTr("gluing z (mm):")
                }

                TextField {
                    id: lineEditgluingz
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(2, text)
                    }
                }

                Label {
                    id: label2
                    width: 38
                    height: 12
                    text: qsTr("moving speed (mm/s):")
                }

                TextField {
                    id: lineEditmovingspeed
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(3, text)
                    }
                }

                Label {
                    id: label3
                    width: 30
                    height: 7
                    text: qsTr("moving acc (mm/s^2):")
                }

                TextField {
                    id: lineEditmovingacc
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(4, text)
                    }
                }

                Label {
                    id: label5
                    width: 30
                    height: 7
                    text: qsTr("gluing speed (mm/s):")
                }

                TextField {
                    id: lineEditgluingspeed
                    width: 200
                    height: 40
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(5, text)
                    }
                }

                Label {
                    id: label6
                    text: qsTr("gluing acc (mm/s^2):")
                }

                TextField {
                    id: lineEditgluingacc
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(6, text)
                    }
                }

                Label {
                    id: label8
                    text: qsTr("gluing time (s):")
                }

                TextField {
                    id: lineEditgluingtime
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(7, text)
                    }
                }

                Label {
                    id: label4
                    text: "gluing pressure:"
                }

                TextField {
                    id: lineEditgluingpressure
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(8, text)
                    }
                }

                Label {
                    id: label9
                    text: "x1 (mm):"
                }

                TextField {
                    id: lineEditx1
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(9,text)
                    }
                }

                Label {
                    id: label11
                    text: "y1 (mm):"
                }

                TextField {
                    id: lineEdity1
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(10,text)

                    }
                }

                Label {
                    id: label12
                    text: "x2 (mm):"
                }

                TextField {
                    id: lineEditx2
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(11,text)

                    }
                }

                Label {
                    id: label13
                    text: "y2 (mm):"
                }

                TextField {
                    id: lineEdity2
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(12,text)

                    }
                }

                Label {
                    id: label14
                    text: "protection force (g):"
                }

                TextField {
                    id: lineEditprotectionforce
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onTextChanged: {
                        model.setConfigData(13,text)

                    }
                }

                MyButton {
                    id: buttonRun
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 120
                    myRadius: 0
                    buttonText: "Run Action"
                    onMyButttonLeftclicked: {
                        model.setCurrentDispenseSettings()
                        model.runCurrentAction()
                    }
                }
                MyButton {
                    id: buttonRunTouchDown
                    Layout.preferredHeight: 40
                    Layout.preferredWidth: 120
                    myRadius: 0
                    buttonText: "Run Touch-Down"
                    onMyButttonLeftclicked: {
                        lineEditTouchDown.text = model.getTouchDownValue()
                    }
                }
            }
        }

        Rectangle {
            id: rectangle2
            width: 200
            height: 200
            color: "#00ffffff"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 4
        }
    }
}
