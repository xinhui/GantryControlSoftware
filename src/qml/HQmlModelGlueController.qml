import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4
import HQmlModelGlue 1.0

Item {

    HQmlModelGlue {
        id: modelGlue
        onMessage: {
            edit.append(msg)
        }
        Component.onCompleted: {
            var ports = modelGlue.getPortNames()
            var port = modelGlue.getCurrentPort()
            var baudrate = modelGlue.getCurrentBaudrate()
            comboBox.model = ports
            comboBox.displayText = port
            comboBox1.model = [115200,19200,38400,9600]
            comboBox1.displayText = baudrate
        }
    }



    CheckBox {
        id: checkBox
        x: 835
        y: 38
        width: 82
        height: 40
        checked: modelGlue.getHex()
        text: qsTr("hex")
        onCheckStateChanged: {
            modelGlue.setHex(checked)
        }
    }

    TextField {
        id: textFieldCommand1
        x: 599
        y: 484
        width: 318
        height: 40
        text: qsTr("DI--")
        placeholderText: qsTr("command")
        onTextChanged: {
            modelGlue.setCommand(0, text)
        }
    }
    Button {
        id: buttonsend1
        x: 935
        y: 484
        text: qsTr("send")
        onClicked: {
            modelGlue.sendCurrentInstructCommand(textFieldCommand1.text)
        }
    }
    TextField {
        id: textFieldCommand2
        x: 599
        y: 552
        width: 318
        height: 40
        text: qsTr("06")
        placeholderText: qsTr("hex command")
        onTextChanged: {
            modelGlue.setCommand(0, text)
        }
    }
    Button {
        id: buttonsend2
        x: 935
        y: 552
        text: qsTr("send")
        onClicked: {
            modelGlue.sendCommand(textFieldCommand2.text)
        }
    }


    Rectangle {
        id: rectangle
        x: 599
        y: 78
        width: 318
        height: 400
        color: "#ffffff"
        border.width: 2

        Flickable {
            id: flick
            anchors.fill: parent
            anchors.rightMargin: 2
            anchors.leftMargin: 2
            anchors.bottomMargin: 2
            anchors.topMargin: 2

            contentWidth: edit.paintedWidth
            contentHeight: edit.paintedHeight
            clip: true

            function ensureVisible(r) {
                if (contentX >= r.x)
                    contentX = r.x
                else if (contentX + width <= r.x + r.width)
                    contentX = r.x + r.width - width
                if (contentY >= r.y)
                    contentY = r.y
                else if (contentY + height <= r.y + r.height)
                    contentY = r.y + r.height - height
            }

            TextEdit {
                id: edit
                width: flick.width
                focus: true
                wrapMode: TextEdit.Wrap
                onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
            }
        }
    }


    GridLayout {
        id:gridLayoutOptionRegion
        x: 23
        y: 8
        width: 596
        height: 210
        flow: GridLayout.LeftToRight
        layoutDirection: Qt.LeftToRight
        rows: 2
        columns: 2
        GridLayout {
            id:gridLayoutPort
            x: 30
            y: 28
            width: 208
            height: 42
            rows: 1
            columns: 2
            Label {
                id: label
                x: 21
                y: 53
                width: 55
                height: 18
                text: qsTr("COM Port:")
            }


            ComboBox {
                id: comboBox
                x: 94
                y: 37
                width: 174
                height: 40
                onCurrentTextChanged: {
                    modelGlue.setCurrentPort(currentText)
                    displayText = currentText
                }
            }
        }
        GridLayout {
            id: gridLayoutBaudRate
            x: 304
            y: 37
            rows: 1
            columns: 2
            Label {
                x: 300
                y: 54
                width: 55
                height: 18
                text: "Baud Rate:"

            }

            ComboBox {
                id: comboBox1
                x: 382
                y: 37
                width: 151
                height: 40
                //displayText: "115200"
                //currentIndex: 0
                //model:[115200,19200,38400,9600]
                onCurrentTextChanged: {
                    comboBox1.displayText = currentText
                    modelGlue.setCurrentBaudRate(currentText)
                    //printLabel.text = currentText
                }
            }
        }

        ButtonGroup {
            id: buttonGroupDispenseMode
        }

        GridLayout{
            id:gridLayoutDispenseMode
            x: 41
            y: 118
            layoutDirection: Qt.RightToLeft
            rows: 1
            columns: 2
            GridLayout {
                id: gridLayout1
                x: 141
                y: 118
                width: 127
                height: 88
                columnSpacing: 1
                rows: 2
                columns: 1

                RadioButton {
                    id: radioButtonTimed
                    x: 150
                    y: 138
                    checked: modelGlue.getCurrentDispenseMode(1)
                    text: qsTr("Timed")
                    ButtonGroup.group: buttonGroupDispenseMode
                    onClicked: {
                        modelGlue.setCurrentDispenseMode(1)
                        modelGlue.sendDispenseModeCommand()
                    }
                }

                RadioButton {
                    id: radioButtonSteady
                    x: 150
                    y: 178
                    checked: modelGlue.getCurrentDispenseMode(2)
                    text: qsTr("Steady")
                    ButtonGroup.group: buttonGroupDispenseMode
                    onClicked: {
                        modelGlue.setCurrentDispenseMode(2)
                        modelGlue.sendDispenseModeCommand()
                    }
                }
            }

            Label {
                id: label1
                x: 39
                y: 159
                width: 86
                height: 24
                text: qsTr("Despense Mode")
            }
        }
        ButtonGroup {
            id: buttonGroupPreUnits

        }

        GridLayout {
            id:gridLayoutUnit
            x: 373
            y: 103
            rows: 1
            columns: 2
            Label {
                id: label2
                x: 39
                y: 159
                width: 86
                height: 24
                text: qsTr("Pressure Units")
            }
            GridLayout {
                id:gridLayout3
                x: 399
                y: 90
                width: 118
                height: 142
                rows: 3
                columns: 1
                RadioButton {
                    id: radioButtonPsi
                    x: 423
                    y: 134
                    checked: modelGlue.getCurrentPressUnits(1)
                    text: qsTr("PSI")
                    ButtonGroup.group: buttonGroupPreUnits
                    onClicked: {
                        textFieldPress.placeholderText = qsTr("0.0")
                        textFieldPress.text = qsTr("1.0")
                        textpress.text = qsTr("Press (psi):")
                        modelGlue.setCurrentPressUnits(1)
                        modelGlue.sendPressureUnitsCommand()
                    }
                }

                RadioButton {
                    id: radioButtonBar
                    x: 423
                    y: 181
                    checked: modelGlue.getCurrentPressUnits(2)
                    text: qsTr("BAR")
                    ButtonGroup.group: buttonGroupPreUnits
                    onClicked: {
                        textFieldPress.placeholderText = qsTr("0.000")
                        textFieldPress.text = qsTr("1.0000")
                        textpress.text = qsTr("Press (bar):")
                        modelGlue.setCurrentPressUnits(2)
                        modelGlue.sendPressureUnitsCommand()
                    }
                }

                RadioButton {
                    id: radioButtonkPa
                    x: 423
                    y: 227
                    checked: modelGlue.getCurrentPressUnits(3)
                    text: qsTr("kPa")
                    ButtonGroup.group: buttonGroupPreUnits
                    onClicked: {
                        textFieldPress.placeholderText = qsTr("0.0")
                        textFieldPress.text = qsTr("1.0")
                        textpress.text = qsTr("Press (kPa):")
                        modelGlue.setCurrentPressUnits(3)
                        modelGlue.sendPressureUnitsCommand()
                    }
                }

                RowLayout {
                    id: rowLayout
                    x: 479
                    y: 316
                    width: 118
                    height: 133
                }
            }
        }
    }

    GridLayout {
        id: gridLayout4
        x: 8
        y: 237
        width: 298
        height: 193
        rows: 4
        columns: 2

        Text {
            id: texttime
            x: 26
            y: 251
            width: 86
            height: 20
            text: qsTr("Time (s):")
            horizontalAlignment: Text.AlignRight
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

        }

        TextField {
            id: textFieldTime
            x: 308
            y: 246
            width: 134
            height: 40
            text: modelGlue.getCurrentDispenseTime()
            placeholderText: qsTr("0.0000")
            onTextChanged: {
                if (text < 10. && text >= 0.0001)
                {
                    modelGlue.setCurrentDispenseTime(text)
                }
            }
        }


        Text {
            id: textpress
            x: 8
            y: 326
            width: 86
            height: 20
            text: qsTr("Press (kPa):")
            horizontalAlignment: Text.AlignRight
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }

        TextField {
            id: textFieldPress
            x: 68
            y: 237
            width: 134
            height: 40
            text: modelGlue.getCurrentPress()
            placeholderText: qsTr("100.0")
            onTextChanged: {
                if (radioButtonPsi.checked && text < 101.5 && text >= 0.1)
                {
                    modelGlue.setCurrentPress(text)
                }
                if (radioButtonBar.checked && text < 7.0 && text >= 0.001)
                {
                    modelGlue.setCurrentPress(text)
                }
                if (radioButtonkPa.checked && text < 101.5 && text >= 0.1)
                {
                    modelGlue.setCurrentPress(text)
                }
            }
        }


        Text {
            id: textvac
            x: 8
            y: 326
            width: 86
            height: 20
            text: qsTr("Vac (H2O):")
            horizontalAlignment: Text.AlignRight
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }

        TextField {
            id: textFieldVac
            x: 68
            y: 237
            width: 134
            height: 40
            text: modelGlue.getCurrentVacuum()
            placeholderText: qsTr("0.0")
            onTextChanged: {
                modelGlue.setCurrentVaccum(text)
            }
        }

        Text {
            id:texttrigger
            width: 86
            height: 20
            text: qsTr("Trigger:")
            horizontalAlignment: Text.AlignRight
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

        }

        TextField {
            id: textFieldTrigger
            x: 68
            y: 237
            width: 134
            height: 40
            text: modelGlue.getCurrentTrigger()
            placeholderText: qsTr("0")
            onTextChanged: {
                if (text >=0 && text <= 99999)
                {
                    modelGlue.setCurrentTrigger(text)
                }
            }

        }


    }

    Rectangle {
        id: rectangle1
        x: 343
        y: 234
        width: 200
        height: 200
        color: "#152be4"
        Text {
            id: textCurrentSetting
            x: 578
            y: 316
            width: 158
            height: 191
            color: "#fefbfb"
            anchors.fill: parent
            font.letterSpacing: 0
            lineHeight: 1.4
            wrapMode: Text.Wrap
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            textFormat: Text.AutoText
            /*text: "<div><table border='1'><caption><h4>Current stats</h4>"+
                 "</caption><tr bgcolor='#9acd32'><th/><th>Number1</th><th>Number2</th></tr> <tr><th>Line1</th>"+
                    "<td> 0 </td> <td> 1 </td> </tr> <tr><th>Line2</th> <td> 0 </td> <td> 1 </td> </tr>"+
                    "<tr><th>Line3</th> <td> 0 </td> <td> 0 </td> </tr> <tr><th>Line4</th> <td> 1 </td> <td> 0 </td> </tr>"+
                    "<tr><th>Line5</th> <td> 1 </td> <td> 1 </td> </tr> <tr><th>Line6</th> <td> 1 </td> <td> 1 </td> </tr> </div>"
            */
            /*text: "Current Stats\n" + "\n" + "MODE:\t" + "TIMED" + "\n" + "TIME:\t" + "000000" + "\n" + "PRESS:\t" + "000000" + "\n" + "VAC:\t" + "000000"+
                  "\n" + "TRIG:\t" + "000000"
             */
            text: modelGlue.getCurrentState()
            //text: qsTr("None")
        }
    }

    Button {
        id: buttonSendSetting
        x: 122
        y: 473
        text: qsTr("Send Settings")
        onClicked: {
            modelGlue.sendSettingsCommand()
        }
    }

    Button {
        id: buttonGetSetting
        x: 393
        y: 473
        text: qsTr("Get Settings")
        onClicked: {
            textCurrentSetting.text = modelGlue.getCurrentState()
        }
    }

    Button {
        id: myButtonDispense
        x: 259
        y: 527
        text: qsTr("Dispense")
        onClicked: {
            modelGlue.sendDispenseCommand()
        }
    }

    Text {
        id: text1
        x: 599
        y: 52
        width: 134
        height: 26
        text: qsTr("Output")
        font.pixelSize: 17
    }

    Text {
        id: textprint
        x: 570
        y: 19
        width: 148
        height: 36
        //text: qsTr("model.getCurrentPressUnits().toString()")
        //text: modelGlue.getCurrentPressUnits().toString()
        font.pixelSize: 12
    }


}

/*##^##
Designer {
    D{i:0;autoSize:true;height:600;width:1200}D{i:1}D{i:2}D{i:3}D{i:4}D{i:5}D{i:6}D{i:9}
D{i:8}D{i:7}D{i:12}D{i:13}D{i:11}D{i:15}D{i:16}D{i:14}D{i:17}D{i:20}D{i:21}D{i:19}
D{i:22}D{i:18}D{i:23}D{i:25}D{i:27}D{i:28}D{i:29}D{i:30}D{i:26}D{i:24}D{i:10}D{i:32}
D{i:33}D{i:34}D{i:35}D{i:36}D{i:37}D{i:38}D{i:39}D{i:31}D{i:41}D{i:40}D{i:42}D{i:43}
D{i:44}D{i:45}
}
##^##*/

