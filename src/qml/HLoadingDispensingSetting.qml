import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlLoadingDispensingSetting 1.0

Window {

    id: window
    title: "Loading Dispensing Setting"
    width: 600
    height: 400
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var configLoadingDispense: [waitingZ, gluingZ, movingSpeed, movingAcc, gluingTime, gluingPressure, protectionForce];
    property var configDispensePosition: [x1, y1, x2, y2, x3, y3, x4, y4]

    function initUI() {
        model.updateLoadingDispenseInfo();
    }

    HQmlLoadingDispensingSetting {
        id: model
        onUpdateLoadingDispenseUI: {
            dispenseMode.currentIndex = cfgDatas[0];
            for (var i = 0; i < configLoadingDispense.length; ++i) {
                configLoadingDispense[i].text = cfgDatas[i + 1];
            }
            for (var j = 0; j < configDispensePosition.length; ++j) {
                configDispensePosition[j].text = gluingPos[j];
            }
            touchDown.text = touchDownZ;
        }
    }

    Component.onCompleted: {
        initUI()
    }


    GroupBox {
        id: groupBox

        height: 380
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.topMargin: 5
        title: qsTr("Settings")

        GridLayout {
            id: gridLayout
            anchors.fill: parent
            rows: 9
            columns: 4

            Label {
                id: labelDispenseMode
                text: qsTr("dispense mode:")
            }

            ComboBox {
                id: dispenseMode
                Layout.minimumWidth: 120
                Layout.preferredHeight: -1
                Layout.fillWidth: true
                currentIndex: 0
                model: ["Timed"]
                onCurrentTextChanged: {
                    model.setGluingParameter("DispenseMode",currentIndex)
                }
            }

            Label {
                id: labelTouchDown
                text: qsTr("touch down z(mm):")
            }

            TextField {
                id: touchDown
                selectByMouse: true
                Layout.preferredHeight: 30
                Layout.fillWidth: true
                placeholderText: qsTr("")
                readOnly: true
            }

            Label {
                id: labelWaitingZ
                width: 30
                height: 7
                text: qsTr("waiting z(mm):")
            }

            TextField {
                id: waitingZ
                selectByMouse: true
                Layout.preferredHeight: 30
                Layout.fillWidth: true
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("WaitingZ", text)
                }
            }

            Label {
                id: labelGluingZ
                text: qsTr("gluing z (mm):")
            }

            TextField {
                id: gluingZ
                selectByMouse: true
                Layout.preferredHeight: 30
                Layout.fillWidth: true
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("GluingZ", text)
                }
            }

            Label {
                id: labelMovingSpeed
                width: 38
                height: 12
                text: qsTr("moving speed (mm/s):")
            }

            TextField {
                id: movingSpeed
                selectByMouse: true
                Layout.preferredHeight: 30
                Layout.fillWidth: true
                placeholderText: qsTr("")
                onEditingFinished:  {
                    model.setGluingParameter("MovingSpeed", text)
                }
            }

            Label {
                id: labelMovingAcc
                width: 30
                height: 7
                text: qsTr("moving acc:")
            }

            TextField {
                id: movingAcc
                selectByMouse: true
                Layout.preferredHeight: 30
                Layout.fillWidth: true
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("MovingAcc", text)
                }
            }

            Label {
                id: labelGluingTime
                text: qsTr("gluing time (s):")
            }

            TextField {
                id: gluingTime
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("GluingTime", text)
                }
            }

            Label {
                id: labelGluingPressure
                text: "gluing pressure:"
            }

            TextField {
                id: gluingPressure
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("GluingPressure", text)
                }
            }

            Label {
                id: labelX1
                text: "x1 (mm):"
            }

            TextField {
                id: x1
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(1, "x", text)
                }
            }

            Label {
                id: labely1
                text: "y1 (mm):"
            }

            TextField {
                id: y1
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(1, "y", text)
                }
            }

            Label {
                id: labelX2
                text: "x2 (mm):"
            }

            TextField {
                id: x2
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(2, "x", text)
                }
            }

            Label {
                id: labelY2
                text: "y2 (mm):"
            }

            TextField {
                id: y2
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(2, "y", text)
                }
            }

            Label {
                id: labelX3
                text: "x3 (mm):"
            }

            TextField {
                id: x3
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(3, "x", text)
                }
            }

            Label {
                id: labelY3
                text: "y3 (mm):"
            }

            TextField {
                id: y3
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(3, "y", text)
                }
            }

            Label {
                id: labelX4
                text: "x4 (mm):"
            }

            TextField {
                id: x4
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(4, "x", text)
                }
            }

            Label {
                id: labelY4
                text: "y4 (mm):"
            }

            TextField {
                id: y4
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingPosition(4, "y", text)
                }
            }

            Label {
                id: labelProtectionForce
                text: "protection force (g):"
            }

            TextField {
                id: protectionForce
                Layout.fillWidth: true
                selectByMouse: true
                Layout.preferredHeight: 30
                placeholderText: qsTr("")
                onEditingFinished: {
                    model.setGluingParameter("ProtectionForce", text)
                }
            }

            MyButton {
                id: buttonRun
                Layout.preferredHeight: 40
                Layout.preferredWidth: 120
                myRadius: 0
                buttonText: "Run Action"
                onMyButttonLeftclicked: {

                }
            }
            MyButton {
                id: buttonRunTouchDown
                Layout.preferredHeight: 40
                Layout.preferredWidth: 120
                myRadius: 0
                buttonText: "Touch-Down"
                onMyButttonLeftclicked: {
                    touchDown.text = model.getTouchDownValue()
                }
            }
        }
    }
}
