﻿import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelSettingCoord 1.0
import HQmlModelCamera 1.0

Window {

    id: window

    width: 700
    height: 795
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var controls: [lineEdit1, lineEdit2, lineEdit3, lineEdit4, lineEdit5, lineEdit6, lineEdit7, lineEdit8, lineEdit9, lineEdit10, lineEdit11, lineEdit12, lineEdit13, lineEdit14, lineEdit15, lineEdit16, lineEdit17, lineEdit39, lineEdit40, lineEdit41, lineEdit18, lineEdit19, lineEdit20, lineEdit21, lineEdit22, lineEdit23, lineEdit24, lineEdit25, lineEdit26, lineEdit27, lineEdit28, lineEdit29, lineEdit30, lineEdit31, lineEdit32, lineEdit33, lineEdit34, lineEdit35, lineEdit36, lineEdit37, lineEdit38]
    property var cameraAlgoris: [spinBox, spinBox1, spinBox2, spinBox3, spinBox4, spinBox5, spinBox6, spinBox7, spinBox8]
    property bool isComboxOK: false
    HQmlModelSettingCoord {
        id: model
        onUpdateAllPanelData: {
            for (var k = 0; k < controls.length; ++k) {
                controls[k].text = datas[k]
            }
            for (var m = 0; m < cameraAlgoris.length; ++m) {
                cameraAlgoris[m].value = alis[m]
            }
        }

        onComputeCompleted: {
            if(isOk)
            {
                cameraCompute.isEnable = true
                clampingCompute.isEnable = true
                lineEdit16.text = model.getCameraErrorX()
                lineEdit17.text = model.getCameraErrorY()
            }
        }
    }

    Component.onCompleted: {
        model.initPanelData()
    }

    onVisibleChanged: {
        isComboxOK = false
        clampingGet.model = model.getGrabNames()
        clampingLet.model = model.getGrabNames()
        clampingGet.currentIndex = model.getGetIndex()
        clampingLet.currentIndex = model.getLetIndex()
        isComboxOK = true
    }

    Flickable {
        anchors.fill: parent
        anchors.margins: 5
        contentWidth: 590
        contentHeight: 795
        clip: true

        ColumnLayout {
            width: window.width - 10
            height: 755
            spacing: 0
            Label {
                text: qsTr(" stage-camera ")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.bottomMargin: -5
                Layout.topMargin: 0
                Layout.preferredHeight: 30
                background: Rectangle {
                    color: "tan"
                }
            }
            Rectangle {
                border.width: 1
                border.color: "tan"
                color: "whitesmoke"
                Layout.fillWidth: true
                Layout.preferredHeight: 300

                ColumnLayout {
                    anchors.fill: parent
                    anchors.topMargin: 5
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos1(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }

                        TextField {
                            id: lineEdit1
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(0, text)
                            }
                        }
                        TextField {
                            id: lineEdit2
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(1, text)
                            }
                        }

                        TextField {
                            id: lineEdit3
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(2, text)
                            }
                        }

                        SpinBox {
                            id: spinBox
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setCameraAli(0, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit1.text = pos[0]
                                lineEdit2.text = pos[1]
                                lineEdit3.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(0, 0)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos2(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit4
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(3, text)
                            }
                        }
                        TextField {
                            id: lineEdit5
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(4, text)
                            }
                        }

                        TextField {
                            id: lineEdit6
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(5, text)
                            }
                        }

                        SpinBox {
                            id: spinBox1
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setCameraAli(1, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit4.text = pos[0]
                                lineEdit5.text = pos[1]
                                lineEdit6.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(0, 1)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos3(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit7
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(6, text)
                            }
                        }
                        TextField {
                            id: lineEdit8
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(7, text)
                            }
                        }
                        TextField {
                            id: lineEdit9
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(8, text)
                            }
                        }

                        SpinBox {
                            id: spinBox2
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setCameraAli(2, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit7.text = pos[0]
                                lineEdit8.text = pos[1]
                                lineEdit9.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(0, 2)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos4(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit10
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setControlData(9, text)
                            }
                        }
                        TextField {
                            id: lineEdit11
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setControlData(10, text)
                            }
                        }
                        TextField {
                            id: lineEdit12
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(11, text)
                            }
                        }

                        SpinBox {
                            id: spinBox3
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setCameraAli(3, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit10.text = pos[0]
                                lineEdit11.text = pos[1]
                                lineEdit12.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(0, 3)
                            }
                        }
                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("verify pos(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit13
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setControlData(12, text)
                            }
                        }
                        TextField {
                            id: lineEdit14
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setControlData(13, text)
                            }
                        }
                        TextField {
                            id: lineEdit15
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(14, text)
                            }
                        }

                        SpinBox {
                            id: spinBox4
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setCameraAli(4, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit13.text = pos[0]
                                lineEdit14.text = pos[1]
                                lineEdit15.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(0, 4)
                            }
                        }
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        MyButton {
                            id: cameraCompute
                            buttonText: qsTr("run")
                            Layout.preferredHeight: 40
                            Layout.preferredWidth: 140
                            myRadius: 5

                            onMyButttonLeftclicked: {
                                isEnable = false
                                clampingCompute.isEnable = false
                                model.computeCameraTrans()
                            }
                        }
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        Label {
                            text: qsTr("trans error(XY):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit16
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                        TextField {
                            id: lineEdit17
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
            Rectangle {
                Layout.preferredHeight: 10
                Layout.fillWidth: true
            }

            Label {
                text: qsTr(" camera-clamping ")
                verticalAlignment: Qt.AlignVCenter
                Layout.bottomMargin: -3
                Layout.preferredHeight: 30
                background: Rectangle {
                    color: "tan"
                }
            }
            Rectangle {
                border.width: 1
                border.color: "tan"
                color: "whitesmoke"
                Layout.fillWidth: true
                Layout.preferredHeight: 415

                ColumnLayout {
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5
                    anchors.topMargin: 5
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("clamping pos(XYT):")
                            horizontalAlignment: Qt.AlignLeft
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                        }

                        TextField {
                            id: lineEdit39
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(15, text)
                            }
                            selectByMouse: true
                        }

                        TextField {
                            id: lineEdit40
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(16, text)
                            }
                            selectByMouse: true
                        }

                        TextField {
                            id: lineEdit41
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(17, text)
                            }
                            selectByMouse: true
                        }

                        MyButton {
                            Layout.preferredHeight: 35
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit39.text = pos[0]
                                lineEdit40.text = pos[1]
                                lineEdit41.text = pos[3]
                            }
                        }

                        MyButton {
                            Layout.preferredHeight: 35
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(1, 0)
                            }
                        }
                    }

                    RowLayout {

                        Label {
                            id: label
                            text: qsTr("clamping get:")
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter
                            Layout.preferredWidth: 120
                        }

                        ComboBox {
                            id: clampingGet
                            Layout.preferredHeight: 35
                            Layout.fillWidth: true
                            onCurrentIndexChanged: {
                                if (isComboxOK) {
                                    model.setGetIndex(currentIndex)
                                }
                            }
                        }

                        Label {
                            id: label1
                            text: qsTr("clamping let")
                        }

                        ComboBox {
                            id: clampingLet
                            Layout.preferredHeight: 35
                            Layout.fillWidth: true
                            onCurrentIndexChanged: {
                                if (isComboxOK) {
                                    model.setLetIndex(currentIndex)
                                }
                            }
                        }

                        Item {
                            id: item1
                            width: 200
                            height: 200
                            Layout.preferredWidth: 75
                            Layout.preferredHeight: 35
                        }
                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("rotate pos1(XYT):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }

                        TextField {
                            id: lineEdit18
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(18, text)
                            }
                        }
                        TextField {
                            id: lineEdit19
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(19, text)
                            }
                        }
                        TextField {
                            id: lineEdit20
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(20, text)
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit18.text = pos[0]
                                lineEdit19.text = pos[1]
                                lineEdit20.text = pos[3]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(1, 1)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("rotate pos2(XYT):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }

                        TextField {
                            id: lineEdit21
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(21, text)
                            }
                        }
                        TextField {
                            id: lineEdit22
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(22, text)
                            }
                        }
                        TextField {
                            id: lineEdit23
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(23, text)
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit21.text = pos[0]
                                lineEdit22.text = pos[1]
                                lineEdit23.text = pos[3]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(1, 2)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("camera pos1_1(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit24
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(24, text)
                            }
                        }
                        TextField {
                            id: lineEdit25
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(25, text)
                            }
                        }
                        TextField {
                            id: lineEdit26
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(26, text)
                            }
                        }

                        SpinBox {
                            id: spinBox5
                            Layout.preferredHeight: 35
                            to: 999
                            onValueChanged: {
                                model.setCameraAli(5, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit24.text = pos[0]
                                lineEdit25.text = pos[1]
                                lineEdit26.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(2, 0)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("camera pos1_2(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit27
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(27, text)
                            }
                        }
                        TextField {
                            id: lineEdit28
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(28, text)
                            }
                        }
                        TextField {
                            id: lineEdit29
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(29, text)
                            }
                        }

                        SpinBox {
                            id: spinBox6
                            Layout.preferredHeight: 35
                            to: 999
                            onValueChanged: {
                                model.setCameraAli(6, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit27.text = pos[0]
                                lineEdit28.text = pos[1]
                                lineEdit29.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(2, 1)
                            }
                        }
                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("camera pos2_1(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit30
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(30, text)
                            }
                        }
                        TextField {
                            id: lineEdit31
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(31, text)
                            }
                        }
                        TextField {
                            id: lineEdit32
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(32, text)
                            }
                        }

                        SpinBox {
                            id: spinBox7
                            Layout.preferredHeight: 35
                            to: 999
                            onValueChanged: {
                                model.setCameraAli(7, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit30.text = pos[0]
                                lineEdit31.text = pos[1]
                                lineEdit32.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(2, 2)
                            }
                        }
                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("camera pos2_2(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            Layout.preferredWidth: 120
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit33
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(33, text)
                            }
                        }
                        TextField {
                            id: lineEdit34
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(34, text)
                            }
                        }
                        TextField {
                            id: lineEdit35
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(35, text)
                            }
                        }

                        SpinBox {
                            id: spinBox8
                            Layout.preferredHeight: 35
                            to: 999
                            onValueChanged: {
                                model.setCameraAli(8, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit33.text = pos[0]
                                lineEdit34.text = pos[1]
                                lineEdit35.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(2, 3)
                            }
                        }
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        MyButton {
                            id: clampingCompute
                            buttonText: qsTr("run")
                            Layout.preferredHeight: 40
                            Layout.preferredWidth: 140
                            myRadius: 5
                            onMyButttonLeftclicked: {
                                isEnable = false
                                cameraCompute.isEnable = false
                                model.computeClampingTrans()
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("offset(XYT):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit36
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(36, text)
                            }
                        }
                        TextField {
                            id: lineEdit37
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(37, text)
                            }
                        }
                        TextField {
                            id: lineEdit38
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setControlData(38, text)
                            }
                        }
                    }
                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
        }
    }
}
