﻿import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelDispensePatternSettings 1.0

MenuBar {

    function closeWindows() {
        captureSetting.close()

        cameraSetting.close()
        flexSetting.close()
        coordSetting.close()
        cameraCalibration.close()
        dispenseSetting.close()
        dispensePatternSetting.close()
        loadingSetting.close()
        loadingDispensingSetting.close()
    }

    background: Rectangle {
        color: "LightSkyBlue"
    }
    HSettingCapture {
        id: captureSetting
        visible: false
    }

    HSettingCoord {
        id: coordSetting
        visible: false
    }
    HSettingFlex{
        id: flexSetting
        visible: false
    }
    HCameraCalibration{
        id: cameraCalibration
        visible: false
    }

    HDispenseActionSetting {
        id:dispenseSetting
        visible: false
    }
    HDispensePatternSetting {
        id: dispensePatternSetting
        visible: false
    }

    HSettingCamera {
        id: cameraSetting
        visible: false
    }

    HLoadingCoordSetting {
        id: loadingCoordSetting
        visible: false
    }

    HLoadingDispensingSetting {
        id: loadingDispensingSetting
        visible: false
    }

    Menu {

        title: qsTr("File")

        MenuSeparator {}
        MenuItem {
            text: qsTr("Quit")
            onTriggered: close()
        }
    }

    Menu {

        title: qsTr("Settings")

        MenuItem {
            id: avcamera
            text: qsTr("Camera Communication")
            onTriggered: cameraSetting.show()
        }

        MenuItem {
            text: qsTr("Camera Parameters")
            onTriggered: cameraCalibration.show()
        }

        //MenuSeparator {}

        MenuItem {
            text: qsTr("Grab And Release Parameters")
            onTriggered: captureSetting.show()
        }

        Menu {
            title: qsTr("Coordinate Associations")

            MenuItem{
                text: qsTr("Camera Coordinate Calibration")
                onTriggered: coordSetting.show()
            }
            MenuItem {
                text: qsTr("Flex Tool Calibration")
                onTriggered: flexSetting.show()
            }
        }

        Menu {
            title: qsTr("Gluing Settings")
            MenuItem {
                text: qsTr("Dispense Action Setting")
                onTriggered: dispenseSetting.show()
            }
            MenuItem {
                text: qsTr("Dispense Pattern Setting")
                onTriggered: {
                    modelDispensePattern.updateGluingOffsetUI()
                    dispensePatternSetting.show()
                }

            }

            HQmlModelDispensePatternSettings{
                id:modelDispensePattern
            }

        }

        Menu {
            title: qsTr("Loading Setting")

            MenuItem {
                text: qsTr("Module Coordinates Setting")
                onTriggered: loadingCoordSetting.show()
            }
            MenuItem{
                text: qsTr("Loading Dispensing Setting")
                onTriggered: {
                    loadingDispensingSetting.show()
                }
            }

        }
    }

    Menu {
        id: men
        title: qsTr("View")

        Menu {

            title: qsTr("Theme")
            MenuItem {
                id: style_Default
                text: qsTr("Default")
                onTriggered: {
                    myMainWindow.switchStyle("Default")
                    style_Default.checked = true
                    style_Material.checked = false
                    style_Fusion.checked = false
                    style_Imagine.checked = false
                    style_Universal.checked = false
                }
                //checked: myMainWindow.getStyle() === text
            }

            MenuItem {
                id: style_Material
                text: qsTr("Material")
                onTriggered: {
                    myMainWindow.switchStyle("Material")
                    style_Default.checked = false
                    style_Material.checked = true
                    style_Fusion.checked = false
                    style_Imagine.checked = false
                    style_Universal.checked = false
                }
                //checked: myMainWindow.getStyle() === text
            }

            MenuItem {
                id: style_Fusion
                text: qsTr("Fusion")
                onTriggered: {
                    myMainWindow.switchStyle("Fusion")
                    style_Default.checked = false
                    style_Material.checked = false
                    style_Fusion.checked = true
                    style_Imagine.checked = false
                    style_Universal.checked = false
                }
                //checked: myMainWindow.getStyle() === text
            }
            MenuItem {
                id: style_Imagine
                text: qsTr("Imagine")
                onTriggered: {
                    myMainWindow.switchStyle("Imagine")
                    style_Default.checked = false
                    style_Material.checked = false
                    style_Fusion.checked = false
                    style_Imagine.checked = true
                    style_Universal.checked = false
                }
                //checked: myMainWindow.getStyle() === text
            }

            MenuItem {
                id: style_Universal
                text: qsTr("Universal")
                onTriggered: {
                    myMainWindow.switchStyle("Universal")
                    style_Default.checked = false
                    style_Material.checked = false
                    style_Fusion.checked = false
                    style_Imagine.checked = false
                    style_Universal.checked = true
                }
                //checked: myMainWindow.getStyle() === text
            }
        }

        Menu {

            title: qsTr("Language")

            MenuItem {
                id: en
                text: qsTr("English")
                onTriggered: {
                    myMainWindow.setLanguage("en")
                    checked = true
                    zh.checked = false
                }
                //checked: myMainWindow.getLanguage() === "en" ? true : false
            }

            MenuItem {
                id: zh
                text: qsTr("简体中文")
                onTriggered: {
                    myMainWindow.setLanguage("zh")
                    checked = true
                    en.checked = false
                }
                //checked: myMainWindow.getLanguage() === "zh" ? true : false
            }

            MenuItem {
                text: qsTr("日本語")
                checked: false
                enabled: false
            }
        }
    }

    Menu {
        title: qsTr("Help")

        MenuItem {
            text: qsTr("About")
        }
    }
}

