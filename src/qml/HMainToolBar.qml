﻿import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelToolbar 1.0

Rectangle {

    HQmlModelToolbar {
        id: model
    }

    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#eee"
        }
        GradientStop {
            position: 0.5
            color: "#ccc"
        }
    }

    Flow {
        anchors.fill: parent
        Row {
            id: row
            height: parent.height
            spacing: 0

            MyButton {
                height: parent.height
                width: parent.height
                myRadius: 0
                buttonText: qsTr("Suck")
                buttonBorderWidth: 0
                imgRes: "qrc:/resource/icons/new.svg"

                onMyButttonLeftclicked: {

                }
            }
            MyButton {
                myRadius: 0
                height: parent.height
                width: parent.height
                buttonBorderWidth: 0
                buttonText: qsTr("Release")
                imgRes: "qrc:/resource/icons/compass.svg"
                onMyButttonLeftclicked: {

                }
            }

            ToolSeparator {
                height: parent.height
            }

            ToolSeparator {
                height: parent.height
            }
            MyButton {
                id: start
                height: parent.height
                width: parent.height
                myRadius: 0
                buttonText: qsTr("Start")
                buttonBorderWidth: 0
                imgRes: "qrc:/resource/icons/start.svg"
                onMyButttonLeftclicked: {
                    model.runFlow()
                }
            }
            MyButton {
                id: stop
                height: parent.height
                width: parent.height
                myRadius: 0
                buttonText: qsTr("Stop")
                buttonBorderWidth: 0
                imgRes: "qrc:/resource/icons/stop.svg"
                onMyButttonLeftclicked: {
                    model.stopFlow()
                }
            }
        }
    }
}
