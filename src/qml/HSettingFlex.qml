import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelSettingFlex 1.0

Window {

    id: window

    width: 700
    height: 400
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var controls: [lineEdit1, lineEdit2, lineEdit3, lineEdit4, lineEdit5, lineEdit6, lineEdit7, lineEdit8, lineEdit9, lineEdit10, lineEdit11, lineEdit12, lineEdit13, lineEdit14, lineEdit15, lineEdit16, lineEdit17, lineEdit18, lineEdit19]
    property var cameraAlgoris: [spinBox, spinBox1, spinBox2, spinBox3, spinBox4]
    property bool isComboxOK: false
    HQmlModelSettingFlex {
        id: model
        onUpdateFlexPanelData: {
            for (var k = 0; k < controls.length; ++k) {
                controls[k].text = datas[k]
            }
            for (var m = 0; m < cameraAlgoris.length; ++m) {
                cameraAlgoris[m].value = alis[m]
            }
        }

        onComputeCompleted: {
            if (isOk)
            {
                flexOffsetCompute.isEnable = true
                lineEdit16.text = model.getFlexCalibrationResult(0)
                lineEdit17.text = model.getFlexCalibrationResult(1)
                lineEdit18.text = model.getFlexCalibrationResult(2)
                lineEdit19.text = model.getFlexCalibrationResult(3)
            }
        }
    }

    Component.onCompleted: {
        model.initPanelData()
    }


    Flickable {
        anchors.fill: parent
        anchors.margins: 5
        anchors.bottomMargin: 5
        contentWidth: 590
        contentHeight: 400
        clip: true

        ColumnLayout {
            width: window.width - 10
            height: 400
            spacing: 0
            Label {
                text: qsTr(" Flex-tool ")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.bottomMargin: -5
                Layout.topMargin: 0
                Layout.preferredHeight: 30
                background: Rectangle {
                    color: "tan"
                }
            }
            Rectangle {
                border.width: 1
                border.color: "tan"
                color: "whitesmoke"
                Layout.fillWidth: true
                Layout.preferredHeight: 350

                ColumnLayout {
                    anchors.fill: parent
                    anchors.topMargin: 5
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos1(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }

                        TextField {
                            id: lineEdit1
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(0, text)
                            }
                        }
                        TextField {
                            id: lineEdit2
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(1, text)
                            }
                        }

                        TextField {
                            id: lineEdit3
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(2, text)
                            }
                        }

                        SpinBox {
                            id: spinBox
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setFlexCameraAli(0, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit1.text = pos[0]
                                lineEdit2.text = pos[1]
                                lineEdit3.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(3, 0)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos2(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit4
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(3, text)
                            }
                        }
                        TextField {
                            id: lineEdit5
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(4, text)
                            }
                        }

                        TextField {
                            id: lineEdit6
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(5, text)
                            }
                        }

                        SpinBox {
                            id: spinBox1
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setFlexCameraAli(1, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit4.text = pos[0]
                                lineEdit5.text = pos[1]
                                lineEdit6.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(3, 1)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos3(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit7
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(6, text)
                            }
                        }
                        TextField {
                            id: lineEdit8
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(7, text)
                            }
                        }
                        TextField {
                            id: lineEdit9
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(8, text)
                            }
                        }

                        SpinBox {
                            id: spinBox2
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setFlexCameraAli(2, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit7.text = pos[0]
                                lineEdit8.text = pos[1]
                                lineEdit9.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(3, 2)
                            }
                        }
                    }
                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("mark pos4(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit10
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setFlexData(9, text)
                            }
                        }
                        TextField {
                            id: lineEdit11
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setFlexData(10, text)
                            }
                        }
                        TextField {
                            id: lineEdit12
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(11, text)
                            }
                        }

                        SpinBox {
                            id: spinBox3
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setFlexCameraAli(3, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit10.text = pos[0]
                                lineEdit11.text = pos[1]
                                lineEdit12.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(3, 3)
                            }
                        }
                    }

                    RowLayout {
                        spacing: 5
                        Label {
                            text: qsTr("Sucking pos(XYZ):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit13
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setFlexData(12, text)
                            }
                        }
                        TextField {
                            id: lineEdit14
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            onTextChanged: {
                                model.setFlexData(13, text)
                            }
                        }
                        TextField {
                            id: lineEdit15
                            selectByMouse: true
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            onTextChanged: {
                                model.setFlexData(14, text)
                            }
                        }

                        SpinBox {
                            id: spinBox4
                            to: 999
                            Layout.preferredHeight: 35
                            onValueChanged: {
                                model.setFlexCameraAli(4, value)
                            }
                        }

                        MyButton {
                            imgRes: "qrc:/resource/icons/location.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                var pos = model.getAxisFPOS()
                                lineEdit13.text = pos[0]
                                lineEdit14.text = pos[1]
                                lineEdit15.text = pos[2]
                            }
                        }
                        MyButton {
                            imgRes: "qrc:/resource/icons/aim.svg"
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            rightButtonEnable: false
                            myRadius: 2
                            onMyButttonLeftclicked: {
                                model.axisMoveTo(3, 4)
                            }
                        }
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        MyButton {
                            id: flexOffsetCompute
                            buttonText: qsTr("offset computing")
                            Layout.preferredHeight: 40
                            Layout.preferredWidth: 140
                            myRadius: 5

                            onMyButttonLeftclicked: {
                                isEnable = false
                                flexOffsetCompute.isEnable = false
                                model.computeFlexOffset()
                            }
                        }
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        Label {
                            text: qsTr("Flex center(XY):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit16
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                        TextField {
                            id: lineEdit17
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                    }
                    RowLayout {
                        Layout.alignment: Qt.AlignCenter
                        Label {
                            text: qsTr("Center distance(mm):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit18
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                        Label {
                            text: qsTr("Rotation angle(deg):")
                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft
                        }
                        TextField {
                            id: lineEdit19
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            readOnly: true
                        }
                    }


                    /*Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }*/
                }
            }
        }
    }
}
