import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelFlow 1.0

Item {
    width: 1200
    height: 800

    property bool isComboxOk: false

    property var toolingPos: [posX_BareModuleTool, posY_BareModuleTool, posZ_BareModuleTool, posX_FlexTool, posY_FlexTool, posZ_FlexTool, posX_GluingTool, posY_GluingTool, posZ_GluingTool, posX_ModuleTool, posY_ModuleTool, posZ_ModuleTool];
    property var targetPos: [posX_Sensor_Left, posY_Sensor_Left, posZ_Sensor_Left, rotAngle_Sensor1,
        posX_Sensor_Right, posY_Sensor_Right, posZ_Sensor_Right, rotAngle_Sensor2,
        posX_Flex, posY_Flex, posZ_Flex, rotAngle_Flex, posX_Module, posY_Module, posZ_Module, rotAngle_Module];
    property var vcPos: [posX_VacuumChuck_Left, posY_VacuumChuck_Left, posZ_VacuumChuck_Left,
                         posX_VacuumChuck_Right, posY_VacuumChuck_Right, posZ_VacuumChuck_Right,
                         posX_FlexOnSensor, posY_FlexOnSensor, posZ_FlexOnSensor];
    property var cameraAlis: [ali0, ali1, ali2, ali3, ali4, ali5]
    property var grabs: [pickReleaseMode_GluingTool, pickReleaseMode_BareModuleTool, pickReleaseMode_FlexTool, pickReleaseMode_ModuleTool,
                         pickReleaseMode_Sensor1,pickReleaseMode_Sensor2,pickReleaseMode_Flex, pickReleaseMode_Module, pickReleaseMode_VC1, pickReleaseMode_VC2, pickReleaseMode_FlexOnSensor]
    property var accvels: [accvel1,accvel2,accvel3,accvel4,accvel5,accvel6,accvel7,accvel8]



    HQmlModelFlow {
        id: model
        onUpdateUI: {
            isComboxOk = false
            var grabNames = model.getCaptureNames()
            for (var k = 0; k < cameraAlis.length; ++k) {
                cameraAlis[k].value = alins[k]
            }

            for (var d = 0; d < grabs.length; ++d) {
                grabs[d].model = grabNames
                grabs[d].currentIndex = combxIndexs[d]
            }
            for (var m = 0; m < toolingPos.length; ++m) {
                toolingPos[m].text = _strTooling[m]
            }
            for (var n = 0; n < targetPos.length; ++n) {
                targetPos[n].text = _strTarget[n]
            }
            for (var i = 0; i < vcPos.length; ++i) {
                vcPos[i].text = _strVC[i]
            }
            for (var j = 0; j < accvels.length; j++)
            {
                accvels[j].text = accVels[j]
            }

            isComboxOk = true
        }

        onUpdateTargetUI: {
            for (var n = 0; n < targetPos.length; ++n) {
                targetPos[n].text = _strTarget[n]
            }
        }

        onUpdateVCUI: {
            for (var i = 0; i < vcPos.length; ++i) {
                vcPos[i].text = _strVC[i]
            }
        }
    }

    Connections{
        target: modelFlow
        onUpdateTargetUI: {
            for (var n = 0; n < targetPos.length; ++n) {
                targetPos[n].text = _strTarget[n]
            }
        }
        onUpdateVCUI: {
            for (var i = 0; i < vcPos.length; ++i) {
                vcPos[i].text = _strVC[i]
            }
        }
    }

    Component.onCompleted: {
        model.updateCtrl()
        model.updateTargetInfo()
        model.updateVCInfo()
        model.updateGluingOffsetInfo()
    }


    GroupBox {
        id: toolLocGroup
        x: 13
        y: 0
        width: 900
        height: 232
        title: qsTr("Location-Tools")

        GridLayout {
            id: gridLayout
            anchors.fill: parent
            columns: 10
//----------------------------------BareModuleTool------------------------------------
            Label {
                id: label12
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("BareModuleTool(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_BareModuleTool
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setToolingPos(1, "x", text)
                }
            }

            TextField {
                id: posY_BareModuleTool
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setToolingPos(1, "y", text)
                }
            }

            TextField {
                id: posZ_BareModuleTool
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setToolingPos(1, "z", text)
                }
            }

            SpinBox {
                id: ali1
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(1, value)
                }
            }

            MyButton {
                id: myButton03
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(1, "Tooling")
                }
            }

            MyButton {
                id: myButton04
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(1, "Tooling")
                }
            }
            ComboBox {
                id: pickReleaseMode_BareModuleTool
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(1, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(1)
                    isComboxOk = true
                }
            }
            MyButton {
                id: buttonPick_BareModuleTool
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_BareModuleTool.currentText)
                }
            }
            MyButton {
                id: buttonRelease_BareModuleTool
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_BareModuleTool.currentText)
                }
            }
//----------------------------------FlexTool------------------------------------

            Label {
                id: label13
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("FlexToolPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_FlexTool
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setToolingPos(2, "x", text)
                }
            }

            TextField {
                id: posY_FlexTool
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setToolingPos(2, "y", text)
                }
            }

            TextField {
                id: posZ_FlexTool
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setToolingPos(2, "z", text)
                }
            }

            SpinBox {
                id: ali2
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(2, value)
                }
            }

            MyButton {
                id: myButton05
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(2, "Tooling")
                }
            }

            MyButton {
                id: myButton06
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(2, "Tooling")
                }
            }
            ComboBox {
                id: pickReleaseMode_FlexTool
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(2, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(2)
                    isComboxOk = true
                }
            }
            MyButton {
                id: buttonPick_FlexTool
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_FlexTool.currentText)
                }
            }
            MyButton {
                id: buttonRelease_FlexTool
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_FlexTool.currentText)
                }
            }
//----------------------------------GluingTool------------------------------------
            Label {
                id: label_GluingTool
                width: 132
                height: 32
                text: qsTr("GluingToolPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

                        TextField {
                            id: posX_GluingTool
                            x: 158
                            y: 359
                            width: 142
                            height: 32
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            placeholderText: qsTr("X")
                            onEditingFinished: {
                                model.setToolingPos(0, "x", text)
                            }
                        }

                        TextField {
                            id: posY_GluingTool
                            x: 306
                            y: 359
                            width: 142
                            height: 32
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            placeholderText: qsTr("Y")
                            onEditingFinished: {
                                model.setToolingPos(0, "y", text)
                            }
                        }

                        TextField {
                            id: posZ_GluingTool
                            x: 454
                            y: 359
                            width: 142
                            height: 32
                            Layout.fillWidth: true
                            Layout.preferredHeight: 35
                            selectByMouse: true
                            placeholderText: qsTr("Z")
                            onEditingFinished: {
                                model.setToolingPos(0, "z", text)
                            }
                        }

                        SpinBox {
                            id: ali0
                            Layout.minimumWidth: 120
                            Layout.preferredHeight: 35
                            Layout.fillWidth: true
                            onValueChanged: {
                                model.setCameraAli(0, value)
                            }
                        }

                        MyButton {
                            id: buttonLocate_GluingTool
                            x: 612
                            y: 359
                            width: 32
                            height: 32
                            Layout.preferredHeight: 35
                            Layout.preferredWidth: 35
                            imgRes: "qrc:/resource/icons/location.svg"
                            myRadius: 0
                            onMyButttonLeftclicked: {
                                model.getCurrentPos(0, "Tooling")
                            }
                        }

                        MyButton {
                            id: buttonAim_GluingTool
                            x: 612
                            y: 359
                            width: 32
                            height: 32
                            Layout.preferredWidth: 35
                            Layout.preferredHeight: 35
                            imgRes: "qrc:/resource/icons/aim.svg"
                            myRadius: 0
                            onMyButttonLeftclicked: {
                                model.movePos(0, "Tooling")
                            }
                        }
                        ComboBox {
                            id: pickReleaseMode_GluingTool
                            width: 190
                            height: 32
                            Layout.preferredHeight: 35
                            onCurrentIndexChanged: {
                                if (isComboxOk) {
                                    model.setGrab(0, currentIndex)
                                }
                            }
                            onHighlighted: {
                                isComboxOk = false
                                this.model = model.getCaptureNames()
                                currentIndex = model.getGrab(0)
                                isComboxOk = true
                            }
                        }
                        MyButton {
                            id: buttonPick_GluingTool
                            x: 612
                            y: 359
                            width: 32
                            height: 32
                            Layout.preferredWidth: 35
                            Layout.preferredHeight: 35
                            imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                            myRadius: 0
                            onMyButttonLeftclicked: {
                                model.pickTooling(pickReleaseMode_GluingTool.currentText)
                            }
                        }
                        MyButton {
                            id: buttonRelease_GluingTool
                            x: 612
                            y: 359
                            width: 32
                            height: 32
                            Layout.preferredWidth: 35
                            Layout.preferredHeight: 35
                            imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                            myRadius: 0
                            onMyButttonLeftclicked: {
                                model.releaseTooling(pickReleaseMode_GluingTool.currentText)
                            }
                        }



                        Label {
                            id: label_ModuleTool
                            width: 132
                            height: 32
                            text: qsTr("ModuleToolPos(XYZ)")
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter
                        }

                                    TextField {
                                        id: posX_ModuleTool
                                        x: 158
                                        y: 359
                                        width: 142
                                        height: 32
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 35
                                        selectByMouse: true
                                        placeholderText: qsTr("X")
                                        onEditingFinished: {
                                            model.setToolingPos(3, "x", text)
                                        }
                                    }

                                    TextField {
                                        id: posY_ModuleTool
                                        x: 306
                                        y: 359
                                        width: 142
                                        height: 32
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 35
                                        selectByMouse: true
                                        placeholderText: qsTr("Y")
                                        onEditingFinished: {
                                            model.setToolingPos(3, "y", text)
                                        }
                                    }

                                    TextField {
                                        id: posZ_ModuleTool
                                        x: 454
                                        y: 359
                                        width: 142
                                        height: 32
                                        Layout.fillWidth: true
                                        Layout.preferredHeight: 35
                                        selectByMouse: true
                                        placeholderText: qsTr("Z")
                                        onEditingFinished: {
                                            model.setToolingPos(3, "z", text)
                                        }
                                    }

                                    SpinBox {
                                        id: ali3
                                        Layout.minimumWidth: 120
                                        Layout.preferredHeight: 35
                                        Layout.fillWidth: true
                                        onValueChanged: {
                                            model.setCameraAli(3, value)
                                        }
                                    }

                                    MyButton {
                                        id: buttonLocate_ModuleTool
                                        x: 612
                                        y: 359
                                        width: 32
                                        height: 32
                                        Layout.preferredHeight: 35
                                        Layout.preferredWidth: 35
                                        imgRes: "qrc:/resource/icons/location.svg"
                                        myRadius: 0
                                        onMyButttonLeftclicked: {
                                            model.getCurrentPos(3, "Tooling")
                                        }
                                    }

                                    MyButton {
                                        id: buttonAim_ModuleTool
                                        x: 612
                                        y: 359
                                        width: 32
                                        height: 32
                                        Layout.preferredWidth: 35
                                        Layout.preferredHeight: 35
                                        imgRes: "qrc:/resource/icons/aim.svg"
                                        myRadius: 0
                                        onMyButttonLeftclicked: {
                                            model.movePos(3, "Tooling")
                                        }
                                    }
                                    ComboBox {
                                        id: pickReleaseMode_ModuleTool
                                        width: 190
                                        height: 32
                                        Layout.preferredHeight: 35
                                        onCurrentIndexChanged: {
                                            if (isComboxOk) {
                                                model.setGrab(3, currentIndex)
                                            }
                                        }
                                        onHighlighted: {
                                            isComboxOk = false
                                            this.model = model.getCaptureNames()
                                            currentIndex = model.getGrab(3)
                                            isComboxOk = true
                                        }
                                    }
                                    MyButton {
                                        id: buttonPick_ModuleTool
                                        x: 612
                                        y: 359
                                        width: 32
                                        height: 32
                                        Layout.preferredWidth: 35
                                        Layout.preferredHeight: 35
                                        imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                                        myRadius: 0
                                        onMyButttonLeftclicked: {
                                            model.pickTooling(pickReleaseMode_ModuleTool.currentText)
                                        }
                                    }
                                    MyButton {
                                        id: buttonRelease_ModuleTool
                                        x: 612
                                        y: 359
                                        width: 32
                                        height: 32
                                        Layout.preferredWidth: 35
                                        Layout.preferredHeight: 35
                                        imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                                        myRadius: 0
                                        onMyButttonLeftclicked: {
                                            model.releaseTooling(pickReleaseMode_ModuleTool.currentText)
                                        }
                                    }
        }
    }
//----------------------------------Sensor & Flex Location Box------------------------------------
    GroupBox {
        id: moduleFlexLocGroup
        x: 13
        y: 238
        width: 900
        height: 195
        title: qsTr("Location-BareModule-Flex")
        GridLayout {
            id: gridLayout1
            anchors.fill: parent
            columns: 12
            rows: 4

            Label {
                id: label21
                width: 132
                height: 32
                text: qsTr("BareModule1 (XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_Sensor_Left
                x: 158
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("X")
            }

            TextField {
                id: posY_Sensor_Left
                x: 306
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Y")
            }

            TextField {
                id: posZ_Sensor_Left
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
            }

            TextField {
                id: rotAngle_Sensor1
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("rot")
            }

            MyButton {
                id: myButton12
                x: 612
                y: 412
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(0, "Target")
                }
            }

            MyButton {
                id: buttonRotateClockwise_Sensor1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-left.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateClockwise(0)
                }
            }

            MyButton {
                id: buttonRotateHome_Sensor1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/home.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.homeT();
                }
            }

            MyButton {
                id: buttonRotateCounterclockwise_Sensor1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-right.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateCounterClockwise(0)
                }
            }

            ComboBox {
                id: pickReleaseMode_Sensor1
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(4, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(4)
                    isComboxOk = true
                }
            }
            MyButton {
                id: buttonPick_Sensor1
                x: 612
                y: 412
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_Sensor1.currentText)
                }
            }
            MyButton {
                id: buttonRelease_Sensor1
                x: 612
                y: 412
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_Sensor1.currentText)
                }
            }


            Label {
                id: label22
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("BareModule2 (XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_Sensor_Right
                x: 158
                y: 412
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("X")
            }

            TextField {
                id: posY_Sensor_Right
                x: 306
                y: 412
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Y")
            }

            TextField {
                id: posZ_Sensor_Right
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(1, "Sensor2", "z")
                }
            }

            TextField {
                id: rotAngle_Sensor2
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(1, "Sensor2", "rot")
                }
            }

            MyButton {
                id: myButton14
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(1, "Target")
                }
            }

            MyButton {
                id: buttonRotateClockwise_Sensor2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-left.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateClockwise(1)
                }
            }

            MyButton {
                id: buttonRotateHome_Sensor2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/home.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.homeT();
                }
            }

            MyButton {
                id: buttonRotateCounterclockwise_Sensor2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-right.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateCounterClockwise(1)
                }
            }

            ComboBox {
                id: pickReleaseMode_Sensor2
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(5, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(5)
                    isComboxOk = true
                }
            }

            MyButton {
                id: buttonPick_Sensor2
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_Sensor2.currentText)
                }
            }
            MyButton {
                id: buttonRelease_Sensor2
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_Sensor2.currentText)
                }
            }


            Label {
                id: label23
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("FlexPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_Flex
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.getPickingPos(2, "Flex", "x")
                }
            }

            TextField {
                id: posY_Flex
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.getPickingPos(2, "Flex", "y")
                }
            }

            TextField {
                id: posZ_Flex
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(2, "Flex", "z")
                }
            }

            TextField {
                id: rotAngle_Flex
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(2, "Flex", "rot")
                }
            }

            MyButton {
                id: myButton16
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(2, "Target")
                }
            }

            MyButton {
                id: buttonRotateClockwise_Flex
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-left.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateClockwise(2)
                }
            }

            MyButton {
                id: buttonRotateHome_Flex
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/home.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.homeT();
                }
            }

            MyButton {
                id: buttonRotateCounterclockwise_Flex
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-right.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateCounterClockwise(2)
                }
            }

            ComboBox {
                id: pickReleaseMode_Flex
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(6, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(6)
                    isComboxOk = true
                }
            }
            MyButton {
                id: buttonPick_Flex
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_Flex.currentText)
                }
            }
            MyButton {
                id: buttonRelease_Flex
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_Flex.currentText)
                }
            }

            Label {
                id: label24
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("ModulePos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_Module
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.getPickingPos(3, "Module", "x")
                }
            }

            TextField {
                id: posY_Module
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.getPickingPos(3, "Module", "y")
                }
            }

            TextField {
                id: posZ_Module
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(3, "Module", "z")
                }
            }

            TextField {
                id: rotAngle_Module
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                readOnly:true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.getPickingPos(3, "Module", "rot")
                }
            }

            MyButton {
                id: myButton17
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(3, "Target")
                }
            }

            MyButton {
                id: buttonRotateClockwise_Module
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-left.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateClockwise(3)
                }
            }

            MyButton {
                id: buttonRotateHome_Module
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/home.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.homeT();
                }
            }

            MyButton {
                id: buttonRotateCounterclockwise_Module
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/arrow-right.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.rotateCounterClockwise(3)
                }
            }

            ComboBox {
                id: pickReleaseMode_Module
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(7, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(7)
                    isComboxOk = true
                }
            }
            MyButton {
                id: buttonPick_Module
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_Module.currentText)
                }
            }
            MyButton {
                id: buttonRelease_Module
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_Module.currentText)
                }
            }
        }
    }
//----------------------------------Vacuum Chuck Location Box------------------------------------
    GroupBox {
        id: chuckLocGroup
        x: 13
        y: 439
        width: 900
        height: 185
        title: qsTr("Location-Chuck")
        GridLayout {
            id: gridLayout2
            anchors.fill: parent
            anchors.leftMargin: 0
            anchors.topMargin: -6
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            columns: 10

            Label {
                id: label31
                width: 132
                height: 32
                text: qsTr("VacuumChuck1(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_VacuumChuck_Left
                x: 158
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("X")
                onEditingFinished: {
                    model.setVCPos(0, "x", text)
                }
            }

            TextField {
                id: posY_VacuumChuck_Left
                x: 306
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Y")
                onEditingFinished: {
                    model.setVCPos(0, "y", text)
                }
            }

            TextField {
                id: posZ_VacuumChuck_Left
                x: 454
                y: 359
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                placeholderText: qsTr("Z")
                onEditingFinished: {
                    model.setVCPos(0, "z", text)
                }
            }

            SpinBox {
                id: ali4
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                Layout.minimumWidth: 120
                onValueChanged: {
                    model.setCameraAli(3, value)
                }
            }

            MyButton {
                id: myButton21
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(0, "VC")
                }
            }

            MyButton {
                id: myButton22
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(0, "VC")
                }
            }

            ComboBox {
                id: pickReleaseMode_VC1
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(8, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(8)
                    isComboxOk = true
                }
            }

            MyButton {
                id: buttonPick_VC1
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_VC1.currentText)
                }
            }
            MyButton {
                id: buttonRelease_VC1
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_VC1.currentText)
                }
            }


            Label {
                id: label32
                x: 15
                y: 412
                width: 132
                height: 32
                text: qsTr("VacuumChuck2(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_VacuumChuck_Right
                x: 158
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("X")
            }

            TextField {
                id: posY_VacuumChuck_Right
                x: 306
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Y")
            }

            TextField {
                id: posZ_VacuumChuck_Right
                x: 454
                y: 412
                width: 142
                height: 32
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z")
            }

            SpinBox {
                id: ali5
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                onValueChanged: {
                    model.setCameraAli(4, value)
                }
            }

            MyButton {
                id: myButton23
                x: 612
                y: 359
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.getCurrentPos(1, "VC")
                }
            }

            MyButton {
                id: myButton24
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                Layout.preferredWidth: 35
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(1, "VC")
                }
            }

            ComboBox {
                id: pickReleaseMode_VC2
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(9, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(9)
                    isComboxOk = true
                }
            }

            MyButton {
                id: buttonPick_VC2
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_VC2.currentText)
                }
            }
            MyButton {
                id: buttonRelease_VC2
                x: 612
                y: 412
                width: 32
                height: 32
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_VC2.currentText)
                }
            }

            Label {
                id: label33
                width: 132
                height: 32
                text: qsTr("FlexOnSensorPos(XYZ)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: posX_FlexOnSensor
                x: 158
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("X")
            }

            TextField {
                id: posY_FlexOnSensor
                x: 306
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Y")
            }

            TextField {
                id: posZ_FlexOnSensor
                x: 454
                y: 359
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z")
            }

            TextField {
                id: tbd
                Layout.preferredWidth: 142
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: false
                readOnly: true
                Layout.columnSpan: 2
                placeholderText: qsTr("TBD")
            }

            MyButton {
                id: move_FlexOnSensor
                x: 612
                y: 412
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePos(2, "VC")
                }
            }

            ComboBox {
                id: pickReleaseMode_FlexOnSensor
                width: 190
                height: 32
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setGrab(10, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getCaptureNames()
                    currentIndex = model.getGrab(10)
                    isComboxOk = true
                }
            }

            MyButton {
                id: buttonPick_FlexOnSensor
                x: 612
                y: 412
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleUp.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.pickTooling(pickReleaseMode_FlexOnSensor.currentText)
                }
            }
            MyButton {
                id: buttonRelease_FlexOnSensor
                x: 612
                y: 412
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseTooling(pickReleaseMode_FlexOnSensor.currentText)
                }
            }

        }
    }

//----------------------------------Motion Parameters------------------------------------
    GroupBox {
        id: motionParaGroup
        x: 925
        y: 0
        width: 180
        height: 320
        title: qsTr("Motion Parameters")

        GridLayout {
            id: gridLayout3
            anchors.fill: parent
            columns:4
            columnSpacing: 5

            Label {
                id: label7
                Layout.column: 1
                Layout.row: 0
                Layout.columnSpan: 2
                text: qsTr("AccVel-X")
            }

            TextField {
                id: accvel1
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(0, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel2
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(1, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label8
                Layout.column: 1
                Layout.row: 2
                Layout.columnSpan: 2
                text: qsTr("AccVel-Y")
            }

            TextField {
                id: accvel3
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 3
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(2, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel4
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 3
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(3, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label9
                Layout.column: 1
                Layout.row: 4
                Layout.columnSpan: 2
                text: qsTr("AccVel-Z")
            }

            TextField {
                id: accvel5
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 5
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(4, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel6
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 5
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(5, text)
                }
                placeholderText: qsTr("VEL")
            }

            Label {
                id: label10
                Layout.column: 1
                Layout.row: 6
                Layout.columnSpan: 2
                text: qsTr("AccVel-T")
            }

            TextField {
                id: accvel7
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 0
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(6, text)
                }
                placeholderText: qsTr("ACC")
            }

            TextField {
                id: accvel8
                Layout.preferredWidth: 80
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 7
                Layout.columnSpan: 2
                selectByMouse: true
                onTextEdited: {
                    model.setAccVels(7, text)
                }
                placeholderText: qsTr("VEL")
            }

        }
    }
}
