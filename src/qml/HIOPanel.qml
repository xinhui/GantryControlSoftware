﻿import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.12

import HQmlModelIO 1.0

Item {
    id: table
    antialiasing: true

    function reSet() {
        for (var k = 0; k < 32; k++) {
            myListModel.append({
                                   "index": k,
                                   "inputName": "",
                                   "input": -1,
                                   "outputName": "",
                                   "output": 1
                               })
        }
    }

    property var typeArray: [0, 1, 2]
    property ListModel myListModel: ListModel {}

    HQmlModelIO {
        id: ioModel
        onUpdateIOs: {
            for (var k = 0; k < 32; ++k) {
                myListModel.setProperty(k, "input", inputs[k])
                myListModel.setProperty(k, "output", outputs[k])
            }
            if (!checkButton.checked)
                forceValue.text = ioModel.getForceValue()
        }
    }
    Component.onCompleted: {
        reSet()
        ioModel.startUpdateIOs(true)

        var ins = ioModel.getInputNames()
        var outs = ioModel.getOutputNames()
        for (var k = 0; k < 32; ++k) {
            myListModel.setProperty(k, "inputName", ins[k])
            myListModel.setProperty(k, "outputName", outs[k])
        }
    }

    Rectangle {
        anchors.fill: parent

        Rectangle {
            id: forceRect
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 40
            color: "skyblue"
            RowLayout {

                anchors.fill: parent
                spacing: 5
                Label {
                    verticalAlignment: Qt.AlignVCenter
                    horizontalAlignment: Qt.AlignRight
                    Layout.fillHeight: true

                    text: qsTr(" force sensor(g): ")
                }
                TextField {
                    id: forceValue
                    font.pixelSize: 16
                    horizontalAlignment: Qt.AlignRight
                    Layout.fillHeight: true
                    Layout.preferredWidth: 150
                    selectByMouse: true
                    readOnly: true
                    text: "null"
                }

                Button {
                    id: checkButton
                    text: "hold"
                    checkable: true
                    Layout.preferredHeight: 35
                    Layout.preferredWidth: 100
                    Layout.alignment: Qt.AlignVCenter
                }
                MyButton {
                    id: zeroButton
                    buttonText: "zero"
                    myRadius: 0
                    Layout.preferredHeight: 35
                    Layout.preferredWidth: 100
                    Layout.alignment: Qt.AlignVCenter
                    onMyButttonLeftclicked: {
                        ioModel.zeroForceSensor()
                    }
                }
                Rectangle {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    color: "skyblue"
                }
            }
        }

        TableView {
            id: tableView

            anchors.top: forceRect.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            antialiasing: true
            highlightOnFocus: true
            selectionMode: SelectionMode.SingleSelection

            /*Component {
                id: textField
                TextField {
                    readOnly: false
                    selectByMouse: true
                    anchors.fill: parent
                    //font.family: "Calibri"
                    text: styleData.value
                    color: "black"
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pixelSize: parent.height * 0.35
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor
                        acceptedButtons: Qt.NoButton
                    }
                    background: Rectangle {
                        anchors.fill: parent
                        color: "mistyrose"
                        border.width: parent.activeFocus ? 2 : 1
                        border.color: parent.activeFocus ? "red" : "black"
                    }
                    onDisplayTextChanged: {
                        if (styleData.row !== -1)
                            myListModel.setProperty(styleData.row,
                                                    "value1", text)
                    }
                }
            }

            Component {
                id: textField1
                Label {
                    color: "black"
                    verticalAlignment: Text.AlignVCenter
                    //font.family: "Calibri"
                    text: styleData.value
                    font.pixelSize: parent.height * 0.25
                    background: Rectangle {
                        anchors.fill: parent
                        //border.color: "#333"
                        color: "paleturquoise"
                        border.width: 1
                    }
                }
                //            TextField{
                //                readOnly: true
                //                color : "black"
                //                selectByMouse:true
                //                anchors.verticalCenter: parent.verticalCenter
                //                anchors.horizontalCenter: parent.horizontalCenter
                //                font.family: "Calibri"
                //                text: styleData.value
                //                font.pixelSize: parent.height*0.4
                //                background: Rectangle {
                //                    anchors.fill: parent
                //                    //border.color: "#333"
                //                    color:"paleturquoise"
                //                    border.width: 1
                //                }
                //            }
            }*/

            Component {
                id: myColorCircle
                Rectangle {
                    anchors.fill: parent
                    border.width: 1
                    border.color: "black"
                    ColumnLayout {
                        anchors.fill: parent
                        Rectangle {
                            Layout.alignment: Qt.AlignCenter
                            height: parent.height - 10
                            width: parent.height - 10
                            radius: parent.height - 10
                            color: styleData.value === -1 ? "grey" : styleData.value
                                                            === 0 ? "green" : "red"
                        }
                    }
                }
            }

            Component {
                id: mySwitch
                Rectangle {
                    anchors.fill: parent
                    border.width: 1
                    border.color: "black"
                    Switch {
                        anchors.fill: parent
                        antialiasing: true
                        wheelEnabled: false
                        checked: styleData.value
                        background: Rectangle {
                            anchors.fill: parent
                            color: "mistyrose"
                            border.width: parent.hovered ? 2 : 1
                            border.color: parent.hovered ? "red" : "black"
                        }

                        onClicked: {
                            ioModel.setOutput(styleData.row, checked)
                        }
                    }
                }
            }

            TableViewColumn {
                id: firstcol
                role: "index"
                title: qsTr("index")
                movable: false
                resizable: false
                delegate: Rectangle {
                    color: "gray"
                    border.width: 1
                    //border.color: "palevioletred"
                    antialiasing: true

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        //anchors.fill: parent
                        anchors.horizontalCenter: parent.horizontalCenter
                        //anchors.leftMargin: 50
                        //font.family: "Calibri"
                        color: "black"
                        text: styleData.value.toString()
                        font.pixelSize: parent.height * 0.35
                    }
                }
                width: 50
            }

            TableViewColumn {
                id: col1
                role: "inputName"
                title: qsTr("Input Name")
                movable: false
                resizable: false
                width: (tableView.viewport.width - firstcol.width) * 0.30
                delegate: TextField {
                    readOnly: false
                    selectByMouse: true
                    anchors.fill: parent
                    //font.family: "Calibri"
                    text: styleData.value
                    color: "black"
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pixelSize: parent.height * 0.35
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor
                        acceptedButtons: Qt.NoButton
                    }
                    background: Rectangle {
                        anchors.fill: parent
                        color: "mistyrose"
                        border.width: parent.activeFocus ? 2 : 1
                        border.color: parent.activeFocus ? "red" : "black"
                    }
                    onDisplayTextChanged: {
                        if (styleData.row !== -1) {
                            myListModel.setProperty(styleData.row,
                                                    "inputName", text)
                            ioModel.setInputName(styleData.row, text)
                        }
                    }
                }
            }

            TableViewColumn {
                id: col2
                role: "input"
                title: qsTr("input")
                movable: false
                resizable: false
                width: (tableView.viewport.width - firstcol.width) * 0.2
                delegate: myColorCircle
            }

            TableViewColumn {
                id: col3
                role: "outputName"
                title: qsTr("Output Name")
                movable: false
                resizable: false
                width: (tableView.viewport.width - firstcol.width) * 0.30
                delegate: TextField {
                    readOnly: false
                    selectByMouse: true
                    anchors.fill: parent
                    //font.family: "Calibri"
                    text: styleData.value
                    color: "black"
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pixelSize: parent.height * 0.35
                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.IBeamCursor
                        acceptedButtons: Qt.NoButton
                    }
                    background: Rectangle {
                        anchors.fill: parent
                        color: "mistyrose"
                        border.width: parent.activeFocus ? 2 : 1
                        border.color: parent.activeFocus ? "red" : "black"
                    }
                    onDisplayTextChanged: {
                        if (styleData.row !== -1) {
                            ioModel.setOutputName(styleData.row, text)
                            myListModel.setProperty(styleData.row,
                                                    "outputName", text)
                        }
                    }
                }
            }

            TableViewColumn {
                id: col4
                role: "output"
                title: qsTr("output")
                movable: false
                resizable: false
                width: (tableView.viewport.width - firstcol.width - col1.width
                        - col2.width - col3.width)
                delegate: mySwitch
            }

            model: myListModel

            headerDelegate: Rectangle {
                gradient: Gradient {
                    GradientStop {
                        position: 0.0
                        color: "gainsboro"
                    }
                    GradientStop {
                        position: 1.0
                        color: "darkgrey"
                    }
                }

                color: styleData.selected ? "blue" : "darkgray"
                height: 40

                border.color: "black"
                border.width: 1
                radius: 0
                Text {
                    //font.family: "Calibri"
                    anchors.centerIn: parent
                    text: styleData.value
                    font.pixelSize: parent.height * 0.35
                }
            }
            rowDelegate: Component {
                Rectangle {
                    color: "wheat"
                    height: 50 //(table.height-42)/tablemode.count
                }
            }

            style: TableViewStyle {
                highlightedTextColor: "#00CCFE"
                backgroundColor: "wheat"
            }
        }
    }

}
