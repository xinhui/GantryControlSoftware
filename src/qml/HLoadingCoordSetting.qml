import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlLoadingCoordSetting 1.0

Window {
    id: window
    title: "Loading Coordinate Setting"
    width: 600
    height: 340
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.WindowMinMaxButtonsHint
           | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowCloseButtonHint

    property var configLoadingCoord: [waitingZ, delOX, delOY, movingSpeed, movingAcc, searchPos1, searchPos2, placingSpeed, placingAcc, searchForce];

    HQmlLoadingCoordSetting {
        id: model
        onUpdateLoadingCoordUI: {
            chooseEvent.enabled = false
            choose.model = cfgNames
            choose.currentIndex = currentCfg
            chooseEvent.enabled = true

            configLoadingCoord[0].text = cfgDatas[0]
            outputGantry.value = cfgDatas[1]
            outputVC.value = cfgDatas[11]
            for (var k = 1; k < configLoadingCoord.length; ++k) {
                configLoadingCoord[k].text = cfgDatas[k+1]
            }
        }
    }

    Component.onCompleted: {
        model.updateLoadingCoordInfo();
    }

    GridLayout {
        id: gridLayout1
        height: 755
        anchors.fill: parent
        rowSpacing: 20
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        columns: 4

        Label {
            id: label10
            width: 49
            height: 12
            text: qsTr("Module Number:")
        }

        ComboBox {
            id: choose
            width: 279
            height: 40
            Layout.fillWidth: true
            editable: true
            selectTextByMouse: true

            Connections {
                id: chooseEvent
                enabled: false
                function onCurrentIndexChanged() {
                    model.setCurrentConfig(choose.currentIndex)
                }
            }
        }

        MyButton {
            id: myButton
            Layout.preferredWidth: 120
            Layout.preferredHeight: 40
            myRadius: 0
            buttonText: "new"
            onMyButttonLeftclicked: {
                model.addConfig(choose.editText)
            }
        }

        MyButton {
            id: myButton1
            Layout.preferredHeight: 40
            Layout.preferredWidth: 120
            myRadius: 0
            isEnable: choose.model.length !== 0
            buttonText: "delete"
            onMyButttonLeftclicked: {
                model.removeConfig(choose.currentIndex)
            }
        }

        GroupBox {
            id: groupBox
            width: 700
            height: 261
            Layout.fillWidth: true
            Layout.columnSpan: 4

            GridLayout {
                id: gridLayout
                anchors.fill: parent
                rows: 8
                columns: 4

                Label {
                    id: labelWaitingZ
                    text: qsTr("Waiting z(mm)")
                }

                TextField {
                    id: waitingZ
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("WaitingZ", text)
                    }
                }

                Label {
                    id: labelOutput
                    width: 30
                    height: 7
                    text: qsTr("output(Gantry):")
                }

                SpinBox {
                    id: outputGantry
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    editable: true
                    to: 31
                    onValueChanged: {
                        model.setConfigData("OutputGantry", value)
                    }
                }

                Label {
                    id: labelDelOX
                    text: qsTr("Del_OX(mm):")
                }

                TextField {
                    id: delOX
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("DelOX", text)
                    }
                }

                Label {
                    id: labelDelOY
                    text: qsTr("Del_OY(mm):")
                }

                TextField {
                    id: delOY
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("DelOY", text)
                    }
                }

                Label {
                    id: labelMovingSpeed
                    width: 30
                    height: 7
                    text: qsTr("Moving Speed(mm/s):")
                }

                TextField {
                    id: movingSpeed
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("MovingSpeed", text)
                    }
                }


                Label {
                    id: labelMovingAcc
                    text: qsTr("Moving Acc(mm/s^2:")
                }

                TextField {
                    id: movingAcc
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onEditingFinished: {
                       model.setConfigData("MovingAcc", text)
                    }
                }

                Label {
                    id: labelSearchPos1
                    width: 38
                    height: 12
                    text: qsTr("Search pos 1(mm):")
                }

                TextField {
                    id: searchPos1
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("SearchPos1", text)
                    }
                }

                Label {
                    id: labelSeachPos2
                    width: 30
                    height: 7
                    text: qsTr("Search pos 2(mm)")
                }

                TextField {
                    id: searchPos2
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    Layout.fillWidth: true
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("SearchPos2", text)
                    }
                }

                Label {
                    id: labelPlacingSpeed
                    width: 30
                    height: 7
                    text: qsTr("Placing speed(mm/s):")
                }

                TextField {
                    id: placingSpeed
                    width: 200
                    height: 40
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("PlacingSpeed", text)
                    }
                }

                Label {
                    id: labelPlacingAcc
                    text: qsTr("Placing acc(mm/s^2):")
                }

                TextField {
                    id: placingAcc
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("PlacingAcc", text)
                    }
                }

                Label {
                    id: labelSeachForce
                    text: "Search force(g)"
                }

                TextField {
                    id: searchForce
                    Layout.fillWidth: true
                    selectByMouse: true
                    Layout.preferredHeight: 30
                    placeholderText: qsTr("")
                    onEditingFinished: {
                        model.setConfigData("SearchForce", text)
                    }
                }

                Label {
                    id: labelVCOutput
                    width: 30
                    height: 7
                    text: qsTr("output(VC):")
                }

                SpinBox {
                    id: outputVC
                    Layout.preferredHeight: -1
                    Layout.fillWidth: true
                    editable: true
                    to: 31
                    onValueChanged: {
                        model.setConfigData("OutputVC", value)
                    }
                }

            //End GridLayout
            }
        //End GroupBox
        }
    //End GridLayout
    }
//End Window
}
