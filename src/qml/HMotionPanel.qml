﻿import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.12

import HQmlModelMotion 1.0

Rectangle {
    id: table
    antialiasing: true
    border.width: 5
    border.color: "tan"

    property ListModel myListModel: ListModel {}
    property var axisNames: ["X", "Y", "Z", "T"]

    function reSet() {
        for (var k = 0; k < axisNames.length; ++k) {
            myListModel.append({
                                   "axis": axisNames[k],
                                   "enable": false,
                                   "home": 0,
                                   "status": -1,
                                   "speed": "10",
                                   "distance": "1",
                                   "stop": 0,
                                   "left": 0,
                                   "right": 0,
                                   "position": "0.0000",
                                   "velocity": "0.000"
                               })
        }
    }

    HQmlModelMotion {
        id: motionModel
        onUpdateMotion: {
            for (var k = 0; k < axisNames.length; ++k) {
                myListModel.setProperty(k, "enable", !states[k])
                myListModel.setProperty(k, "status", states[k])
                myListModel.setProperty(k, "position", positions[k])
                myListModel.setProperty(k, "velocity", velocitys[k])
            }
        }
    }

    Component.onCompleted: {
        reSet()
        for (var k = 0; k < axisNames.length; ++k) {
            myListModel.setProperty(k, "speed", motionModel.getSpeed(k))
            myListModel.setProperty(k, "distance", motionModel.getDistance(k))
        }
        motionModel.startUpdateMotions(true)
    }

    TableView {
        id: tableView

        anchors.fill: parent
        anchors.margins: 5
        antialiasing: true
        highlightOnFocus: true
        selectionMode: SelectionMode.SingleSelection

        TableViewColumn {
            id: column1
            role: "axis"
            title: qsTr("Axis")
            movable: false
            resizable: false
            width: 61
            delegate: Rectangle {
                color: "gray"
                border.width: 1
                //border.color: "palevioletred"
                antialiasing: true

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    //anchors.fill: parent
                    anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.leftMargin: 50
                    //font.family: "Calibri"
                    color: "black"
                    text: styleData.value
                    font.pixelSize: parent.height * 0.35
                }
            }
        }

        TableViewColumn {
            id: column2
            role: "enable"
            title: qsTr("Enable")
            movable: false
            resizable: false
            width: 120
            delegate: Rectangle {
                anchors.fill: parent
                border.width: 1
                border.color: "black"
                Switch {
                    anchors.fill: parent
                    antialiasing: true
                    wheelEnabled: false
                    checked: styleData.value
                    background: Rectangle {
                        anchors.fill: parent
                        color: "mistyrose"
                        border.width: parent.hovered ? 2 : 1
                        border.color: parent.hovered ? "red" : "black"
                    }

                    onClicked: {
                        motionModel.enableAxis(styleData.row, checked)
                    }
                }
            }
        }

        TableViewColumn {
            id: column3
            role: "home"
            title: qsTr("Home")
            movable: false
            resizable: false
            width: 50
            delegate: MyButton {
                myRadius: 0
                imgRes: "qrc:/resource/icons/home.svg"
                onMyButttonLeftclicked: {
                    motionModel.homeAxis(styleData.row)
                }
            }
        }

        TableViewColumn {
            id: column4
            role: "status"
            title: qsTr("Axis State")
            movable: false
            resizable: false
            width: (tableView.viewport.width - 380) * 0.15
                   < 100 ? 100 : (tableView.viewport.width - 380) * 0.15
            delegate: Rectangle {

                function getColor(state) {
                    switch (state) {
                    case -1:
                        return "red"
                    case 0:
                        return "lightgreen"
                    case 1:
                        return "red"
                    case 2:
                        return "silver"
                    }
                }

                color: getColor(styleData.value)
                border.width: 1
                //border.color: "palevioletred"
                antialiasing: true
                Text {

                    function getText(state) {
                        switch (state) {
                        case -1:
                            return "not connected"
                        case 0:
                            return "enable"
                        case 1:
                            return "error"
                        case 2:
                            return "disable"
                        }
                    }

                    anchors.verticalCenter: parent.verticalCenter
                    //anchors.fill: parent
                    anchors.horizontalCenter: parent.horizontalCenter
                    //anchors.leftMargin: 50
                    //font.family: "Calibri"
                    color: "black"
                    text: getText(styleData.value)
                    font.pixelSize: parent.height * 0.35
                }
            }
        }

        TableViewColumn {
            id: column5
            role: "speed"
            title: qsTr("Speed")
            movable: false
            resizable: false
            width: (tableView.viewport.width - 380) * 0.15
                   < 100 ? 100 : (tableView.viewport.width - 380) * 0.15
            delegate: TextField {
                readOnly: false
                selectByMouse: true
                anchors.fill: parent
                //font.family: "Calibri"
                text: styleData.value
                color: "black"
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pixelSize: parent.height * 0.35
                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.IBeamCursor
                    acceptedButtons: Qt.NoButton
                }
                background: Rectangle {
                    anchors.fill: parent
                    color: "mistyrose"
                    border.width: parent.activeFocus ? 2 : 1
                    border.color: parent.activeFocus ? "red" : "black"
                }
                onDisplayTextChanged: {
                    if (styleData.row !== -1) {
                        myListModel.setProperty(styleData.row, "speed", text)
                        motionModel.setSpeed(styleData.row, text)
                    }
                }
            }
        }

        TableViewColumn {
            id: column6
            role: "distance"
            title: qsTr("Distance")
            movable: false
            resizable: false
            width: (tableView.viewport.width - 380) * 0.15
                   < 100 ? 100 : (tableView.viewport.width - 380) * 0.15
            delegate: TextField {
                readOnly: false
                selectByMouse: true
                anchors.fill: parent
                //font.family: "Calibri"
                text: styleData.value
                color: "black"
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pixelSize: parent.height * 0.35
                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.IBeamCursor
                    acceptedButtons: Qt.NoButton
                }
                background: Rectangle {
                    anchors.fill: parent
                    color: "mistyrose"
                    border.width: parent.activeFocus ? 2 : 1
                    border.color: parent.activeFocus ? "red" : "black"
                }
                onDisplayTextChanged: {
                    if (styleData.row !== -1) {
                        myListModel.setProperty(styleData.row, "distance", text)
                        motionModel.setDistance(styleData.row, text)
                    }
                }
            }
        }

        TableViewColumn {
            id: column7
            role: "stop"
            title: qsTr("Stop")
            movable: false
            resizable: false
            width: 50
            delegate: MyButton {
                myRadius: 0
                imgRes: "qrc:/resource/icons/stop.svg"
                onMyButttonLeftpressed: {
                    motionModel.stopAxis(styleData.row)
                }
            }
        }

        TableViewColumn {
            id: column8
            role: "left"
            title: qsTr("-")
            movable: false
            resizable: false
            width: 50
            delegate: MyButton {
                myRadius: 0
                imgRes: "qrc:/resource/icons/arrow-left.svg"
                onMyButttonLeftclicked: {
                    motionModel.moveRelative(styleData.row, false)
                }
                onMyButttonRightclicked: {
                    motionModel.moveAbsolute(styleData.row)
                }
            }
        }

        TableViewColumn {
            id: column9
            role: "right"
            title: qsTr("+")
            movable: false
            resizable: false
            width: 50
            delegate: MyButton {
                myRadius: 0
                imgRes: "qrc:/resource/icons/arrow-right.svg"

                onMyButttonLeftclicked: {
                    motionModel.moveRelative(styleData.row, true)
                }
                onMyButttonRightclicked: {
                    motionModel.moveAbsolute(styleData.row)
                }
            }
        }

        TableViewColumn {
            id: column10
            role: "position"
            title: qsTr("Axis Position")
            movable: false
            resizable: false
            width: (tableView.viewport.width - 380) * 0.275
                   < 140 ? 140 : (tableView.viewport.width - 380) * 0.275
            delegate: Rectangle {
                color: "gray"
                border.width: 1
                //border.color: "palevioletred"
                antialiasing: true

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignRight
                    verticalAlignment: Qt.AlignVCenter
                    //anchors.leftMargin: 50
                    //font.family: "Calibri"
                    color: "black"
                    text: styleData.value
                    font.pixelSize: parent.height * 0.35
                }
            }
        }

        TableViewColumn {
            id: column11
            role: "velocity"
            title: qsTr("Axis Velocity")
            movable: false
            resizable: false
            width: (tableView.viewport.width - 380) * 0.275
                   < 140 ? 140 : (tableView.viewport.width - 380) * 0.275
            delegate: Rectangle {
                color: "gray"
                border.width: 1
                //border.color: "palevioletred"
                antialiasing: true

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignRight
                    verticalAlignment: Qt.AlignVCenter
                    //anchors.leftMargin: 50
                    //font.family: "Calibri"
                    color: "black"
                    text: styleData.value
                    font.pixelSize: parent.height * 0.35
                }
            }
        }

        model: myListModel

        headerDelegate: Rectangle {
            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    color: "gainsboro"
                }
                GradientStop {
                    position: 1.0
                    color: "darkgrey"
                }
            }

            color: styleData.selected ? "blue" : "darkgray"
            height: 40

            border.color: "black"
            border.width: 1
            radius: 0
            Text {
                //font.family: "Calibri"
                anchors.centerIn: parent
                text: styleData.value
                font.pixelSize: parent.height * 0.35
            }
        }
        rowDelegate: Component {
            Rectangle {
                color: "wheat"
                height: 40 //(table.height-42)/tablemode.count
            }
        }

        style: TableViewStyle {
            highlightedTextColor: "#00CCFE"
            backgroundColor: "wheat"
        }
    }
}
