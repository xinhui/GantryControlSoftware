import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlLoading 1.0

Item {
    width: 1200;
    height: 800;

    property bool isComboxOk: false

    property var markPosition: [mark_x, mark_y, mark_Focus];
    property var moduleList:[position_module1, position_module2, position_module3, position_module4,
        position_module5, position_module6, position_module7, position_module8,
        position_module9, position_module10, position_module11, position_module12]
    property var featurePosition: [feature1Pos_x, feature1Pos_y, feature2Pos_x, feature2Pos_y, feature3Pos_x, feature3Pos_y]
    property var rotationAngle;


    HQmlLoading {
        id: model
        onUpdateLoadingPanelUI:{
            markPosition[0].text = markPos[0];
            markPosition[1].text = markPos[1];
            markPosition[2].text = markPos[2];

            isComboxOk = false;
            var moduleNum = model.getModuleConfigNames()
            for (var i = 0; i < moduleList.length; ++i) {
                moduleList[i].model = moduleNum
                moduleList[i].currentIndex = comboIndex[i]
            }

            for (var j = 0; j < featurePosition.length; ++j) {
                featurePosition[j].text = featurePos[j]
            }

            rotationAngle.text = rotAngle

            isComboxOk = true;
        }

    }

    Component.onCompleted: {
        model.updateLoadingPanelInfo()
    }


    GroupBox {
        id: mark
        x: 13
        y: 0
        width: 1020
        height: 80
        title: qsTr("BI01DU Mark Position")

        GridLayout {
            id: gridlayout_Mark
            anchors.fill: parent
            columns: 10
// focus on sensor
            Label {
                id: lable_SensorFocus
                text: qsTr("Mark position")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: mark_x
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                //readOnly: true
                placeholderText: qsTr("X(mark)")
            }

            TextField {
                id: mark_y
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                //readOnly: true
                placeholderText: qsTr("Y(mark)")
            }

            MyButton {
                id: locate_mark
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setPosToFindMark()
                }
            }

            MyButton {
                id: move_mark
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePosToFindMark()
                }
            }

            MyButton {
                id: recognize_mark
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.findMark();
                }
            }

            Label {
                id: lable_MarkFocus
                text: qsTr("FocusHeight(Mark)")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: mark_Focus
                Layout.preferredWidth: 92
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Z (Mark)")
            }

            MyButton {
                id: locate_MarkFocus
                Layout.preferredHeight: 35
                Layout.preferredWidth: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setFocusHeight();
                }
            }

            MyButton {
                id: move_MarkFocus
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePosToFocus();
                }
            }
            //End Gridlayout gridMark
        }
    //End Groupbox Mark
    }

    GroupBox {
        id: modulePosition
        x: 13
        y: 80
        width: 1020
        height: 310
        title: qsTr("Module Position in BI01DU")

        GridLayout{
            id: gridlayout_ModulePosition
            anchors.fill: parent
            columns: 15
            rows: 4
            // 1-4
            // module 1
            Label {
                id: label_module1
                Layout.column: 0
                Layout.row: 0
                text: qsTr("1:")
            }

            ComboBox {
                id: position_module1
                Layout.column: 1
                Layout.row: 0
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(0, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(0)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 0
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(0)
                }
            }
            MyButton {
                id: release_module1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 3
                Layout.row: 0
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module1.currentText)
                }
            }
            MyButton {
                id: dispense_module1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 0
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(0)
                }
            }
            // module 2
            Label {
                id: label_module2
                Layout.column: 0
                Layout.row: 1
                text: qsTr("2:")
            }

            ComboBox {
                id: position_module2
                Layout.column: 1
                Layout.row: 1
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(1, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(1)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 1
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(1)
                }
            }
            MyButton {
                id: release_module2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 3
                Layout.row: 1
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module2.currentText)
                }
            }
            MyButton {
                id: dispense_module2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 1
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(1)
                }
            }
            // module 3
            Label {
                id: label_module3
                Layout.column: 0
                Layout.row: 2
                text: qsTr("3:")
            }

            ComboBox {
                id: position_module3
                Layout.column: 1
                Layout.row: 2
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(2, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(2)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 2
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(2)
                }
            }
            MyButton {
                id: release_module3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 3
                Layout.row: 2
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module3.currentText)
                }
            }
            MyButton {
                id: dispense_module3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 2
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(2)
                }
            }

            // module 4
            Label {
                id: label_module4
                Layout.column: 0
                Layout.row: 3
                text: qsTr("4:")
            }

            ComboBox {
                id: position_module4
                Layout.column: 1
                Layout.row: 3
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(3, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(3)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module4
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 2
                Layout.row: 3
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(3)
                }
            }
            MyButton {
                id: release_module4
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 3
                Layout.row: 3
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module4.currentText)
                }
            }
            MyButton {
                id: dispense_module4
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 4
                Layout.row: 3
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(3)
                }
            }

            // 5-8
            // module 5
            Label {
                id: label_module5
                Layout.column: 5
                Layout.row: 0
                text: qsTr("5:")
            }

            ComboBox {
                id: position_module5
                Layout.column: 6
                Layout.row: 0
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(4, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(4)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module55
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 7
                Layout.row: 0
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(4)
                }
            }
            MyButton {
                id: release_module5
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 8
                Layout.row: 0
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module5.currentText)
                }
            }
            MyButton {
                id: dispense_module5
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 9
                Layout.row: 0
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(4)
                }
            }

            // module 6
            Label {
                id: label_module6
                Layout.column: 5
                Layout.row: 1
                text: qsTr("6:")
            }

            ComboBox {
                id: position_module6
                Layout.column: 6
                Layout.row: 1
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(5, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(5)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module6
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 7
                Layout.row: 1
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(5)
                }
            }
            MyButton {
                id: release_module6
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 8
                Layout.row: 1
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 06
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module1.currentText)
                }
            }
            MyButton {
                id: dispense_module6
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 9
                Layout.row: 1
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(5)
                }
            }

            // module 7
            Label {
                id: label_module7
                Layout.column: 5
                Layout.row: 2
                text: qsTr("7:")
            }

            ComboBox {
                id: position_module7
                Layout.column: 6
                Layout.row: 2
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(6, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(6)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module7
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 7
                Layout.row: 2
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(6)
                }
            }
            MyButton {
                id: release_module7
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 8
                Layout.row: 2
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module7.currentText)
                }
            }
            MyButton {
                id: dispense_module7
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 9
                Layout.row: 2
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(6)
                }
            }

            // module 8
            Label {
                id: label_module8
                Layout.column: 5
                Layout.row: 3
                text: qsTr("8:")
            }

            ComboBox {
                id: position_module8
                Layout.column: 6
                Layout.row: 3
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(7, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(7)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module8
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 7
                Layout.row: 3
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(7)
                }
            }
            MyButton {
                id: release_module8
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 8
                Layout.row: 3
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module8.currentText)
                }
            }
            MyButton {
                id: dispense_module8
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 9
                Layout.row: 3
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(7)
                }
            }

            // 9-12
            // module 9
            Label {
                id: label_module9
                Layout.column: 10
                Layout.row: 0
                text: qsTr("9:")
            }

            ComboBox {
                id: position_module9
                Layout.column: 11
                Layout.row: 0
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(8, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(8)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module9
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 12
                Layout.row: 0
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(8)
                }
            }
            MyButton {
                id: release_module9
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 13
                Layout.row: 0
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module9.currentText)
                }
            }
            MyButton {
                id: dispense_module9
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 14
                Layout.row: 0
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(8)
                }
            }

            // module 10
            Label {
                id: label_module10
                Layout.column: 10
                Layout.row: 1
                text: qsTr("10:")
            }

            ComboBox {
                id: position_module10
                Layout.column: 11
                Layout.row: 1
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(9, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(9)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module10
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 12
                Layout.row: 1
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(9)
                }
            }
            MyButton {
                id: release_module10
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 13
                Layout.row: 1
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module10.currentText)
                }
            }
            MyButton {
                id: dispense_module10
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 14
                Layout.row: 1
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(9)
                }
            }

            // module 11
            Label {
                id: label_module11
                Layout.column: 10
                Layout.row: 2
                text: qsTr("11:")
            }

            ComboBox {
                id: position_module11
                Layout.column: 11
                Layout.row: 2
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(10, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(10)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module11
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 12
                Layout.row: 2
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(10)
                }
            }
            MyButton {
                id: release_module11
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 13
                Layout.row: 2
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module11.currentText)
                }
            }
            MyButton {
                id: dispense_module11
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 14
                Layout.row: 2
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(10)
                }
            }

            // module 12
            Label {
                id: label_module12
                Layout.column: 10
                Layout.row: 3
                text: qsTr("12:")
            }

            ComboBox {
                id: position_module12
                Layout.column: 11
                Layout.row: 3
                Layout.preferredWidth: 170
                Layout.preferredHeight: 35
                onCurrentIndexChanged: {
                    if (isComboxOk) {
                        model.setPickModuleMode(11, currentIndex)
                    }
                }
                onHighlighted: {
                    isComboxOk = false
                    this.model = model.getModuleConfigNames()
                    currentIndex = model.getPickModuleMode(11)
                    isComboxOk = true
                }
            }
            MyButton {
                id: move_module12
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 12
                Layout.row: 3
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.moveToModule(11)
                }
            }
            MyButton {
                id: release_module12
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 13
                Layout.row: 3
                imgRes: "qrc:/resource/icons/ChevronDoubleDown.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.releaseModule(position_module12.currentText)
                }
            }
            MyButton {
                id: dispense_module12
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                Layout.column: 14
                Layout.row: 3
                imgRes: "qrc:/resource/icons/rotate.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.dispenseGlue(11)
                }
            }

            //End GridLayout gridlayout_ModulePosition
        }
        //End GroupBox modulePosition
    }


    GroupBox {
        id: calculateRotation
        x: 13
        y: 400
        width: 810
        height: 230
        title: qsTr("Calculate Chuck Rotation")

        GridLayout {
            id: gridlayout_calc
            anchors.fill: parent
            columns: 6

            //feature 1
            Label {
                id: lable_feature1
                text: qsTr("Feature1Pos")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: feature1Pos_x
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("X1")
            }

            TextField {
                id: feature1Pos_y
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Y1")
            }

            MyButton {
                id: locate_feature1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setPosToFindCalcBias(0)
                }
            }

            MyButton {
                id: move_feature1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePosToCalcBias(0)
                }
            }

            MyButton {
                id: recognize_feature1
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.findMarkToCalcBias(0)
                }
            }

            //feature 2
            Label {
                id: lable_feature2
                text: qsTr("Feature2Pos")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: feature2Pos_x
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("X2")
            }

            TextField {
                id: feature2Pos_y
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Y2")
            }

            MyButton {
                id: locate_feature2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setPosToFindCalcBias(1)
                }
            }

            MyButton {
                id: move_feature2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePosToCalcBias(1)
                }
            }

            MyButton {
                id: recognize_feature2
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.findMarkToCalcBias(1)
                }
            }

            //feature 3
            Label {
                id: lable_feature3
                text: qsTr("Feature3Pos")
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: feature3Pos_x
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("X3")
            }

            TextField {
                id: feature3Pos_y
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("Y1")
            }

            MyButton {
                id: locate_feature3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/location.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.setPosToFindCalcBias(2)
                }
            }

            MyButton {
                id: move_feature3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/aim.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.movePosToCalcBias(2)
                }
            }

            MyButton {
                id: recognize_feature3
                Layout.preferredWidth: 35
                Layout.preferredHeight: 35
                imgRes: "qrc:/resource/icons/camera.svg"
                myRadius: 0
                onMyButttonLeftclicked: {
                    model.findMarkToCalcBias(2)
                }
            }

            MyButton {
                id: button_calculate
                Layout.preferredWidth: 150
                Layout.preferredHeight: 35
                myRadius: 0
                Layout.column: 1
                Layout.row: 3
                buttonText: "calculateRot"
                onMyButttonLeftclicked: {
                    model.calculateChuckRotation()
                }
            }

            TextField {
                id: rotationAngle
                Layout.preferredWidth: 46
                Layout.preferredHeight: 35
                Layout.fillWidth: true
                selectByMouse: true
                readOnly: true
                placeholderText: qsTr("rotation")
            }

            //End Gridlayout gridMark
        }
    //End Groupbox Mark
    }


//End Item
}
