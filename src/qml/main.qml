﻿import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.15
import HQmlModelFlow 1.0
import "greater/"

ApplicationWindow {

    id: window
    width: 1200
    height: 600
    visible: true
    title: qsTr("Gantry System Controller")

    visibility: Window.Maximized

    Component.onCompleted: {

        //welcome.start()
    }

    onClosing: {
        menuBar.closeWindows()
    }

    //    HInoryPicture {
    //        id: welcome
    //        anchors.fill: parent
    //        z: 99

    //        onPlayEnd: {
    //            visible = false
    //            welcome.destroy()
    //        }
    //    }
    header: HMainMenuBar {
        id: menuBar
    }

    Rectangle {
        anchors.fill: parent
        border.width: 0
        border.color: "tan"

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 0
            spacing: 0
            HMainToolBar {
                Layout.fillWidth: true
                Layout.preferredHeight: 85
            }
            HMotionPanel {
                id: panelMotion
                Layout.fillWidth: true
                Layout.preferredHeight: 211
            }
            RowLayout {
                id: mainRLayout
                spacing: 0
                Layout.fillWidth: true
                Layout.fillHeight: true

                //                HIOPanel {
                //                    id: panelIO
                //                    Layout.preferredWidth: 600
                //                    Layout.fillHeight: true
                //                }
                ListModel {
                    id: buttonInfo

                    ListElement {
                        name: qsTr("InputOutputs")
                        img: "qrc:/resource/icons/new.svg"
                    }

                    ListElement {
                        name: qsTr("Grab And Release")
                        img: "qrc:/resource/icons/new.svg"
                    }

                    ListElement {
                        name: qsTr("Flow")
                        img: "qrc:/resource/icons/new.svg"
                    }

                    ListElement {
                        name: qsTr("Common")
                        img: "qrc:/resource/icons/new.svg"
                    }

                    //        ListElement {
                    //            name: qsTr("Common")
                    //            img: "qrc:/Resource/icons/paper-plane.svg"
                    //        }
                }

                Rectangle {
                    id: programRect
                    color: "#ffffff"
                    Layout.preferredWidth: 810
                    Layout.fillHeight: true

                    border.color: "lightcoral"
                    border.width: 5

                    Rectangle {
                        id: listRect
                        width: programRect.width * 0.25 + 0.5
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 6
                        anchors.top: parent.top
                        anchors.topMargin: 6
                        anchors.left: parent.left
                        anchors.leftMargin: 6

                        antialiasing: true
                        border.width: 1
                        border.color: "black"

                        ScrollView {
                            anchors.margins: 1
                            anchors.fill: parent
                            antialiasing: true
                            focusPolicy: Qt.WheelFocus | Qt.ClickFocus
                            clip: true
                            contentHeight: 460
                            contentWidth: listRect.width - 2

                            background: Rectangle {
                                color: "ivory"
                            }

                            ListView {
                                id: mylistView
                                anchors.bottomMargin: -408
                                anchors.rightMargin: 36
                                anchors.fill: parent
                                contentWidth: listRect.width - 2
                                contentHeight: 60
                                clip: false
                                model: buttonInfo
                                delegate: Item {
                                    width: listRect.width - 2
                                    height: 60
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: mylistView.currentIndex = index
                                    }

                                    RowLayout {
                                        id: rowLayout
                                        anchors.rightMargin: 0
                                        anchors.bottomMargin: 0
                                        anchors.fill: parent
                                        anchors.leftMargin: 5
                                        Image {
                                            id: imgh
                                            antialiasing: true
                                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                                            Layout.preferredHeight: 40
                                            Layout.preferredWidth: 40
                                            clip: false
                                            Layout.fillHeight: false
                                            Layout.fillWidth: false
                                            source: img
                                        }

                                        Text {
                                            text: name
                                            Layout.fillHeight: true
                                            Layout.fillWidth: true
                                            verticalAlignment: Text.AlignVCenter
                                            horizontalAlignment: Text.AlignLeft
                                            font.pixelSize: parent.width * 0.08
                                            //font.family: "Calibri"
                                        }
                                    }
                                }


                                highlight: Rectangle {
                                    color: "lightsteelblue"
                                    radius: 0
                                    y: mylistView.currentItem.y
                                    Behavior on y {
                                        SpringAnimation {
                                            spring: 5
                                            damping: 0.2
                                        }
                                    }
                                }
                                focus: true
                                currentIndex: 0
                            }
                        }
                    }

                    Rectangle {
                        id: listContentRect
                        width: programRect.width - listRect.width
                        color: "#ffffff"
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 5
                        anchors.left: listRect.right
                        anchors.leftMargin: 1
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.right: parent.right
                        anchors.rightMargin: 5
                        border.width: 0
                        border.color: "black"

                        StackLayout {
                            anchors.fill: parent
                            currentIndex: mylistView.currentIndex

                            HIOPanel {}
                        }
                    }
                }

                Rectangle {

                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    TabBar {
                        id: bar
                        height: 40
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.right: parent.right
                        TabButton {
                            text: qsTr("Flow")
                            onClicked: {
                                modelFlow.updateTargetInfo()
                                modelFlow.updateVCInfo()
                            }

                        }
                        TabButton {
                            text: qsTr("Digital Scope")
                        }
                        TabButton {
                            text: qsTr("Glue Controller")
                        }
                        TabButton {
                            text: qsTr("Object localization")
                        }
                        TabButton {
                            text:qsTr("Loading")
                        }
                    }

                    HQmlModelFlow{
                        id:modelFlow
                    }


                    StackLayout {
                        anchors.top: bar.bottom
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        currentIndex: bar.currentIndex

                        HPanelFlowSettings {
                            id:panelFlow
                        }
                        HDataVisual {}
                        HQmlModelGlueController {}
                        HPatternRecognition {}
                        HLoading {}
                    }
                }
            }
        }
    }
}
