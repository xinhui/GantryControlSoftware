import QtQuick 2.15
import QtCharts 2.3
import Qt.labs.platform 1.1
import QtQuick.Particles 2.12
import QtQuick.Shapes 1.15

Item {
    id: page4

    function start() {
        sys.start()
        emitter.burst(16383)
        timer2.start()
    }

    signal playEnd

    Rectangle {
        id: rectangle3
        anchors.fill: parent
        Image {
            id: image1
            anchors.fill: parent
            source: "qrc:/resource/images/inory.png"
            fillMode: Image.PreserveAspectFit
            verticalAlignment: Image.AlignBottom
            visible: false
            z: 1
        }
        Rectangle {
            id: root

            width: image1.paintedWidth
            height: image1.paintedHeight
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0

            ParticleSystem {
                id: sys
            }
            CustomParticle {
                system: sys
                property real maxWidth: root.width
                property real maxHeight: root.height
                ShaderEffectSource {
                    id: pictureSource
                    sourceItem: picture
                    hideSource: true
                    samples: 8
                }
                Image {
                    id: picture
                    //anchors.fill: parent
                    //height: width
                    source: "qrc:/resource/images/inory.png"
                    //fillMode: Image.PreserveAspectFit
                }

                ShaderEffectSource {
                    id: particleSource
                    sourceItem: particle
                    hideSource: true
                }
                Image {
                    id: particle
                    source: "qrc:/resource/images/fuzzydot.png"
                }
                //! [vertex]
                vertexShader: "
uniform highp float maxWidth;
uniform highp float maxHeight;
varying highp vec2 fTex2;
varying lowp float fFade;
uniform lowp float qt_Opacity;

void main() {
fTex2 = vec2(qt_ParticlePos.x, qt_ParticlePos.y);
//Uncomment this next line for each particle to use full texture, instead of the solid color at the center of the particle.
fTex2 = fTex2 + ((- qt_ParticleData.z / 2. + qt_ParticleData.z) * qt_ParticleTex); //Adjusts size so it's like a chunk of image.
fTex2 = fTex2 / vec2(maxWidth, maxHeight);
highp float t = (qt_Timestamp- qt_ParticleData.x) / qt_ParticleData.y;
fFade = min(t*8., (1.-t*t)*.75) * qt_Opacity;
defaultMain();
}
"
                //! [vertex]
                property variant particleTexture: particleSource
                property variant pictureTexture: pictureSource
                //! [fragment]
                fragmentShader: "
uniform sampler2D particleTexture;
uniform sampler2D pictureTexture;
varying highp vec2 qt_TexCoord0;
varying highp vec2 fTex2;
varying lowp float fFade;
void main() {
gl_FragColor = texture2D(pictureTexture, fTex2) * texture2D(particleTexture, qt_TexCoord0).w * fFade;
}"
                //! [fragment]
            }

            Emitter {
                id: emitter
                system: sys
                enabled: false
                lifeSpan: 8000
                maximumEmitted: 16383
                emitRate: 2

                //startTime:80
                anchors.fill: parent
                size: 20
                acceleration: PointDirection {
                    xVariation: 8
                    yVariation: 8
                }
            }

            Timer {
                id: timer2
                interval: 8000
                repeat: false
                onTriggered: {
                    image1.visible = true
                    sys.stop()
                    playEnd()
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    image1.visible = false
                    sys.start()
                    emitter.burst(16383)
                    timer2.start()
                }
            }
        }
    }
}
