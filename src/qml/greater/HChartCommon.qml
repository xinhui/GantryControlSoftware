import QtQuick 2.15
import QtCharts 2.15
import Qt.labs.platform 1.1
import QtQuick.Particles 2.12

Item {
    antialiasing: true

    property bool isAutoY: true
    property bool isScrolX: true

    property var sources: ["Position-X", "Position-Y", "Position-Z", "Position-T", "Force"]
    property var colors: ["red", "red", "red", "red", "red"]
    property var visibles: [false, false, false, false, false]

    function setXMin(value) {
        xAxis.min = value
    }

    function setXMax(value) {
        xAxis.max = value
    }

    function setYMin(value) {
        yAxis.min = value
    }

    function setYMax(value) {
        yAxis.max = value
    }

    function init() {
        for (var k = 0; k < sources.length; ++k) {
            var series = charView.createSeries(ChartView.SeriesTypeLine,
                                               sources[k], xAxis, yAxis)
            series.useOpenGL = false
            series.width = 1
            series.color = colors[k]
            series.visible = visibles[k]
            series.pointsVisible = true
        }
    }

    function clearAllData() {
        for (var k = 0; k < sources.length; ++k) {
            charView.series(k).clear()
        }
    }

    function setOGL(isUse) {
        for (var k = 0; k < sources.length; ++k) {
            charView.series(k).useOpenGL = isUse
        }
    }

    function setSourceVisiable(index, visiable) {
        charView.series(index).visible = visiable
    }

    function processData(source, vx, vy, isScroll) {
        charView.series(source).append(vx, vy)
        if (isScrolX && isScroll) {
            var value = (vx - xAxis.max) / (xAxis.max - xAxis.min)
            if (value > 0) {
                charView.scrollRight(value * charView.plotArea.width)
            }
        }
        if (isAutoY) {
            if (vy > yAxis.max) {
                yAxis.max = vy
            } else if (vy < yAxis.min) {
                yAxis.min = vy
            }
        }

        //        if (vy > charView.series(source).axisY.max - 1) {
        //            charView.scrollUp(
        //                        (vy - (charView.series(source).axisY.max - 1)) * charView.plotArea.height
        //                        / charView.series(source).axisY.tickCount / 4)
        //        }
        //        if (vy < charView.series(source).axisY.min + 1) {
        //            charView.scrollDown(
        //                        (-vy + charView.series(source).axisY.min + 1) * charView.plotArea.height
        //                        / charView.series(source).axisY.tickCount / 4)
        //        }
    }

    ChartView {
        id: charView
        anchors.fill: parent
        z: 1
        plotAreaColor: "honeydew"
        backgroundColor: "#65c8a7"
        backgroundRoundness: 0.5
        dropShadowEnabled: true
        antialiasing: true
        animationOptions: ChartView.SeriesAnimations

        ValueAxis {
            id: xAxis
            min: 0
            max: 10000
            labelFormat: "%d"
            //tickInterval: 1000
            //tickType: ValueAxis.TicksDynamic
            tickCount: ((max - min) / 1000 + 1) > 11 ? 11 : ((max - min) / 1000 + 1)
            titleText: qsTr("time(ms)")
        }

        ValueAxis {
            id: yAxis
            labelFormat: "%.2f"
            min: -10
            max: 10
        }

        //plotArea: Qt.rect(x, y, height, height)
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            z: 2

            onClicked: {
                if (mouse.button === Qt.RightButton)
                    contextMenu.open()
            }
            onPressAndHold: {
                if (mouse.source === Qt.MouseEventNotSynthesized)
                    contextMenu.open()
            }

            Menu {
                id: contextMenu
                MenuItem {
                    text: qsTr("Clear dispaly")
                    onTriggered: {

                    }
                }

                MenuItem {
                    text: qsTr("Show maximized")
                    property bool isBig: false
                    onTriggered: {

                        //                        if (!isBig) {

                        //                            text = qsTr("Show normal")
                        //                            isBig = true
                        //                        } else {
                        //                            line.anchors.fill = page3
                        //                            text = qsTr("Show maximized")
                        //                            isBig = false
                        //                        }
                    }
                }
            }
        }
    }
}
