﻿import QtQuick 2.15
import QtCharts 2.15
import Qt.labs.platform 1.1
import QtQuick.Particles 2.12

Item {
    antialiasing: true
    Timer {
        id: timer
        interval: 2
        running: true
        repeat: true
        property int num: 0
        property double km: 3

        function processData(lines, chartview, linex, liney, pointc, vx, vy) {
            pointc.clear()
            pointc.append(vx, vy)
            lines.append(vx, vy)
            if (vx > linex.max - 1) {
                chartview.scrollRight(
                            (vx - (linex.max - 1)) * chartview.plotArea.width
                            / lineSeries.axisX.tickCount / 4)
            }
            if (vx < linex.min + 1) {
                chartview.scrollLeft(
                            (-vx + linex.min + 1) * chartview.plotArea.width
                            / lineSeries.axisX.tickCount / 4)
            }
            if (vy > liney.max - 1) {
                chartview.scrollUp(
                            (vy - (liney.max - 1)) * chartview.plotArea.height
                            / lineSeries.axisY.tickCount / 4)
            }
            if (vy < liney.min + 1) {
                chartview.scrollDown(
                            (-vy + liney.min + 1) * chartview.plotArea.height
                            / lineSeries.axisY.tickCount / 4)
            }
        }

        onTriggered: {
            //console.log(count,Math.random())
            if (num > 10000000) {
                lineSeries.clear()
                num = 0
            }
            var x = km * (2 * Math.sin(num / 20) - Math.sin(2 * num / 20))
            var yk = km * (2 * Math.cos(num / 20) - Math.cos(2 * num / 20))
            var y = yk - (3 - km)

            if (x > -0.1 && x < 0.1 && yk > km - 0.1 && yk < km + 0.1) {
                km -= 0.01
            }

            //                    if(km<-3){
            //                        stop()
            //                    }

            //if (Math.abs(point.at(0).x - x) > 0.001 || Math.abs(
            //       point.at(0).y - y) > 0.001) {
            if (km > 0)
                processData(lineSeries, line, line1x, line1y, point, x, y)
            else {
                //processData(lineSeries1,line,line1x1,line1y1,point1,x,y)
                stop()
            }
            num++
            // }
        }
    }

    ChartView {
        id: line
        anchors.fill: parent
        z: 1
        plotAreaColor: "honeydew"
        backgroundColor: "#65c8a7"
        backgroundRoundness: 0.5
        dropShadowEnabled: true
        antialiasing: true

        //plotArea: Qt.rect(x, y, height, height)
        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            z: 2

            onClicked: {
                if (mouse.button === Qt.RightButton)
                    contextMenu.open()
            }
            onPressAndHold: {
                if (mouse.source === Qt.MouseEventNotSynthesized)
                    contextMenu.open()
            }

            Menu {
                id: contextMenu
                MenuItem {
                    text: qsTr("Clear dispaly")
                    onTriggered: {
                        lineSeries.clear()
                        timer.num = 0
                        timer.km = 3
                        point.clear()
                        timer.start()
                    }
                }

                MenuItem {
                    text: qsTr("Show maximized")
                    property bool isBig: false
                    onTriggered: {

                        //                        if (!isBig) {

                        //                            text = qsTr("Show normal")
                        //                            isBig = true
                        //                        } else {
                        //                            line.anchors.fill = page3
                        //                            text = qsTr("Show maximized")
                        //                            isBig = false
                        //                        }
                    }
                }
            }
        }

        SplineSeries {
            name: qsTr("")
            id: lineSeries
            pointLabelsVisible: false
            axisX: ValueAxis {
                id: line1x
                min: -20
                max: 20
            }
            axisY: ValueAxis {
                id: line1y
                min: -10
                max: 10
            }
            useOpenGL: true
            color: /*"royalblue"*/ "#dc7aa9"
            width: 1
        }

        ScatterSeries {
            id: point
            name: qsTr("latest point")
            markerSize: 10
            pointLabelsVisible: false
            XYPoint {
                x: 0
                y: 0
            }
            useOpenGL: true
        }
    }
}
