﻿#include "serialport.h"

#include <QSerialPort>
#include <QEventLoop>
#include <QTime>
#include <QCoreApplication>
#include <QDebug>


HSerialport::HSerialport(QObject *parent) :
    QThread(parent)
{
    connect(this, &HSerialport::response, this, [ =, this ](const QString & s) {
        m_returnStr = s;
        m_returnCode = 0;

    });
    connect(this, &HSerialport::error, this, [ =, this ](const QString & s) {
        m_returnStr = s;
        m_returnCode = -1;
    });
    connect(this, &HSerialport::timeout, this, [ =, this ](const QString & s) {
        m_returnStr = s;
        m_returnCode = -1;
    });
}

HSerialport::~HSerialport()
{

}

void HSerialport::unInit()
{
    m_quit = true;
    if (!isRunning())
        start();
    else
        m_cond.wakeOne();
}

void HSerialport::setPortName(const QString &portName, int waitTime)
{
    if (portName == m_portName && waitTime == m_waitTimeout) {
        return;
    }
    const QMutexLocker locker(&m_mutex);
    m_portName = portName;
    m_waitTimeout = waitTime;
}

QString HSerialport::requestCommond(const QString &commond, bool wait)
{
    m_returnCode = 1;
    transaction(commond);
    if (wait) {
        while (m_returnCode == 1) {
            QTime _Timer = QTime::currentTime().addMSecs(1000);
            while (QTime::currentTime() < _Timer)
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        }

        if (m_returnCode == 0) {
            //qDebug() << commond << m_returnStr;
            return m_returnStr;
        } else {
            //qDebug() << commond << m_returnStr;
            return nullptr;
        }

    } else {
        //qDebug() << commond << m_returnStr;
        return nullptr;
    }

}

void HSerialport::transaction(const QString &request)
{
    const QMutexLocker locker(&m_mutex);

    m_request = request;

    if (!isRunning())
        start();
    else
        m_cond.wakeOne();
}

void HSerialport::run()
{
    bool currentPortNameChanged = false;

    m_mutex.lock();
    QString currentPortName;
    if (currentPortName != m_portName) {
        currentPortName = m_portName;
        currentPortNameChanged = true;
    }

    int currentWaitTimeout = m_waitTimeout;
    QString currentRequest = m_request;
    m_mutex.unlock();
    QSerialPort serial;

    if (currentPortName.isEmpty()) {
        emit error(tr("No port name specified"));
        return;
    }

    while (!m_quit) {
        if (currentPortNameChanged) {
            serial.close();
            serial.setPortName(currentPortName);
            serial.setBaudRate(921600);

            if (!serial.open(QIODevice::ReadWrite)) {
                emit error(tr("Can't open %1, error code %2")
                           .arg(m_portName).arg(serial.error()));
                return;
            }
        }
        const QByteArray requestData = currentRequest.toUtf8();
        serial.write(requestData);
        if (serial.waitForBytesWritten(m_waitTimeout)) {

            if (serial.waitForReadyRead(currentWaitTimeout)) {
                QByteArray responseData = serial.readAll();
                while (serial.waitForReadyRead(10) || responseData.right(2) != "\r\n")
                    responseData += serial.readAll();


                const QString response = QString::fromUtf8(responseData);
                emit this->response(response);
            } else {
                emit timeout(tr("Wait read response timeout %1")
                             .arg(QTime::currentTime().toString()));
            }
        } else {
            emit timeout(tr("Wait write request timeout %1")
                         .arg(QTime::currentTime().toString()));
        }
        m_mutex.lock();
        m_cond.wait(&m_mutex);
        if (currentPortName != m_portName) {
            currentPortName = m_portName;
            currentPortNameChanged = true;
        } else {
            currentPortNameChanged = false;
        }
        currentWaitTimeout = m_waitTimeout;
        currentRequest = m_request;
        m_mutex.unlock();
    }
}
