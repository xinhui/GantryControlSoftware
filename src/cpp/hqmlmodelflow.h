#ifndef HQMLMODELFLOW_H
#define HQMLMODELFLOW_H

#include "hqmlmodelsettingcoord.h"
#include <QThread>
#include <QDataStream>
#include <QString>
#include "hqmlPatternRecognition.h"

class HQmlModelFlow : public QThread
{
    Q_OBJECT
public:
    explicit HQmlModelFlow(QObject *parent = nullptr);
    ~HQmlModelFlow();



    bool runFlow();
    void stopFlow();

    static HQmlModelFlow *flowPtr;
signals:

    void updateUI(QStringList _strTooling, QStringList _strTarget, QStringList _strVC, QStringList _strGrabs, QList<int> combxIndexs, QVector<quint16> alins, QStringList accVels);
    void updateTargetUI(QStringList _strTarget);
    void updateVCUI(QStringList _strVC);
    void receiveSignalForGluingOffset(MPointXYZT _glueDotPos);

    bool callCamera(QStringList &result, quint16 fff);
public slots:

    QStringList getCaptureNames();

    void setAccVels(int index, QString str);
    void setToolingPos(int _index, QString _axis, QString _str);
    void setTargetPos(int _index, QString _axis, QString _str);
    void setVCPos(int _index, QString _axis, QString _str);
    void setGrab(int index, int value);
    void setCameraAli(int index, quint16 value);

    int getGrab(int index);

    bool movePos(int _index, QString _type);
    bool moveZ(QString zFocus);
    bool moveZ(double zFocus);
    bool moveXY(QPointF xyPoint);
    bool moveXY(QString x, QString y);
    bool homeT();
    bool rotateT(int _index);
    bool rotateT(QString _angle);
    bool rotateClockwise(int _index);
    bool rotateCounterClockwise(int _index);
    void getCurrentPos(int _index, QString _type);
    void getCurrentXY(int _index, QString _type);
    void getCurrentZ(int _index, QString _type);

    void getPickingPos(int _index, QString _type, QString _axis);

    Q_INVOKABLE void updateCtrl();
    void updateTargetInfo();
    void updateVCInfo();
    void updateGluingOffsetInfo();

    bool teeeees(QStringList &result, quint16 fff);

    void pickTooling(const QString &PickReleaseMode);
    void releaseTooling(const QString &PickReleaseMode);
    QPointF getGlueDotPos();

protected:
    bool loadConfig();
    bool saveConfig();
    friend QDataStream &operator>>(QDataStream &input, HQmlModelFlow &obj);
    friend QDataStream &operator<<(QDataStream &output, const HQmlModelFlow &obj);

private:

    bool moveT(const double &_angle, bool isWait = false);
    bool moveToZ(const double &pos, bool isWait = false);
    bool moveToT(const double &_angle, bool isWait = false);
    bool moveToXY(const QPointF &pos, bool isWait = false);
    bool moveToXYZ(const MPointXYZT &pos, bool isWait = false);
    bool moveToXYZT(const MPointXYZT &pos, bool isWait = false);
    bool moveToZXYT(const MPointXYZT &pos, bool isWait = false);
    bool getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali, bool isresss);

    QVector<double> m_accVels{1000, 60, 1000, 60, 1000, 20, 1000, 10};

    QVector<MPointXYZT> m_ToolingPos;
    QVector<MPointXYZT> m_TargetPos;
    QVector<MPointXYZT> m_VCPos;
    MPointXYZT m_FlexOnSensorPos;
    QVector<QString> m_grabs;
    QVector<QString> m_patterns;
    QVector<quint16> m_cameraAlis;

public:
// location & bias paremeters
    double gapDistance = 0.15;
    double pickFlexBiasX = 0.0544;
    double pickFlexBiasY = -0.0473;

    double pickModuleBiasX = 0.;
    double pickModuleBiasY = 0.;

    double biasX_Sensor1FlexCentre = 0;
    double biasY_Sensor1FlexCentre = -0.4;
    double disX_Sensor1FlexCentre = 7.9150 + 2.29;
    double disY_Sensor1FlexCentre = 5.0592 - 0.22;

    double disX_Sensor1CentreFlexHV1 = 7.9150 -0.5-0.7719+1.0998 -2;
    double disY_Sensor1CentreFlexHV1 = 5.0592 + 5.85 -0.5 -2.5 -5.9342+4.1946;

    HMotionController *m_motion;
};

#endif // HQMLMODELFLOW_H
