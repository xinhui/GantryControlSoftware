#include "QLogWorker.h"
#include "QWriteElement.h"
#include <QStandardPaths>
#include <QDir>
#include <QCoreApplication>
#include <QtMath>

#define MAXSIZE_LOG 100 * 1024 * 1024               // 100M
#define INTERVAL_CHANGEDLOG 12 * 60 * 60            // change log file per 12 hours
#define TIME_LOGSAVE   7 * 24 * 60 * 60             // log saving time (7 days)

#define FORMAT_TIME_FILENAME    "yyyyMMddhhmmss"

QLogWorker::QLogWorker(QObject *parent) :
    QObject(parent),
    m_logPath(QString()),
    m_eFlags(QLog::eLogFlags()),
    m_element(new QWriteElement()),
    m_pLogTimer(new QTimer(this)),
    m_maxSize(MAXSIZE_LOG),
    m_creatInterval(INTERVAL_CHANGEDLOG),
    m_saveTime(TIME_LOGSAVE)
{

#if _DEBUG
    SetLogFlags(QLog::eLogFlags(QLog::eLog_Info | QLog::eLog_Critical | QLog::eLog_Fatal | QLog::eLog_Debug));
#else
    SetLogFlags(QLog::eLogFlags(QLog::eLog_Info | QLog::eLog_Critical | QLog::eLog_Fatal));
#endif
    //SetLogPath(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator() + "Log");
    SetLogPath(QCoreApplication::applicationDirPath() + QDir::separator() + "Log");

    m_pLogTimer->setInterval(5*1000);
    connect(m_pLogTimer, &QTimer::timeout, this, &QLogWorker::OnLogTimeout);
}

QLogWorker::~QLogWorker()
{
    m_element->Close();
    delete m_element;
    m_element = nullptr;
}

void QLogWorker::SetLogFlags(QLog::eLogFlags flags)
{
    m_eFlags = flags;
}

void QLogWorker::SetLogPath(const QString &path)
{
    if (m_logPath != path || IsLogFileNameNeedChanged())
    {
        m_logTime = QDateTime::currentDateTime();
        m_logPath = path;

        if (!m_pLogTimer->isActive())
        {
            m_pLogTimer->start();
        }
        m_element->SetFileName(MakeFileName());
    }
}

void QLogWorker::SetLogFileMaxSize(qint64 size)
{
    m_maxSize = size;
}

void QLogWorker::SetLogFileCreatInterval(qint64 interval)
{
    m_creatInterval = interval;
}

void QLogWorker::SetLogFileSaveTime(qint64 time)
{
    m_saveTime = time;
}

void QLogWorker::WriteLog(QLog::eLogFlag flag, const QString &message)
{
    if (m_eFlags &flag)
    {
        m_element->WriteMessage(message);
    }
}

QList<QString> QLogWorker::GetLogList()
{
    QList<QString> filePathList;

    QString dirPath = GetLogDir();
    QDir dir(dirPath);
    QFileInfoList fileList = dir.entryInfoList();
    for (const QFileInfo &fileinfo : qAsConst(fileList))
    {
        if(fileinfo.isFile())
            filePathList.append(fileinfo.fileName());
    }

    return filePathList;
}

QString QLogWorker::GetLogDir()
{
    return m_logPath;
}

void QLogWorker::OnLogTimeout()
{
    if (!QFile(MakeFileName()).exists())
    {
        // set new log path
        SetLogPath(m_logPath);
        // clear logs
        ClearLogs();
    }
}

bool QLogWorker::IsLogFileNameNeedChanged() const
{
    qint64 fileIndex = m_logTime.toMSecsSinceEpoch() / (qreal)(m_creatInterval * 1000 * 2) + 0.5;
    qint64 currentIndex = QDateTime::currentMSecsSinceEpoch() / (qreal)(m_creatInterval * 1000 * 2) + 0.5;
    bool timeFlag = currentIndex > fileIndex;
    qint64 maxSize = 0;
    maxSize = qMax(m_element->GetLogSize(), maxSize);

    bool sizeFlag = maxSize > m_maxSize;
    return timeFlag || sizeFlag;
}

void QLogWorker::ClearLogs()
{
    ClearLogsByPath(m_logPath);
}

void QLogWorker::ClearLogsByPath(const QString &path)
{
    QDir dir(path);
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    dir.setSorting(QDir::Size | QDir::Reversed);

    QFileInfoList list = dir.entryInfoList();
    for (int i = 0; i < list.size(); ++i)
    {
        QFileInfo fileInfo = list.at(i);
        QString fileName = fileInfo.fileName();
        QDateTime logTime = QDateTime::fromString(fileName.left(fileName.indexOf(".txt")), FORMAT_TIME_FILENAME);
        if (!logTime.isValid())
        {
            dir.remove(fileName);
        }
        else
        {
            if (QDateTime::currentMSecsSinceEpoch() - logTime.toMSecsSinceEpoch() > m_saveTime * 1000)
            {
                dir.remove(fileName);
            }
        }
    }
}

QString QLogWorker::MakeFileName()
{
    return m_logPath + QDir::separator() + m_logTime.toString(FORMAT_TIME_FILENAME) + "_log.txt";
}

void QLogWorker::AddWriteElement(QLog::eLogFlag flag)
{
    m_eFlags = m_eFlags | flag;
}

void QLogWorker::RemoveWriteElement(QLog::eLogFlag flag)
{
    m_eFlags = m_eFlags & ~flag;
}
