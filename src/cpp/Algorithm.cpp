#include "Algorithm.h"
#include <iostream>
#include <cmath>

#define PI acos(-1)


Sensor::Sensor(){

    if(points.size()!=4) points.resize(4);
    if(searchPoints.size()!=4) searchPoints.resize(4);

    for(int i = 0; i < 4; i++){
        points[i].setX(0.);
        points[i].setY(0.);
        searchPoints[i].setX(0.);
        searchPoints[i].setY(0.);
    }
}

QPointF Sensor::centre(){
    QPointF centre;
    double Xc = 0.;
    double Yc = 0.;
    for(int i = 0; i < points.size(); i++){
        Xc += points.at(i).x()/4.;
        Yc += points.at(i).y()/4.;
    }

    centre.setX(Xc);
    centre.setY(Yc);

    return centre;
}

double Sensor::rotAngle(){
    double angleX1, angleX2, angleY1, angleY2, rotAngle;
    if(points.at(1).x()-points.at(0).x()==0|| points.at(3).x()-points.at(2).x()==0 ||
       points.at(2).x()-points.at(0).x()==0 || points.at(3).x()-points.at(1).x()==0){
        rotAngle = 0.;
    }
    else{
        angleX1 = atan((points.at(1).y()-points.at(0).y())/(points.at(1).x()-points.at(0).x()));
        angleX2 = atan((points.at(3).y()-points.at(2).y())/(points.at(3).x()-points.at(2).x()));
        angleY1 = atan((points.at(0).x()-points.at(2).x())/(points.at(0).y()-points.at(2).y()));
        angleY2 = atan((points.at(1).x()-points.at(3).x())/(points.at(1).y()-points.at(3).y()));
        rotAngle = (180./PI)*(angleX1+angleX2-angleY1-angleY2)/4.;

        /*
        std::cout<<"P1: "<<points.at(0).x() <<", "<<points.at(0).y()<<std::endl;
        std::cout<<"P2: "<<points.at(1).x() <<", "<<points.at(1).y()<<std::endl;
        std::cout<<"P3: "<<points.at(2).x() <<", "<<points.at(2).y()<<std::endl;
        std::cout<<"P4: "<<points.at(3).x() <<", "<<points.at(3).y()<<std::endl;
        std::cout<<"aX1: "<<angleX1 <<", aX2: "<<angleX2<<", aY1: "<<angleY1<<", aY2: "<<angleY2<<std::endl;
        std::cout<<"================================"<<std::endl;
         */
    }

    return rotAngle;
}

double Sensor::width(){
    double widthTop, widthBottom;
    widthTop = sqrt(pow(points.at(1).x()-points.at(0).x(), 2) + pow(points.at(1).y()-points.at(0).y(), 2));
    widthBottom = sqrt(pow(points.at(3).x()-points.at(2).x(), 2) + pow(points.at(3).y()-points.at(2).y(), 2));
    if(widthTop < widthBottom){
        return widthBottom;
    }
    else{
        return widthTop;
    }
}

double Sensor::height(){
    double heightLeft, heightRight;
    heightLeft = sqrt(pow(points.at(2).x()-points.at(0).x(), 2) + pow(points.at(2).y()-points.at(0).y(), 2));
    heightRight = sqrt(pow(points.at(3).x()-points.at(1).x(), 2) + pow(points.at(3).y()-points.at(1).y(), 2));
    if(heightLeft < heightRight){
        return heightRight;
    }
    else{
        return heightLeft;
    }
}

QDataStream &operator>>(QDataStream &input, Sensor &obj)
{
    return input >> obj.points >> obj.searchPoints >> obj.focusHeight;
}

QDataStream &operator<<(QDataStream &output, const Sensor &obj)
{
    return output << obj.points << obj.searchPoints << obj.focusHeight;
}

// ===========================================================
// ===========================================================

Flex::Flex(){
    if(highVolt.size()!=2) highVolt.resize(2);
    if(searchPoints.size()!=2) searchPoints.resize(2);
    for(int i = 0; i < 2; i++){
        highVolt[i].setX(0.);
        highVolt[i].setY(0.);
        searchPoints[i].setX(0.);
        searchPoints[i].setY(0.);
    }
}

QPointF Flex::markHV1(){
    QPointF markHV1;
    double X1 = highVolt.at(0).x();
    double Y1 = highVolt.at(0).y();

    markHV1.setX(X1);
    markHV1.setY(Y1);
    return markHV1;
}

double Flex::rotAngle(){
    double rotAngle;
    if(highVolt.at(1).x()-highVolt.at(0).x() == 0){
        rotAngle = 0;
    }
    else{
        rotAngle = (180./PI)*atan((highVolt.at(1).y()-highVolt.at(0).y())/(highVolt.at(1).x()-highVolt.at(0).x()));
    }
    
    return rotAngle;
}

QDataStream &operator>>(QDataStream &input, Flex &obj)
{
    return input >> obj.highVolt >> obj.searchPoints >>obj.focusHeight;
}

QDataStream &operator<<(QDataStream &output, const Flex &obj)
{
    return output << obj.highVolt << obj.searchPoints << obj.focusHeight;
}


cv::Mat diskStrel(int radius){

    int n = 2 * floor((double)radius * (1.-sqrt(1./2.)));

    cv::Mat sel(2*radius - 1, 2*radius - 1, CV_8UC1, cv::Scalar(255));

    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            sel.at<uchar>(i, j) = 0;
            sel.at<uchar>(i, sel.cols - 1 - j) = 0;
            sel.at<uchar>(sel.rows - 1 - i, j) = 0;
            sel.at<uchar>(sel.rows - 1 - i, sel.cols - 1 - j) = 0;
        }
    }
    return sel;
}

