﻿#ifndef HQMLMODELIO_H
#define HQMLMODELIO_H

#include <QObject>
#include <QDataStream>

class HMotionController;

class HQmlModelIO : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelIO(QObject *parent = nullptr);
    ~HQmlModelIO();

    static HQmlModelIO * ioPtr;

signals:

    void updateIOs(const QList<int> &inputs, const QList<int> &outputs);

public slots:

    void startUpdateIOs(bool isStart);
    bool setOutput(int index, int value);
    void zeroForceSensor();

    QString getForceValue();

    QStringList getInputNames() {return m_inputNames.toList();}
    QStringList getOutputNames() {return m_outputNames.toList();}
    void setInputName(int index, const QString &name);
    void setOutputName(int index, const QString &name);

protected:
    friend bool operator>>(QDataStream &input, HQmlModelIO &obj);

    friend bool operator<<(QDataStream &output, const HQmlModelIO &obj);

private:

    bool loadConfig();
    bool saveConfig();

    double m_forceOffset=0.0;


    QTimer *m_timer;
    HMotionController *m_motion;

    QVector<QString> m_inputNames;
    QVector<QString> m_outputNames;


};

#endif // HQMLMODELIO_H
