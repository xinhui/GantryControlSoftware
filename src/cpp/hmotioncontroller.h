#ifndef HMOTIONCONTROLLER_H
#define HMOTIONCONTROLLER_H

#include <QString>
#include <QMap>
#include <QPair>
#include <QThread>
#include <QReadWriteLock>

enum HAxisDefine {
    Axis_X = 1,
    Axis_Y = 0,
    Axis_Z = 4,
    Axis_T = 5,
};

static const int HAxises[] = {Axis_X, Axis_Y, Axis_Z, Axis_T};

class HMotionController
{
public:
    enum AxisState {
        NotConnected = -1,
        Enable,
        Error,
        Disable
    };
    static HMotionController *GetInstance();
    static void Release();

    void addAxisAccVel(int axis, double acc, double vel);

    bool openController();
    void startForceThread();
    void closeController();
    AxisState getAxisState(int axis);
    double getAxisPosition(int axis);
    double getAxisVelocity(int axis);
    double getAnalogInput(int port);
    bool getForceValue(double *value);
    double getForceValue();
    void setForceOffset(double offset);
    void setFilterWidth(double width);
    int getDigitalInput(int unit, int port);
    int getDigitalOutput(int unit, int port);

    bool ExecuteCommond(int index, const QString &commond);
    bool RunProgram(int index, const QString &program);
    bool RunProgramWait(int index, const QString &program);
    bool RunProgramFile(int index, const QString &programFile);
    bool RunProgramFileWait(int index, const QString &programFile);
    bool StopProgramWait(int index);
    //-2,error;-1,running;0,done;others,error line
    int GetProgramState(int index);


    bool enableAxis(int axis, bool isEnable = true);
    bool homeAxis(int axis);
    bool moveAbs(int axis, double pos, double vel, double acc = 1000, bool isWait = false);
    bool moveAbs(int axis, double pos, bool isWait = false);
    bool moveAbs1(int axis, double pos, bool isWait = false);
    bool moveRel(int axis, double step, double vel, double acc = 1000, bool isWait = false);
    bool stopAxis(int axis);
    bool killAxis(int axis);
    bool isAxisMove(int axis);
    bool waitAxisStop(int axis, int timeOut = 30000);
    bool waitAllAxisStop(int timeOut = 30000);
    bool setDigitalOutput(int unit, int port, int val);

private:
    QThread *gthread;
    QReadWriteLock lock;
    double m_currentForce = 0;
    double m_filterBandWidth = 2;
    void averageFilter(double &value);

    void waitProgramComplete(int index);
    int getErrorLine(int index);

    short m_bufferMode = 0;;
    double m_forceOffset = 0.0;
    QMap<int, QPair<double, double>> m_defaultAccVel;

private:
    HMotionController();
    HMotionController(const HMotionController &signal) = delete;
    ~HMotionController();
    const HMotionController &operator=(const HMotionController &signal) = delete;
};

#endif // HMOTIONCONTROLLER_H
