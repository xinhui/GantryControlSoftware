﻿#ifndef HQMLMODELCAPTURE_H
#define HQMLMODELCAPTURE_H

#include <QObject>
#include <QDataStream>
#include <stdio.h>
#include <QTextStream>
#include <iostream>
class HQmlModelCapture : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelCapture(QObject *parent = nullptr);
    ~HQmlModelCapture();

    static HQmlModelCapture *capturePtr;

    const QStringList &getConfigNames() {return m_configNames;}
    QString getCurrent(){return m_currentConfig;}


signals:
    void updateControlData(int currentCfg, QStringList cfgNames, QStringList cfgDatas, int getter, int setter);

public slots:
    QStringList getOutputs();

    void addConfig(QString name);
    void removeConfig(int index);
    void setCurrentConfig(int index);
    void setConfigData(int index, const QString &value);

    void updateUI();

    bool getFixture(const QString &name);
    bool getFixture(int index);
    bool letFixture(const QString &name);
    bool letFixture(int index);


protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlModelCapture &obj);
    friend bool operator<<(QDataStream &output, const HQmlModelCapture &obj);

private:



private:
    QStringList m_configNames;
    QString m_currentConfig;

    QString m_defaultGetter, m_defaultLetter;


    struct ConfigVars {
        ConfigVars() {}
        int typeIndex = 1;
        int outputIndex_Gantry = 31;
        int outputIndex_VC = 27;
        double waitPos = 70.0;
        double fastSpeed = 15.;
        double fastAcc = 1000.;
        double srhPos1 = 10.0;
        double srhPos2 = 0.0;
        double srhSpeed = 0.01;
        double srhAcc = 100.;
        double srhForce = 10.;

        QStringList getStrs()
        {
            return QStringList{QString::number(typeIndex), QString::number(outputIndex_Gantry), 
                               QString::number(outputIndex_VC), QString::number(waitPos, 'f', 4),
                               QString::number(fastSpeed, 'f', 4), QString::number(fastAcc, 'f', 4),
                               QString::number(srhPos1, 'f', 4), QString::number(srhPos2, 'f', 4), 
                               QString::number(srhSpeed, 'f', 4), QString::number(srhAcc, 'f', 4), 
                               QString::number(srhForce, 'f', 4)};
        }
        void setValue(int index, double value)
        {
            std::cout<< "========================================================"<<std::endl;
            if (index > 2 && index < 11) {
                double *p = (double *)this;
                p[index-1] = value;
                std::cout<<"index: "<<index<<", this: "<<waitPos<<std::endl;
            }
            else if(index == 0){
                typeIndex = int(value);
                std::cout << "type index: "<< index << ", waitPos: "<<typeIndex<<std::endl;
            }
            else if(index == 1) {
                outputIndex_Gantry = value + 0.5;
                std::cout << "output(Gantry): "<< index << ", output(Gantry): "<<outputIndex_Gantry<<std::endl;
            }
            else if(index == 2){
                outputIndex_VC = value + 0.5;
                std::cout << "output(VC): "<< index << ", output(VC): "<<outputIndex_VC<<std::endl;
            }
        }

        friend bool operator>>(QDataStream &input, ConfigVars &obj)
        {
            input >> obj.typeIndex >> obj.outputIndex_Gantry >> obj.outputIndex_VC >> obj.waitPos >> obj.fastSpeed >> obj.fastAcc >> obj.srhPos1
                  >> obj.srhPos2 >> obj.srhSpeed >> obj.srhAcc >> obj.srhForce;
            return true;
        }

        friend bool operator<<(QDataStream &output, const ConfigVars &obj)
        {
            output << obj.typeIndex << obj.outputIndex_Gantry << obj.outputIndex_VC << obj.waitPos << obj.fastSpeed << obj.fastAcc << obj.srhPos1
                  << obj.srhPos2 << obj.srhSpeed << obj.srhAcc << obj.srhForce;
            return true;
        }
    };

    QVector<ConfigVars> m_configDatas;

};

#endif // HQMLMODELCAPTURE_H
