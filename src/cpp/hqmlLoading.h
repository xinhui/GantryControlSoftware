#ifndef HQMLLOADING_H
#define HQMLLOADING_H

#include <QThread>
#include <QDataStream>
#include <QString>

#include "hqmlmodelflow.h"
#include "hqmlLoadingCoordSetting.h"
#include "hqmlLoadingDispensingSetting.h"

class HQmlLoading : public QThread{
    Q_OBJECT
public:
    explicit HQmlLoading(QObject *parent = nullptr);
    ~HQmlLoading();

    static HQmlLoading *loadingPtr;

signals:

    void updateLoadingPanelUI(QStringList markPos, QStringList moduleModeList, QList<int> comboIndex, QStringList featurePos, QStringList rotAngle);

    void receiveSignalForModuleCentre(MPointXYZT _moduleCentre);

public slots:
    // Find mark
    void setPosToFindMark();
    bool movePosToFindMark();
    void setFocusHeight();
    bool movePosToFocus();
    void findMark();

    // Get module
    QStringList getModuleConfigNames();
    void setPickModuleMode(int _index, int _value);
    int getPickModuleMode(int _index);

    bool moveToModule(int _index);
    void releaseModule(const QString &_moduleNum);
    void dispenseGlue(int _moduleNum);

    // Calculate location of the VC
    void setPosToFindCalcBias(int _index);
    bool movePosToCalcBias(int _index);
    void findMarkToCalcBias(int _index);
    void calculateChuckRotation();

    void updateLoadingPanelInfo();

protected:
    bool loadConfig();
    bool saveConfig();
    friend QDataStream &operator>>(QDataStream &input, HQmlLoading &obj);
    friend QDataStream &operator<<(QDataStream &output, const HQmlLoading &obj);

private:
    QVector<QString> m_moduleList;
    QVector<QString> m_measureMarkList;
    QPointF m_toFindMark = QPointF(0., 0.);

    HMotionController *m_motion;
    HQmlModelFlow *m_modelflow;


public:
    double x_mark = 0.;
    double y_mark = 0.;
    double z_focus = 0.;
    double m_chuckRotationAngle = 0.;

    QPointF markPos;
    QVector<QPointF> m_toFindMarkToCalcRot, m_markToCalcRot;

    double x_biasToGlue = 1.0-2.5;
    double y_biasToGlue = -1.8-0.5-1;


};


#endif // HQMLLOADING_H
