#ifndef IMAGELOADING_H
#define IMAGELOADING_H

#include<stdio.h>
#include<iostream>

#include<opencv2/opencv.hpp>

typedef std::vector<std::string> ImageFiles;

namespace ImageUtils {

    enum class Type{
        Focus,
        Chessboard,
        Glass,
        ASIC,
        Sensor,
        Hybrid,
        Flex,
        Gap,
        GlueDot,
        Pad,
        Warning,
    };

    static const char *type_str[] = {
        "AutoFocus",
        "Chessboard",
        "Glass",
        "ASIC",
        "Sensor",
        "Hybrid",
        "Flex",
        "Gap",
        "GlueDot",
        "Pad",
        "Warning"
    };

    enum class Num{
        Single,
        Multi,
        AllImages,
    };

    enum class Format{
        bmp,
        jpg,
    };

    static const char *format_str[] = {
        ".bmp",
        ".jpg",
    };
};

namespace ImageLoading{
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, std::string imageName);
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, ImageUtils::Type imageType =  ImageUtils::Type::Warning);
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, std::string num, ImageUtils::Type imageType =  ImageUtils::Type::Warning);
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, ImageFiles ImageArray,  ImageUtils::Type imageType =  ImageUtils::Type::Warning);

    cv::Mat readImage(std::string loc);
}
#endif
