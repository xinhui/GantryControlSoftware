﻿#include "hmotioncontroller.h"

#include <QString>
#include <QDir>
#include <QFile>
#include <QtConcurrent>
#include <QCoreApplication>

#include <ACSC.h>

static HANDLE hComm = nullptr;

HMotionController *HMotionController::GetInstance()
{
    static HMotionController res;
    return &res;
}

void HMotionController::Release()
{
    HMotionController::GetInstance()->closeController();
}

void HMotionController::addAxisAccVel(int axis, double acc, double vel)
{
    m_defaultAccVel[axis] = qMakePair<double, double>(acc, vel);
}

bool HMotionController::openController()
{
    QString ip("10.0.0.100");
    hComm = acsc_OpenCommEthernet(ip.toLocal8Bit().data(), 701);

    if (hComm != ACSC_INVALID) {
        QFile file(QCoreApplication::applicationDirPath() + "/user/pulseGenerator.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;
        QByteArray buffer = QString(file.readAll()).toLocal8Bit();

        if (acsc_StopBuffer(hComm, 5,  nullptr)) {
            if (acsc_LoadBuffer(hComm, 5, buffer.data(), buffer.size(), nullptr))
                if (acsc_CompileBuffer(hComm, 5, nullptr))
                    return acsc_RunBuffer(hComm, 5, nullptr, nullptr);
        }
        return false;
    } else {
        hComm = acsc_OpenCommSimulator();
        return false;
    }
}

void HMotionController::startForceThread()
{
    gthread = QThread::create([ & ]() {
        double value = 0;
        while (!gthread->isInterruptionRequested()) {
            bool flag = acsc_GetAnalogInputNT(hComm, 6, &value, nullptr);
            if (!flag) {
                value = 0;
            }
            value /= 10;
            value = value / 10 * 3000;
            value += m_forceOffset;
            averageFilter(value);
            //QThread::msleep(1);
        }
    });
    gthread->start(QThread::TimeCriticalPriority);
}

void HMotionController::closeController()
{
    gthread->requestInterruption();
    gthread->quit();
    gthread->wait();
    acsc_StopBuffer(hComm, 0,  nullptr);
    stopAxis(Axis_X);
    stopAxis(Axis_Y);
    stopAxis(Axis_T);
    stopAxis(Axis_Z);
    acsc_CloseComm(hComm);
}

HMotionController::AxisState HMotionController::getAxisState(int axis)
{
//    int fault = 1;
    int state = 1;
//    if (acsc_GetFault(hComm, axis, &fault, nullptr)) {
//        if (acsc_GetMotorState(hComm, axis, &state, nullptr)) {
//            if (fault) {
//                return HMotionController::Error;
//            }
//            if (ACSC_MST_ENABLE & state) {
//                return HMotionController::Enable;
//            }
//            return HMotionController::Disable;
//        }
//    }
//    return HMotionController::NotConnected;

    if (acsc_GetMotorState(hComm, axis, &state, nullptr)) {
        if (ACSC_MST_ENABLE & state) {
            return HMotionController::Enable;
        } else {
            return HMotionController::Disable;
        }
    }
    return HMotionController::NotConnected;
}

double HMotionController::getAxisPosition(int axis)
{
    double pos = 0.0;
    acsc_GetFPosition(hComm, axis, &pos, nullptr);
    return pos;
}

double HMotionController::getAxisVelocity(int axis)
{
    double vel = 0.0;
    acsc_GetFVelocity(hComm, axis, &vel, nullptr);
    return vel;
}

double HMotionController::getAnalogInput(int port)
{
    double value = 0.0;
    acsc_GetAnalogInputNT(hComm, port, &value, nullptr);
    return value;
}

static double before = 0;
bool HMotionController::getForceValue(double *value)
{

    lock.lockForRead();
    before = m_currentForce * 0.5 + before * 0.5;
    *value = before;
    lock.unlock();
    return true;
}

double HMotionController::getForceValue()
{
    double value = 0;
    lock.lockForRead();
    before = m_currentForce * 0.5 + before * 0.5;
    value = before;
    lock.unlock();
    return value;
}

void HMotionController::setForceOffset(double offset)
{
    m_forceOffset = offset;
}

void HMotionController::setFilterWidth(double width)
{
    m_filterBandWidth = width;
}

int HMotionController::getDigitalInput(int unit, int port)
{
    int val = -1;
    acsc_GetInput(hComm, unit, port, &val, nullptr);
    return val;
}


int HMotionController::getDigitalOutput(int unit, int port)
{
    int val = 0;
    acsc_GetOutput(hComm, unit, port, &val, nullptr);
    return val;
}

bool HMotionController::ExecuteCommond(int index, const QString &commond)
{
    if (commond.isEmpty())
        return false;
    if (!acsc_StopBuffer(hComm, index, nullptr))
        return false;
    auto str = commond.toLocal8Bit();
    if (!acsc_LoadBuffer(hComm, index, str.data(), str.size(), nullptr))
        return false;
    if (!acsc_CompileBuffer(hComm, index, nullptr))
        return false;
    if (!acsc_RunBuffer(hComm, index, nullptr, nullptr)) {
        return false;
    }
    waitProgramComplete(index);
    return true;
}

bool HMotionController::RunProgram(int index, const QString &program)
{
    if (program.isEmpty())
        return false;
    auto str = program.toLocal8Bit();
    if (acsc_StopBuffer(hComm, index, nullptr)) {
        m_bufferMode = 0;
        if (acsc_LoadBuffer(hComm, index, str.data(), str.size(), nullptr)) {
            if (acsc_CompileBuffer(hComm, index, nullptr))
                if (acsc_RunBuffer(hComm, index, nullptr, nullptr)) {
                    return true;
                }
        }
    }
    return false;
}

bool HMotionController::RunProgramWait(int index, const QString &program)
{
    short flag = RunProgram(index, program);
    if (!flag) {
        return flag;
    }
    waitProgramComplete(index);
    return true;
}

bool HMotionController::RunProgramFile(int index, const QString &programFile)
{
    if (programFile.isEmpty())
        return false;
    auto str = QDir::toNativeSeparators(programFile).toLocal8Bit();
    if (acsc_StopBuffer(hComm, index, nullptr))
        if (acsc_LoadBuffersFromFile(hComm, str.data(), nullptr)) {
            if (acsc_CompileBuffer(hComm, index, nullptr))
                if (acsc_RunBuffer(hComm, index, nullptr, nullptr)) {
                    return true;
                }
        }
    return false;
}

bool HMotionController::RunProgramFileWait(int index, const QString &programFile)
{
    if (RunProgramFile(index, programFile)) {
        return false;
    }
    waitProgramComplete(index);
    return true;
}

bool HMotionController::StopProgramWait(int index)
{
    return acsc_StopBuffer(hComm, index, nullptr);
}

int HMotionController::GetProgramState(int index)
{
    int bufferState = 0;
    if (!acsc_GetProgramState(hComm, index, &bufferState, nullptr)) {
        return -2;
    }
    if (bufferState > 1) {
        return -1;
    } else {
        auto line = getErrorLine(index);
        return  line ? line : 0;
    }
}

bool HMotionController::enableAxis(int axis, bool isEnable)
{
    return isEnable ? acsc_Enable(hComm, axis, nullptr) : acsc_Disable(hComm, axis, nullptr);
}

bool HMotionController::homeAxis(int axis)
{
    QString filePath;
    int bufferIndex = 0;
    switch (axis) {
    case Axis_X: {
        filePath = QCoreApplication::applicationDirPath() + "/user/homeBuffer-X.txt";
        bufferIndex = 1;
    }
    break;
    case Axis_Y: {
        filePath = QCoreApplication::applicationDirPath() + "/user/homeBuffer-Y.txt";
        bufferIndex = 2;
    }
    break;
    case Axis_Z: {
        filePath = QCoreApplication::applicationDirPath() + "/user/homeBuffer-Z.txt";
        bufferIndex = 3;
    }
    break;
    case Axis_T: {
        filePath = QCoreApplication::applicationDirPath() + "/user/homeBuffer-T.txt";
        bufferIndex = 4;
    }
    break;
    default:
        break;
    }
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    QByteArray buffer = QString(file.readAll()).toLocal8Bit();
    ++axis;
    if (acsc_StopBuffer(hComm, bufferIndex,  nullptr)) {
        if (acsc_LoadBuffer(hComm, bufferIndex, buffer.data(), buffer.size(), nullptr))
            if (acsc_CompileBuffer(hComm, bufferIndex, nullptr))
                return acsc_RunBuffer(hComm, bufferIndex, nullptr, nullptr);
    }
    return false;
}

bool HMotionController::moveAbs(int axis, double pos, double vel, double acc, bool isWait)
{
    if (acsc_SetVelocityImm(hComm, axis, vel, nullptr)) {
        if (acsc_SetAccelerationImm(hComm, axis, acc, nullptr)) {
            if (acsc_SetDecelerationImm(hComm, axis, acc, nullptr)) {
                if (isWait) {
                    if (acsc_ToPoint(hComm, 0, axis, pos, nullptr)) {
                        return acsc_WaitMotionEnd(hComm, axis, -1);
                    }
                } else {
                    return acsc_ToPoint(hComm, 0, axis, pos, nullptr);
                }
            }
        }
    }
    return false;
}

bool HMotionController::moveAbs(int axis, double pos, bool isWait)
{
    auto accVel = m_defaultAccVel[axis];
    return moveAbs(axis, pos, accVel.second, accVel.first, isWait);
}


bool HMotionController::moveAbs1(int axis, double pos, bool isWait)
{
    auto accVel = m_defaultAccVel[axis];
    return moveAbs(axis, pos, accVel.second, accVel.first, isWait);
}

bool HMotionController::moveRel(int axis, double step, double vel, double acc, bool isWait)
{
    if (acsc_SetVelocityImm(hComm, axis, vel, nullptr)) {
        if (acsc_SetAccelerationImm(hComm, axis, acc, nullptr)) {
            if (acsc_SetDecelerationImm(hComm, axis, acc, nullptr)) {
                if (isWait) {
                    if (acsc_ToPoint(hComm, ACSC_AMF_RELATIVE, axis, step, nullptr)) {
                        return acsc_WaitMotionEnd(hComm, axis, -1);
                    }
                } else {
                    return acsc_ToPoint(hComm, ACSC_AMF_RELATIVE, axis, step, nullptr);
                }
            }
        }
    }
    return false;
}

bool HMotionController::stopAxis(int axis)
{
    return acsc_Halt(hComm, axis, nullptr);
}

bool HMotionController::killAxis(int axis)
{
    return acsc_Kill(hComm, axis, nullptr);
}

bool HMotionController::isAxisMove(int axis)
{
    int state = 1;
    if (acsc_GetMotorState(hComm, axis, &state, nullptr)) {
        return ACSC_MST_MOVE & state;
    }
    return false;
}

bool HMotionController::waitAxisStop(int axis, int timeOut)
{
    return acsc_WaitMotionEnd(hComm, axis, timeOut);
}

bool HMotionController::waitAllAxisStop(int timeOut)
{
    acsc_WaitMotionEnd(hComm, Axis_X, -1);
    acsc_WaitMotionEnd(hComm, Axis_Y, -1);
    acsc_WaitMotionEnd(hComm, Axis_Z, -1);
    return acsc_WaitMotionEnd(hComm, Axis_T, -1);
}

bool HMotionController::setDigitalOutput(int unit, int port, int val)
{
    return acsc_SetOutput(hComm, unit, port, val, nullptr);
}

double computeAverage(const QVector<double> &array)
{
    double sum = 0;
    for (const auto &k : array) {
        sum += k;
    }
    return sum / array.size();
}

void HMotionController::averageFilter(double &value)
{
    static QVector<double> datas(100, 0);
    static int index = 0;
    if (index == 100) {
        index = 0;
    }
    datas[index++] = value;
    double ave = computeAverage(datas);
    lock.lockForWrite();
    m_currentForce = ave;
    lock.unlock();
}

void HMotionController::waitProgramComplete(int index)
{
    while (true) {
        if (GetProgramState(index) == 0) {
            Sleep(100);
            continue;
        } else {
            return;
        }
    }
}

int HMotionController::getErrorLine(int bufferIndex)
{
    int errorLine = 0;
    auto cmd = QString("PERL(%1)\r").arg(bufferIndex).toLocal8Bit();
    if (!acsc_ReadInteger(hComm, ACSC_NONE, cmd.data(), -1, -1, -1, -1, &errorLine, nullptr)) {
        return -1;
    }
    return  errorLine;
}

HMotionController::HMotionController()
{
    m_defaultAccVel[Axis_X] = qMakePair<double, double>(1000, 60);
    m_defaultAccVel[Axis_Y] = qMakePair<double, double>(1000, 60);
    m_defaultAccVel[Axis_Z] = qMakePair<double, double>(1000, 20);
    m_defaultAccVel[Axis_T] = qMakePair<double, double>(1000, 10);
}

HMotionController::~HMotionController()
{
    closeController();
}
