﻿
#include "hnetworkcommond.h"

#include <QTcpSocket>
#include <QtNetwork>

static void hDelay(int mSec)
{

    //    QEventLoop loop;
    //    QTimer::singleShot(mSec, &loop, &QEventLoop::quit);
    //    loop.exec();

    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    timer.callOnTimeout(&loop, &QEventLoop::quit);
    timer.start(mSec);
    loop.exec();

    //    QCoreApplication::processEvents(QEventLoop::AllEvents, mSec);

    //    QTime dieTime = QTime::currentTime().addMSecs(mSec);
    //       while( QTime::currentTime() < dieTime )
    //       QCoreApplication::processEvents(QEventLoop::AllEvents, 1);

}

HNetworkCommond *HNetworkCommond::m_instance = nullptr;
HNetworkCommond *HNetworkCommond::GetInstance()
{
    if (m_instance == nullptr) {
        m_instance = new HNetworkCommond();
        atexit(Release);
    }
    return m_instance;
}
void HNetworkCommond::Release()
{
    if (m_instance) {
        delete m_instance;
        m_instance = nullptr;
    }
}


HNetworkCommond::HNetworkCommond(QObject *parent): QObject(parent), tcpSocket(new QTcpSocket(this))
{

    // find out name of this machine
    QString name = QHostInfo::localHostName();
    if (!name.isEmpty()) {
        availableHosts << name;
        QString domain = QHostInfo::localDomainName();
        if (!domain.isEmpty())
            availableHosts << (name + QChar('.') + domain);
    }
    if (name != QLatin1String("localhost"))
        availableHosts << QString("localhost");
    // find out IP addresses of this machine
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // add non-localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (!ipAddressesList.at(i).isLoopback())
            availableHosts << ipAddressesList.at(i).toString();
    }
    // add localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i).isLoopback())
            availableHosts << ipAddressesList.at(i).toString();
    }

    connect(tcpSocket, &QAbstractSocket::errorOccurred, this, [ = ](QAbstractSocket::SocketError socketError) {
        switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
            break;
        case QAbstractSocket::HostNotFoundError:
            lastError = tr("The host was not found. Please check the "
                           "host name and port settings.");
            break;
        case QAbstractSocket::ConnectionRefusedError:
            lastError = tr("The connection was refused by the peer. "
                           "Make sure the fortune server is running, "
                           "and check that the host name and port "
                           "settings are correct.");
            break;
        default:
            lastError = tr("The following error occurred: %1.")
                        .arg(tcpSocket->errorString());
        }

        emit errorOccurd(lastError);
    });


    connect(tcpSocket, &QAbstractSocket::readyRead, this, &HNetworkCommond::readRely);

}


HNetworkCommond::~HNetworkCommond()
{
    tcpSocket->abort();
    tcpSocket->disconnectFromHost();
}



void HNetworkCommond::setHost(const QString &host)
{
    hostName = host;
    createConnection();
}

void HNetworkCommond::setPort(quint16 port)
{
    portNumber = port;
    createConnection();
}

void HNetworkCommond::setHostAndPort(const QString &host, quint16 port)
{
    hostName = host;
    portNumber = port;
    createConnection();
}

bool HNetworkCommond::createConnection()
{
    tcpSocket->abort();
    isRelyReady = true;
    tcpSocket->connectToHost(hostName, portNumber);
    return tcpSocket->waitForConnected(3000);
}

void HNetworkCommond::closeConnection()
{
    tcpSocket->abort();
}

bool HNetworkCommond::isConnected()
{
    return tcpSocket->state();
}

qint32 HNetworkCommond::execCommond(const QString &cmd, QString &result, const QString &args, qint32 timeOut)
{

    tcpRelyBlock.clear();

    if (tcpSocket->isValid() == false) {
        createConnection();
        if (!tcpSocket->waitForConnected()) {
            result = lastError.toLocal8Bit();
            return -2;
        }
    }

    isRelyReady = false;
    tcpSocket->write(cmd.toLocal8Bit());

    qint32 mg = 0;
    while (!isRelyReady) {
        hDelay(1);
        ++mg;
        if (mg > timeOut) {
            result = lastError.toLocal8Bit();
            return -1;
        }
    }

    result = QString::fromLocal8Bit(tcpRelyBlock);
    return 0;
}

const QString &HNetworkCommond::getLastError()
{
    return lastError;
}


void HNetworkCommond::readRely()
{
    static QByteArray end("\r");

    if (isRelyReady == false) {
        tcpRelyBlock.append(tcpSocket->readAll());
        if (tcpRelyBlock.right(1) == end) {
            isRelyReady = true;
        }

    }
}




