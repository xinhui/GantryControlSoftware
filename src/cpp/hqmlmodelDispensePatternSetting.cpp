#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QThread>
#include <iostream>
#include <string>
#include <sstream>

#include "hqmlmodelgluecontroller.h"
#include "hmotioncontroller.h"
#include "hqmlmodelio.h"
#include "hqmlmodelflow.h"

#include "hqmlmodelDispenseActionSetting.h"
#include "hqmlmodelDispensePatternSetting.h"
#include "hqmlmodelsettingcoord.h"
#include "QLog.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}

QMessageLogContext context_pattern;
HQmlModelDispensePatternSettings *HQmlModelDispensePatternSettings::patternPtr = nullptr;

HQmlModelDispensePatternSettings::HQmlModelDispensePatternSettings(QObject *parent) : QObject(parent)
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "construct dispensing pattern setting");
    std::cout << "start initializing the PatternSettings...." << std::endl;
    loadConfig();
    std::cout << "initialized the PatternSettings successfully" << std::endl;
    std::cout << "start updateUi...." << std::endl;
    updateUI();
    std::cout << "updateUi done" << std::endl;
    std::cout << "number of pattern:" << m_patterns.length() << std::endl;
    patternPtr = this;
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "construct dispensing pattern setting successfully");
}

HQmlModelDispensePatternSettings::~HQmlModelDispensePatternSettings()
{
    std::cout << "start saving config..." << std::endl;
    std::cout << "number of patterns:" << m_patterns.length()<< std::endl;
    saveConfig();
    std::cout << " saving config done" << std::endl;
    std::cout << "number of patterns:" << m_patterns.length()<< std::endl;
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "destroy dispensing pattern setting");
}

QStringList HQmlModelDispensePatternSettings::getActionNames()
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "get all action names");
    return HQmlModelDispenseActionSettings::DispensePtr->getAtcionNames();
}

void HQmlModelDispensePatternSettings::addPattern(QString name, int patternType)
{
    std::ostringstream oss;
    if (m_patternNames.contains(name) || name.isEmpty()) {
        return;
    }
    m_patternNames << name;
    m_patternTypes.append(patternType);
    m_patterns << DispensePattern();
    m_currentPatternName = name;
    m_currentActionNames.clear();
    m_actionNameCollection.append(m_currentActionNames);
    updateUI();
}

void HQmlModelDispensePatternSettings::removePattern(int index)
{
    std::ostringstream oss;
    std::cout << " removing pattern name..." << std::endl;
    m_patternNames.removeAt(index);
    m_patternTypes.removeAt(index);
    std::cout << "removing patterns..." << std::endl;
    m_patterns.removeAt(index);
    std::cout << "set current pattern name" << std::endl;
    m_currentPatternName = m_patternNames.value(index);
    std::cout << "updateUI" << std::endl;
    updateUI();
}

void HQmlModelDispensePatternSettings::setCurrentPattern(int index)
{
    std::ostringstream oss;
    oss << "set current dispensing pattern " << index;
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, QString::fromStdString(oss.str()));
    if (index > -1 && index < m_patternNames.size()) {
        m_currentPatternName = m_patternNames[index];
        m_currentActionNames = m_actionNameCollection[index];
    }
    updateUI();
}

void HQmlModelDispensePatternSettings::appendActionToCurrentPattern(QString actionName)
{
    std::ostringstream oss;
    oss << "append dispensing action " << actionName.toStdString() << " to current pattern";
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, QString::fromStdString(oss.str()));
    int cindex = m_patternNames.indexOf(m_currentPatternName);
    if (cindex != -1) {
        m_patterns[cindex].appendDispenseAction(HQmlModelDispenseActionSettings::DispensePtr->getAction(actionName));
        m_currentActionNames << actionName;
        m_actionNameCollection[cindex].append(actionName);
        updateUI();
    }
}
void HQmlModelDispensePatternSettings::removeActionFromCurrentPattern(int index)
{
    std::ostringstream oss;
    oss << "removing dispensing action " << index << " from current pattern";
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, QString::fromStdString(oss.str()));
    std::cout << "start removing.." << std::endl;
    int cindex = m_patternNames.indexOf(m_currentPatternName);
    std::cout << "cindex = " << cindex << std::endl;
    if (cindex != -1) {
        std::cout << "pattern removing..." << std::endl;
        m_patterns[cindex].removeDispenseAction(index);
        std::cout << "action name removing..." << std::endl;
        m_currentActionNames.removeAt(index);
        std::cout << "name collection removing..." << std::endl;
        m_actionNameCollection[cindex].removeAt(index);
        updateUI();
    }
}

void HQmlModelDispensePatternSettings::clearCurrentActions()
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "Clear all actions from current pattern");
    int cindex = m_patternNames.indexOf(m_currentPatternName);
    if (cindex != -1) {
        m_currentActionNames.clear();
        m_actionNameCollection[cindex].clear();
        m_patterns[cindex].clearActions();
        updateUI();
    }


}

void HQmlModelDispensePatternSettings::updateUI()
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "Update dispensing pattern setting UI");
    std::cout << "get the pattern index..." << std::endl;
    int patternIndex = m_patternNames.contains(m_currentPatternName) ? m_patternNames.indexOf(m_currentPatternName) : 0;
    std::cout << "pattern index = " << patternIndex << std::endl;
    std::cout << "start emit signal..." << std::endl;
    //std::cout << "m_patternNames:" << m_patternNames << std::endl;
    int currentPatternType = m_patternTypes.isEmpty() ? 0 :  m_patternTypes[patternIndex];
    std::cout << "currentPatternType:" << currentPatternType << std::endl;


    //HQmlModelDispensePatternSettings::getGlueDotPos();
    QStringList patternOffset = QStringList{QString::number(m_patternOffset.x, 'f', 4), QString::number(m_patternOffset.y, 'f', 4), QString::number(m_patternOffset.z, 'f', 4), QString::number(m_patternOffset.t, 'f', 4)};
    emit updateControlData(patternIndex, m_patternNames, currentPatternType, m_currentActionNames, patternOffset);
    std::cout << "emit signal done!" << std::endl;
}

void HQmlModelDispensePatternSettings::receiveSignalForGluingOffset(MPointXYZT _glueDotPos){
    m_patternOffset.x = _glueDotPos.x;
    m_patternOffset.y = _glueDotPos.y;
    m_patternOffset.z = 0.;
    m_patternOffset.t = 0.;

    updateGluingOffsetUI();
}

void HQmlModelDispensePatternSettings::updateGluingOffsetUI(){
    std::cout<<"Offset x: "<<m_patternOffset.x << ", y: " << m_patternOffset.y << std::endl;

    QStringList _strGluingOffset = QStringList{QString::number(m_patternOffset.x, 'f', 4), QString::number(m_patternOffset.y, 'f', 4), QString::number(m_patternOffset.z, 'f', 4), QString::number(m_patternOffset.t, 'f', 4)};

    emit updateGluingOffset(_strGluingOffset);

}

bool HQmlModelDispensePatternSettings::runPattern(const QString &name,MPointXYZT offset)
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("start running dispensing pattern: %1").arg(name));
    int index = -1;
    if (name.isEmpty()) {
        QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("dispensing pattern %1 is empty").arg(name));
        return false;
    } else {
        index = m_patternNames.indexOf(name);
    }
    if (index < 0 || index >= m_patterns.size()) {
        QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("dispensing pattern %1 not found in the pattern name list").arg(name));
        return false;
    }
    //int cindex = m_patternNames.indexOf(m_currentPatternName);
    for (int i = 0; i < m_currentActionNames.length(); i++)
    {
        QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("running dispensing action %1").arg(i));
        if (i==0)
        {
            if (!m_patterns[index].getDispenseAction(i).setDispenseSettings()) return false;
        }
        if (!m_patterns[index].getDispenseAction(i).runAction(offset)) return false;
    }
    return true;
}

QStringList HQmlModelDispensePatternSettings::getAxisFPOS()
{
    m_patternOffset.x = m_motion->getAxisPosition(Axis_X);
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("get dispensing pattern offset x: %1 ").arg(m_patternOffset.x));
    m_patternOffset.y = m_motion->getAxisPosition(Axis_Y);
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("get dispensing pattern offset y: %1 ").arg(m_patternOffset.y));
    m_patternOffset.z = m_motion->getAxisPosition(Axis_Z);
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("get dispensing pattern offset z: %1 ").arg(m_patternOffset.z));
    m_patternOffset.t = m_motion->getAxisPosition(Axis_T);
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, tr("get dispensing pattern offset t: %1 ").arg(m_patternOffset.t));
    return QStringList{QString::number(m_motion->getAxisPosition(Axis_X), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Y), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Z), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_T), 'f', 4)};
}


bool HQmlModelDispensePatternSettings::runCurrentPattern()
{
    return runPattern(m_currentPatternName, m_patternOffset);
}

bool HQmlModelDispensePatternSettings::loadConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "start loading dispensing pattern setting configure");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/DispensePatternSettingsConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "loading dispensing pattern setting configure successfully");
    return isok;
}

bool HQmlModelDispensePatternSettings::saveConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "start saving dispensing pattern setting configure");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/DispensePatternSettingsConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    QLog::LogMessageOutput(QtInfoMsg,context_pattern, "saving dispensing pattern setting configure successfully");
    return true;
}

bool operator>>(QDataStream &input, HQmlModelDispensePatternSettings &obj)
{
    input  >> obj.m_patternNames >> obj.m_patternTypes >> obj.m_currentPatternName  >> obj.m_defaultPattern >> obj.m_actionNameCollection >> obj.m_currentActionNames >> obj.m_patterns >> obj.m_patternOffset;
    //for (int i=0; i < obj.m_patterns.length(); i++)
    //{
    //    input >> obj.m_patterns[i];
    //}
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelDispensePatternSettings &obj)
{
    output <<  obj.m_patternNames << obj.m_patternTypes << obj.m_currentPatternName << obj.m_defaultPattern << obj.m_actionNameCollection << obj.m_currentActionNames << obj.m_patterns << obj.m_patternOffset;
    //for (int i=0; i < obj.m_patterns.length(); i++)
    //{
    //    output << obj.m_patterns[i];
    //}

    return true;
}

