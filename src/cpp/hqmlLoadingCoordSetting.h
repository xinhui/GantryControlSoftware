#ifndef HQMLLOADINGCOORDSETTING_H
#define HQMLLOADINGCOORDSETTING_H


#include <QObject>
#include <QDataStream>
#include <QTextStream>

#include<iostream>
#include<map>


class HQmlLoadingCoordSetting : public QObject{
    Q_OBJECT
public:
    explicit HQmlLoadingCoordSetting(QObject *parent = nullptr);
    ~HQmlLoadingCoordSetting();

    static HQmlLoadingCoordSetting *loadingCoordPtr;

    const QStringList &getConfigNames() {return m_configNames;}
    QString getCurrent(){return m_currentConfig;}


signals:
    void updateLoadingCoordUI(int currentCfg, QStringList cfgNames, QStringList cfgDatas, int getter, int setter);

public slots:

    void addConfig(QString name);
    void removeConfig(int index);
    void setCurrentConfig(int index);
    void setConfigData(const QString &index, const QString &value);

    void updateLoadingCoordInfo();

    bool getFixture(const QString &name);
    bool getFixture(int index);
    bool letFixture(const QString &name);
    bool letFixture(int index);

    QPointF getModuleCentre(int _index);


protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlLoadingCoordSetting &obj);
    friend bool operator<<(QDataStream &output, const HQmlLoadingCoordSetting &obj);

private:
    QStringList m_configNames;
    QString m_currentConfig;

    QString m_defaultGetter, m_defaultLetter;

    enum LoadingCoordPar{
        WaitingZ = 0,
        OutputGantry = 1,
        DelOX,
        DelOY,
        MovingSpeed,
        MovingAcc,
        SearchPos1,
        SearchPos2,
        PlacingSpeed,
        PlacingAcc,
        SearchForce,
        OutputVC
    };

    std::map<QString, LoadingCoordPar> map_LoadingCoordPar;

    struct ConfigVars {
        ConfigVars() {}
        double waitPos = 70.0;
        int outputIndex_Gantry = 31;
        double del_OX = 0.;
        double del_OY = 0.;
        double fastSpeed = 15.;
        double fastAcc = 1000.;
        double srhPos1 = 10.0;
        double srhPos2 = 0.0;
        double srhSpeed = 0.01;
        double srhAcc = 100.;
        double srhForce = 10.;
        int outputIndex_VC = 1;

        QStringList getStrs()
        {
            return QStringList{QString::number(waitPos, 'f', 4), QString::number(outputIndex_Gantry),
                               QString::number(del_OX, 'f', 4), QString::number(del_OY, 'f', 4),
                               QString::number(fastSpeed, 'f', 4), QString::number(fastAcc, 'f', 4),
                               QString::number(srhPos1, 'f', 4), QString::number(srhPos2, 'f', 4), 
                               QString::number(srhSpeed, 'f', 4), QString::number(srhAcc, 'f', 4), 
                               QString::number(srhForce, 'f', 4), QString::number(outputIndex_VC)};
        }
        void setValue(int index, double value)
        {
            std::cout<< "========================================================"<<std::endl;
            if (index == OutputGantry){
                outputIndex_Gantry = value + 0.5;
                std::cout << "output(Gantry): "<< index << ", output(Gantry): "<<outputIndex_Gantry<<std::endl;
            }
            else if (index == OutputVC){
                outputIndex_VC = value + 0.5;
                std::cout << "output(VC): "<< index << ", output(VC): "<<outputIndex_VC<<std::endl;
            }
            else if(index <= OutputVC){
                double *p = (double *)this;
                p[index] = value;
                std::cout<<"index: "<<index<<", this: "<<waitPos<<std::endl;
            }
        }

        friend bool operator>>(QDataStream &input, ConfigVars &obj)
        {
            input >> obj.waitPos >> obj.outputIndex_Gantry >> obj.del_OX >> obj.del_OY >> obj.fastSpeed >> obj.fastAcc >> obj.srhPos1
                  >> obj.srhPos2 >> obj.srhSpeed >> obj.srhAcc >> obj.srhForce >> obj.outputIndex_VC;
            return true;
        }

        friend bool operator<<(QDataStream &output, const ConfigVars &obj)
        {
            output << obj.waitPos << obj.outputIndex_Gantry << obj.del_OX << obj.del_OY << obj.fastSpeed << obj.fastAcc << obj.srhPos1
                  << obj.srhPos2 << obj.srhSpeed << obj.srhAcc << obj.srhForce << obj.outputIndex_VC;
            return true;
        }
    };

    QVector<ConfigVars> m_configDatas;

};

#endif // HQMLLOADINGCOORDSETTING_H
