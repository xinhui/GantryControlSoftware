﻿#include "hqmlmodelCapture.h"
#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QThread>

#include "hqmlmodelio.h"
#include "hmotioncontroller.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}

HQmlModelCapture *HQmlModelCapture::capturePtr = nullptr;

HQmlModelCapture::HQmlModelCapture(QObject *parent) : QObject(parent)
{
    std::cout<<"Load Model Capture"<<std::endl;
    loadConfig();
    updateUI();
    capturePtr = this;
}

HQmlModelCapture::~HQmlModelCapture()
{
    std::cout<<"quit HQmlModel Capture ============"<<std::endl;
    saveConfig();
}

QStringList HQmlModelCapture::getOutputs()
{
    return HQmlModelIO::ioPtr->getOutputNames();
}

void HQmlModelCapture::addConfig(QString name)
{
    if (m_configNames.contains(name) || name.isEmpty()) {
        return;
    }
    m_configNames << name;
    m_configDatas << ConfigVars();
    m_currentConfig = name;
    updateUI();
}

void HQmlModelCapture::removeConfig(int index)
{
    m_configNames.removeAt(index);
    m_configDatas.removeAt(index);
    m_currentConfig = m_configNames.value(index);
    updateUI();
}

void HQmlModelCapture::setCurrentConfig(int index)
{
    if (index > -1 && index < m_configNames.size()) {
        m_currentConfig = m_configNames[index];
    }
    updateUI();
}

void HQmlModelCapture::setConfigData(int index, const QString &value)
{
    int cindex = m_configNames.indexOf(m_currentConfig);
    if (cindex != -1) {
        m_configDatas[cindex].setValue(index, value.toDouble());
    }
}

void HQmlModelCapture::updateUI()
{
    int cfgIndex = m_configNames.contains(m_currentConfig) ? m_configNames.indexOf(m_currentConfig) : 0;
    emit updateControlData(cfgIndex, m_configNames, m_configDatas.value(cfgIndex).getStrs(), m_configNames.indexOf(m_defaultGetter),
                           m_configNames.indexOf(m_defaultLetter));
    saveConfig();
}



bool HQmlModelCapture::getFixture(const QString &name)
{
    int index = -1;
    if (name.isEmpty()) {
        return false;
    } else {
        index = m_configNames.indexOf(name);
    }
    if (index < 0 || index >= m_configDatas.size()) {
        return false;
    }

    HMotionController *motion = HMotionController::GetInstance();
    ConfigVars configs = m_configDatas[index];

    CMoveAbs(Axis_Z, configs.srhPos1, configs.fastSpeed, configs.fastAcc, true);
    QThread::msleep(3000);
    HQmlModelIO::ioPtr->zeroForceSensor();
    CMoveAbs(Axis_Z, configs.srhPos2, configs.srhSpeed, configs.srhAcc, false);

    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto NotFind;
        }
        if (!motion->getForceValue(&force)) {
            goto error;
        }
        if (force > configs.srhForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(1000);
            if(configs.typeIndex == 0){
                goto PickTooling;
            }
            else if(configs.typeIndex == 1){
                goto PickTarget;
            }
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }

    }

NotFind:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

PickTooling:
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 1)) {
        goto error;
    }
    QThread::msleep(3000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

PickTarget:
    QThread::msleep(3000);
    if (!motion->setDigitalOutput(2, configs.outputIndex_VC, 0)) {
        goto error;
    }
    QThread::msleep(3000);
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 1)) {
        goto error;
    }
    QThread::msleep(3000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

error:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return false;
}

bool HQmlModelCapture::getFixture(int index)
{
    return getFixture(m_configNames.value(index));
}

bool HQmlModelCapture::letFixture(const QString &name)
{
    int index = -1;
    if (name.isEmpty()) {
        return false;
    } else {
        index = m_configNames.indexOf(name);
    }
    if (index < 0 || index >= m_configDatas.size()) {
        return false;
    }

    HMotionController *motion = HMotionController::GetInstance();
    ConfigVars configs = m_configDatas[index];

    CMoveAbs(Axis_Z, configs.srhPos1, configs.fastSpeed, configs.fastAcc, true)
    QThread::msleep(3000);
    HQmlModelIO::ioPtr->zeroForceSensor();
    CMoveAbs(Axis_Z, configs.srhPos2, configs.srhSpeed, configs.srhAcc, false)

    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto NotFind;
        }
        if (!motion->getForceValue(&force)) {
            goto error;
        }
        if (force > configs.srhForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(500);
            if(configs.typeIndex == 0){
                goto ReleaseTooling;
            }
            else if(configs.typeIndex == 1){
                goto ReleaseTarget;
            }
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }
    }
    
NotFind:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

ReleaseTooling:
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 0)) {
        goto error;
    }
    QThread::msleep(3000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true)
    return true;

ReleaseTarget:
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 0)) {
        goto error;
    }
    QThread::msleep(2000);
    if (!motion->setDigitalOutput(2, configs.outputIndex_VC, 1)) {
        goto error;
    }
    QThread::msleep(2000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true)
    return true;

error:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true)
    return false;
}

bool HQmlModelCapture::letFixture(int index)
{
    return letFixture(m_configNames.value(index));
}

bool HQmlModelCapture::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/captureConfig.cfg";
    std::cout<<"Capture config file path: "<< configFile.toStdString() <<std::endl;
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        std::cout<<"load capture config failed!"<<std::endl;
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    return isok;
}

bool HQmlModelCapture::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/captureConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        std::cout<<"cannot write into Capture file!"<<std::endl;
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}

bool operator>>(QDataStream &input, HQmlModelCapture &obj)
{
    input  >> obj.m_configNames >> obj.m_currentConfig >> obj.m_configDatas >> obj.m_defaultGetter >> obj.m_defaultLetter;
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelCapture &obj)
{
    output <<  obj.m_configNames <<  obj.m_currentConfig << obj.m_configDatas << obj.m_defaultGetter << obj.m_defaultLetter;
    return true;
}
