#include "hqmlPatternRecognition.h"
#include "hmathfunctions.h"
#include "hqmlmodelcamera.h"
#include "hmotioncontroller.h"
#include "hqmlmodelCapture.h"
#include "hqmlmodelsettingcoord.h"
#include "hqmlcameraparameter.h"
#include "Algorithm.h"
#include "ImageProcess.h"

#include <QtConcurrent>
#include <QFutureWatcher>
#include <ctime>
#include <fstream>
#include <cstdio>


HQmlPatternRecognition *HQmlPatternRecognition::recognitionPtr = nullptr;
HQmlPatternRecognition::HQmlPatternRecognition(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Recognition config"<<std::endl;

    m_motion = HMotionController::GetInstance();
    if (!loadConfig()) {
        std::cout << "loading Recognition config fail!" << std::endl;
    }
    recognitionPtr = this;
}

HQmlPatternRecognition::~HQmlPatternRecognition()
{
    if (saveConfig()) std::cout << "saving Recognition config" << std::endl;
}

void HQmlPatternRecognition::setPointX(QString _type, int _index, const QString &_text){
    if(_type == "Sensor1"){
        Sensor1.points[_index].setX(_text.toDouble());
    }
    if(_type == "Sensor2"){
        Sensor2.points[_index].setX(_text.toDouble());
    }
    if(_type == "Flex"){
        ModuleFlex.highVolt[_index].setX(_text.toDouble());
    }
    if(_type == "Module"){
        Module.highVolt[_index].setX(_text.toDouble());
    }

    saveConfig();
    updateCentreInfo();
}

void HQmlPatternRecognition::setPointY(QString _type, int _index, const QString &_text){
    if(_type == "Sensor1"){
        Sensor1.points[_index].setY(_text.toDouble());
    }
    if(_type == "Sensor2"){
        Sensor2.points[_index].setY(_text.toDouble());
    }
    if(_type == "Flex"){
        ModuleFlex.highVolt[_index].setY(_text.toDouble());
    }
    if(_type == "Module"){
        Module.highVolt[_index].setY(_text.toDouble());
    }
    saveConfig();
    updateCentreInfo();
}

void HQmlPatternRecognition::setSearchPoint(QString _type, int _index){
    if(_type == "Sensor1"){
        Sensor1.searchPoints[_index].setX(m_motion->getAxisPosition(Axis_X));
        Sensor1.searchPoints[_index].setY(m_motion->getAxisPosition(Axis_Y));
        std::cout<<Sensor1.searchPoints[_index].x()<<", "<<Sensor1.searchPoints[_index].y()<<std::endl;
    }
    if(_type == "Sensor2"){
        Sensor2.searchPoints[_index].setX(m_motion->getAxisPosition(Axis_X));
        Sensor2.searchPoints[_index].setY(m_motion->getAxisPosition(Axis_Y));
        std::cout<<Sensor2.searchPoints[_index].x()<<", "<<Sensor2.searchPoints[_index].y()<<std::endl;
    }
    if(_type == "Flex"){
        ModuleFlex.searchPoints[_index].setX(m_motion->getAxisPosition(Axis_X));
        ModuleFlex.searchPoints[_index].setY(m_motion->getAxisPosition(Axis_Y));
    }
    if(_type == "Module"){
        Module.searchPoints[_index].setX(m_motion->getAxisPosition(Axis_X));
        Module.searchPoints[_index].setY(m_motion->getAxisPosition(Axis_Y));
    }

    saveConfig();
}

void HQmlPatternRecognition::setFocusHeight(QString _type){
    if(_type == "Sensor1"){
        Sensor1.focusHeight = m_motion->getAxisPosition(Axis_Z);
    }
    if(_type == "Sensor2"){
        Sensor2.focusHeight = m_motion->getAxisPosition(Axis_Z);
    }
    if(_type == "Flex"){
        ModuleFlex.focusHeight = m_motion->getAxisPosition(Axis_Z);
    }
    if(_type == "Module"){
        Module.focusHeight = m_motion->getAxisPosition(Axis_Z);
    }
    saveConfig();
    updateCentreInfo();

}

bool HQmlPatternRecognition::moveXY(QString _type, int _index){
    QPointF PointXY;
    if(_type == "Sensor1"){
        std::cout<<Sensor1.searchPoints[_index].x()<<", "<<Sensor1.searchPoints[_index].y()<<std::endl;
        PointXY.setX(Sensor1.searchPoints.at(_index).x());
        PointXY.setY(Sensor1.searchPoints.at(_index).y());
        if (!m_motion->moveAbs(Axis_X, PointXY.x()))
            return false;
        if (!m_motion->moveAbs(Axis_Y, PointXY.y()))
            return false;
        return true;
    }
    if(_type == "Sensor2"){
        std::cout<<Sensor2.searchPoints[_index].x()<<", "<<Sensor2.searchPoints[_index].y()<<std::endl;
        PointXY.setX(Sensor2.searchPoints.at(_index).x());
        PointXY.setY(Sensor2.searchPoints.at(_index).y());
        if (!m_motion->moveAbs(Axis_X, PointXY.x()))
            return false;
        if (!m_motion->moveAbs(Axis_Y, PointXY.y()))
            return false;
        return true;
    }
    if(_type == "Flex"){
        PointXY.setX(ModuleFlex.searchPoints.at(_index).x());
        PointXY.setY(ModuleFlex.searchPoints.at(_index).y());
        if (!m_motion->moveAbs(Axis_X, PointXY.x()))
            return false;
        if (!m_motion->moveAbs(Axis_Y, PointXY.y()))
            return false;
        return true;
    }
    if(_type == "Module"){
        PointXY.setX(Module.searchPoints.at(_index).x());
        PointXY.setY(Module.searchPoints.at(_index).y());
        if (!m_motion->moveAbs(Axis_X, PointXY.x()))
            return false;
        if (!m_motion->moveAbs(Axis_Y, PointXY.y()))
            return false;
        return true;
    }

    else{
        return false;
    }
}

bool HQmlPatternRecognition::moveToFocus(QString _type){
    if(_type == "Sensor1"){
        if (!m_motion->moveAbs(Axis_Z, Sensor1.focusHeight))
            return false;
        return true;
    }
    if(_type == "Sensor2"){
        if (!m_motion->moveAbs(Axis_Z, Sensor2.focusHeight))
            return false;
        return true;
    }
    if(_type == "Flex"){
        if (!m_motion->moveAbs(Axis_Z, ModuleFlex.focusHeight))
            return false;
        return true;
    }
    if(_type == "Module"){
        if (!m_motion->moveAbs(Axis_Z, Module.focusHeight))
            return false;
        return true;
    }
    else{
        return false;
    }
}

QPointF HQmlPatternRecognition::getCentre(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.centre();
    }
    if(_type == "Sensor2"){
        return Sensor2.centre();
    }
    if(_type == "Flex"){
        QPointF FlexCentre;
        FlexCentre.setX( ModuleFlex.markHV1().x() + FlexCentreBiasX*cos(ModuleFlex.rotAngle()*PI/180.) + FlexCentreBiasY*sin(ModuleFlex.rotAngle()*PI/180.) );
        FlexCentre.setY(ModuleFlex.markHV1().y() + FlexCentreBiasX*sin(ModuleFlex.rotAngle()*PI/180.) - FlexCentreBiasY*cos(ModuleFlex.rotAngle()*PI/180.) );
        return FlexCentre;
    }
    if(_type == "Module"){
        QPointF FlexCentre;
        FlexCentre.setX( Module.markHV1().x() + ModuleCentreBiasX*cos(Module.rotAngle()*PI/180.) + ModuleCentreBiasY*sin(Module.rotAngle()*PI/180.) );
        FlexCentre.setY(Module.markHV1().y() + ModuleCentreBiasX*sin(Module.rotAngle()*PI/180.) - ModuleCentreBiasY*cos(Module.rotAngle()*PI/180.) );
        return FlexCentre;
    }

    QPointF point(-999., -999.);
    return point;
}

double HQmlPatternRecognition::getRotation(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.rotAngle();
    }
    if(_type == "Sensor2"){
        return Sensor2.rotAngle();
    }
    if(_type == "Flex"){
        return ModuleFlex.rotAngle();
    }
    if(_type == "Module"){
        return Module.rotAngle();
    }
    double angle = -999.;
    return angle;
}

double HQmlPatternRecognition::getWidth(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.width();
    }
    if(_type == "Sensor2"){
        return Sensor2.width();
    }
    return -999.;
}

double HQmlPatternRecognition::getHeight(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.height();
    }
    if(_type == "Sensor2"){
        return Sensor2.height();
    }
    return -999.;
}

void HQmlPatternRecognition::selectPointInPicture(QString _type, int _index){

    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    int SensorNum, ali;
    if(_type == "Sensor1"){
        SensorNum = 1;
        ali = 10+_index;
        std::cout<<"Select Point of " << "Sensor1" <<", at Point: "<< _index <<std::endl;
    }
    if(_type == "Sensor2"){
        SensorNum = 2;
        ali = 20+_index;
        std::cout<<"Select Point of " << "Sensor2" <<", at Point: "<< _index <<std::endl;
    }

    QStringList cameraResult;
    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(ali, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(1000);
    HQmlModelCamera::execCommond("R0", cameraResult);
    QThread::msleep(1000);
    HQmlModelCamera::execCommond("T1", cameraResult, 100);

    std::cout<<"Finish taking picture"<<std::endl;
    QThread::msleep(500);
    // Here is for Room107
    QString dumpRoot = "C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_0" + QString::number(SensorNum) + QString::number(_index) + "/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));

    // Here is for local test
    /*
    QString dumpRoot = "C:/Users/Frede/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_0" + QString::number(SensorNum) + QString::number(_index) + "/";
    QString date = QString("231023").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
*/
    QString imagePath = dumpRoot + date + "/CAM1/Sensor1p" + QString::number(_index) + ".bmp";

    std::cout << imagePath.toLocal8Bit().constData() << std::endl;

    Mouse::getsrcImage(imagePath.toLocal8Bit().constData());

    Mouse::ClickPoint();
    QPointF selectedPoint = Mouse::getPointWithMouse();
    std::cout<<"Find Point(In Picture) :" << selectedPoint.x() <<", "<<selectedPoint.y()<<std::endl;
    QPointF selectedPointInGantry = HQmlCameraParameter::cameraPtr->transPicureToGantry(selectedPoint);
    std::cout<<"Find Point(In Gantry) :" << selectedPointInGantry.x() <<", "<<selectedPointInGantry.y()<<std::endl;

    if(_type == "Sensor1"){

        std::cout<<"Select Point of " << "Sensor1" <<", at Point: "<< _index <<std::endl;
        Sensor1.points[_index].setX(selectedPointInGantry.x());
        Sensor1.points[_index].setY(selectedPointInGantry.y());
    }
    if(_type == "Sensor2"){

        std::cout<<"Select Point of " << "Sensor2" <<", at Point: "<< _index <<std::endl;
        Sensor2.points[_index].setX(selectedPointInGantry.x());
        Sensor2.points[_index].setY(selectedPointInGantry.y());
    }
    saveConfig();
    updateCentreInfo();
}

void HQmlPatternRecognition::recognizeCorner(QString _type, int _index){
// get file name & pictures
    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    int SensorNum, ali;
    if(_type == "Sensor1"){
        SensorNum = 1;
        ali = 10+_index;
        std::cout<<"Select Point of " << "Sensor1" <<", at Point: "<< _index <<std::endl;
    }
    if(_type == "Sensor2"){
        SensorNum = 2;
        ali = 20+_index;
        std::cout<<"Select Point of " << "Sensor2" <<", at Point: "<< _index <<std::endl;
    }

    QStringList cameraResult;
    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(ali, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(1000);
    HQmlModelCamera::execCommond("R0", cameraResult);
    QThread::msleep(1000);
    HQmlModelCamera::execCommond("T1", cameraResult, 100);
    QThread::msleep(1500);

    // Here is for Room107
    QString dumpRoot = "C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_0" + QString::number(SensorNum) + QString::number(_index) + "/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));

    // Here is for local test
    /*
    QString dumpRoot = "C:/Users/Frede/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_0" + QString::number(SensorNum) + QString::number(_index) + "/";
    QString date = QString("231023").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    */

    QString imagePath = dumpRoot + date + "/CAM1/Sensor1p" + QString::number(_index) + ".bmp";

    std::cout << imagePath.toLocal8Bit().constData() << std::endl;

    cv::Point2f sensorCorner = Orb::FindSensorCorner(imagePath.toLocal8Bit().constData(), 1e05+5e04, 80, 200);
    QPointF sensorCornerInPicture;
    sensorCornerInPicture.setX(sensorCorner.x);
    sensorCornerInPicture.setY(sensorCorner.y);
    QPointF sensorCornerInGantry = HQmlCameraParameter::cameraPtr->transPicureToGantry(sensorCornerInPicture);
    std::cout<<"Recognize Corner (In Gantry) :" << sensorCornerInGantry.x() <<", "<<sensorCornerInGantry.y()<<std::endl;

    if(_type == "Sensor1"){
        Sensor1.points[_index].setX(sensorCornerInGantry.x());
        Sensor1.points[_index].setY(sensorCornerInGantry.y());
    }
    if(_type == "Sensor2"){
        Sensor2.points[_index].setX(sensorCornerInGantry.x());
        Sensor2.points[_index].setY(sensorCornerInGantry.y());
    }
    saveConfig();
    updateCentreInfo();
}


void HQmlPatternRecognition::recognizeFlex(int _ali, QString _type){
    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    QStringList cameraResult;

    QString outputDir = QString("C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/result/SD1_%1").arg(_ali, 3, 10, QLatin1Char('0')) + "/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    QString resultPath = outputDir + date + "*";

    std::cout << resultPath.toLocal8Bit().constData() << std::endl;

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali+1, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(1000);

    QFileInfo fileInfo(outputDir);
    QDir qDir(outputDir);
    if(fileInfo.isDir()){
        std::cout<<"Find dir, try to delete"<<std::endl;
        qDir.removeRecursively();
    }

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("R0", cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("T1", cameraResult, 100);
    QThread::msleep(500);

    QStringList fileList = qDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    QString outputFileName = fileList.at(0);
    std::cout<<outputFileName.toLocal8Bit().constData()<<std::endl;
    QFile outputFile(outputDir + outputFileName);
    if(!outputFile.open(QIODevice::ReadOnly)){
        std::cout<<"Can't open file"<<std::endl;
    }

    QTextStream in(&outputFile);
    QString Line;
    QStringList list;
    QPointF HV1InPicture, HV2InPicture;

    while(!in.atEnd()){
        Line = in.readLine();
        list = Line.split(',');
        if(list.size() >=2){
            HV1InPicture.setX(list.at(0).toDouble());
            HV1InPicture.setY(list.at(1).toDouble());

            HV2InPicture.setX(list.at(3).toDouble());
            HV2InPicture.setY(list.at(4).toDouble());
        }
    }
    if(_type == "Flex"){
        ModuleFlex.highVolt[0] = HQmlCameraParameter::cameraPtr->transPicureToGantry(HV1InPicture);
        ModuleFlex.highVolt[1] = HQmlCameraParameter::cameraPtr->transPicureToGantry(HV2InPicture);
    }
    if(_type == "Module"){
        Module.highVolt[0] = HQmlCameraParameter::cameraPtr->transPicureToGantry(HV1InPicture);
        Module.highVolt[1] = HQmlCameraParameter::cameraPtr->transPicureToGantry(HV2InPicture);
    }



    saveConfig();
    updateCentreInfo();
}

void HQmlPatternRecognition::updateCentreInfo()
{
    Sensor _sensor1, _sensor2;
    Flex _flex, _module;

    _sensor1 = Sensor1;
    _sensor2 = Sensor2;
    _flex = ModuleFlex;
    _module = Module;

    QStringList _strSensor1, _strSensor2, _strFlex, _strModule;
    QStringList _strSearchSensor1, _strSearchSensor2, _strSearchFlex, _strSearchModule;
    QStringList _strCentreSensor1, _strCentreSensor2, _strCentreFlex, _strCentreModule;
    QString _strSensor1Focus, _strSensor2Focus, _strFlexFocus, _strModuleFocus;


    for(int i = 0; i < 4; i++){
        _strSensor1 << QString::number(_sensor1.points.at(i).x(), 'f', 4);
        _strSensor1 << QString::number(_sensor1.points.at(i).y(), 'f', 4);
        _strSearchSensor1 << QString::number(_sensor1.searchPoints.at(i).x(), 'f', 4);
        _strSearchSensor1 << QString::number(_sensor1.searchPoints.at(i).y(), 'f', 4);
        _strCentreSensor1 << QString::number(_sensor1.centre().x(), 'f', 4);
        _strCentreSensor1 << QString::number(_sensor1.centre().y(), 'f', 4);
        _strCentreSensor1 << QString::number(_sensor1.rotAngle(), 'f', 6);

        _strSensor2 << QString::number(_sensor2.points.at(i).x(), 'f', 4);
        _strSensor2 << QString::number(_sensor2.points.at(i).y(), 'f', 4);
        _strSearchSensor2 << QString::number(_sensor2.searchPoints.at(i).x(), 'f', 4);
        _strSearchSensor2 << QString::number(_sensor2.searchPoints.at(i).y(), 'f', 4);
        _strCentreSensor2 << QString::number(_sensor2.centre().x(), 'f', 4);
        _strCentreSensor2 << QString::number(_sensor2.centre().y(), 'f', 4);
        _strCentreSensor2 << QString::number(_sensor2.rotAngle(), 'f', 6);
    }

    for(int i = 0; i < 2; i++){
        _strFlex << QString::number(_flex.highVolt.at(i).x(), 'f', 4);
        _strFlex << QString::number(_flex.highVolt.at(i).y(), 'f', 4);
        _strSearchFlex << QString::number(_flex.searchPoints.at(i).x(), 'f', 4);
        _strSearchFlex << QString::number(_flex.searchPoints.at(i).y(), 'f', 4);
        _strCentreFlex << QString::number(getCentrePoint("Flex").x(), 'f', 4);
        _strCentreFlex << QString::number(getCentrePoint("Flex").y(), 'f', 4);
        _strCentreFlex << QString::number(_flex.rotAngle(), 'f', 4);

        _strModule << QString::number(_module.highVolt.at(i).x(), 'f', 4);
        _strModule << QString::number(_module.highVolt.at(i).y(), 'f', 4);
        _strSearchModule << QString::number(_module.searchPoints.at(i).x(), 'f', 4);
        _strSearchModule << QString::number(_module.searchPoints.at(i).y(), 'f', 4);
        _strCentreModule << QString::number(getCentrePoint("Module").x(), 'f', 4);
        _strCentreModule << QString::number(getCentrePoint("Module").y(), 'f', 4);
        _strCentreModule << QString::number(_module.rotAngle(), 'f', 4);
    }

    _strSensor1Focus = QString::number(_sensor1.focusHeight, 'f', 4);
    _strSensor2Focus = QString::number(_sensor1.focusHeight, 'f', 4);
    _strFlexFocus = QString::number(_flex.focusHeight, 'f', 4);
    _strModuleFocus = QString::number(_module.focusHeight, 'f', 4);

    emit updateUI(_strSensor1, _strSensor2, _strFlex, _strModule, _strSearchSensor1, _strSearchSensor2, _strSearchFlex, _strSearchModule, _strCentreSensor1, _strCentreSensor2, _strCentreFlex, _strCentreModule, _strSensor1Focus, _strSensor2Focus, _strFlexFocus, _strModuleFocus);
}

void HQmlPatternRecognition::setPointX(QString _type, int _index, double _value){
    if(_type == "Sensor1"){
        Sensor1.points[_index].setX(_value);
    }
    if(_type == "Sensor2"){
        Sensor2.points[_index].setX(_value);
    }
    if(_type == "Flex"){
        ModuleFlex.highVolt[_index].setX(_value);
    }
    if(_type == "Module"){
        Module.highVolt[_index].setX(_value);
    }

}


QPointF HQmlPatternRecognition::getCentrePoint(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.centre();
    }
    if(_type == "Sensor2"){
        return Sensor2.centre();
    }
    if(_type == "Flex"){
        QPointF FlexCentre;
        FlexCentre.setX( ModuleFlex.markHV1().x() + FlexCentreBiasX*cos(ModuleFlex.rotAngle()*PI/180.) + FlexCentreBiasY*sin(ModuleFlex.rotAngle()*PI/180.) );
        FlexCentre.setY(ModuleFlex.markHV1().y() + FlexCentreBiasX*sin(ModuleFlex.rotAngle()*PI/180.) - FlexCentreBiasY*cos(ModuleFlex.rotAngle()*PI/180.) );

        return FlexCentre;
    }
    if(_type == "Module"){
        QPointF FlexCentre;
        FlexCentre.setX( Module.markHV1().x() + ModuleCentreBiasX*cos(Module.rotAngle()*PI/180.) + ModuleCentreBiasY*sin(Module.rotAngle()*PI/180.) );
        FlexCentre.setY(Module.markHV1().y() + ModuleCentreBiasX*sin(Module.rotAngle()*PI/180.) - ModuleCentreBiasY*cos(Module.rotAngle()*PI/180.) );

        return FlexCentre;
    }

    return QPointF(-999., -999.);
}

double HQmlPatternRecognition::getCentrePointX(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.centre().x();
    }
    if(_type == "Sensor2"){
        return Sensor2.centre().x();
    }
    if(_type == "Flex"){
        return ModuleFlex.markHV1().x() + FlexCentreBiasX*cos(ModuleFlex.rotAngle()*PI/180.) + FlexCentreBiasY*sin(ModuleFlex.rotAngle()*PI/180.);
    }
    if(_type == "Module"){
        return Module.markHV1().x() + ModuleCentreBiasX*cos(Module.rotAngle()*PI/180.) + ModuleCentreBiasY*sin(Module.rotAngle()*PI/180.);
    }

    return -999.;
}

double HQmlPatternRecognition::getCentrePointY(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.centre().y();
    }
    if(_type == "Sensor2"){
        return Sensor2.centre().y();
    }
    if(_type == "Flex"){
        return ModuleFlex.markHV1().y() + FlexCentreBiasX*sin(ModuleFlex.rotAngle()*PI/180.) - FlexCentreBiasY*cos(ModuleFlex.rotAngle()*PI/180.);
    }
    if(_type == "Module"){
        return Module.markHV1().y() + ModuleCentreBiasX*sin(Module.rotAngle()*PI/180.) - ModuleCentreBiasY*cos(Module.rotAngle()*PI/180.);
    }
    return -999.;
}

double HQmlPatternRecognition::getRotAngle(QString _type){
    if(_type == "Sensor1"){
        return Sensor1.rotAngle();
    }
    if(_type == "Sensor2"){
        return Sensor2.rotAngle();
    }
    if(_type == "Flex"){
        return ModuleFlex.rotAngle();
    }
    if(_type == "Module"){
        return Module.rotAngle();
    }
    return -999.;
}

QPointF HQmlPatternRecognition::getCurrentXY(){
    QPointF CurrentPoint;
    CurrentPoint.setX(m_motion->getAxisPosition(Axis_X));
    CurrentPoint.setY(m_motion->getAxisPosition(Axis_Y));

    return CurrentPoint;
}

bool HQmlPatternRecognition::loadConfig()
{
    std::cout << "load Recognition config" << std::endl;
    QString configFile = QCoreApplication::applicationDirPath() + "/user/RecognitionConfig.cfg";
    std::cout << configFile.toStdString() << std::endl;

    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();
    return isok;
}

bool HQmlPatternRecognition::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/RecognitionConfig.cfg";

    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}



QDataStream &operator>>(QDataStream &input, HQmlPatternRecognition &obj)
{
    return input >> obj.Sensor1 >> obj.Sensor2 >> obj.ModuleFlex >> obj.Module;
}

QDataStream &operator<<(QDataStream &output, const HQmlPatternRecognition &obj)
{
    return output << obj.Sensor1 << obj.Sensor2 << obj.ModuleFlex << obj.Module;
}
