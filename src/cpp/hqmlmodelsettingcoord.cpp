﻿#include "hqmlmodelsettingcoord.h"
#include "hmathfunctions.h"
#include "hqmlmodelcamera.h"
#include "hmotioncontroller.h"
#include "hqmlmodelCapture.h"
#include "hqmlmodelflow.h"
#include "hqmlmodelCapture.h"

#include <QtConcurrent>
#include <QFutureWatcher>



HQmlModelSettingCoord *HQmlModelSettingCoord::ptr = nullptr;
HQmlModelSettingCoord::HQmlModelSettingCoord(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model Setting Coord!"<<std::endl;

    m_motion = HMotionController::GetInstance();
    if (!loadConfig()) {
        m_markPoss.resize(5);
        m_clampingPoss.resize(3);
        m_cameraPoss.resize(4);
        m_cameraAlgoris.fill(0, 9);
        std::cout << "loading Camera coord config fail!" << std::endl;
    }
    ptr = this;
}

HQmlModelSettingCoord::~HQmlModelSettingCoord()
{
    if (saveConfig()) std::cout << "saving SettingCoord config" << std::endl;
}

MPointXYZT HQmlModelSettingCoord::getClampingOffset()
{
    return MPointXYZT(m_clampingOffset.x, m_clampingOffset.y, m_clampingOffset.z, m_clampingOffset.t);
}

QTransform HQmlModelSettingCoord::getCameraTrans()
{
    return m_transCamera;
}

bool HQmlModelSettingCoord::moveToClampingPos()
{
    return moveToXYTWait(m_clampingPoss[0]);
}

bool HQmlModelSettingCoord::grabClamping()
{
    return HQmlModelCapture::capturePtr->getFixture(m_getName);
}

bool HQmlModelSettingCoord::releaseClamping()
{
    return HQmlModelCapture::capturePtr->letFixture(m_getName);
}

void HQmlModelSettingCoord::initPanelData()
{
    updatePanelData();
}

void HQmlModelSettingCoord::computeCameraTrans()
{

    QVector<QPointF>  src;
    QVector<QPointF>  dst;

    QPointF centerppt;

    MPointXYZT cameraResult;

    // move to mark pos and get camera pos
    for (int k = 0; k < 5; ++k) {
        if (!getCameraPos(cameraResult, m_markPoss[k], m_cameraAlgoris[k]))
            goto error;
        src.append(QPointF(cameraResult.x, cameraResult.y));
        dst.append(QPointF(m_markPoss[k].x, m_markPoss[k].y));
    }

    m_transCamera = HMathFunc::GetPerspectiveTransform(src, dst);
    m_cameraError = dst.last() - m_transCamera.map(src.last());

    centerppt = m_transCamera.map(QPointF(0, 0));
    for (auto &k : dst) {
        k = k - centerppt;
    }
    m_transCamera = HMathFunc::GetPerspectiveTransform(src, dst);
    m_isOK = true;
    emit computeCompleted(true);
error:
    emit computeCompleted(false);
    return;
}

void getCircleCenter(const QVector<QPointF> &pts, QPointF &center, double &radius)
{
    double a, b, c, d, e, f;
    a = 2 * (pts.at(1).x() - pts.at(0).x());
    b = 2 * (pts.at(1).y() - pts.at(0).y());
    c = pts.at(1).x() * pts.at(1).x() + pts.at(1).y() * pts.at(1).y() - pts.at(0).x() * pts.at(0).x() - pts.at(0).y() * pts.at(0).y();
    d = 2 * (pts.at(2).x() - pts.at(1).x());
    e = 2 * (pts.at(2).y() - pts.at(1).y());
    f = pts.at(2).x() * pts.at(2).x() + pts.at(2).y() * pts.at(2).y() - pts.at(1).x() * pts.at(1).x() - pts.at(1).y() * pts.at(1).y();
    center.setX((b * f - e * c) / (b * d - e * a));
    center.setY((d * c - a * f) / (b * d - e * a));
    radius = sqrt((center.x() - pts.at(0).x()) * (center.x() - pts.at(0).x()) + (center.y() - pts.at(0).y()) * (center.y() - pts.at(0).y())); //radius
}

void HQmlModelSettingCoord::computeClampingTrans()
{
    QVector<QPointF>  cameraPoss;
    QLineF line1, line2, line3;
    QPointF ccenter, offset, center1, center2;
    double  offsetA, radius;
    MPointXYZT cameraResult;

//    if (!moveToXYTWait(m_cameraPoss[0]))
//        goto error;
//    if (!HQmlModelCapture::capturePtr->getFixture(m_getName))
//        goto error;

//    if (!moveToXYTWait(m_clampingPoss[1]))
//        goto error;
//    if (!HQmlModelCapture::capturePtr->letFixture(m_letName))
//        goto error;

    if (!getCameraPos(cameraResult, m_cameraPoss[0], m_cameraAlgoris[5]))
        goto error;
    cameraPoss << QPointF(m_motion->getAxisPosition(Axis_X), m_motion->getAxisPosition(Axis_Y)) - m_transCamera.map(QPointF(cameraResult.x, cameraResult.y));
//        if (!getCameraPos(cameraResult, m_cameraPoss[1], m_cameraAlgoris[6]))
//            goto error;
//        cameraPoss << QPointF(m_motion->getAxisPosition(Axis_X), m_motion->getAxisPosition(Axis_Y)) - m_transCamera.map(QPointF(cameraResult.x, cameraResult.y));

//    if (!moveToXYTWait(m_clampingPoss[1]))
//        goto error;

//    // get fixture
//    if (!HQmlModelCapture::capturePtr->getFixture(m_letName))
//        goto error;

//    if (!moveToXYTWait(m_clampingPoss[2]))
//        goto error;

//    // let fixture
//    if (!HQmlModelCapture::capturePtr->letFixture(m_letName))
//        goto error;

//    if (!getCameraPos(cameraResult, m_cameraPoss[2], m_cameraAlgoris[7]))
//        goto error;
    QPointF(m_motion->getAxisPosition(Axis_X), m_motion->getAxisPosition(Axis_Y)) - m_transCamera.map(QPointF(cameraResult.x, cameraResult.y));
//    if (!getCameraPos(cameraResult, m_cameraPoss[3], m_cameraAlgoris[8]))
//        goto error;
    QPointF(m_motion->getAxisPosition(Axis_X), m_motion->getAxisPosition(Axis_Y)) - m_transCamera.map(QPointF(cameraResult.x, cameraResult.y));


//        line1.setPoints(cameraPoss[0], cameraPoss[1]);
////        line2.setPoints(cameraPoss[1], cameraPoss[3]);
////        line3.setPoints(cameraPoss[0], cameraPoss[1]);


////        line1.translate(line1.center() - line1.p1());
////        line1.setAngle(line1.angle() + 90.0);

////        line2.translate(line2.center() - line2.p1());
////        line2.setAngle(line2.angle() + 90.0);

////        line1.intersects(line2, &ccenter);

//        offset=center1-QPointF(m_clampingPoss[1].x,m_clampingPoss[1].y);


//        m_offsetX = offset.x();
//        m_offsetY = offset.y();
////        m_offsetA = line3.angle() - m_clampingPoss[1].t;
////        if(qAbs(m_offsetA)>180){
////            m_offsetA=m_offsetA+m_offsetA<0?180:-180;
////        }
////        m_radius = HMathFunc::GetPointToLineDis(ccenter, line1);
////        m_isOK = true;
///
//        m_offsetA = line1.angle();
//        if (qAbs(m_offsetA) > 180) {
//            m_offsetA = m_offsetA + m_offsetA < 0 ? 180 : -180;
//        }

    m_clampingOffset.x = m_clampingPoss[0].x - cameraPoss.first().x();
    m_clampingOffset.y = m_clampingPoss[0].y - cameraPoss.first().y();
    m_clampingOffset.z = 0;
    m_clampingOffset.t = m_clampingPoss[0].t - cameraResult.t;
    std::cout << "clampingOffset.x = " << m_clampingOffset.x << std::endl;
    std::cout << "clampingOffset.y = " << m_clampingOffset.y << std::endl;
    std::cout << "clampingOffset.z = " << m_clampingOffset.z << std::endl;
    std::cout << "clampingOffset.t = " << m_clampingOffset.t << std::endl;
//    if (!moveToXYTWait(m_clampingPoss[2]))
//        goto error;
//    if (!HQmlModelCapture::capturePtr->getFixture(m_letName))
//        goto error;
//    if (!moveToXYTWait(m_clampingPoss[0]))
//        goto error;
//    if (!HQmlModelCapture::capturePtr->letFixture(m_getName))
//        goto error;
    emit computeCompleted(true);
error:
    emit computeCompleted(false);
    return;

}

QStringList HQmlModelSettingCoord::getAxisFPOS()
{
    return QStringList{QString::number(m_motion->getAxisPosition(Axis_X), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Y), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Z), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_T), 'f', 4)};
}

void HQmlModelSettingCoord::axisMoveTo(short type, int index)
{
    switch (type) {
    case 0: {
        m_motion->moveAbs1(Axis_Z, m_markPoss[index].z);
        m_motion->moveAbs1(Axis_X, m_markPoss[index].x);
        m_motion->moveAbs1(Axis_Y, m_markPoss[index].y);

    }
    break;
    case 1: {
        m_motion->moveAbs1(Axis_T, m_clampingPoss[index].t,true);
        m_motion->moveAbs1(Axis_X, m_clampingPoss[index].x);
        m_motion->moveAbs1(Axis_Y, m_clampingPoss[index].y);

    }
    break;
    case 2: {
        m_motion->moveAbs1(Axis_X, m_cameraPoss[index].x);
        m_motion->moveAbs1(Axis_Y, m_cameraPoss[index].y);
        m_motion->moveAbs1(Axis_Z, m_cameraPoss[index].z);
    }
    break;
    default:
        break;
    }
}

QStringList HQmlModelSettingCoord::getGrabNames()
{
    return HQmlModelCapture::capturePtr->getConfigNames();
}

void HQmlModelSettingCoord::setGetIndex(int index)
{
    m_getName = HQmlModelCapture::capturePtr->getConfigNames().value(index);
}

int HQmlModelSettingCoord::getGetIndex()
{
    return HQmlModelCapture::capturePtr->getConfigNames().indexOf(m_getName);
}

void HQmlModelSettingCoord::setLetIndex(int index)
{
    m_letName = HQmlModelCapture::capturePtr->getConfigNames().value(index);
}

int HQmlModelSettingCoord::getLetIndex()
{
    return HQmlModelCapture::capturePtr->getConfigNames().indexOf(m_letName);
}

void HQmlModelSettingCoord::setControlData(int index, const QString &text)
{
    std::cout << "set control data" << std::endl;
    if (index < 15) {
        if (index % 3 == 0) {
            std::cout << "before: m_markPoss[" << index / 3 << "].x = " << m_markPoss[index /3].x << std::endl;
            m_markPoss[index / 3].x = text.toDouble();
            std::cout << "after: m_markPoss[" << index / 3 << "].x = " << m_markPoss[index /3].x << std::endl;
        } else if (index % 3 == 1) {
            m_markPoss[index / 3].y = text.toDouble();
        } else {
            m_markPoss[index / 3].z = text.toDouble();
        }
    } else if (index < 24) {
        index -= 15;
        if (index % 3 == 0) {
            m_clampingPoss[index / 3].x = text.toDouble();
        } else if (index % 3 == 1) {
            m_clampingPoss[index / 3].y = text.toDouble();
        } else {
            m_clampingPoss[index / 3].t = text.toDouble();
        }
    } else if (index < 36) {
        index -= 24;
        if (index % 3 == 0) {
            m_cameraPoss[index / 3].x = text.toDouble();
        } else if (index % 3 == 1) {
            m_cameraPoss[index / 3].y = text.toDouble();
        } else {
            m_cameraPoss[index / 3].z = text.toDouble();
        }
    } else {
        index -= 36;
        m_clampingOffset.z = 0;
        if (index % 3 == 0) {
            m_clampingOffset.x = text.toDouble();
        } else if (index % 3 == 1) {
            m_clampingOffset.y = text.toDouble();
        } else {
            m_clampingOffset.t = text.toDouble();
        }
    }
}


void HQmlModelSettingCoord::setCameraAli(int index, quint16 value)
{
    std::cout << "set camera ali" << std::endl;
    m_cameraAlgoris[index] = value;
}

bool HQmlModelSettingCoord::loadConfig()
{
    std::cout << "load SetttingCoord config" << std::endl;
    QString configFile = QCoreApplication::applicationDirPath() + "/user/coordSettingConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();
    return isok;
}

bool HQmlModelSettingCoord::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/coordSettingConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}

void HQmlModelSettingCoord::updatePanelData()
{
    QStringList list;
    for (const auto &k : qAsConst(m_markPoss)) {
        list << k.getStrs_XYZ();
    }

    list << QString::number(m_cameraError.x(), 'f', 4) << QString::number(m_cameraError.y(), 'f', 4);
    for (const auto &k : qAsConst(m_clampingPoss)) {
        list << k.getStrs_XYT();
    }

    for (const auto &k : qAsConst(m_cameraPoss)) {
        list << k.getStrs_XYZ();
    }

    list << m_clampingOffset.getStrs_XYT();
    emit updateAllPanelData(list, m_cameraAlgoris);
}

bool HQmlModelSettingCoord::getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali)
{
    static quint16 kkkkk = -1;
    if (!m_motion->moveAbs1(Axis_X, pt.x))
        return false;
    if (!m_motion->moveAbs1(Axis_Y, pt.y))
        return false;
    if (!m_motion->waitAllAxisStop())
        return false;
    if (!m_motion->moveAbs1(Axis_Z, pt.z, true))
        return false;
    QThread::msleep(500);
    QStringList cameraResult;
    if (kkkkk != ali) {
        //PW-检测设定切换, 1-SD1, arg(ali,3,10,QLatin1Char('0')-储存卡的nnn号(0~999)
        //储存卡ali号已经被占用
        if (HQmlModelCamera::execCommond(QString("PW,1,%1").arg(ali, 3, 10, QLatin1Char('0')), cameraResult))
            return false;
        // not null QStringList
        if (cameraResult.size() != 1)
            return false;
        kkkkk = ali;    // kkkk为ali，存在储存卡中的kkkk号
    }
    if (HQmlModelCamera::execCommond("T1", cameraResult))   // HQmlModelCamera::execCommond("T1", cameraResult) return -1 if it succeeds
        return false; // if if() false, then pass
    if (cameraResult.size() < 4)
        return false;
    if (!cameraResult.first().toInt())  // ???迷惑行为, cameraResult.first() = "T1"
        return false;

    pos.x = cameraResult[1].toDouble() - 1216;  //picture size 2432*2040
    pos.y = cameraResult[2].toDouble() - 1020;
    pos.t = cameraResult[3].toDouble();


    return true;
}


bool HQmlModelSettingCoord::moveToXYTWait(const MPointXYZT &pos)
{
    if (!m_motion->moveAbs1(Axis_X, pos.x))
        return false;
    if (!m_motion->moveAbs1(Axis_Y, pos.y))
        return false;
    if (!m_motion->moveAbs1(Axis_T, pos.t))
        return false;
    return m_motion->waitAllAxisStop();
}

QDataStream &operator>>(QDataStream &input, HQmlModelSettingCoord &obj)
{
    return input >> obj.m_markPoss >> obj.m_cameraAlgoris >> obj.m_transCamera >> obj.m_cameraError >> obj.m_getName >> obj.m_letName >> obj.m_clampingPoss >> obj.m_cameraPoss   >> obj.m_clampingOffset;
}

QDataStream &operator<<(QDataStream &output, const HQmlModelSettingCoord &obj)
{
    std::cout << "obj.m_markPoss[0].x = " << obj.m_markPoss[0].x << std::endl;
    return output << obj.m_markPoss << obj.m_cameraAlgoris << obj.m_transCamera << obj.m_cameraError << obj.m_getName << obj.m_letName << obj.m_clampingPoss << obj.m_cameraPoss << obj.m_clampingOffset;
}
