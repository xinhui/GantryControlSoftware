#ifndef HQMLMODELGLUECONTROLLER_H
#define HQMLMODELGLUECONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/qglobal.h>

class HQmlModelGlueController : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelGlueController(QObject *parent = nullptr);
    ~HQmlModelGlueController();

    static HQmlModelGlueController *dispenserPtr;

public slots:

    QStringList getPortNames();
    void setCurrentPort(const QString &port);
    void setCurrentBaudRate(qint32 baudrate);
    void setCurrentDispenseMode(qint8 dispensemode);
    bool getCurrentDispenseMode(qint8 index) {return index == m_dispensemode;}
    void setCurrentPressUnits(qint8 unit);
    bool getCurrentPressUnits(qint8 index) {return index == m_pressunits;}
    void setCurrentDispenseTime(QString timetext);
    QString getCurrentDispenseTime() {return m_time;}
    void setCurrentPress(QString presstext);
    QString getCurrentPress() {return m_press;}
    void setCurrentVaccum(QString vactext);
    QString getCurrentVacuum() {return m_vac;}
    void setCurrentTrigger(QString triggertext);
    QString getCurrentTrigger() {return m_trigger;}
    QString getCurrentPort() {return m_portName;}
    qint32 getCurrentBaudrate() {return m_baudrate;}
    QByteArray getFeedBack() {return m_feedback;}

    QString getCommand(int index);
    void setCommand(int index, const QString &command);

    void setHex(bool isHex);
    bool getHex() {return m_isHex;}
    bool sendCommand(const QString &text);
    bool sendCommand(const char *writedata);
    bool sendDispenseCommand();
    bool sendDispenseModeCommand();
    bool sendPressureUnitsCommand();
    bool sendSettingsCommand();
    bool sendCurrentInstructCommand(const QString &command);
    QString getCurrentState();

signals:

    void message(const QString &msg);
protected:
    bool loadConfig();
    bool saveConfig();
    friend QDataStream &operator>>(QDataStream &input, HQmlModelGlueController &obj);
    friend QDataStream &operator<<(QDataStream &output, const HQmlModelGlueController &obj);

private:
    QString m_portName;
    QStringList m_commands;
    bool m_isHex = true;
    qint32 m_baudrate = 115200;
    qint8 m_dispensemode = 1;  // 1: timed mode; 2: steady mode
    qint8 m_pressunits = 3;    // 1: psi; 2: bar; 3: kpa
    QString m_time;
    QString m_press;
    QString m_vac;
    QString m_trigger;
    QByteArray m_feedback;

};

#endif // HQMLMODELGLUECONTROLLER_H
