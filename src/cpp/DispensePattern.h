#ifndef DISPENSEPATTERN_H
#define DISPENSEPATTERN_H

#include "src/cpp/DispenseAction.h"

class DispensePattern
{
public:
    //DispensePattern() {m_actions.append(DispenseAction());}
    DispenseAction getDispenseAction(int index) {return m_actions[index];}
    void appendDispenseAction(DispenseAction action) {m_actions.append(action);}
    void removeDispenseAction(int index) {m_actions.removeAt(index);}
    void clearActions() {m_actions.clear();}

//protected:
    friend bool operator>>(QDataStream &input, DispensePattern &obj)
    {
        input >> obj.m_actions;
        return true;
    }
    friend bool operator<<(QDataStream &output, DispensePattern &obj)
    {
        output << obj.m_actions;
        return true;
    }
    friend bool operator<<(QDataStream &output, const DispensePattern &obj)
    {
        output << obj.m_actions;
        return true;
    }
private:
    //QString m_patternName = "pattern";
    QVector<DispenseAction> m_actions;
};

#endif // DISPENSEPATTERN_H
