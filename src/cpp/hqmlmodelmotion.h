﻿#ifndef HQMLMODELMOTION_H
#define HQMLMODELMOTION_H

#include <QObject>

#include "hmotioncontroller.h"

class HQmlModelMotion : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelMotion(QObject *parent = nullptr);
    ~HQmlModelMotion();

signals:

    void updateMotion(const QList<int> &states, const QList<QString> &positions, const QList<QString> &velocitys);

public slots:

    QString getSpeed(int axis) {return QString::number(m_speeds[axis], 'f', 3);}
    QString getDistance(int axis) {return QString::number(m_distances[axis], 'f', 4);}

    void setSpeed(int axis, const QString &text) {m_speeds[axis] = text.toDouble();}
    void setDistance(int axis, const QString &text) {m_distances[axis] = text.toDouble();}

    void startUpdateMotions(bool isStart);


    bool enableAxis(int axis, bool enable);
    bool moveRelative(int axis, bool dir);
    bool moveAbsolute(int axis);
    bool homeAxis(int axis);
    bool stopAxis(int axis);

protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlModelMotion &obj);
    friend bool operator<<(QDataStream &output, const HQmlModelMotion &obj);

private:
    QTimer *m_timer;

    HMotionController *m_motion;
    QVector<double> m_speeds = {10, 10, 10, 10};
    QVector<double> m_distances = {1, 1, 1, 1};

    const int m_axisIndexs[4] = {Axis_X, Axis_Y, Axis_Z, Axis_T};
};

#endif // HQMLMODELMOTION_H
