﻿#ifndef HQMLMODELSETTINGCOORD_H
#define HQMLMODELSETTINGCOORD_H

#include <QObject>
#include <QTransform>
#include <QDataStream>

class MPointXYZT
{
public:
    double x = 0.0;
    double y = 0.0;
    double z = 30;
    double t = 0.0;

    MPointXYZT(double px = 0.0, double py = 0.0, double pz = 30, double pt = 0.0): x(px), y(py), z(pz), t(pt)
    {

    }

    MPointXYZT &operator-(MPointXYZT &pt)
    {
        x -= pt.x;
        y -= pt.y;
        z -= pt.z;
        t -= pt.t;
        return *this;
    }

    MPointXYZT &operator+(MPointXYZT &pt)
    {
        x += pt.x;
        y += pt.y;
        z += pt.z;
        t += pt.t;
        return *this;
    }

    MPointXYZT &operator=(const QPointF &pt)
    {
        x = pt.x();
        y = pt.y();
        return *this;
    }

    MPointXYZT &operator/(double k)
    {
        x = x/k;
        y = y/k;
        z = z/k;
        t = t/k;
        return *this;
    }

    QPointF toPoint() const
    {
        return QPointF(x, y);
    }

    QStringList getStrs_XYZ() const {return QStringList{QString::number(x, 'f', 4), QString::number(y, 'f', 4), QString::number(z, 'f', 4)};}
    QStringList getStrs_XYT() const {return QStringList{QString::number(x, 'f', 4), QString::number(y, 'f', 4), QString::number(t, 'f', 4)};}
    QStringList getStrs_XYZT() const {return QStringList{QString::number(x, 'f', 4), QString::number(y, 'f', 4), QString::number(z, 'f', 4), QString::number(t, 'f', 4)};}

    friend  QDataStream &operator>>(QDataStream &input, MPointXYZT &obj)
    {
        return input >> obj.x >> obj.y >> obj.z >> obj.t;
    }

    friend  QDataStream &operator<<(QDataStream &output, const MPointXYZT &obj)
    {
        return output << obj.x << obj.y << obj.z << obj.t;
    }
};

class HMotionController;

class HQmlModelSettingCoord : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelSettingCoord(QObject *parent = nullptr);
    ~HQmlModelSettingCoord();

    MPointXYZT getClampingOffset();
    QTransform getCameraTrans();

    bool moveToClampingPos();
    bool grabClamping();
    bool releaseClamping();

    static HQmlModelSettingCoord *ptr;

signals:
    void updateAllPanelData(const QStringList &datas, const QVector<quint16> &alis);
    void computeCompleted(bool isOk);

public slots:
    void initPanelData();

    void computeCameraTrans();
    void computeClampingTrans();
    QStringList getAxisFPOS();
    void axisMoveTo(short type, int index);

    QStringList getGrabNames();
    void setGetIndex(int index);
    int getGetIndex();
    void setLetIndex(int index);
    int getLetIndex();
    float getCameraErrorX(){return m_cameraError.x();}
    float getCameraErrorY(){return m_cameraError.y();}

    void setControlData(int index, const QString &text);
    void setCameraAli(int index, quint16 value);

protected:
    friend QDataStream &operator>>(QDataStream &input, HQmlModelSettingCoord &obj);

    friend QDataStream &operator<<(QDataStream &output, const HQmlModelSettingCoord &obj);

private:

    bool loadConfig();
    bool saveConfig();


    void updatePanelData();
    bool getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali);
    bool moveToXYTWait(const MPointXYZT &pos);


private:

    bool m_isOK = false;

    QVector<MPointXYZT> m_markPoss;
    QVector<quint16> m_cameraAlgoris;

    QTransform m_transCamera;
    QPointF m_cameraError;

    QString m_getName, m_letName;
    QVector<MPointXYZT> m_clampingPoss;
    QVector<MPointXYZT> m_cameraPoss;

    double m_offsetX = 0, m_offsetY = 0, m_offsetA = 0, m_radius = 0;
    MPointXYZT m_clampingOffset;


    HMotionController *m_motion;

};

#endif // HQMLMODELSETTINGCOORD_H
