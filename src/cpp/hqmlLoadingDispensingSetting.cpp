#include "hqmlLoadingDispensingSetting.h"
#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QThread>
#include <iostream>
#include <string>
#include <sstream>

#include "hqmlmodelgluecontroller.h"
#include "hmotioncontroller.h"
#include "hqmlmodelio.h"
#include "QLog.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}

HQmlLoadingDispensingSetting *HQmlLoadingDispensingSetting::LoadingDispensePtr = nullptr;

HQmlLoadingDispensingSetting::HQmlLoadingDispensingSetting(QObject *parent) : QObject(parent){
    std::cout<<"construct the loading dispensing setting"<<std::endl;

    if (!loadConfig()) {
        m_LoadingDispenseData.resize(4);
        std::cout<<"Loading Module loading config fail!"<<std::endl;
    }
    if (m_LoadingDispenseData.size() != 4) m_LoadingDispenseData.resize(4);
    updateLoadingDispenseInfo();

    std::cout<<"m_loadingDispenseData size: "<<m_LoadingDispenseData.size() <<std::endl;
    for(int i = 0; i < m_LoadingDispenseData.size(); i++){
        std::cout<<"Dot "<<i<<" : "<<m_LoadingDispenseData[i].getStrs().value(9).toStdString()<<", " << m_LoadingDispenseData[i].getStrs().value(10).toStdString()<<std::endl;
    }

    map_gluingPar["DispenseMode"] = DispenseMode;
    map_gluingPar["WaitingZ"] = WaitingZ;
    map_gluingPar["GluingZ"] = GluingZ;
    map_gluingPar["MovingSpeed"] = MovingSpeed;
    map_gluingPar["MovingAcc"] = MovingAcc;
    map_gluingPar["GluingTime"] = GluingTime;
    map_gluingPar["GluingPressure"] = GluingPressure;
    map_gluingPar["X1"] = X1;
    map_gluingPar["Y1"] = Y1;
    map_gluingPar["X2"] = X2;
    map_gluingPar["Y2"] = Y2;
    map_gluingPar["ProtectionForce"] = ProtectionForce;

    for(int i = 0; i < m_LoadingDispenseData.size(); i++){
        m_LoadingDispenseData[i].setValue(map_gluingPar["DispenseMode"], 0);
    }

    LoadingDispensePtr = this;
}

HQmlLoadingDispensingSetting::~HQmlLoadingDispensingSetting(){
    std::cout<<"start saving loading dispensing config"<<std::endl;
    saveConfig();
}

void HQmlLoadingDispensingSetting::updateLoadingDispenseInfo()
{
    QStringList m_cfgData, m_gluingPos;
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(DispenseMode);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(WaitingZ);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(GluingZ);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(MovingSpeed);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(MovingAcc);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(GluingTime);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(GluingPressure);
    m_cfgData << m_LoadingDispenseData[0].getStrs().at(ProtectionForce);

    for(int i=0;i<4; i++){
        m_gluingPos << m_LoadingDispenseData[i].getStrs().at(X1);
        m_gluingPos << m_LoadingDispenseData[i].getStrs().at(Y1);
    }


    emit updateLoadingDispenseUI(m_cfgData, m_gluingPos, m_touchDownHeight);
}

QString HQmlLoadingDispensingSetting::getTouchDownValue(){
    return HQmlModelDispenseActionSettings::DispensePtr->getTouchDownValue();
}


void HQmlLoadingDispensingSetting::setGluingParameter(const QString &index, const QString &value){
    for(int i = 0; i < m_LoadingDispenseData.size(); i++){
        m_LoadingDispenseData[i].setValue(map_gluingPar[index], value.toDouble());
    }
    std::cout<<index.toStdString()<<", index: " << map_gluingPar[index] <<", value: " << value.toDouble() <<", " << m_LoadingDispenseData[0].getStrs().at(map_gluingPar[index]).toStdString()<<std::endl;
    saveConfig();
    updateLoadingDispenseInfo();
}

void HQmlLoadingDispensingSetting::setGluingPosition(int index, const QString &axis, const QString &value){
    if(axis == "x"){
        m_LoadingDispenseData[index-1].setValue(X1, value.toDouble());
        std::cout<<index<<", X1 index: " << map_gluingPar["X1"] <<", X1 value: " << value.toDouble() <<", " << m_LoadingDispenseData[index-1].getStrs().at(map_gluingPar["X1"]).toStdString()<<std::endl;

    }
    else if(axis == "y"){
        m_LoadingDispenseData[index-1].setValue(Y1, value.toDouble());
        std::cout<<index<<", Y1 index: " << map_gluingPar["Y1"] <<", Y1 value: " << value.toDouble() <<", " << m_LoadingDispenseData[index-1].getStrs().at(map_gluingPar["Y1"]).toStdString()<<std::endl;
    }

    saveConfig();
    updateLoadingDispenseInfo();
}

bool HQmlLoadingDispensingSetting::runLoadingDispensing(QPointF markPos){
    loadConfig();
    MPointXYZT moduleCentre;
    moduleCentre.x = markPos.x();
    moduleCentre.y = markPos.y();
    moduleCentre.z = 30.;
    moduleCentre.t = 0.;
    for(int i = 0; i < m_LoadingDispenseData.size(); i++){
        if (!m_LoadingDispenseData[i].runAction(moduleCentre)) return false;
    }
    return true;
}

bool HQmlLoadingDispensingSetting::loadConfig(){
    QString configFile = QCoreApplication::applicationDirPath() + "/user/LoadingDispensingSettingConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();

    return isok;
}



bool HQmlLoadingDispensingSetting::saveConfig(){
    std::cout<<"m_loadingDispenseData size: "<<m_LoadingDispenseData.size() <<std::endl;
    for(int i = 0; i < m_LoadingDispenseData.size(); i++){
        std::cout<<"Dot "<<i<<" : "<<m_LoadingDispenseData[i].getStrs().value(9).toStdString()<<", " << m_LoadingDispenseData[i].getStrs().value(10).toStdString()<<std::endl;
    }

    QString configFile = QCoreApplication::applicationDirPath() + "/user/LoadingDispensingSettingConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();

    return true;
}

bool operator>>(QDataStream &input, HQmlLoadingDispensingSetting &obj){
    input  >> obj.m_LoadingDispenseData >> obj.m_touchDownHeight;
    return true;
}

bool operator<<(QDataStream &output, const HQmlLoadingDispensingSetting &obj){
    output << obj.m_LoadingDispenseData << obj.m_touchDownHeight;
    return true;
}
