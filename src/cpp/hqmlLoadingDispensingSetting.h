#ifndef HQMLLOADINGDISPENSINGSETTING_H
#define HQMLLOADINGDISPENSINGSETTING_H

#include <QObject>
#include <QDataStream>

#include <map>
#include <iostream>

#include "src/cpp/DispenseAction.h"
#include "src/cpp/hqmlmodelsettingcoord.h"
#include "src/cpp/hqmlmodelDispenseActionSetting.h"
#include "src/cpp/hqmlLoadingCoordSetting.h"

class HMotionController;

class HQmlLoadingDispensingSetting : public QObject{
    Q_OBJECT
public:
    explicit HQmlLoadingDispensingSetting(QObject *parent = nullptr);
    ~HQmlLoadingDispensingSetting();

    static HQmlLoadingDispensingSetting *LoadingDispensePtr;

    enum gluingPar{
        DispenseMode,
        WaitingZ,
        GluingZ,
        MovingSpeed,
        MovingAcc,
        GluingSpeed,
        GluingAcc,
        GluingTime,
        GluingPressure,
        X1,
        Y1,
        X2,
        Y2,
        ProtectionForce
    };

    std::map<QString, gluingPar> map_gluingPar;
signals:
    void updateLoadingDispenseUI(QStringList cfgDatas, QStringList gluingPos, double touchDownZ);

public slots:
    void updateLoadingDispenseInfo();

    QString getTouchDownValue();

    void setGluingParameter(const QString &index, const QString &value);
    void setGluingPosition(int index, const QString &axis, const QString &value);

    bool runLoadingDispensing(QPointF markPos);

protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlLoadingDispensingSetting &obj);
    friend bool operator<<(QDataStream &output, const HQmlLoadingDispensingSetting &obj);


private:
    double m_touchDownHeight;

    QVector<DispenseAction> m_LoadingDispenseData;


};


#endif // HQMLLOADINGDISPENSINGSETTING_H
