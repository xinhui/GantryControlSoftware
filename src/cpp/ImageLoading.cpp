#include "ImageLoading.h"

namespace ImageLoading{

    // read any image
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, std::string imageName){
        ImageFiles images = {};
        std::cout<< "read other images" <<std::endl;
        std::string newloc = loc + "/" + imageName  + ImageUtils::format_str[static_cast<int>(format)];
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        std::cout<< "read images from: " + newloc <<std::endl;
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        cv::glob(newloc, images);
        if (images.size() == 0){
            std::cout << "File is empty!" << std::endl;
        }
        return images;
    }


    // read all images
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, ImageUtils::Type imageType){
        ImageFiles images = {};
        if (imageType == ImageUtils::Type::Warning){
            std::cout << "Warning: no type of images specified. Return null image list" << std::endl;
            return  images;
        }
        std::cout<< "read all images" <<std::endl;
        std::string newloc = loc + "/" + ImageUtils::type_str[static_cast<int>(imageType)] + "/*"  + ImageUtils::format_str[static_cast<int>(format)];
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        std::cout<< "read images from: " + newloc <<std::endl;
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        cv::glob(newloc, images);
        if (images.size() == 0){
            std::cout << "File is empty!" << std::endl;
        }
        return images;
    }


    // read single image
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, std::string num, ImageUtils::Type imageType){
        ImageFiles images = {};
        if (imageType == ImageUtils::Type::Warning){
            std::cout << "Warning: no type of images specified. Return null image list" << std::endl;
            return  images;
        }
        std::cout<< "read one image: " + num <<std::endl;
        std::string newloc = loc + "/" + ImageUtils::type_str[static_cast<int>(imageType)] + "/*" + num + ImageUtils::format_str[static_cast<int>(format)];
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        std::cout<< "read images from: " + newloc <<std::endl;
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        cv::glob(newloc, images);
        if (images.size() == 0){
            std::cout << "File is empty!" << std::endl;
        }
        else if (images.size() > 1){
            std::cout << "Warning: input #Single# option but read multiple images"<< std::endl;
        }
        return images;
    }

    // read mulitple images
    ImageFiles getImages(const std::string & loc, ImageUtils::Format format, ImageFiles imageArray, ImageUtils::Type imageType){
        ImageFiles images = {};
        if (imageType == ImageUtils::Type::Warning){
            std::cout << "Warning: no type of images specified. Return null image list" << std::endl;
            return  images;
        }
        std::cout<< "read multiple images " <<std::endl;
        std::string newloc = loc + "/" + ImageUtils::type_str[static_cast<int>(imageType)] + "/*" + ImageUtils::format_str[static_cast<int>(format)];
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        std::cout<< "read images from: " + newloc + ", *: ";
        for (int i = 0; i < imageArray.size(); i++){
            if (i < imageArray.size()-1){
                std::cout << imageArray.at(i) << ", ";
            }
            if (i == imageArray.size()-1){
                std::cout << imageArray.at(i);
            }
        }
        std::cout<< " " << std::endl;
        std::cout<< "-------------------------------------------------------------------------------- " <<std::endl;
        for (int i = 0; i < imageArray.size(); i++){
            std::string readloc = loc + "/" + ImageUtils::type_str[static_cast<int>(imageType)] + "/*" + imageArray.at(i) + ImageUtils::format_str[static_cast<int>(format)];
            ImageFiles tempFile;
            cv::glob(readloc, tempFile);
            if (tempFile.size() == 0){
                std::cout<< "Can not find image: " << imageArray.at(i) <<std::endl;
            }
            for (int i = 0; i < tempFile.size(); i++){
                images.push_back(tempFile.at(i));
            }
        }

        if (images.size() == 0){
            std::cout << "File is empty!" << std::endl;
        }
        else if (images.size() == 1){
            std::cout << "Warning: input #Multiple# option but read Single image"<< std::endl;
        }

        return images;
    }


    // read a image from the files vec
    cv::Mat readImage(std::string loc){
        cv::Mat dst = {};
        dst = cv::imread(loc);
        if( dst.empty()){
            std::cout<<"Could not load image at : "<< loc <<std::endl;
            return dst;
        }
        return dst;
    }
}

