#ifndef HQMLMODELSETTINGFLEX_H
#define HQMLMODELSETTINGFLEX_H
#include <QObject>
#include <QTransform>
#include <QDataStream>
#include "hqmlmodelsettingcoord.h"
#include "QLog.h"

class MPointXYZT;
class HMotionController;

class HQmlModelSettingFlex : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelSettingFlex(QObject *parent = nullptr);
    ~HQmlModelSettingFlex();

    static HQmlModelSettingFlex *ptr;

signals:
    void updateFlexPanelData(const QStringList &datas, const QVector<quint16> &alis);
    void computeCompleted(bool isOk);

public slots:
    void initPanelData();

    void computeFlexPos();
    void computeFlexOffset();
    QStringList getAxisFPOS();
    void axisMoveTo(short type, int index);

    //function to get the flex calibration result, which can be used for display and other calculation
    double getFlexCalibrationResult(int index);


    void setFlexData(int index, const QString &text);
    void setFlexCameraAli(int index, quint16 value);

protected:
    friend QDataStream &operator>>(QDataStream &input, HQmlModelSettingFlex &obj);

    friend QDataStream &operator<<(QDataStream &output, const HQmlModelSettingFlex &obj);

private:

    bool loadConfig();
    bool saveConfig();


    void updatePanelData();
    bool getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali);
    bool moveToXYTWait(const MPointXYZT &pos);


private:

    bool m_isOK = false;

    QVector<MPointXYZT> m_markFlexPoss;
    QVector<quint16> m_cameraFlexAlgoris;

    QPointF m_flexCenter;
    double m_centerDistance;
    double m_offsetAngle;

    HMotionController *m_motion;

};

#endif // HQMLMODELSETTINGFLEX_H
