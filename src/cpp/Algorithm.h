#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <QObject>
#include <QTransform>
#include <QDataStream>
#include <cmath>
#include <iostream>

#include "hqmlmodelsettingcoord.h"

#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>



#define PI acos(-1)


class Sensor{
    public:
        Sensor();
        QVector<QPointF> points;
        QVector<QPointF> searchPoints;

        QPointF centre();
        double rotAngle();
        double width();
        double height();
        double focusHeight = 0.;

protected:
    friend QDataStream &operator>>(QDataStream &input, Sensor &obj);

    friend QDataStream &operator<<(QDataStream &output, const Sensor &obj);


};

class Flex{
    public:
        Flex();
        QVector<QPointF> highVolt;
        QVector<QPointF> searchPoints;

        QPointF markHV1();
        double rotAngle();
        double focusHeight = 0.;

        void setPoint(int _index, double _value);

protected:
    friend QDataStream &operator>>(QDataStream &input, Flex &obj);

    friend QDataStream &operator<<(QDataStream &output, const Flex &obj);


};

cv::Mat diskStrel(int radius);


#endif // ALGORITHM_H
