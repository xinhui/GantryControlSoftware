#ifndef QWRITEELEMENT_H
#define QWRITEELEMENT_H


#include <QObject>
#include <QFile>
#include <QThread>
#include <QMutex>

class QWriteElement : public QObject
{
    Q_OBJECT
public:
    explicit QWriteElement(QObject *parent = nullptr);

public:
    // set file name
    void SetFileName(const QString &fileName);
    // write message
    void WriteMessage(const QString &message);
    // close writing
    void Close();
    // get log file size
    qint64 GetLogSize() const;

signals:
    // change file name
    void FileNameChanged(const QString &name);
    // used for changing thread
    void MessageReady(const QString &message);

private slots:
    // receive log message
    void OnMessageReady(const QString &mesage);
    // when file name changed
    void OnFileNameChanged(const QString &fileName);

private:
    // standalone thread
    QThread m_thread;
    // log file
    QFile *m_pLogfile;
    // for locking file
    QMutex m_lock;
};




#endif // QWRITEELEMENT_H
