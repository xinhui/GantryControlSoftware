#include <cmath>
#include <sstream>
#include <string>
#include <iostream>
#include "src/cpp/DispenseAction.h"
#include "hqmlmodelsettingcoord.h"
#include "hqmlmodelgluecontroller.h"
#include "hmotioncontroller.h"
#include "hqmlmodelio.h"
#include "QLog.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}
#define CMoveRel(axis,pos,speed,acc,isWait) if(motion->moveRel(axis, pos, speed, acc, isWait) == false) {return false;}
QMessageLogContext context_dispensingaction;
bool DispenseAction::setDispenseSettings()
{
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "start setting dispensing settings");
    HQmlModelGlueController* dispenser = HQmlModelGlueController::dispenserPtr;
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("set current dispensing mode: %1 ").arg(m_actionVarObj.modeIndex + 1));
    dispenser->setCurrentDispenseMode(m_actionVarObj.modeIndex + 1);
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("set current dispensing time: %1 ").arg(m_actionVarObj.gluingTime));
    dispenser->setCurrentDispenseTime(QString::number(m_actionVarObj.gluingTime, 'f', 4));
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("set current dispensing time: %1 ").arg(m_actionVarObj.gluingTime));
    dispenser->setCurrentPress(QString::number(m_actionVarObj.gluingPress,'f',4));

    return dispenser->sendDispenseModeCommand() && dispenser->sendSettingsCommand();
}

bool DispenseAction::runAction(MPointXYZT offset)
{
    std::ostringstream oss;
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "start running action");
    double delta_x = m_actionVarObj.x2 - m_actionVarObj.x1;
    double delta_y = m_actionVarObj.y2 - m_actionVarObj.y1;
    double speedX = m_actionVarObj.gluingSpeed * fabs(delta_x/sqrt(delta_x*delta_x + delta_y*delta_y));
    double speedY = m_actionVarObj.gluingSpeed * fabs(delta_y/sqrt(delta_x*delta_x + delta_y*delta_y));
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "got all parameters for running action");
    HMotionController *motion = HMotionController::GetInstance();
    CMoveAbs(Axis_Z, m_actionVarObj.waitingz, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to waitingz: %1 ").arg(m_actionVarObj.waitingz));
    CMoveAbs(Axis_X, m_actionVarObj.x1 + offset.x, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to dispensing x1: %1 ").arg(m_actionVarObj.x1 + offset.x));
    CMoveAbs(Axis_Y, m_actionVarObj.y1 + offset.y, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to dispensing y1: %1 ").arg(m_actionVarObj.y1 + offset.y));
    HQmlModelIO::ioPtr->zeroForceSensor();
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "zeroed pressure sensor");
    CMoveAbs(Axis_Z, m_actionVarObj.gluingz, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, false)
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to gluingz: %1 ").arg(m_actionVarObj.gluingz));
    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto ok;
        }
        if (!motion->getForceValue(&force)) {
            QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "fail to get force value");
            goto error;
        }
        if (force > m_actionVarObj.protectionForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(500);
            goto ok;
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }

    }

ok:
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "moved to gluingz ok");
    if (m_actionVarObj.modeIndex == 0)
    {
        QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "running timed dispensing mode");
        if (!HQmlModelGlueController::dispenserPtr->sendDispenseCommand()) {
            QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "failed to send dispensing command");
            goto error;
        }
        QThread::msleep(m_actionVarObj.gluingTime*1000 + 1000); // +1s
        QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("waiting %1 ms for gluing").arg(m_actionVarObj.gluingTime*1000 + 1000));
        CMoveAbs(Axis_Z, m_actionVarObj.waitingz, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, true)
        QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to waiting z: %1").arg(m_actionVarObj.waitingz));
        return true;
    }
    else if (m_actionVarObj.modeIndex == 1) {
        QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "running steady dispensing mode");
        if (!HQmlModelGlueController::dispenserPtr->sendDispenseCommand()) {
            QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "failed to send dispensing command");
            goto error;
        }
        CMoveAbs(Axis_X, m_actionVarObj.x2 + offset.x, speedX, m_actionVarObj.gluingAcc, false)
        CMoveAbs(Axis_Y, m_actionVarObj.y2 + offset.y, speedY, m_actionVarObj.gluingAcc, false)
        QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moving to (x2,y2): (%1,%2)").arg(m_actionVarObj.x2,m_actionVarObj.y2));
        while (true) {
            if (!motion->isAxisMove(Axis_X) && !motion->isAxisMove(Axis_Y)) {
                if (!HQmlModelGlueController::dispenserPtr->sendDispenseCommand())
                {
                    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "failed to send dispensing command");
                    goto error;
                }
                else
                    return true;
            }
            if (!motion->getForceValue(&force)) {
                QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "fail to get force value");
                goto error;
            }
            if (force > m_actionVarObj.protectionForce) {
                motion->killAxis(Axis_X);
                motion->killAxis(Axis_Y);
                QThread::msleep(500);
                goto error;
            }
            if (QThread::currentThread()->isInterruptionRequested()) {
                goto error;
            }

        }
    }
    else {
        goto error;
    }

error:
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "moved to run gluing action error");
    motion->stopAxis(Axis_Z);
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, "stopped z axis");
    //motion->setDigitalOutput(2, configs.outputIndex, 0);
    CMoveAbs(Axis_Z, m_actionVarObj.waitingz, m_actionVarObj.movingSpeed, m_actionVarObj.movingAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_dispensingaction, QObject::tr("moved to waiting z: %1").arg(m_actionVarObj.waitingz));
    return false;
}
