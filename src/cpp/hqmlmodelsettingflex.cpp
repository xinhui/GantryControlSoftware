#include "hqmlmodelsettingflex.h"
#include "hmathfunctions.h"
#include "hqmlmodelcamera.h"
#include "hmotioncontroller.h"
#include "hqmlmodelCapture.h"
#include "hqmlmodelflow.h"
#include "hqmlmodelCapture.h"
#include "QLog.h"

#include <QtConcurrent>
#include <QFutureWatcher>

QMessageLogContext context;

HQmlModelSettingFlex *HQmlModelSettingFlex::ptr = nullptr;
HQmlModelSettingFlex::HQmlModelSettingFlex(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model Setting Flex"<<std::endl;

    QLog::LogMessageOutput(QtInfoMsg,context, "construct Flex Setting");
    m_motion = HMotionController::GetInstance();
    if (!loadConfig()) {
        m_markFlexPoss.resize(5);
        m_cameraFlexAlgoris.fill(0,5);
    }
    if (m_markFlexPoss.size() != 5)  m_markFlexPoss.resize(5);
    if (m_cameraFlexAlgoris.size() != 5) m_cameraFlexAlgoris.fill(0,5);
    ptr = this;
}

HQmlModelSettingFlex::~HQmlModelSettingFlex()
{
    if (saveConfig()) std::cout << "saving SettingCoord config" << std::endl;
}


void HQmlModelSettingFlex::initPanelData()
{
    QLog::LogMessageOutput(QtInfoMsg,context, "flex panel data initializition");
    updatePanelData();
}

void HQmlModelSettingFlex::computeFlexPos()
{
    std::ostringstream oss;
    QLog::LogMessageOutput(QtInfoMsg,context, "compute the flex position");
    QVector<QPointF> flexMarkPos;
    MPointXYZT cameraResult;
    MPointXYZT desPos;
    auto offset = HQmlModelSettingCoord::ptr->getClampingOffset();
    for (int i = 0; i<4; i++)
    {
        if (!getCameraPos(cameraResult, m_markFlexPoss[i], m_cameraFlexAlgoris[i]))
            goto error;
        desPos = cameraResult + offset;
        std::cout << "camera" << i << ".x = " << cameraResult.x << std::endl;
        std::cout << "camera" << i << ".y = " << cameraResult.y << std::endl;
        std::cout << "desPos" << i << ".x = " << desPos.x << std::endl;
        std::cout << "desPos" << i << ".y = " << desPos.y << std::endl;
        flexMarkPos.append(QPointF(desPos.x, desPos.y));
    }
    QLineF line1, line2;
    line1.setPoints(flexMarkPos[0],flexMarkPos[3]);
    line2.setPoints(flexMarkPos[1], flexMarkPos[2]);
    QPointF intersectP;
    line1.intersects(line2, &intersectP);
    m_flexCenter = intersectP;
    QLineF lineFlex;
    lineFlex.setPoints(0.5*flexMarkPos[0]+0.5*flexMarkPos[2], 0.5*flexMarkPos[1]+0.5*flexMarkPos[2]);
    m_offsetAngle = lineFlex.angle();
    emit computeCompleted(true);
    error:
    QLog::LogMessageOutput(QtInfoMsg,context, "failed to compute the flex position");
    emit computeCompleted(false);
    return;
}
void HQmlModelSettingFlex::computeFlexOffset()
{
    std::ostringstream oss;
    QLog::LogMessageOutput(QtInfoMsg,context, "compute the flex offset");
    computeFlexPos();
    QPointF head_center = QPointF(m_markFlexPoss[4].x, m_markFlexPoss[4].y);
    QLineF lineC2C;  //center to center line
    lineC2C.setPoints(head_center,m_flexCenter);
    m_centerDistance = lineC2C.length();
    MPointXYZT FlexPos;
    double x = m_flexCenter.x();
    double y = m_flexCenter.y();
    double d = m_centerDistance;
    double theta = m_offsetAngle;
    FlexPos.x = x - d * sin(theta);
    FlexPos.y = y + d * cos(theta);
    FlexPos.z = 70.;
    FlexPos.t = -theta;
    std::cout << "header.x = " << FlexPos.x << std::endl;
    std::cout << "header.y = " << FlexPos.y << std::endl;
    std::cout << "delta x = " << FlexPos.x - m_markFlexPoss[4].x << std::endl;
    std::cout << "delta y = " << FlexPos.y - m_markFlexPoss[4].y << std::endl;
    oss << "get flex head position: (" << FlexPos.x << "," << FlexPos.y << ")";
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    emit computeCompleted(true);
}


QStringList HQmlModelSettingFlex::getAxisFPOS()
{
    QLog::LogMessageOutput(QtInfoMsg,context, "get current axis position");
    return QStringList{QString::number(m_motion->getAxisPosition(Axis_X), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Y), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_Z), 'f', 4), QString::number(m_motion->getAxisPosition(Axis_T), 'f', 4)};
}

void HQmlModelSettingFlex::axisMoveTo(short type, int index)
{
    std::ostringstream oss;
    oss << "move to flex index " << index;
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    switch (type) {
    case 3: {
        m_motion->moveAbs1(Axis_Z, m_markFlexPoss[index].z,true);
        m_motion->moveAbs1(Axis_X, m_markFlexPoss[index].x);
        m_motion->moveAbs1(Axis_Y, m_markFlexPoss[index].y);     
    }
    default:
        break;
    }
}


void HQmlModelSettingFlex::setFlexData(int index, const QString &text)
{
    std::ostringstream oss;
    oss << "set flex position data " << index;
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    if (index % 3 == 0) {
        std::cout << "before: m_markFlexPoss[" << index / 3 << "].x = " << m_markFlexPoss[index /3].x << std::endl;
        m_markFlexPoss[index / 3].x = text.toDouble();
        std::cout << "after: m_markFlexPoss[" << index / 3 << "].x = " << m_markFlexPoss[index /3].x << std::endl;
    } else if (index % 3 == 1) {
        m_markFlexPoss[index / 3].y = text.toDouble();
    } else {
        m_markFlexPoss[index / 3].z = text.toDouble();
    }
}

void HQmlModelSettingFlex::setFlexCameraAli(int index, quint16 value)
{
    std::ostringstream oss;
    oss << "set flex camera algorithm " << index;
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    m_cameraFlexAlgoris[index] = value;
}

bool HQmlModelSettingFlex::loadConfig()
{
    std::cout << "load SetttingCoord config" << std::endl;
    QLog::LogMessageOutput(QtInfoMsg,context, "load flex setting config file");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/flexSettingConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();
    QLog::LogMessageOutput(QtInfoMsg,context, "load flex setting config file successfully");
    return isok;
}

bool HQmlModelSettingFlex::saveConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context, "saving flex setting config file");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/flexSettingConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    QLog::LogMessageOutput(QtInfoMsg,context, "saving flex setting config file successfully");
    return true;
}

void HQmlModelSettingFlex::updatePanelData()
{
    QStringList list2;
    for (const auto &k : qAsConst(m_markFlexPoss)) {
        list2 << k.getStrs_XYZ();
    }
    list2 << QString::number(m_flexCenter.x(), 'f', 4) << QString::number(m_flexCenter.y(), 'f', 4);
    list2 << QString::number(m_centerDistance, 'f', 4) << QString::number(m_offsetAngle, 'f', 4);
    QLog::LogMessageOutput(QtInfoMsg,context, "update flex panel data");
    emit updateFlexPanelData(list2, m_cameraFlexAlgoris);
}

bool HQmlModelSettingFlex::getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali)
{
    QLog::LogMessageOutput(QtInfoMsg,context, "run get camera position");
    static quint16 kkkkk = -1;
    if (!m_motion->moveAbs1(Axis_X, pt.x))
        return false;
    if (!m_motion->moveAbs1(Axis_Y, pt.y))
        return false;
    if (!m_motion->waitAllAxisStop())
        return false;
    if (!m_motion->moveAbs1(Axis_Z, pt.z, true))
        return false;
    QThread::msleep(500);
    QStringList cameraResult;
    if (kkkkk != ali) {
        if (HQmlModelCamera::execCommond(QString("PW,1,%1").arg(ali, 3, 10, QLatin1Char('0')), cameraResult))
            return false;
        if (cameraResult.size() != 1)
            return false;
        kkkkk = ali;
    }
    if (HQmlModelCamera::execCommond("T1", cameraResult))
        return false;
    if (cameraResult.size() < 4)
        return false;
    if (!cameraResult.first().toInt())
        return false;

    pos.x = cameraResult[1].toDouble() - 1216;
    pos.y = cameraResult[2].toDouble() - 1020;
    pos.t = cameraResult[3].toDouble();

    std::ostringstream oss;
    oss << "get camera position(x,y,t): (" << pos.x << "," << pos.y << "," << pos.t << ")";
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    return true;
}

double HQmlModelSettingFlex::getFlexCalibrationResult(int index)
{
    std::ostringstream oss;
    QVector<double> results;
    results.push_back(m_flexCenter.x());
    results.push_back(m_flexCenter.y());
    results.push_back(m_centerDistance);
    results.push_back(m_offsetAngle);
    if (index < 4)
    {
        oss << "get flex calibration result " << index << ":" << results[index];
        QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
        return results[index];
    }
    else
    {
        QLog::LogMessageOutput(QtInfoMsg,context, "WRONG index, get no flex calibration result (return 999)");
        return -999.;
    }
}

bool HQmlModelSettingFlex::moveToXYTWait(const MPointXYZT &pos)
{
    std::ostringstream oss;
    oss << "moving to XYT: (" << pos.x << "," << pos.y << "," << pos.t << ")";
    QLog::LogMessageOutput(QtInfoMsg,context, QString::fromStdString(oss.str()));
    if (!m_motion->moveAbs1(Axis_X, pos.x))
    {
        QLog::LogMessageOutput(QtInfoMsg,context, "failed to move to X");
        return false;
    }
    if (!m_motion->moveAbs1(Axis_Y, pos.y))
    {
        QLog::LogMessageOutput(QtInfoMsg,context, "failed to move to Y");
        return false;
    }
    if (!m_motion->moveAbs1(Axis_T, pos.t))
    {
        QLog::LogMessageOutput(QtInfoMsg,context, "failed to move to T");
        return false;
    }
    return m_motion->waitAllAxisStop();
}

QDataStream &operator>>(QDataStream &input, HQmlModelSettingFlex &obj)
{
    return input >> obj.m_markFlexPoss >> obj.m_cameraFlexAlgoris >> obj.m_flexCenter >> obj.m_centerDistance >> obj.m_offsetAngle;
}

QDataStream &operator<<(QDataStream &output, const HQmlModelSettingFlex &obj)
{
    std::cout << "obj.m_markFlexPoss[0].x = " << obj.m_markFlexPoss[0].x << std::endl;
    return output << obj.m_markFlexPoss << obj.m_cameraFlexAlgoris << obj.m_flexCenter << obj.m_centerDistance << obj.m_offsetAngle;
}
