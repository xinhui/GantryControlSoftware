﻿
#include "hqmlmodeleditor.h"

#include <QFile>
#include <QFileInfo>
#include <QFileSelector>
#include <QQmlFile>
#include <QQmlFileSelector>
#include <QQuickTextDocument>
#include <QTextCharFormat>
#include <QTextDocument>
#include <QDebug>
#include <QSettings>

HQmlModelEditor::HQmlModelEditor(QObject *parent)
    : QObject(parent)
    , m_document(nullptr)
    , m_cursorPosition(-1)
    , m_selectionStart(0)
    , m_selectionEnd(0)
{

}

QQuickTextDocument *HQmlModelEditor::document() const
{
    return m_document;
}

void HQmlModelEditor::setDocument(QQuickTextDocument *document)
{
    if (document == m_document)
        return;

    m_document = document;
    emit documentChanged();
}

int HQmlModelEditor::cursorPosition() const
{
    return m_cursorPosition;
}

void HQmlModelEditor::setCursorPosition(int position)
{
    if (position == m_cursorPosition)
        return;

    m_cursorPosition = position;
    reset();
    emit cursorPositionChanged();
}

int HQmlModelEditor::selectionStart() const
{
    return m_selectionStart;
}

void HQmlModelEditor::setSelectionStart(int position)
{
    if (position == m_selectionStart)
        return;

    m_selectionStart = position;
    emit selectionStartChanged();
}

int HQmlModelEditor::selectionEnd() const
{
    return m_selectionEnd;
}

void HQmlModelEditor::setSelectionEnd(int position)
{
    if (position == m_selectionEnd)
        return;

    m_selectionEnd = position;
    emit selectionEndChanged();
}

QString HQmlModelEditor::fontFamily() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void HQmlModelEditor::setFontFamily(const QString &family)
{
    QTextCharFormat format;
    format.setFontFamily(family);
    mergeFormatOnWordOrSelection(format);
    emit fontFamilyChanged();
}

QColor HQmlModelEditor::textColor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void HQmlModelEditor::setTextColor(const QColor &color)
{
    QTextCharFormat format;
    format.setForeground(QBrush(color));
    mergeFormatOnWordOrSelection(format);
    emit textColorChanged();
}

Qt::Alignment HQmlModelEditor::alignment() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return textCursor().blockFormat().alignment();
}

void HQmlModelEditor::setAlignment(Qt::Alignment alignment)
{
    QTextBlockFormat format;
    format.setAlignment(alignment);
    QTextCursor cursor = textCursor();
    cursor.mergeBlockFormat(format);
    emit alignmentChanged();
}

bool HQmlModelEditor::bold() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontWeight() == QFont::Bold;
}

void HQmlModelEditor::setBold(bool bold)
{
    QTextCharFormat format;
    format.setFontWeight(bold ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(format);
    emit boldChanged();
}

bool HQmlModelEditor::italic() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontItalic();
}

void HQmlModelEditor::setItalic(bool italic)
{
    QTextCharFormat format;
    format.setFontItalic(italic);
    mergeFormatOnWordOrSelection(format);
    emit italicChanged();
}

bool HQmlModelEditor::underline() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontUnderline();
}

void HQmlModelEditor::setUnderline(bool underline)
{
    QTextCharFormat format;
    format.setFontUnderline(underline);
    mergeFormatOnWordOrSelection(format);
    emit underlineChanged();
}

int HQmlModelEditor::fontSize() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void HQmlModelEditor::setFontSize(int size)
{
    if (size <= 0)
        return;

    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    if (cursor.charFormat().property(QTextFormat::FontPointSize).toInt() == size)
        return;

    QTextCharFormat format;
    format.setFontPointSize(size);
    mergeFormatOnWordOrSelection(format);
    emit fontSizeChanged();
}

QString HQmlModelEditor::fileContent() const
{
    return text;
}

QString HQmlModelEditor::fileName() const
{

    const QString filePath = QQmlFile::urlToLocalFileOrQrc(m_fileUrl);

    const QString fileName = QFileInfo(filePath).fileName();
    if (fileName.isEmpty())
        return QStringLiteral("untitled.pgm");
    return fileName;
}

QString HQmlModelEditor::filePath() const
{
    return QQmlFile::urlToLocalFileOrQrc(m_fileUrl);
}

QString HQmlModelEditor::fileType() const
{
    return QFileInfo(fileName()).suffix();
}

QUrl HQmlModelEditor::fileUrl() const
{
    return m_fileUrl;
}



void HQmlModelEditor::load(const QUrl &fileUrl)
{
    //    if (fileUrl == m_fileUrl)
    //        return;



//    QQmlEngine *engine = qmlEngine(this);
//    if (!engine) {
//        qWarning() << "load() called before HQmlModelEditor has QQmlEngine";
//        return;
//    }

//    const QUrl path = QQmlFileSelector::get(engine)->selector()->select(fileUrl);
//    const QString fileName = QQmlFile::urlToLocalFileOrQrc(path);
//    if (QFile::exists(fileName)) {
//        QFile file(fileName);
//        if (file.open(QFile::ReadOnly)) {
//            QByteArray data = file.readAll();
//            text = tr(data);
//            QTextCodec *codec = QTextCodec::codecForHtml(data);
//            if (QTextDocument *doc = textDocument())
//                doc->setModified(false);

//            QSettings settings("AOC", "userData");
//            settings.setValue("Laser/A3200proDefaultOpenDir", fileName);

//            m_fileUrl = fileUrl;
//            emit fileUrlChanged();
//            emit loaded(codec->toUnicode(data));
//            reset();
//        }
//    }


}

void HQmlModelEditor::saveAs(const QUrl &fileUrl)
{
    QTextDocument *doc = textDocument();
    if (!doc)
        return;

    const QString filePath = fileUrl.toLocalFile();
    const bool isHtml = QFileInfo(filePath).suffix().contains(QLatin1String("htm"));
    QFile file(filePath);
    if (!file.open(QFile::WriteOnly | QFile::Truncate | (isHtml ? QFile::NotOpen : QFile::Text))) {
        emit error(tr("Cannot save: ") + file.errorString());
        return;
    }
    file.write((isHtml ? doc->toHtml() : doc->toPlainText()).toUtf8());
    file.close();

    if (fileUrl == m_fileUrl)
        return;

    QSettings settings("AOC", "userData");
    settings.setValue("Laser/A3200proDefaultSaveDir", filePath);

    m_fileUrl = fileUrl;
    emit fileUrlChanged();
}

QString HQmlModelEditor::getDefaultDir(int type)
{

    QSettings settings("AOC", "userData");
    if (type == 0)
        return settings.value("Laser/A3200proDefaultOpenDir").toString();
    else
        return settings.value("Laser/A3200proDefaultSaveDir").toString();
}

void HQmlModelEditor::reset()
{
    emit fontFamilyChanged();
    emit alignmentChanged();
    emit boldChanged();
    emit italicChanged();
    emit underlineChanged();
    emit fontSizeChanged();
    emit textColorChanged();
}

QTextCursor HQmlModelEditor::textCursor() const
{
    QTextDocument *doc = textDocument();
    if (!doc)
        return QTextCursor();

    QTextCursor cursor = QTextCursor(doc);
    if (m_selectionStart != m_selectionEnd) {
        cursor.setPosition(m_selectionStart);
        cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    } else {
        cursor.setPosition(m_cursorPosition);
    }
    return cursor;
}

QTextDocument *HQmlModelEditor::textDocument() const
{
    if (!m_document)
        return nullptr;

    return m_document->textDocument();
}

void HQmlModelEditor::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
}
