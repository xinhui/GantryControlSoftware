#include "hqmlmodelgluecontroller.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QTime>
#include <QDataStream>
#include <QCoreApplication>
#include <QFile>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "QLog.h"

using namespace std;

const string DISPENSE_COMMAND = "DI\x20";
const string MEMORY_CHANGE_COMMAND = "CH\x20\x20";
const string TIMED_MODE_COMMAND = "TT\x20\x20";
const string STEADY_MODE_COMMAND = "MT\x20\x20";
const string PRESSURE_SET_COMMAND = "PS\x20\x20";
const string VACUUM_SET_COMMAND = "VS\x20\x20";
const string TIME_SET_COMMAND = "DS\x20\x20T";
const string PRESSURE_UNITS_SET_COMMAND = "E6\x20\x20";
const string VACUUM_UNITS_SET_COMMAND = "E7\x20\x20";
const string SET_TRIGGER_VALUE_COMMAND = "EQ\x20\x20T";
const string MEMORY_LOCATION_FEEDBACK_COMMAND = "UA\x20\x20\x20";
const string PRESSURE_UNITS_FEEDBACK_COMMAND = "E4\x20\x20";
const string MEM_PRE_TIME_FEEDBACK_COMMAND = "UD\x20\x20\x20";
const string VACUUM_UNITS_FEEDBACK_COMMAND = "E5\x20\x20";
const string PRE_TIME_VACUUM_FEEDBACK_COMMAND = "E8";
const string TRIGGER_VALUE_FEEDBACK_COMMAND = "ER\x20\x20";
const string ACK = "\x06";

string replaceDashWithHex(string replacedstr);
qint16 CheckSum(std::string str);
string CharToAscii(quint8 hex);
string GetCommandSequence(std::string command);
QString ConvertCurrentDispenseTime(QString timetext);
QString ConvertCurrentPress(QString presstext,  qint8 unit);
QString ConvertCurrentVaccum(QString vactext);
QString ConvertCurrentTrigger(QString triggertext);

HQmlModelGlueController *HQmlModelGlueController::dispenserPtr = nullptr;
QMessageLogContext context_gluecontroller;

HQmlModelGlueController::HQmlModelGlueController(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model Glue Controller"<<std::endl;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "construct dispensing controller");
    m_commands << "" << "" << "" << "" << "" << "";
    loadConfig();
    dispenserPtr = this;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "construct dispensing controller successfully");
}

HQmlModelGlueController::~HQmlModelGlueController()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "destroy dispensing controller");
    saveConfig();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "destroy dispensing controller successfully");
}

QStringList HQmlModelGlueController::getPortNames()
{
    QStringList names;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        names << info.portName();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "get port names successfully");
    return names;
}

void HQmlModelGlueController::setCurrentPort(const QString &port)
{
    std::ostringstream oss;
    m_portName = port;
    oss << "set current port name: " << m_portName.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

void HQmlModelGlueController::setCurrentBaudRate(qint32 baudrate)
{
    std::ostringstream oss;
    m_baudrate = baudrate;
    oss << "set current baudrate: " << m_baudrate;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}
void HQmlModelGlueController::setCurrentDispenseMode(qint8 dispensemode)
{
    std::ostringstream oss;
    m_dispensemode = dispensemode;
    oss << "set current dispense mode: " << m_dispensemode;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}
void HQmlModelGlueController::setCurrentPressUnits(qint8 unit)
{
    m_pressunits = unit;
    std::ostringstream oss;
    oss << "set current pressure unit: " << m_pressunits;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

QString ConvertCurrentDispenseTime(QString timetext)
{
    std::stringstream ss;
    double tmp_double = timetext.toDouble() * 10000;
    int tmp_int = (int)tmp_double;
    ss << std::setw(5) << std::setfill('0') << tmp_int;
    //std::cout << ss.str() << std::endl;
    return QString::fromStdString(ss.str());
}

QString ConvertCurrentPress(QString presstext,  qint8 unit)
{
    int tmp_int(0);
    double tmp_double(0.);
    std::stringstream ss;
    if (unit == 1)
    {
        tmp_double = presstext.toDouble() * 10;
    }
    else if (unit == 2)
    {
        tmp_double = presstext.toDouble() * 1000;
    }
    else if (unit ==3)
    {
        tmp_double = presstext.toDouble() * 10;
    }
    tmp_int = (int)tmp_double;
    ss << std::setw(4) << std::setfill('0') << tmp_int;
    return QString::fromStdString(ss.str());

}

QString ConvertCurrentVaccum(QString vactext)
{
    std::stringstream ss;
    double tmp_double = vactext.toDouble() * 10;
    int tmp_int = (int)tmp_double;
    ss << std::setw(4) << std::setfill('0') << tmp_int;
    //std::cout << ss.str() << std::endl;
    return QString::fromStdString(ss.str());
}
QString ConvertCurrentTrigger(QString triggertext)
{
    stringstream ss;
    ss << std::setw(5) << std::setfill('0') << triggertext.toUInt();
    return QString::fromStdString(ss.str());
}

QString HQmlModelGlueController::getCommand(int index)
{
    std::ostringstream oss;
    oss << "get command index: " << index;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
    return m_commands.value(index);
}

void HQmlModelGlueController::setCommand(int index, const QString &command)
{
    m_commands[index] = command;
    std::ostringstream oss;
    oss << "set command index: " << index << "= " << command.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

void HQmlModelGlueController::setCurrentDispenseTime(QString timetex)
{
    m_time = timetex;
    std::ostringstream oss;
    oss << "set current dispensing time text: " << timetex.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

void HQmlModelGlueController::setCurrentPress(QString presstext)
{
    m_press = presstext;
    std::ostringstream oss;
    oss << "set current dispensing pressure text: " << presstext.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

void HQmlModelGlueController::setCurrentVaccum(QString vactext)
{
    m_vac = vactext;
    std::ostringstream oss;
    oss << "set current vaccum text: " << vactext.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}
void HQmlModelGlueController::setCurrentTrigger(QString triggertext)
{
    m_trigger = triggertext;
    std::ostringstream oss;
    oss << "set current trigger text: " << triggertext.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}

void HQmlModelGlueController::setHex(bool isHex)
{
    m_isHex = isHex;
    std::ostringstream oss;
    oss << "set isHex: " << isHex;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
}


static char ConvertHexChar(char ch)
{
    if ((ch >= '0') && (ch <= '9'))
        return ch - 0x30;
    else if ((ch >= 'A') && (ch <= 'F'))
        return ch - 'A' + 10;
    else if ((ch >= 'a') && (ch <= 'f'))
        return ch - 'a' + 10;
    else return (-1);
}

static QByteArray QString2Hex(QString str)
{
    QByteArray senddata;
    int hexdata, lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len / 2);
    char lstr, hstr;
    for (int i = 0; i < len;) {
        hstr = str[i].toLatin1();
        if (hstr == ' ') {
            i++;
            continue;
        }
        i++;
        if (i >= len)
            break;
        lstr = str[i].toLatin1();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if ((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata * 16 + lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
    return senddata;
}

bool HQmlModelGlueController::sendCommand(const QString &text)
{
    std::string oss;
    oss = "start sending command: " + text.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss));
    QSerialPort serial;
    serial.close();
    serial.setPortName(m_portName);
    oss = "set port name: " + m_portName.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss));
    serial.setBaudRate(m_baudrate);
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("set baudrate: %1").arg(m_baudrate));
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss));
    if (!serial.open(QIODevice::ReadWrite)) {
        emit message(tr("Error:Can't open %1, error code %2").arg(m_portName).arg(serial.error()));
        QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Error:Can't open %1, error code %2").arg(m_portName).arg(serial.error()));
        return false;
    }
    QByteArray requestData;
    if (m_isHex) {
        requestData = QString2Hex(text);
    } else {
        requestData = text.toLatin1();
    }
    serial.write(requestData);
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "written data into serial port");
    if (serial.waitForBytesWritten(5000)) {
        if (serial.waitForReadyRead(5000)) {
            QByteArray responseData = serial.readAll();
            while (serial.waitForReadyRead(10))
                responseData += serial.readAll();
            m_feedback = responseData;
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "read response data from serial port");
            if (m_isHex)
            {
                emit this->message(responseData.toHex());
            }
            else {

                emit this->message(responseData);
            }
            serial.close();
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "close serial port, return true");
            return true;

        } else {
            emit message(tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
        }
    } else {
        emit message(tr("Wait write request timeout %1").arg(QTime::currentTime().toString()));
        QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
    }
    serial.close();
    return false;
}

bool HQmlModelGlueController::sendCommand(const char *writedata)
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("start sending command: %1").arg(writedata));
    QSerialPort serial;
    serial.close();
    serial.setPortName(m_portName);
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("set port name: %1").arg(m_portName));
    serial.setBaudRate(m_baudrate);
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("set baudrate: %1").arg(m_baudrate));
    if (!serial.open(QIODevice::ReadWrite)) {
        emit message(tr("Error:Can't open %1, error code %2").arg(m_portName).arg(serial.error()));
        QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Error:Can't open %1, error code %2").arg(m_portName).arg(serial.error()));
        return false;
    }
    serial.write(writedata);
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "written data into serial port");
    if (serial.waitForBytesWritten(5000)) {
        if (serial.waitForReadyRead(5000)) {
            QByteArray responseData = serial.readAll();
            while (serial.waitForReadyRead(10))
                responseData += serial.readAll();
            m_feedback = responseData;
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "read response data from serial port");
            if (m_isHex)
            {
                emit this->message(responseData.toHex());
            }
            else {

                emit this->message(responseData);
            }

            serial.close();
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "close serial port, return true");
            return true;

        } else {
            emit message(tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
            QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
        }
    } else {
        emit message(tr("Wait write request timeout %1").arg(QTime::currentTime().toString()));
        QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, tr("Wait read response timeout %1").arg(QTime::currentTime().toString()));
    }
    serial.close();
    return false;
}

qint16 CheckSum(std::string str)
{
    qint32 sum(0);
    qint16 result(0);
    for (auto ch : str)
    {
        sum += ch;
    }
    sum = -sum;
    result = sum & 0xff;
    return result;
}

std::string CharToAscii(quint8 hex)
{
    std::string result;
    std::string dic = "0123456789ABCDEF";
    result += dic[0x0f & (hex >> 4)];
    result += dic[0x0f & hex];
    return result;
}

std::string GetCommandSequence(std::string command)
{
    std::string commandseq;
    quint8 bytesize = command.length();
    std::string bytesize_str = CharToAscii(bytesize);
    quint8 checkedsum = CheckSum(bytesize_str + command);
    std::string sumstr = CharToAscii(checkedsum);
    commandseq = "\x05\x02" + bytesize_str + command + sumstr + '\x03';
    return commandseq;
}

bool HQmlModelGlueController::sendDispenseCommand()
{
    //qint16 testsum = CheckSum("\x30\x38\x50\x53\x20\x20\x30\x35\x30\x30");
    //std::cout << "testsum=" << testsum << std::endl;
    //quint8 hex = 15;
    //std::cout << CharToAscii('\xdf') << std::endl;
    //std::string dispense_str = "\x05\x02\x30\x35\x44\x49\x20\x45\x45\x03\x00";
    //std::string dispense_command = "PS\x20\x20";
    //dispense_command += "0500";
    //std::string dispense_commandseq = GetCommandSequence(dispense_command);
    std::ostringstream oss;
    oss << "sending dispensing command";
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
    return sendCommand(GetCommandSequence(DISPENSE_COMMAND).c_str());
}
bool HQmlModelGlueController::sendDispenseModeCommand()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending dispensing mode command");
    if (m_dispensemode == 1)
    {
        return sendCommand(GetCommandSequence(TIMED_MODE_COMMAND).c_str());
    }
    else if (m_dispensemode == 2)
    {
        return sendCommand(GetCommandSequence(STEADY_MODE_COMMAND).c_str());
    }
    else {
        return false;
    }
}

bool HQmlModelGlueController::sendPressureUnitsCommand()
{
    string command;
    switch (m_pressunits) {
        case 1:
            command = PRESSURE_UNITS_SET_COMMAND + "00"; break;
        case 2:
            command = PRESSURE_UNITS_SET_COMMAND + "01"; break;
        case 3:
            command = PRESSURE_UNITS_SET_COMMAND + "02"; break;
        default:
            break;
    }
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending pressure units command");
    return sendCommand(GetCommandSequence(command).c_str());
}

bool HQmlModelGlueController::sendSettingsCommand()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "start sending setting command");
    string time_command = TIME_SET_COMMAND + ConvertCurrentDispenseTime(m_time).toStdString();
    string press_command = PRESSURE_SET_COMMAND + ConvertCurrentPress(m_press,m_pressunits).toStdString();
    string vacuum_command = VACUUM_SET_COMMAND + ConvertCurrentVaccum(m_vac).toStdString();
    string trigger_command = SET_TRIGGER_VALUE_COMMAND + ConvertCurrentTrigger(m_trigger).toStdString();
    std::cout << "time_command:" << time_command << std::endl;
    std::cout << "press_command:" << press_command << std::endl;
    //std::cout << "m_pressmode:" << m_pressunits
    std::cout << "vacuum_command:" << vacuum_command << std::endl;
    std::cout << "trigger_command:" << trigger_command << std::endl;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending time command");
    bool result1 = sendCommand(GetCommandSequence(time_command).c_str());
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending pressure command");
    bool result2 = sendCommand(GetCommandSequence(press_command).c_str());
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending vacuum command");
    bool result3 = sendCommand(GetCommandSequence(vacuum_command).c_str());
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending trigger command");
    bool result4 = sendCommand(GetCommandSequence(trigger_command).c_str());
    return result1 && result2 && result3 && result4;
}
bool HQmlModelGlueController::sendCurrentInstructCommand(const QString &command)
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "sending current instuct command");
    string real_command = replaceDashWithHex(command.toStdString());
    return sendCommand(GetCommandSequence(real_command).c_str());
}

string replaceDashWithHex(string replacedstr)
{
    string newstring;
    for (auto c : replacedstr)
    {
        if (c=='-') newstring += '\x20';
        else
            newstring += c;
    }
    return newstring;
}
QString HQmlModelGlueController::getCurrentState()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "getting current instuct command");
    string command = GetCommandSequence(MEM_PRE_TIME_FEEDBACK_COMMAND);
    if (!sendCommand(command.c_str())) return QString::fromStdString("ERROR");
    string feedbackstr = getFeedBack().toStdString();
    //std::cout << "feedback 1:" << feedbackstr << std::endl;
    sendCommand(ACK.c_str());
    feedbackstr = getFeedBack().toStdString();
    std::cout << "feedback 1:" << feedbackstr << std::endl;
    string channel = "";
    if (feedbackstr.find("CH") != string::npos) {
        channel = feedbackstr.substr(feedbackstr.find("CH")+2,3);
        std::cout << "channel = " << channel << std::endl;
    }
    string pressure = "";
    if (feedbackstr.find("PD") != string::npos) pressure = feedbackstr.substr(feedbackstr.find("PD")+2,4);
    string dispensetime = "";
    if (feedbackstr.find("DT") != string::npos) dispensetime = feedbackstr.substr(feedbackstr.find("DT")+2,5);
    if (channel != "") {
        string channel_command = PRE_TIME_VACUUM_FEEDBACK_COMMAND + channel;
        sendCommand(GetCommandSequence(channel_command).c_str());
        sendCommand(ACK.c_str());
        feedbackstr = getFeedBack().toStdString();
        std::cout << "feedback 2:" << feedbackstr << std::endl;
    }
    string vacuum = "";
    if (feedbackstr.find("VC") != string::npos) vacuum = feedbackstr.substr(feedbackstr.find("VC"),feedbackstr.find("VC")+4);
    string trigger = "";
    sendCommand(GetCommandSequence(TRIGGER_VALUE_FEEDBACK_COMMAND).c_str());
    sendCommand(ACK.c_str());
    std::cout << "feedback 3:" << feedbackstr << std::endl;
    if (feedbackstr.find("TV") != string::npos) trigger = feedbackstr.substr(feedbackstr.find("TV"),feedbackstr.find("TV")+5);
    string stats_string = "Current Stats\n\nTIME:\t" + dispensetime + "\nPRESS:\t" + pressure + "\nVAC:\t" + vacuum + "\nTRIG:\t" + trigger;
    std::ostringstream oss;
    oss << "get current states: " << stats_string;
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, QString::fromStdString(oss.str()));
    return QString::fromStdString(stats_string);
}

bool HQmlModelGlueController::loadConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "start loading glue controller configure");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/GlueConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "loaded glue controller configure successfully");
    return isok;
}

bool HQmlModelGlueController::saveConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "start saving glue controller configure");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/GlueConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    QLog::LogMessageOutput(QtInfoMsg,context_gluecontroller, "saved glue controller configure successfully");
    return true;
}


QDataStream &operator>>(QDataStream &input, HQmlModelGlueController &obj)
{
    return input >> obj.m_portName >> obj.m_commands >> obj.m_isHex >> obj.m_baudrate >> obj.m_dispensemode >> obj.m_pressunits >> obj.m_time >> obj.m_press >> obj.m_vac >> obj.m_trigger;
}

QDataStream &operator<<(QDataStream &output, const HQmlModelGlueController &obj)
{
    return output << obj.m_portName << obj.m_commands << obj.m_isHex << obj.m_baudrate << obj.m_dispensemode << obj.m_pressunits<< obj.m_time << obj.m_press << obj.m_vac << obj.m_trigger;
}
