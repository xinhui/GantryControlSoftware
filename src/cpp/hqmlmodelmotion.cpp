﻿#include "hqmlmodelmotion.h"

#include <QTimer>
#include <QDataStream>
#include <QCoreApplication>
#include <QFile>
#include <iostream>


HQmlModelMotion::HQmlModelMotion(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model Motion"<<std::endl;

    m_motion = HMotionController::GetInstance();
    loadConfig();
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [ = ]() {
        QList<int> states;
        QList<QString> positions;
        QList<QString> velocitys;
        for (int k = 0; k < 3; ++k) {
            states.append(m_motion->getAxisState(m_axisIndexs[k]));
            positions.append("  " + QString::asprintf("%.4f",m_motion->getAxisPosition(m_axisIndexs[k])) + "  mm ");
            velocitys.append("  " + QString::asprintf("%.4f",m_motion->getAxisVelocity(m_axisIndexs[k])) + "  mm/s ");
        }
        states.append(m_motion->getAxisState(m_axisIndexs[3]));
        positions.append("  " + QString::asprintf("%.4f",m_motion->getAxisPosition(m_axisIndexs[3])) + "  deg ");
        velocitys.append("  " + QString::asprintf("%.4f",m_motion->getAxisVelocity(m_axisIndexs[3])) + "  deg/s ");
        emit updateMotion(states, positions, velocitys);
    });
    m_timer->setInterval(50);
}

HQmlModelMotion::~HQmlModelMotion()
{
    saveConfig();
}

void HQmlModelMotion::startUpdateMotions(bool isStart)
{
    if (isStart) {
        m_timer->start();
    } else {
        m_timer->stop();
    }
}

bool HQmlModelMotion::enableAxis(int axis, bool enable)
{
    return m_motion->enableAxis(m_axisIndexs[axis], enable);
}

bool HQmlModelMotion::moveRelative(int axis, bool dir)
{
    return m_motion->moveRel(m_axisIndexs[axis], dir ? m_distances[axis] : -m_distances[axis], m_speeds[axis]);
}

bool HQmlModelMotion::moveAbsolute(int axis)
{
    return m_motion->moveAbs(m_axisIndexs[axis], m_distances[axis], m_speeds[axis]);
}

bool HQmlModelMotion::homeAxis(int axis)
{
    return m_motion->homeAxis(m_axisIndexs[axis]);
}

bool HQmlModelMotion::stopAxis(int axis)
{
    return m_motion->stopAxis(m_axisIndexs[axis]);
}

bool HQmlModelMotion::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/motionConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    return isok;
}

bool HQmlModelMotion::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/motionConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}


bool operator>>(QDataStream &input, HQmlModelMotion &obj)
{
    input >> obj.m_speeds >> obj.m_distances;
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelMotion &obj)
{
    output << obj.m_speeds << obj.m_distances;
    return true;
}


