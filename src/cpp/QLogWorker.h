#ifndef QLOGWORKER_H
#define QLOGWORKER_H

#include <QObject>
#include <QMap>
#include <QDateTime>
#include <QTimer>
#include "QLog.h"

class QWriteElement;

class QLogWorker : public QObject
{
    Q_OBJECT
public:
    QLogWorker(QObject *parent = NULL);
    ~QLogWorker();

public:
    /** set log flags */
    void SetLogFlags(QLog::eLogFlags flags);
    /** set log path */
    void SetLogPath(const QString &path);
    // set the log file max size (if exceeding the max size, the program will create a new log file) (bytes)
    void SetLogFileMaxSize(qint64 size);
    /** set the interval of creating new log file automatically, if the time is exceeded the new log file will be created */
    void SetLogFileCreatInterval(qint64 interval);
    /** set the life of the log file. The log file will be deleted if exceeding the saving time */
    void SetLogFileSaveTime(qint64 time);
    /** write log */
    void WriteLog(QLog::eLogFlag flag, const QString &message);
    /** get all log list */
    QList<QString> GetLogList();
    /** get the log directory */
    QString GetLogDir();

private slots:
    /** check the log timer */
    void OnLogTimeout();

private:
    /** judge if we need to change the file name */
    bool IsLogFileNameNeedChanged() const;
    /** clear logs*/
    void ClearLogs();
    /** clear the logs through path */
    void ClearLogsByPath(const QString &path);

private:
    QString MakeFileName();
    /** add a new element */
    void AddWriteElement(QLog::eLogFlag flag);
    /** remove an element */
    void RemoveWriteElement(QLog::eLogFlag flag);

private:
    QString											m_logPath;			// log path
    QLog::eLogFlags                                 m_eFlags;			// log flags: the categories of log we need to print
    QDateTime									    m_logTime;			// log creation time
    QWriteElement*								    m_element;			// write or read file
    QTimer*											m_pLogTimer;		// timer for how long the log file is created
    qint64											m_maxSize;			// max file size of log file
    qint64											m_creatInterval;	// the time of how long the log file has been created
    qint64											m_saveTime;			// the saving time for the log file
};


#endif // QLOGWORKER_H
