#include "hqmlLoading.h"
#include <QCoreApplication>
#include <QFile>
#include <QThread>
#include <QtConcurrent>

#include "hqmlmodelCapture.h"
#include "hmotioncontroller.h"
#include "hqmlmodelcamera.h"
#include "hqmlmodelsettingcoord.h"
#include "hmathfunctions.h"
#include "hqmlPatternRecognition.h"
#include "hqmlmodelDispensePatternSetting.h"
#include "hqmlLoadingCoordSetting.h"
#include "hqmlLoadingDispensingSetting.h"

#include <iostream>
#include <ctime>

#define PI acos(-1)

HQmlLoading *HQmlLoading::loadingPtr = nullptr;

HQmlLoading::HQmlLoading(QObject *parent) : QThread(parent){
    std::cout<<"Loading Module info"<<std::endl;
    if(!loadConfig()){
        m_moduleList.resize(12);
        m_markToCalcRot.resize(3);
        m_toFindMarkToCalcRot.resize(3);
    }
    if(m_moduleList.size()!=12) m_moduleList.resize(12);
    if(m_markToCalcRot.size()!=3) m_markToCalcRot.resize(3);
    if(m_toFindMarkToCalcRot.size()!=3) m_toFindMarkToCalcRot.resize(3);
    loadConfig();
    updateLoadingPanelInfo();
}

HQmlLoading::~HQmlLoading(){
    std::cout<<"quit loading panel" <<std::endl;
    saveConfig();
}

// Find mark
// ==============================================
void HQmlLoading::setPosToFindMark(){
    m_toFindMark.setX(m_motion->getAxisPosition(Axis_X));
    m_toFindMark.setY(m_motion->getAxisPosition(Axis_Y));

    saveConfig();
    updateLoadingPanelInfo();
}

bool HQmlLoading::movePosToFindMark(){
    return HQmlModelFlow::flowPtr->moveXY(m_toFindMark);
}

void HQmlLoading::setFocusHeight(){
    z_focus = m_motion->getAxisPosition(Axis_Z);

    saveConfig();
    updateLoadingPanelInfo();
}

bool HQmlLoading::movePosToFocus(){
    return HQmlModelFlow::flowPtr->moveZ(z_focus);
}

void HQmlLoading::findMark(){
    std::cout<<"Go Find Mark " <<std::endl;

    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    QStringList cameraResult;
    int _ali = 71;
    QString outputDir = QString("C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/result/SD1_%1").arg(_ali, 3, 10, QLatin1Char('0')) + "/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    QString resultPath = outputDir + date + "*";

    std::cout << resultPath.toLocal8Bit().constData() << std::endl;

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali+1, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(1000);

    QFileInfo fileInfo(outputDir);
    QDir qDir(outputDir);
    if(fileInfo.isDir()){
        std::cout<<"Find dir, try to delete"<<std::endl;
        qDir.removeRecursively();
    }

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("R0", cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("T1", cameraResult, 100);
    QThread::msleep(500);

    QStringList fileList = qDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    QString outputFileName = fileList.at(0);
    std::cout<<outputFileName.toLocal8Bit().constData()<<std::endl;
    QFile outputFile(outputDir + outputFileName);
    if(!outputFile.open(QIODevice::ReadOnly)){
        std::cout<<"Can't open file"<<std::endl;
    }

    QTextStream in(&outputFile);
    QString Line;
    QStringList list;
    QPointF MarkPos;

    while(!in.atEnd()){
        Line = in.readLine();
        list = Line.split(',');
        if(list.size() >=2){
            MarkPos.setX(list.at(0).toDouble());
            MarkPos.setY(list.at(1).toDouble());
        }
    }

    markPos = HQmlCameraParameter::cameraPtr->transPicureToGantry(MarkPos);
    x_mark = markPos.x();
    y_mark = markPos.y();

    saveConfig();

    updateLoadingPanelInfo();
}

// Get module
// ==============================================
QStringList HQmlLoading::getModuleConfigNames(){
    return HQmlLoadingCoordSetting::loadingCoordPtr->getConfigNames();
}

void  HQmlLoading::setPickModuleMode(int _index, int _value){
    m_moduleList[_index] = HQmlLoadingCoordSetting::loadingCoordPtr->getConfigNames().value(_value);
}

int HQmlLoading::getPickModuleMode(int _index){
    return HQmlLoadingCoordSetting::loadingCoordPtr->getConfigNames().indexOf(m_moduleList.value(_index));
}

bool HQmlLoading::moveToModule(int _index){
    QPointF modulePos;
    modulePos.setX(x_mark + HQmlLoadingCoordSetting::loadingCoordPtr->getModuleCentre(_index).x());
    modulePos.setY(y_mark + HQmlLoadingCoordSetting::loadingCoordPtr->getModuleCentre(_index).y() + y_biasToGlue);
    std::cout<< modulePos.x()<<", "<<modulePos.y()<<std::endl;
    return HQmlModelFlow::flowPtr->moveXY(modulePos);
}

void HQmlLoading::releaseModule(const QString &_moduleNum){
    QThread *gthread = QThread::create([ = ]() {
        HQmlLoadingCoordSetting::loadingCoordPtr->letFixture(_moduleNum);
    });
    gthread->start(QThread::TimeCriticalPriority);
}

void HQmlLoading::dispenseGlue(int _moduleNum){
    QPointF markPos;
    markPos.setX(x_mark + HQmlLoadingCoordSetting::loadingCoordPtr->getModuleCentre(_moduleNum).x() + x_biasToGlue);
    markPos.setY(y_mark + HQmlLoadingCoordSetting::loadingCoordPtr->getModuleCentre(_moduleNum).y() + y_biasToGlue);
    QThread *gthread = QThread::create([ = ]() {
        HQmlLoadingDispensingSetting::LoadingDispensePtr->runLoadingDispensing(markPos);
    });
    gthread->start(QThread::TimeCriticalPriority);
}

// To calculate rotation
void HQmlLoading::setPosToFindCalcBias(int _index){
    m_toFindMarkToCalcRot[_index].setX(m_motion->getAxisPosition(Axis_X));
    m_toFindMarkToCalcRot[_index].setY(m_motion->getAxisPosition(Axis_Y));

    saveConfig();
}

bool HQmlLoading::movePosToCalcBias(int _index){
    return HQmlModelFlow::flowPtr->moveXY(m_toFindMarkToCalcRot.at(_index));
}

void HQmlLoading::findMarkToCalcBias(int _index){

    std::cout<<"Go Find Mark to calculate rotation" <<std::endl;

    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    QStringList cameraResult;
    int _ali = 51;
    QString outputDir = QString("C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/result/SD1_%1").arg(_ali, 3, 10, QLatin1Char('0')) + "/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    QString resultPath = outputDir + date + "*";

    std::cout << resultPath.toLocal8Bit().constData() << std::endl;

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali+1, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(1000);

    QFileInfo fileInfo(outputDir);
    QDir qDir(outputDir);
    if(fileInfo.isDir()){
        std::cout<<"Find dir, try to delete"<<std::endl;
        qDir.removeRecursively();
    }

    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(_ali, 3, 10, QLatin1Char('0')), cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("R0", cameraResult);
    QThread::msleep(2000);
    HQmlModelCamera::execCommond("T1", cameraResult, 100);
    QThread::msleep(500);

    QStringList fileList = qDir.entryList(QDir::Files | QDir::NoDotAndDotDot);
    QString outputFileName = fileList.at(0);
    std::cout<<outputFileName.toLocal8Bit().constData()<<std::endl;
    QFile outputFile(outputDir + outputFileName);
    if(!outputFile.open(QIODevice::ReadOnly)){
        std::cout<<"Can't open file"<<std::endl;
    }

    QTextStream in(&outputFile);
    QString Line;
    QStringList list;
    QPointF MarkPos;

    while(!in.atEnd()){
        Line = in.readLine();
        list = Line.split(',');
        if(list.size() >=2){
            MarkPos.setX(list.at(0).toDouble());
            MarkPos.setY(list.at(1).toDouble());
        }
    }

    m_markToCalcRot[_index] = HQmlCameraParameter::cameraPtr->transPicureToGantry(MarkPos);

    saveConfig();
    updateLoadingPanelInfo();

}

void HQmlLoading::calculateChuckRotation(){
    double x_mean = 0.;
    double y_mean = 0.;
    double x_SqrSum = 0.;
    double xy_CorSum = 0.;

    for(int i = 0; i< m_markToCalcRot.size(); i++){
        x_mean += m_markToCalcRot.at(i).x()/m_markToCalcRot.size();
        y_mean += m_markToCalcRot.at(i).y()/m_markToCalcRot.size();
        x_SqrSum += pow(m_markToCalcRot.at(i).x(),2);
        xy_CorSum += m_markToCalcRot.at(i).x()*m_markToCalcRot.at(i).y();
    }

    double k = (xy_CorSum-m_markToCalcRot.size()*x_mean*y_mean)/(x_SqrSum-m_markToCalcRot.size()*x_mean*x_mean);

    m_chuckRotationAngle = atan(1./k)*180./PI;

    saveConfig();
    updateLoadingPanelInfo();

}

void HQmlLoading::updateLoadingPanelInfo(){
    QStringList _strMarkPos, _strModuleList;
    QList<int> _strComboIndex;
    QStringList _strFeaturePos;
    QStringList _strRotationAngle;

    std::cout<<"Mark pos: "<<x_mark<<", "<<y_mark<<std::endl;
    _strMarkPos << QString::number(x_mark, 'f', 4) << QString::number(y_mark, 'f', 4) << QString::number(z_focus, 'f', 4);

    _strModuleList << m_moduleList.toList();

    auto cfgs = HQmlLoadingCoordSetting::loadingCoordPtr->getConfigNames();
    for (int i = 0; i < m_moduleList.size(); i++){
        _strComboIndex << cfgs.indexOf(m_moduleList.at(i));
    }

    for (int i = 0; i < m_markToCalcRot.size(); i++){
        _strFeaturePos << QString::number(m_markToCalcRot.at(i).x(), 'f', 4) << QString::number(m_markToCalcRot.at(i).y(), 'f', 4);
    }

    _strRotationAngle << QString::number(m_chuckRotationAngle, 'f', 4);
    emit updateLoadingPanelUI(_strMarkPos, _strModuleList, _strComboIndex, _strFeaturePos, _strRotationAngle);
}

bool HQmlLoading::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/loadingConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();

    updateLoadingPanelInfo();

    return isok;
}

bool HQmlLoading::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/loadingConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}


QDataStream &operator>>(QDataStream &input, HQmlLoading &obj)
{
    return input >> obj.x_mark >> obj.y_mark >> obj.z_focus >> obj.m_moduleList >> obj.m_toFindMarkToCalcRot >> obj.m_markToCalcRot >> obj.m_chuckRotationAngle;
}

QDataStream &operator<<(QDataStream &output, const HQmlLoading &obj)
{
    return output << obj.x_mark << obj.y_mark << obj.z_focus << obj.m_moduleList << obj.m_toFindMarkToCalcRot << obj.m_markToCalcRot << obj.m_chuckRotationAngle;
}
