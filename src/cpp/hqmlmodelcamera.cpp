﻿#include "hqmlmodelcamera.h"
#include <QTcpSocket>
#include <QtNetwork>
#include <QDataStream>
#include <QApplication>
#include <iostream>

static void hDelay(int mSec)
{

    //    QEventLoop loop;
    //    QTimer::singleShot(mSec, &loop, &QEventLoop::quit);
    //    loop.exec();

    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    timer.callOnTimeout(&loop, &QEventLoop::quit);
    timer.start(mSec);
    loop.exec();

    //    QCoreApplication::processEvents(QEventLoop::AllEvents, mSec);

    //    QTime dieTime = QTime::currentTime().addMSecs(mSec);
    //       while( QTime::currentTime() < dieTime )
    //       QCoreApplication::processEvents(QEventLoop::AllEvents, 1);

}


HQmlModelCamera *HQmlModelCamera::instance = nullptr;

HQmlModelCamera::HQmlModelCamera(QObject *parent): QObject(parent), tcpSocket(new QTcpSocket)
{
    // find out name of this machine
    QString name = QHostInfo::localHostName();
    if (!name.isEmpty()) {
        availableHosts << name;
        QString domain = QHostInfo::localDomainName();
        if (!domain.isEmpty())
            availableHosts << (name + QChar('.') + domain);
    }
    if (name != QLatin1String("localhost"))
        availableHosts << QString("localhost");
    // find out IP addresses of this machine
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // add non-localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (!ipAddressesList.at(i).isLoopback())
            availableHosts << ipAddressesList.at(i).toString();
    }
    // add localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i).isLoopback())
            availableHosts << ipAddressesList.at(i).toString();
    }

    ///tcpSocket->moveToThread(QApplication::instance()->thread());
    m_connection1 = connect(tcpSocket, &QAbstractSocket::errorOccurred, this, [ = ](QAbstractSocket::SocketError socketError) {
        switch (socketError) {
        case QAbstractSocket::RemoteHostClosedError:
            break;
        case QAbstractSocket::HostNotFoundError:
            lastError = tr("The host was not found. Please check the "
                           "host name and port settings.\n");
            break;
        case QAbstractSocket::ConnectionRefusedError:
            lastError = tr("The connection was refused by the peer. "
                           "Make sure the fortune server is running, "
                           "and check that the host name and port "
                           "settings are correct.\n");
            break;
        default:
            lastError = tr("The following error occurred: %1.\n")
                        .arg(tcpSocket->errorString());
        }

        emit errorOccurd(lastError);
    }, Qt::QueuedConnection);


    m_connection2 = connect(tcpSocket, &QAbstractSocket::readyRead, this, &HQmlModelCamera::readRely, Qt::QueuedConnection);
    instance = this;
    std::cout<<"Load Model Camera"<<std::endl;
    loadConfig();
}


HQmlModelCamera::~HQmlModelCamera()
{
    tcpSocket->abort();
    tcpSocket->disconnectFromHost();
    saveConfig();
}

void HQmlModelCamera::setThread(QThread *thread)
{
    qDebug() << tcpSocket->thread() << thread << this->thread();
    tcpSocket->moveToThread(thread ? thread : this->thread());
}



void HQmlModelCamera::setHost(QString host)
{
    hostName = host;
    //createConnection();
}

void HQmlModelCamera::setPort(quint16 port)
{
    portNumber = port;
    //createConnection();
}

void HQmlModelCamera::setHostAndPort(const QString &host, quint16 port)
{
    hostName = host;
    portNumber = port;
    //createConnection();
}

bool HQmlModelCamera::createConnection()
{
    tcpSocket->abort();
    isRelyReady = true;
    tcpSocket->connectToHost(hostName, portNumber);
    if (tcpSocket->waitForConnected(3000)) {
        return true;
    } else {
        tcpSocket->abort();
        return false;
    }
}

void HQmlModelCamera::closeConnection()
{
    tcpSocket->abort();
}

bool HQmlModelCamera::isConnected()
{
    return tcpSocket->state();
}

qint32 HQmlModelCamera::execCommond(QString cmd, QStringList &result,  qint32 timeOut)
{
    cmd += instance->m_endMark;
    instance->tcpRelyBlock.clear();

    if (instance->tcpSocket->state() != QAbstractSocket::ConnectedState) {
        if (!instance->createConnection()) {
            result.append(instance->lastError.toLocal8Bit());
            return -2;
        }
    }

    instance->isRelyReady = false;
    instance->tcpSocket->write(cmd.toLocal8Bit());

//    QString ffffff= QString::fromLocal8Bit(instance->tcpRelyBlock);
//    result=ffffff.split('\r');
//    if(result.size()>1){
//       result= result[1].split(',');
//       return 0;
//    } else {
//        return cmd!=ffffff;
//    }

    int mg=0;
     QString ffffff;

    while (true) {
        if (instance->isRelyReady) {
            ffffff = QString::fromLocal8Bit(instance->tcpRelyBlock);
            auto mmmbs=ffffff.split(instance->m_endMark,Qt::SkipEmptyParts);
            if (mmmbs.size()>1) {
                result = mmmbs[1].split(',');
                return 0;
            }else {

                result =mmmbs;
                 if(result.first()=="T1"){
                     goto here;
                 }
                return 0;
            }

        }
        here:
        hDelay(1);
        ++mg;
        if (mg > timeOut) {
            result.append(instance->lastError.toLocal8Bit());
            return -1;
        }
    }
    return -1;
}

bool HQmlModelCamera::sendCommond(QString cmd)
{
    cmd += m_endMark;
    tcpRelyBlock.clear();

    if (tcpSocket->state() != QAbstractSocket::ConnectedState) {
        if (!createConnection()) {
            return false;
        }
    }
    isRelyReady = false;
    tcpSocket->write(cmd.toLocal8Bit());
    return true;
}

QString HQmlModelCamera::getLastError()
{
    return lastError;
}


void HQmlModelCamera::readRely()
{
        tcpRelyBlock.append(tcpSocket->readAll());
        if (tcpRelyBlock.right(1) == m_endMark) {
            isRelyReady = true;
            emit cmdResponsed(QString::fromLocal8Bit(tcpRelyBlock));
        }

}



bool HQmlModelCamera::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/cameraConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    return isok;
}

bool HQmlModelCamera::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/cameraConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}


bool operator>>(QDataStream &input, HQmlModelCamera &obj)
{
    input >> obj.hostName >> obj.portNumber >> obj.m_endMark;
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelCamera &obj)
{
    output << obj.hostName << obj.portNumber << obj.m_endMark;
    return true;
}


