#include "hqmlLoadingCoordSetting.h"
#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QThread>

#include "hqmlmodelio.h"
#include "hmotioncontroller.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}

HQmlLoadingCoordSetting *HQmlLoadingCoordSetting::loadingCoordPtr = nullptr;

HQmlLoadingCoordSetting::HQmlLoadingCoordSetting(QObject *parent) : QObject(parent){
    std::cout<<"Load Module loading coordinate Setting"<<std::endl;
    loadConfig();
    updateLoadingCoordInfo();

    map_LoadingCoordPar["WaitingZ"] = WaitingZ;
    map_LoadingCoordPar["OutputGantry"] = OutputGantry;
    map_LoadingCoordPar["DelOX"] = DelOX;
    map_LoadingCoordPar["DelOY"] = DelOY;
    map_LoadingCoordPar["MovingSpeed"] = MovingSpeed;
    map_LoadingCoordPar["MovingAcc"] = MovingAcc;
    map_LoadingCoordPar["SearchPos1"] = SearchPos1;
    map_LoadingCoordPar["SearchPos2"] = SearchPos2;
    map_LoadingCoordPar["PlacingSpeed"] = PlacingSpeed;
    map_LoadingCoordPar["PlacingAcc"] = PlacingAcc;
    map_LoadingCoordPar["SearchForce"] = SearchForce;
    map_LoadingCoordPar["OutputVC"] = OutputVC;

    loadingCoordPtr = this;
}

HQmlLoadingCoordSetting::~HQmlLoadingCoordSetting(){
    std::cout<<"quit HQml LoadingCoord Setting ============"<<std::endl;
    saveConfig();
}

void HQmlLoadingCoordSetting::addConfig(QString name){
    if (m_configNames.contains(name) || name.isEmpty()) {
        return;
    }
    m_configNames << name;
    m_configDatas << ConfigVars();
    m_currentConfig = name;
    updateLoadingCoordInfo();
}

void HQmlLoadingCoordSetting::removeConfig(int index){
    m_configNames.removeAt(index);
    m_configDatas.removeAt(index);
    m_currentConfig = m_configNames.value(index);
    updateLoadingCoordInfo();
}

void HQmlLoadingCoordSetting::setCurrentConfig(int index){
    if (index > -1 && index < m_configNames.size()) {
        m_currentConfig = m_configNames[index];
    }
    updateLoadingCoordInfo();
}

void HQmlLoadingCoordSetting::setConfigData(const QString &index, const QString &value){
    int cindex = m_configNames.indexOf(m_currentConfig);
    std::cout<<"cindex: "<<cindex<<", "<<"m_currentConfig: "<<m_currentConfig.toStdString()<<std::endl;
    if (cindex != -1) {

        m_configDatas[cindex].setValue(map_LoadingCoordPar[index], value.toDouble());

        std::cout<<index.toStdString()<<", index: " << map_LoadingCoordPar[index] <<", value: " << value.toDouble() <<std::endl;
        std::cout<<"cindex: "<<cindex<<", " << m_configDatas[cindex].getStrs().at(map_LoadingCoordPar[index]).toStdString()<<std::endl;
    }
    std::cout<<"test line5"<<std::endl;
    saveConfig();
    updateLoadingCoordInfo();
}

void HQmlLoadingCoordSetting::updateLoadingCoordInfo(){
    int cfgIndex = m_configNames.contains(m_currentConfig) ? m_configNames.indexOf(m_currentConfig) : 0;
    emit updateLoadingCoordUI(cfgIndex, m_configNames, m_configDatas.value(cfgIndex).getStrs(), m_configNames.indexOf(m_defaultGetter),
                           m_configNames.indexOf(m_defaultLetter)   );
    saveConfig();
}



bool HQmlLoadingCoordSetting::getFixture(const QString &name){
    int index = -1;
    if (name.isEmpty()) {
        return false;
    } else {
        index = m_configNames.indexOf(name);
    }
    if (index < 0 || index >= m_configDatas.size()) {
        return false;
    }

    HMotionController *motion = HMotionController::GetInstance();
    ConfigVars configs = m_configDatas[index];

    CMoveAbs(Axis_Z, configs.srhPos1, configs.fastSpeed, configs.fastAcc, true);
    QThread::msleep(3000);
    HQmlModelIO::ioPtr->zeroForceSensor();
    CMoveAbs(Axis_Z, configs.srhPos2, configs.srhSpeed, configs.srhAcc, false);

    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto NotFind;
        }
        if (!motion->getForceValue(&force)) {
            goto error;
        }
        if (force > configs.srhForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(1000);
            goto PickFlex;
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }

    }

NotFind:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

PickFlex:
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 1)) {
        goto error;
    }
    QThread::msleep(3000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

error:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return false;
}

bool HQmlLoadingCoordSetting::getFixture(int index){
    return getFixture(m_configNames.value(index));
}

bool HQmlLoadingCoordSetting::letFixture(const QString &name){
    int index = -1;
    if (name.isEmpty()) {
        return false;
    } else {
        index = m_configNames.indexOf(name);
    }
    if (index < 0 || index >= m_configDatas.size()) {
        return false;
    }

    HMotionController *motion = HMotionController::GetInstance();
    ConfigVars configs = m_configDatas[index];

    CMoveAbs(Axis_Z, configs.srhPos1, configs.fastSpeed, configs.fastAcc, true)
    QThread::msleep(3000);
    HQmlModelIO::ioPtr->zeroForceSensor();
    CMoveAbs(Axis_Z, configs.srhPos2, configs.srhSpeed, configs.srhAcc, false)

    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto NotFind;
        }
        if (!motion->getForceValue(&force)) {
            goto error;
        }
        if (force > configs.srhForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(500);
            goto ReleaseFlex;
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }
    }
    
NotFind:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true);
    return true;

ReleaseFlex:
    if (!motion->setDigitalOutput(2, configs.outputIndex_Gantry, 0)) {
        goto error;
    }
    QThread::msleep(2000);
    if (!motion->setDigitalOutput(2, configs.outputIndex_VC, 1)) {
        goto error;
    }
    QThread::msleep(2000);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true)
    return true;

error:
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, configs.waitPos, configs.fastSpeed, configs.fastAcc, true)
    return false;
}

QPointF HQmlLoadingCoordSetting::getModuleCentre(int _index){
    QPointF centre;
    centre.setX(m_configDatas[_index].del_OX);
    centre.setY(m_configDatas[_index].del_OY);

    return centre;
}


bool HQmlLoadingCoordSetting::letFixture(int index){
    return letFixture(m_configNames.value(index));
}

bool HQmlLoadingCoordSetting::loadConfig(){
    QString configFile = QCoreApplication::applicationDirPath() + "/user/LoadingCoordSettingConfig.cfg";
    std::cout<<"Capture config file path: "<< configFile.toStdString() <<std::endl;
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        std::cout<<"load module loading coordinate setting config file failed!"<<std::endl;
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    return isok;
}

bool HQmlLoadingCoordSetting::saveConfig(){
    QString configFile = QCoreApplication::applicationDirPath() + "/user/LoadingCoordSettingConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        std::cout<<"cannot write into module loading coordinate setting config file!"<<std::endl;
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}

bool operator>>(QDataStream &input, HQmlLoadingCoordSetting &obj){
    input  >> obj.m_configNames >> obj.m_currentConfig >> obj.m_configDatas >> obj.m_defaultGetter >> obj.m_defaultLetter;
    return true;
}

bool operator<<(QDataStream &output, const HQmlLoadingCoordSetting &obj){
    output <<  obj.m_configNames <<  obj.m_currentConfig << obj.m_configDatas << obj.m_defaultGetter << obj.m_defaultLetter;
    return true;
}
