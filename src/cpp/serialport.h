﻿#ifndef HSERIALPORT_H
#define HSERIALPORT_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>

//! [0]
class HSerialport : public QThread
{
    Q_OBJECT

public:
    explicit HSerialport(QObject *parent = nullptr);
    ~HSerialport() override;

    void unInit();

    void setPortName(const QString &portName, int waitTime);
    QString requestCommond(const QString &commond, bool wait);

signals:
    void response(const QString &s);
    void error(const QString &s);
    void timeout(const QString &s);

private:
    void transaction(const QString &request);
    void run() override;

    QString m_portName;
    QString m_request;
    int m_waitTimeout = 0;
    QMutex m_mutex;
    QWaitCondition m_cond;
    bool m_quit = false;

    short m_returnCode = 1;
    QString m_returnStr;


};

#endif // HSERIALPORT_H
