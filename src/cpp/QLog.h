#ifndef QLOG_H
#define QLOG_H


#include <QObject>
#include <QMutex>

class QLogWorker;
class QLog : public QObject
{
    Q_OBJECT
public:
    enum eLogFlag   // log categories
    {
        eLog_Debug =  0x001,
        eLog_Info =   0x002,
        eLog_Warning = 0x004,
        eLog_Critical = 0x008,
        eLog_Fatal  = 0x010,
    };
    Q_DECLARE_FLAGS(eLogFlags, eLogFlag)

public:
    /** get log singleton instance */
    static QLog *Instance();
    /** destroy singleton instance */
    static void Destroy();
    /** monitor the message output */
    static void LogMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    /** default handle setter */
    static void SetDefaultHandle(QtMessageHandler handler);

public:
    /** log flags setter */
    void SetLogFlags(eLogFlags flags);
    /** log path setter */
    void SetLogPath(const QString &path);
    /** get log list */
    QList<QString> GetLogList();
    /** get log path */
    QString GetLogDir();

protected:
    explicit QLog(QObject *parent = NULL);

private:
    /** get worker pointer */
    QLogWorker *Worker() const;

private:

    //
    static QMutex m_mutex;
    // synchrolock
    static QMutex m_useMutex;
    // static singleton instance
    static QLog *m_pInstance;
    // default handle
    static QtMessageHandler gDefaultHandler;
private:
    /** LogWorker class */
    QLogWorker *m_pWorker;
};



#endif // QLOG_H
