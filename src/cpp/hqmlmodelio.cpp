﻿#include "hqmlmodelio.h"
#include <QTimer>
#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <iostream>

#include "hmotioncontroller.h"

HQmlModelIO *HQmlModelIO::ioPtr = nullptr;

HQmlModelIO::HQmlModelIO(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model IO"<<std::endl;

    m_motion = HMotionController::GetInstance();

    if (!loadConfig()) {
        m_inputNames.resize(32);
        m_outputNames.resize(32);
        m_inputNames.fill("1");
        m_outputNames.fill("2");
    }
    m_motion->setForceOffset(-m_forceOffset);

    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, [ = ]() {
        QList<int> inputs, outputs;
        for (int k = 0; k < 32; ++k) {
            inputs.append(m_motion->getDigitalInput(2, k));
            outputs.append(m_motion->getDigitalOutput(2, k));
        }
        emit updateIOs(inputs, outputs);
    });
    m_timer->setInterval(100);
    ioPtr = this;
}

HQmlModelIO::~HQmlModelIO()
{
    saveConfig();
}

void HQmlModelIO::startUpdateIOs(bool isStart)
{
    if (isStart) {
        m_timer->start();
    } else {
        m_timer->stop();
    }
}

bool HQmlModelIO::setOutput(int index, int value)
{
    return m_motion->setDigitalOutput(2, index, value);
}

void HQmlModelIO::zeroForceSensor()
{
    double value = 0;
    m_motion->getForceValue(&value);
    m_forceOffset+=value;
    m_motion->setForceOffset(-m_forceOffset);
}

QString HQmlModelIO::getForceValue()
{
    double value = 0;
    m_motion->getForceValue(&value);
    return QString::number(value, 'f', 4);
}

void HQmlModelIO::setInputName(int index, const QString &name)
{
    m_inputNames[index] = name;
}

void HQmlModelIO::setOutputName(int index, const QString &name)
{
    m_outputNames[index] = name;
}

bool HQmlModelIO::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/ioConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    return isok;
}

bool HQmlModelIO::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/ioConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}


bool operator>>(QDataStream &input, HQmlModelIO &obj)
{
    input >>obj.m_forceOffset>> obj.m_inputNames >> obj.m_outputNames;
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelIO &obj)
{
    output <<obj.m_forceOffset<< obj.m_inputNames << obj.m_outputNames;
    return true;
}
