#ifndef HQMLMODELTOOLBAR_H
#define HQMLMODELTOOLBAR_H

#include <QObject>

class HQmlModelToolbar : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelToolbar(QObject *parent = nullptr);
public slots:

    void PickTooling();
    void ReleaseTooling();
    void PickTarget();
    void ReleaseTarget();
    void moveToWaitPos();
    void moveToLocationPos1();
    void moveToLocationPos2();
    void runFlow();
signals:

};

#endif // HQMLMODELTOOLBAR_H
