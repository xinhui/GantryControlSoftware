#include "hqmlmodeldatavisual.h"

#include "hmotioncontroller.h"
#include <iostream>

HQmlModelDataVisual::HQmlModelDataVisual(QObject *parent) : QObject(parent)
{
    std::cout<<"Load Model Data Visual" <<std::endl;
    m_buffer.fill(0, 5);
    m_motion = HMotionController::GetInstance();
}

QVector<double> HQmlModelDataVisual::getDatas()
{
    if (m_sources[0]) {
        m_buffer[0] = m_motion->getAxisPosition(Axis_X);
    }
    if (m_sources[1]) {
        m_buffer[1] = m_motion->getAxisPosition(Axis_Y);
    }
    if (m_sources[2]) {
        m_buffer[2] = m_motion->getAxisPosition(Axis_Z);
    }
    if (m_sources[3]) {
        m_buffer[3] = m_motion->getAxisPosition(Axis_T);
    }
    if (m_sources[4]) {
        m_buffer[4] = m_motion->getForceValue();
    }
    return m_buffer;
}



