#include "hqmlmodeltoolbar.h"
#include "hqmlmodelCapture.h"

#include <QThread>

#include "hqmlmodelflow.h"

HQmlModelToolbar::HQmlModelToolbar(QObject *parent) : QObject(parent)
{
    std::cout<<"Loading Model Toolbar!"<<std::endl;

}

void HQmlModelToolbar::PickTooling()
{
    QThread *gthread = QThread::create([ = ]() {
        HQmlModelCapture::capturePtr->getFixture(HQmlModelCapture::capturePtr->getCurrent());
    });
    gthread->start(QThread::TimeCriticalPriority);
}

void HQmlModelToolbar::ReleaseTooling()
{
    QThread *gthread = QThread::create([ = ]() {
        HQmlModelCapture::capturePtr->letFixture(HQmlModelCapture::capturePtr->getCurrent());
    });
    gthread->start(QThread::TimeCriticalPriority);
}

void HQmlModelToolbar::PickTarget(){

}

void HQmlModelToolbar::ReleaseTarget(){
    
}

void HQmlModelToolbar::moveToWaitPos()
{
    //HQmlModelFlow::flowPtr->moveToWaitPos();
}

void HQmlModelToolbar::moveToLocationPos1()
{
    // HQmlModelFlow::flowPtr->moveToLocationPos1();
}

void HQmlModelToolbar::moveToLocationPos2()
{
    //HQmlModelFlow::flowPtr->moveToLocationPos2();
}

void HQmlModelToolbar::runFlow()
{
    HQmlModelFlow::flowPtr->runFlow();
}
