﻿#ifndef HNETWORKCOMMOND_H
#define HNETWORKCOMMOND_H


#include <QtCore>

class QTcpSocket;

struct HImgData {
    quint32 rows = 0;
    quint32 cols = 0;
    quint32 type = 0;
    quint32 chns = 0;
};

class  HNetworkCommond : public QObject
{
    Q_OBJECT
public:
    static HNetworkCommond *GetInstance();
    static void Release();

    void setHost(const QString &host);
    void setPort(quint16 port);
    void setHostAndPort(const QString &host, quint16 port);
    QString getHost() {return hostName;}
    quint16 getPort() {return portNumber;}


    bool createConnection();
    void closeConnection();
    bool isConnected();

    qint32 execCommond(const QString &cmd, QString &result, const QString &args = "", qint32 timeOut = 10000);

    const QStringList &getAvailableHosts() const {return availableHosts;}

    const QString &getLastError();

signals:

    void errorOccurd(const QString &str);

private:

    explicit HNetworkCommond(QObject *parent = nullptr);
    ~HNetworkCommond();
    HNetworkCommond(const HNetworkCommond &signal) = delete;

    const HNetworkCommond &operator=(const HNetworkCommond &signal) = delete;

    static HNetworkCommond *m_instance;


private:
    std::atomic_bool isRelyReady;
    QByteArray tcpRelyBlock;


private slots:

    void readRely();;

private:
    QString hostName =/*"2755n6f683.wicp.vip"*/"192.168.20.15";
    quint16 portNumber = 8500;

    QString lastError;

    QStringList availableHosts;

    QTcpSocket *tcpSocket = nullptr;
};

#endif // HNETWORKCOMMOND_H
