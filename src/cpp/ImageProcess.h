#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H

#include <QObject>
#include <QTransform>
#include <QDataStream>
#include<iostream>
#include<cmath>
#include<algorithm>

#include<opencv2/opencv.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>

#include "ImageLoading.h"
#include "Algorithm.h"

typedef std::vector<cv::Point2f> Corners;
typedef cv::Point2f Corner;
typedef std::vector<cv::Mat> vecMat;

class ComplexSpectrum{
    public:
        cv::Mat transformMat;
        cv::Mat x;
        cv::Mat y;
        double mag_min;
        double mag_max;
        cv::Mat magnitude;
        cv::Mat phase;
};

class ImageWithLevel{
    public:
        cv::Mat image;
        cv::Mat * ptrImage;
        int level;
};


namespace ImageProcess {

    double getImageMeanGradValue(cv::Mat src,  int ddepth, int dx, int dy, int ksize = 3, double scale = (1.0), double delta = (0.0), int borderType = 4);
    double getImageMeanGrayValue(cv::Mat src);
    double getImageMeanStdValue(cv::Mat src);

    cv::Mat getBinaryImage(cv::Mat src, double thres_value, double maxval = 255, int type = cv::THRESH_BINARY);

    cv::Mat getSpectrumDFT(cv::Mat src);
    ComplexSpectrum getComplexSpectrum(cv::Mat src);

    cv::Mat getSpectrumiDFT(cv::Mat srcSpectrum, ComplexSpectrum complexSpectrum, int nfactor = 1);


    Corners getCornerFeatures(cv::Mat src, int maxCorners, double qualityLevel, double minDistance, cv::InputArray mask = cv::noArray(), int blockSize = 3, bool useHarrisDetector = false, double k = (0.04));
    Corners getCornerSubPix(cv::Mat src, Corners corners, cv::Size winSize, cv::Size zeroZone, cv::TermCriteria criteria);

    cv::Mat getRectROI(cv::Mat src, Corner LeftUP, double width, double height);
    cv::Mat getRectROI(cv::Mat src, cv::Rect SearchArea);
    cv::Rect RectROI(cv::Mat src, cv::Rect SearchArea);

    cv::Mat DilateErode(cv::Mat src, std::vector<int> Mat_erode, std::vector<int> Mat_dilate);


};

namespace ImageShow{
    void showImage(cv::Mat image, double scale, const std::string ImageName = "show Image", bool TrackBar = false);
    void showImage(cv::Mat image, double scale, const std::string TrackBarName, int *value, int count, cv::TrackbarCallback onChange = 0, void* userdata = 0, const std::string ImageName = "show Image", bool TrackBar = true);
};

namespace ManualOperation{

};

namespace Filter{
    cv::Mat GaussianBlur(cv::Mat src, cv::Size ksize, double sigmaX, double sigmaY = (0.0), int borderType = 4);

    cv::Mat GaussianBandFreqFilter(cv::Mat srcSpectrum, double sigmaX, double sigmaY, double centX, double centY);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, double X, double Y, double width, double height);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, Corner LedtUP, double width, double height);
    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, cv::Rect Filter);
};

namespace Mouse{
    void getsrcImage(std::string _str);
    void on_Mouse(int event, int x, int y, int flags, void* param);
    void on_Mouse_ZoomIn(int event, int x, int y, int flags, void* param);
    QPointF ClickPoint();
    QPointF getPointWithMouse();


};

namespace Orb{
    cv::Point2f FindSensorCorner(std::string loc, int _pointNum, int diskStrelRadius, int thres);
}

#endif
