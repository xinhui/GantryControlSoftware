#ifndef HQMLPATTERNRECOGNITION_H
#define HQMLPATTERNRECOGNITION_H

#include <QObject>
#include <QTransform>
#include <QDataStream>
#include <iostream>

#include "hqmlmodelsettingcoord.h"
#include "Algorithm.h"
#include "ImageLoading.h"
#include "ImageProcess.h"
#include "hqmlcameraparameter.h"

#include <opencv2/opencv.hpp>


class HQmlPatternRecognition : public QObject
{
    Q_OBJECT
public:
    explicit HQmlPatternRecognition(QObject *parent = nullptr);
    ~HQmlPatternRecognition();


    static HQmlPatternRecognition *recognitionPtr;

signals:
    void updateUI(QStringList _strSensor1, QStringList _strSensor2, QStringList _strFlex, QStringList _strModule, QStringList _strSearchSensor1, QStringList _strSearchSensor2, QStringList _strSearchFlex, QStringList _strSearchModule, QStringList _strCentreSensor1, QStringList _strCentreSensor2, QStringList _strCentreFlex, QStringList _strCentreModule, QString _strSensor1Focus, QString _strSensor2Focus, QString _strFlexFocus, QString _strModuleFocus);
    void computeCompleted(bool isOK);


public slots:
    void setPointX(QString _type, int _index, const QString &_text);
    void setPointY(QString _type, int _index, const QString &_text);
    void setSearchPoint(QString _type, int _index);
    void setFocusHeight(QString _type);

    bool moveXY(QString _type, int _index);
    bool moveToFocus(QString _type);

    void updateCentreInfo();

    QPointF getCentre(QString _type);
    double getRotation(QString _type);
    double getWidth(QString _type);
    double getHeight(QString _type);

    void selectPointInPicture(QString _type, int _index);
    void recognizeCorner(QString _type, int _index);
    void recognizeFlex(int _ali, QString _type);

protected:
    friend QDataStream &operator>>(QDataStream &input, HQmlPatternRecognition &obj);

    friend QDataStream &operator<<(QDataStream &output, const HQmlPatternRecognition &obj);

private:

    bool loadConfig();
    bool saveConfig();

    void setPointX(QString _type, int _index, double _value);

    QPointF getCentrePoint(QString _type);
    double getCentrePointX(QString _type);
    double getCentrePointY(QString _type);
    double getRotAngle(QString _type);

    QPointF getCurrentXY();


private:

    Sensor Sensor1;
    Sensor Sensor2;
    Flex ModuleFlex;
    Flex Module;

    double FlexCentreBiasX = 2.29;
    double FlexCentreBiasY = 0.22;

    double ModuleCentreBiasX = 2.29;
    double ModuleCentreBiasY = -4.9160;

    HMotionController *m_motion;

};

#endif // HQMLPATTERNRECOGNITION_H
