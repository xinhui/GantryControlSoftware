#ifndef HQMLMODELDISPENSEPATTERNSETTING_H
#define HQMLMODELDISPENSEPATTERNSETTING_H

#include <QObject>
#include <QDataStream>
#include "src/cpp/DispensePattern.h"
#include "hqmlmodelsettingcoord.h"
#include "hqmlmodelflow.h"

class HQmlModelDispensePatternSettings: public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelDispensePatternSettings(QObject *parent = nullptr);
    ~HQmlModelDispensePatternSettings();

    static HQmlModelDispensePatternSettings *patternPtr;

    const QStringList &getPatternNames() {return m_patternNames;}
    DispensePattern getCurrentPattern(){return m_patterns[m_patternNames.indexOf(m_currentPatternName)];}


signals:
    void updateControlData(int currentPatternIndex, QStringList patternNames, int currentPatternType, QStringList currentActionNames,QStringList patternOffset);
    void updateGluingOffset(QStringList _strGluingOffset);

public slots:
    QStringList getActionNames();

    void addPattern(QString name, int patternType);
    void removePattern(int index);
    void setCurrentPattern(int index);
    void appendActionToCurrentPattern(QString actionName);
    void removeActionFromCurrentPattern(int index);
    void clearCurrentActions();
    QStringList getAxisFPOS();

    void updateUI();
    void receiveSignalForGluingOffset(MPointXYZT _glueDotPos);
    void updateGluingOffsetUI();

    //bool setCurrentDispenseSettings() {return getCurrentAction().setDispenseSettings();};
    bool runPattern(const QString &patterName, MPointXYZT offset = MPointXYZT());
    bool runCurrentPattern();


protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlModelDispensePatternSettings &obj);
    friend bool operator<<(QDataStream &output, const HQmlModelDispensePatternSettings &obj);


private:
    QStringList m_patternNames;
    QVector<int> m_patternTypes;
    QString m_currentPatternName;
    QString m_defaultPattern;
    QVector<QStringList> m_actionNameCollection;
    QStringList m_currentActionNames;

    QVector<DispensePattern> m_patterns;

    HMotionController *m_motion;
    MPointXYZT m_patternOffset;
};

#endif // HQMLMODELDISPENSEPATTERNSETTING_H
