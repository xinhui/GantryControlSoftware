﻿#ifndef HQMLMODELCAMERA_H
#define HQMLMODELCAMERA_H

#include <QObject>
#include <QDebug>

class QTcpSocket;

class HQmlModelCamera : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelCamera(QObject *parent = nullptr);
    ~HQmlModelCamera();

    static HQmlModelCamera *instance;

    static qint32 execCommond(QString cmd, QStringList &result, qint32 timeOut = 5000);

    void setThread(QThread *thread);

public slots:
    void setHost(QString host);
    void setPort(quint16 port);
    void setHostAndPort(const QString &host, quint16 port);
    QString getHost() {return hostName;}
    quint16 getPort() {return portNumber;}
    int getEndMark() {return m_endMarkes.indexOf(m_endMark);}


    bool createConnection();
    void closeConnection();
    bool isConnected();



    bool sendCommond(QString cmd);

    QStringList getEndMarks() {return m_endMarks;}
    void setEndMark(int index) {m_endMark = m_endMarkes[index];}

    QString getLastError();

protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlModelCamera &obj);
    friend bool operator<<(QDataStream &output, const HQmlModelCamera &obj);

private:
    std::atomic_bool isRelyReady;
    QByteArray tcpRelyBlock;

    QMetaObject::Connection m_connection1;
    QMetaObject::Connection m_connection2;


signals:

    void errorOccurd(const QString &str);
    void cmdResponsed(const QString &str);
private slots:

    void readRely();;

private:
    QString hostName =/*"2755n6f683.wicp.vip"*/"192.168.20.15";
    quint16 portNumber = 8500;

    QString lastError;

    QStringList availableHosts;
    QStringList m_endMarks{"\\r", "\\n", "\\r\\n"};
    QStringList m_endMarkes{"\r", "\n", "\r\n"};
    QString m_endMark = "\n";
    QTcpSocket *tcpSocket = nullptr;


};

#endif // HQMLMODELCAMERA_H
