#include "hqmlcameraparameter.h"
#include "hmathfunctions.h"
#include "hqmlmodelcamera.h"
#include "hmotioncontroller.h"
#include "hqmlmodelCapture.h"
#include "hqmlmodelflow.h"
#include "hqmlmodelsettingcoord.h"


#include <QtConcurrent>
#include <QFutureWatcher>
#include <ctime>
#include <fstream>
#include <cstdio>

using namespace cv;

HQmlCameraParameter *HQmlCameraParameter::cameraPtr = nullptr;
HQmlCameraParameter::HQmlCameraParameter(QObject *parent) : QObject(parent)
{
    std::cout << "loading Camera Parameter config" << std::endl;
    m_motion = HMotionController::GetInstance();
    if (!loadConfig()) {
        Num = 0;
        Z_Focus = 0;
        CameraAlgoris = 0;
        Array_CameraAlgoris.fill(0,10);
        Array_ScanRectangle.resize(4);
        ChessboardPattern.resize(2);
        Array_Recognition.resize(Num);
    std::cout << "loading Camera Parameter config fail!" << std::endl;
    }
    if (Array_ScanRectangle.size() != 4) Array_ScanRectangle.resize(4);
    if (ChessboardPattern.size() != 2) ChessboardPattern.resize(2);
    if (Array_Recognition.size() != Num) Array_Recognition.resize(Num);
    cameraPtr = this;
}

HQmlCameraParameter::~HQmlCameraParameter()
{
    if (saveConfig()) std::cout << "saving Camera Parameter config" << std::endl;
}

void HQmlCameraParameter::setCameraAliArray(int index, const QString &text){
    std::cout << "set camera ali array" << std::endl;
    Array_CameraAlgoris[index] = text.toInt();
}

void HQmlCameraParameter::setCameraAli(const QString &text){
    std::cout << "set camera ali" << std::endl;
    CameraAlgoris = text.toInt();
}

void HQmlCameraParameter::setFocusHeight(const QString &text){
    Z_Focus = text.toDouble();
    std::cout<<Z_Focus<<std::endl;
}

QStringList HQmlCameraParameter::getAxisFPOS()
{
    return QStringList{QString::number(m_motion->getAxisPosition(Axis_X), 'f', 4),
                QString::number(m_motion->getAxisPosition(Axis_Y), 'f', 4),
                QString::number(m_motion->getAxisPosition(Axis_Z), 'f', 4),
                QString::number(m_motion->getAxisPosition(Axis_T), 'f', 4)};
}

bool HQmlCameraParameter::moveZ(const QString &zFocus)
{
    return moveToZ(zFocus.toDouble());
}

bool HQmlCameraParameter::moveXY(const QString &x, const QString &y){
    QPointF PointXY;
    PointXY.setX(x.toDouble());
    PointXY.setY(y.toDouble());
    std::cout << " moveToXY, X: " << PointXY.x() << ", Y: " << PointXY.y()<<std::endl;
    return moveToXY(PointXY);
}

void HQmlCameraParameter::setScanRangeData(int index, const QString axis, const QString &text){
    if (axis == "x")    Array_ScanRectangle[index].setX(text.toDouble());
    if (axis == "y")    Array_ScanRectangle[index].setY(text.toDouble());

    std::cout<<"x: " << Array_ScanRectangle[index].x() <<
               ", y: " << Array_ScanRectangle[index].y() <<std::endl;
}

void HQmlCameraParameter::setScanNum(const QString &text){
    Num = text.toInt();
    if (Array_Recognition.size() != Num) Array_Recognition.resize(Num);
    std::cout << "Num: " << Num << std::endl;
}

void HQmlCameraParameter::setChessboardPattern(const QString index, const QString axis, const QString &text){
    if (axis == "x")    {
        if (index == "dimension")   ChessboardPattern[1].setX(text.toDouble());
        if (index == "col")    ChessboardPattern[0].setX(text.toInt());
    }
    if (axis == "y"){
        if (index == "dimension")   ChessboardPattern[1].setY(text.toDouble());
        if (index == "row")    ChessboardPattern[0].setY(text.toInt());
    }
    //std::cout << "Col: " << ChessboardPattern[0].x() << ", Row: "<<ChessboardPattern[0].y() <<std::endl;
    //std::cout << "Width: " << ChessboardPattern[1].x() << ", Height: "<<ChessboardPattern[1].y() <<std::endl;
}

void HQmlCameraParameter::computeInstrisincParMatrix(){

    QStringList cameraResult;
    HQmlModelCamera::execCommond(QString("PW,1,%1").arg(CameraAlgoris, 3, 10, QLatin1Char('0')), cameraResult);

// get file name & pictures
    // get local date & time
    time_t now = time(0);
    tm *ltm = localtime(&now);

    QString dumpRoot = "C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_000/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    QString fileRoot = dumpRoot.append(date).append("/CAM1/");
    QString imageCali_path = fileRoot.append("Cali_*.bmp");
    std::cout<<fileRoot.toStdString()<<std::endl;

    std::vector<String> images_path;

    std::vector<std::vector<cv::Point3f>> ObjectPoints;
    std::vector<cv::Point3f> ObjectPoint;
    std::vector<std::vector<cv::Point2f>> ImagePoints;
    std::vector<cv::Point2f> ImagePoint;
    std::vector<cv::Point2f> tempPoint;
    cv::Size ImageSize(2432, 2040);
    CameraMatrix = cv::Mat::eye(3, 3, CV_64F);
    DistCoeffs = cv::Mat::eye(1, 5, CV_64F);

    cv::Mat Rot_Inverse, Rot3D, ObjectPointTransverse;
    cv::Mat OutputTempMat;
    cv::Mat image, image_gray;
    double totalEro = 0;
    double Ero = 0;
    std::vector<cv::Point2f> image_points2;

    std::cout << "compute camera matrix " <<std::endl;

    QVector<QPointF> RCPList;

    // create chessboard in camera reference frame
    ObjectPoint = BuildChessBoard(ChessboardPattern);

    // create RCPList
    RCPList = CreateRandomCameraPos(Num, Array_ScanRectangle);
    //std::cout << "RCPList.size(): " << RCPList.size()<<std::endl;
    std::cout << "RCPList Complete!" <<std::endl;

    /*
    for (auto &k : RCPList){
        std::cout << "RCPList: " << k.x() <<", " << k.y() <<std::endl;
    }
    */

    // move to RCP and get the picture
    for( quint32 i = 0; i < Num ; i++){
        tempPoint.resize(0);
        if (!getCaliCameraPos(tempPoint, RCPList[i], 16, CameraAlgoris))  goto error;
        std::cout <<"pic " <<i+1<<" took"<<std::endl;
        /*
        std::cout << "******ObjectPoint*****"<<std::endl;
        for (auto &k : ObjectPoint){
            std::cout << "ObjectPoint: " << i << ", " << k <<std::endl;
        }
        */

    }

    QThread::msleep(2000);

    // recognize the corner
    cv::glob(imageCali_path.toStdString(), images_path);
    std::cout <<"images_path.size()" <<images_path.size()<<std::endl;
    for (quint32 i = 0; i < Num; i++){
        std::cout <<"Run " <<i<<" start, ";
        image = cv::imread(images_path[i]);
        std::cout <<images_path[i]<<std::endl;
        image_gray = cv::Mat::eye(image.size(), CV_64F);
        cv::cvtColor(image, image_gray, COLOR_BGR2GRAY);
        if (1 == cv::findChessboardCorners(image_gray, Size(ChessboardPattern[0].x(), ChessboardPattern[0].y()), ImagePoint)){
            cv::find4QuadCornerSubpix(image_gray, ImagePoint, Size(ChessboardPattern[0].x(), ChessboardPattern[0].y()));
            ImagePoints.push_back(ImagePoint);
            ObjectPoints.push_back(ObjectPoint);
            std::cout <<"Run " <<i+1<<" finished"<<std::endl;
/*
            for (auto &k : ImagePoint){
                std::cout << "ImagePoint: " << i << ", " << k <<std::endl;
            }
            cv::drawChessboardCorners(image_gray,Size(ChessboardPattern[0].x(), ChessboardPattern[0].y()),ImagePoint,false);
            cv::resize(image_gray, image_gray, Size(912, 765));
            cv::imshow("Camera Calibration",image_gray);
            cv::waitKey(500);
*/
        }
        else{
            std::cout <<"Run " <<i+1<<" failed!"<<std::endl;
            cv::drawChessboardCorners(image,Size(ChessboardPattern[0].x(), ChessboardPattern[0].y()),ImagePoint,false);
            cv::resize(image, image, Size(912, 765));
            cv::imshow("Error",image);
            cv::waitKey(500);
            goto error;
        }
    }

    // start calibration
    std::cout << "start calibration" <<std::endl;
    cv::calibrateCamera(ObjectPoints, ImagePoints, ImageSize, CameraMatrix, DistCoeffs, RotationMatrix, TransformationMatrix);
    std::cout << "complete calibration" <<std::endl;
    QStrCameraMats = getStrs_Mats(CameraMatrix);
    QStrDistCoeffs = getStrs_Mats(DistCoeffs);

    /*
    for (auto &k : qAsConst(QStrCameraMats)){
        std::cout<<k.toStdString()<<std::endl;
    }
    for (auto &k : qAsConst(QStrDistCoeffs)){
        std::cout<<k.toStdString()<<std::endl;
    }
    */
    std::cout << "***********Camera Matrix*********"<<std::endl;
    std::cout<<CameraMatrix<<std::endl;
    std::cout << "***********DistCoeffs*********"<<std::endl;
    std::cout<<DistCoeffs<<std::endl;
    std::cout << "***********Trans*********"<<std::endl;

//---------------evaluating Camera Matrix--------------------------
    std::cout<<"evaluating" <<std::endl;
    std::cout<<"calibrate error: "<<std::endl;
    for (int i=0; i < Num;i++){
        std::vector<cv::Point3f> tempPointSet=ObjectPoints[i];
        cv::projectPoints(tempPointSet,RotationMatrix[i],TransformationMatrix[i],CameraMatrix,DistCoeffs,image_points2);
        std::vector<cv::Point2f> tempImagePoint = ImagePoints[i];
        Mat tempImagePointMat = Mat(1,tempImagePoint.size(),CV_32FC2);
        Mat image_points2Mat = Mat(1,image_points2.size(), CV_32FC2);

        for (int i = 0; i < tempImagePoint.size(); i++){
            std::cout << " PointSet: " << i << ", " << tempPointSet[i] <<
                         " ImagePoint: "   << tempImagePoint[i] <<
                         " -> "   << image_points2[i] <<
                         std::endl;
        }

        for (int j = 0 ; j < tempImagePoint.size(); j++){
            image_points2Mat.at<cv::Vec2f>(0,j) = cv::Vec2f(image_points2[j].x, image_points2[j].y);
            tempImagePointMat.at<cv::Vec2f>(0,j) = cv::Vec2f(tempImagePoint[j].x, tempImagePoint[j].y);
            //std::cout << "show0: " <<  image_points2Mat.at<cv::Vec2f>(0,j) << ", " << tempImagePointMat.at<cv::Vec2f>(0,j) << std::endl;
            //std::cout << j+1 << " mean error: " << norm(image_points2Mat.at<cv::Vec2f>(0,j), tempImagePointMat.at<cv::Vec2f>(0,j), NORM_L2) <<" px" <<std::endl;
        }
        //std::cout << image_points2Mat <<std::endl;
        //std::cout << tempImagePointMat <<std::endl;
        Ero = cv::norm(image_points2Mat, tempImagePointMat, NORM_L2);
        totalEro += Ero /= 16;
        std::cout << "Image " << i+1 << " mean error: " << Ero <<" px" <<std::endl;
    }

    //-----------------------Calculating Z_c-------------------------
    std::cout << "total error: " << totalEro/Num << std::endl;

    std::cout<< "evaluation complete" <<std::endl;

    Rot3D = getRotationMatrix3D(RotationMatrix[0].at<double>(0),
            RotationMatrix[0].at<double>(1),
            RotationMatrix[0].at<double>(2));
    std::cout<<"******RotationMatrix[0]******"<<std::endl;
    std::cout<<Rot3D<<std::endl;
    cv:invert(Rot3D, Rot_Inverse, DECOMP_LU);
    ObjectPointTransverse = cv::Mat::zeros(3, 16, CV_64F);
    OutputTempMat = cv::Mat::zeros(3, 1, CV_64F);
    for (int i = 0; i < 16; i++){
        ObjectPointTransverse.at<double>(0,i) = ObjectPoint[i].x;
        ObjectPointTransverse.at<double>(1,i) = ObjectPoint[i].y;
        ObjectPointTransverse.at<double>(2,i) = ObjectPoint[i].z;
    }
    std::cout<<ObjectPointTransverse<<std::endl;
    cv::solve(Rot_Inverse, ObjectPointTransverse, OutputTempMat);

    X_c =OutputTempMat.at<double>(0,0)+TransformationMatrix[0].at<double>(0,0);
    Y_c =OutputTempMat.at<double>(1,0)+TransformationMatrix[0].at<double>(0,1);
    Z_c =OutputTempMat.at<double>(2,0)+TransformationMatrix[0].at<double>(0,2);

    std::cout<<"X_c: "<< X_c << "Y_c: "<< Y_c << "Z_c: "<< Z_c <<std::endl;


error:
    computeCompleted(true);
    return;
}

//void HQmlCameraParameter::showInstrisincParMatrix(int row, int col){}


void HQmlCameraParameter::setCameraOffset(const QString axis, const QString &text){
    if (axis == "x") CameraPosOffset.setX(text.toDouble());
    if (axis == "y") CameraPosOffset.setY(text.toDouble());
}

void HQmlCameraParameter::setPositionInGantry(const QString axis, const QString &text){
    if (axis == "x") PositionInGantry.setX(text.toDouble());
    if (axis == "y") PositionInGantry.setY(text.toDouble());
}

void HQmlCameraParameter::setPositionInPicture(const QString axis, const QString &text){
    if (axis == "x") PositionInPicture.setX(text.toDouble());
    if (axis == "y") PositionInPicture.setY(text.toDouble());
}

void HQmlCameraParameter::transPictureToGantry(){
/*
    cv::Mat image, image_gray, undistort_image_gray;
    time_t now = time(0);
    tm *ltm = localtime(&now);
    QString dumpRoot = "C:/Users/CTOS/Documents/KEYENCE/CV-X Series Terminal-Software/10.0.0.15/SD2/cv-x/image/SD1_000/";
    QString date = QString("%1%2%3").arg(ltm->tm_year-100, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mon+1, 2, 10, QLatin1Char('0'))
            .arg(ltm->tm_mday, 2, 10, QLatin1Char('0'));
    QString fileRoot = dumpRoot.append(date).append("/CAM1/");
    QString imageCali_path = fileRoot.append("Cali_20220919_01.bmp");
    std::cout<<imageCali_path.toStdString()<<std::endl;
    image = cv::imread(imageCali_path.toStdString());
    image_gray = cv::Mat::eye(image.size(), CV_32FC1);
    cv::cvtColor(image, image_gray, COLOR_BGR2GRAY);
    std::cout<<"image_gray generated"<<std::endl;
    cv::resize(image_gray, image_gray, Size(912, 765));
    undistort_image_gray = cv::Mat::eye(image_gray.size(), CV_32FC1);
*/
    //cv::undistort(image_gray, undistort_image_gray, CameraMatrix, DistCoeffs);
    //cv::imshow("Undistort",undistort_image_gray);
    cv::waitKey(500);

    std::cout<<"******Transform Picture to Gantry******"<<std::endl;

    std::vector<Point2f> tempPIP, undistortPIP;
    tempPIP.resize(0);
    cv::Point2f tempPoint(PositionInPicture.x(), PositionInPicture.y());
    tempPIP.push_back(tempPoint);

    CameraMatrix = getMats_Strs(QStrCameraMats, 3, 3);
    DistCoeffs = getMats_Strs(QStrDistCoeffs, 1, 5);

    std::cout<<"X_c: "<< X_c << "Y_c: "<< Y_c << "Z_c: "<< Z_c <<std::endl;

    /*
    std::cout << "***********Camera Matrix*********"<<std::endl;
    for (auto &k : qAsConst(QStrCameraMats)){
        std::cout<<k.toStdString()<<std::endl;
    }
    std::cout<<CameraMatrix<<std::endl;
    std::cout << "***********DistCoeffs*********"<<std::endl;
    for (auto &k : qAsConst(QStrDistCoeffs)){
        std::cout<<k.toStdString()<<std::endl;
    }
    std::cout<<DistCoeffs<<std::endl;
    */
    cv::undistortPoints(tempPIP, undistortPIP, CameraMatrix, DistCoeffs, cv::noArray(), CameraMatrix);
    std::cout<<"tempPIP: " << tempPIP<<std::endl;
    std::cout<<"PIP undistorted: " << undistortPIP<<std::endl;

    cv::Mat PIP = (cv::Mat_<double>(3, 1) << tempPIP[0].x, tempPIP[0].y, 1);
    cv::Mat PIPundistort = (cv::Mat_<double>(3, 1) << undistortPIP[0].x, undistortPIP[0].y, 1);
    std::cout<<"PIP: " << PIP<<std::endl;
    cv::Mat PIC = cv::Mat::zeros(3, 1, CV_64F);
    cv::Mat PIG = cv::Mat::zeros(3, 1, CV_64F);
    cv::Mat Offset = (cv::Mat_<double>(3,1) << CameraPosOffset.x(), CameraPosOffset.y(), 0);
    cv::Mat CM_Inverse = cv::Mat::zeros(3, 3, CV_64F);
    cv::invert(CameraMatrix, CM_Inverse, DECOMP_LU);
    cv::Mat TransCameraToGantry = ::Mat::zeros(3, 3, CV_64F);
    TransCameraToGantry.at<double>(0,0) = 1;
    TransCameraToGantry.at<double>(1,1) = -1;
    TransCameraToGantry.at<double>(2,2) = 1;

    PIC = Z_c*CM_Inverse*PIP;
    PIG = TransCameraToGantry*PIC-Offset;
    std::cout<<"PIC: "<<PIC<<std::endl;
    std::cout<<"PIG: "<<PIG<<std::endl;

    PositionInGantry.setX(m_motion->getAxisPosition(Axis_X)+(PIG.at<double>(0, 0)));
    PositionInGantry.setY(m_motion->getAxisPosition(Axis_Y)+(PIG.at<double>(1, 0)));


}

QPointF HQmlCameraParameter::transPicureToGantry(QPointF _pointInPicture){

        std::cout<<"******Transform Picture to Gantry******"<<std::endl;

        QPointF PointInGantry;
        std::vector<Point2f> vec_PIP, vec_undistortPIP;
        vec_PIP.resize(0);
        cv::Point2f PointInPicture(_pointInPicture.x(), _pointInPicture.y());
        vec_PIP.push_back(PointInPicture);

        CameraMatrix = getMats_Strs(QStrCameraMats, 3, 3);
        DistCoeffs = getMats_Strs(QStrDistCoeffs, 1, 5);

        cv::undistortPoints(vec_PIP, vec_undistortPIP, CameraMatrix, DistCoeffs, cv::noArray(), CameraMatrix);
        std::cout<<"vec_PIP: " << vec_PIP<<std::endl;
        std::cout<<"PIP undistorted: " << vec_undistortPIP<<std::endl;

        cv::Mat PIP = (cv::Mat_<double>(3, 1) << vec_PIP[0].x, vec_PIP[0].y, 1);
        cv::Mat PIPundistort = (cv::Mat_<double>(3, 1) << vec_undistortPIP[0].x, vec_undistortPIP[0].y, 1);
        std::cout<<"PIP: " << PIP<<std::endl;
        cv::Mat PIC = cv::Mat::zeros(3, 1, CV_64F);
        cv::Mat PIG = cv::Mat::zeros(3, 1, CV_64F);
        cv::Mat Offset = (cv::Mat_<double>(3,1) << CameraPosOffset.x(), CameraPosOffset.y(), 0);
        cv::Mat CM_Inverse = cv::Mat::zeros(3, 3, CV_64F);
        cv::invert(CameraMatrix, CM_Inverse, DECOMP_LU);
        cv::Mat TransCameraToGantry = ::Mat::zeros(3, 3, CV_64F);
        TransCameraToGantry.at<double>(0,0) = 1;
        TransCameraToGantry.at<double>(1,1) = -1;
        TransCameraToGantry.at<double>(2,2) = 1;

        PIC = Z_c*CM_Inverse*PIP;
        PIG = TransCameraToGantry*PIC-Offset;
        std::cout<<"PIC: "<<PIC<<std::endl;
        std::cout<<"PIG: "<<PIG<<std::endl;

        PointInGantry.setX(m_motion->getAxisPosition(Axis_X)+(PIG.at<double>(0, 0)));
        PointInGantry.setY(m_motion->getAxisPosition(Axis_Y)+(PIG.at<double>(1, 0)));

        return PointInGantry;
}

bool HQmlCameraParameter::loadConfig()
{
    std::cout << "load Camera config" << std::endl;
    QString configFile = QCoreApplication::applicationDirPath() + "/user/cameraPosConfig.cfg";
    std::cout << configFile.toStdString() << std::endl;
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();
    return isok;
}

bool HQmlCameraParameter::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/cameraPosConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}

void HQmlCameraParameter::updateCameraData()
{
    QString num;
    QString zFocus;
    QString cameraAli;
    QStringList scanList;
    QStringList chessboardPat;
    QStringList cameraMats;
    QStringList distCoeffs;
    QStringList rot;
    QStringList transf;
    QStringList cameraPosOffset;
    QStringList positionInGantry;
    QStringList positionInPicture;

    num = QString::number(Num);

    zFocus = QString::number(Z_Focus);

    cameraAli = QString::number(CameraAlgoris);

    for (auto &k : qAsConst(Array_ScanRectangle)){
        scanList << getStrs_XY(k);
    }

    for (auto &k : qAsConst(ChessboardPattern)){
        chessboardPat << getStrs_XY(k);
    }

    cameraMats = QStrCameraMats;
    distCoeffs = QStrDistCoeffs;

    cameraPosOffset = getStrs_XY(CameraPosOffset);
    positionInPicture = getStrs_XY(PositionInPicture);
    positionInGantry = getStrs_XY(PositionInGantry);


    std::cout <<"update camera panel data "<< num.toStdString()<<std::endl;

    emit updateAllCameraData(num, zFocus, cameraAli, scanList, chessboardPat, Array_CameraAlgoris,
                            cameraMats, distCoeffs,
                            cameraPosOffset,
                            positionInGantry, positionInPicture);
}

void HQmlCameraParameter::updateCameraMatrixData(){
    QStringList cameraMats;
    cameraMats = QStrCameraMats;
    emit updateAllCameraMatrixData(cameraMats);
}

void HQmlCameraParameter::updatePIGData(){
    QStringList positionInGantry;
    positionInGantry = getStrs_XY(PositionInGantry);
    emit updateAllPIGData(positionInGantry);
}

// move to position pt
// take a picture with pixel coordinate pos
// read cornor point with number num
// save at ali in the SD card
bool HQmlCameraParameter::getCaliCameraPos(std::vector<cv::Point2f> &pixel, const QPointF &pos, quint32 num, quint16 ali)
{
    static quint16 kkkkk = -1;
    double vel = 5; // moving speed
    double acc = 5; // moving accelerater
    if (!m_motion->moveAbs(Axis_X, pos.x(), vel, acc))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y(), vel, acc))
        return false;
    if (!m_motion->waitAllAxisStop())
        return false;
    QThread::msleep(2000);
    QStringList cameraResult;
    if (kkkkk != ali) {
        if (HQmlModelCamera::execCommond("R0", cameraResult)){
            return false;
        }
        //PW-检测设定切换, 1-SD1, ali-检测设定
        if (HQmlModelCamera::execCommond(QString("PW,1,%1").arg(ali, 3, 10, QLatin1Char('0'))
                                         , cameraResult))
            return false;
        // not null QStringList
        if (cameraResult.size() != 1)
            return false;
        kkkkk = ali;
    }
    // HQmlModelCamera::execCommond("T1", cameraResult) return -1 if it succeeds
    if (HQmlModelCamera::execCommond("T1", cameraResult)){
        return false;
    }
    if (cameraResult.size() < num*2)
        return false;
    for (int i = 0; i < num; i++){
        cv::Point2f tempPoint(cameraResult[2*i].toDouble(),cameraResult[2*i+1].toDouble());
        pixel.push_back(tempPoint);
    }

    return true;
}

bool HQmlCameraParameter::moveToZ(double pos, bool isWait)
{
    double vel = 10; // moving speed
    double acc = 5; // moving accelerater
    if (!m_motion->moveAbs(Axis_Z, pos, vel, acc, true))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlCameraParameter::moveToXY(const QPointF &pos, bool isWait)
{
    double vel = 8; // moving speed
    double acc = 5; // moving accelerater
    if (!m_motion->moveAbs(Axis_X, pos.x(), vel, acc))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y(), vel, acc))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlCameraParameter::moveToXYWait(const QPointF &pos)
{
    if (!m_motion->moveAbs(Axis_X, pos.x()))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y()))
        return false;
    return m_motion->waitAllAxisStop();
}

QStringList HQmlCameraParameter::getStrs_XY(const QPointF &pos){
    return QStringList{QString::number(pos.x(), 'f', 4), QString::number(pos.y(), 'f', 4)};
}

QStringList HQmlCameraParameter::getStrs_Mats(cv::Mat Mat){
    QStringList MatStrList;
    for (int i = 0; i < Mat.rows; i++){
        for (int j = 0; j < Mat.cols; j++){
            MatStrList.push_back(QString::number(Mat.at<double>(i,j)));
        }
    }
    return MatStrList;
}

cv::Mat HQmlCameraParameter::getMats_Strs(const QStringList &QStrMat, int row, int col){
    cv::Mat FnlMat;
    FnlMat = cv::Mat::eye(row, col, CV_64F);
    for (int i = 0; i < row; i++){
        for (int j = 0; j < col; j++){
            FnlMat.at<double>(i,j) = QStrMat[i*3+j].toDouble(0);
        }
    }
    return FnlMat;
}

std::vector<cv::Point3f> HQmlCameraParameter::BuildChessBoard(const QVector<QPointF> &ChessboardPattern){
    std::vector<cv::Point3f> ObjectPoint;
    for (int row = 0; row < ChessboardPattern[0].y(); row++){
        for (int col = 0; col < ChessboardPattern[0].x(); col++){
            ObjectPoint.push_back(cv::Point3f(col*ChessboardPattern[1].x(), row*ChessboardPattern[1].y(), 0));
        }
    }

    return ObjectPoint;
}

QVector<QPointF> HQmlCameraParameter::CreateRandomCameraPos(int num, const QVector<QPointF> &array_ScanRectangle){
    QVector<QPointF> RCPList;
    QPointF RCP;
    double RectWidth = array_ScanRectangle[3].x()-array_ScanRectangle[0].x();
    double RectHeight = array_ScanRectangle[1].y()-array_ScanRectangle[0].y();
    srand(time(NULL));
    int N = 999;

    for (int i = 0; i < num; i++){
        double x_rand = rand() % (N+1) / (double)(N+1);
        double y_rand = rand() % (N+1) / (double)(N+1);

        RCP.setX(array_ScanRectangle[0].x()+x_rand*RectWidth);
        RCP.setY(array_ScanRectangle[0].y()+y_rand*RectHeight);

        RCPList.push_back(RCP);
    }
    return RCPList;
}

cv::Mat HQmlCameraParameter::getRotationMatrix3D(double theta, double phi, double omega){
    cv::Mat R1, R2, R3;
    R1 = cv::Mat::zeros(3, 3, CV_64F);
    R2 = cv::Mat::zeros(3, 3, CV_64F);
    R3 = cv::Mat::zeros(3, 3, CV_64F);
    R1.at<double>(0,0) = cos(theta);
    R1.at<double>(0,1) = -sin(theta);
    R1.at<double>(0,0) = cos(theta);
    R1.at<double>(1,0) = sin(theta);
    R1.at<double>(2,2) = 1;
    //std::cout<<"R1"<<std::endl;
    //std::cout<<R1<<std::endl;
    R2.at<double>(0,0) = 1;
    R2.at<double>(1,1) = cos(phi);
    R2.at<double>(1,2) = sin(phi);
    R2.at<double>(2,1) = -sin(phi);
    R2.at<double>(2,2) = cos(phi);
    //std::cout<<"R2"<<std::endl;
    //std::cout<<R2<<std::endl;
    R3.at<double>(0,0) = cos(omega);
    R3.at<double>(0,2) = -sin(omega);
    R3.at<double>(1,1) = 1;
    R3.at<double>(2,0) = sin(omega);
    R3.at<double>(2,2) = cos(omega);
    //std::cout<<"R3"<<std::endl;
    //std::cout<<R3<<std::endl;
    return R1*R2*R3;
}

QDataStream &operator>>(QDataStream &input, HQmlCameraParameter &obj)
{
    return input >> obj.Num >> obj.Z_Focus >> obj.Array_CameraAlgoris >> obj.CameraAlgoris >>
                    obj.Array_ScanRectangle >> obj.ChessboardPattern >> obj.Array_Recognition >>
                    obj.QStrCameraMats >> obj.QStrDistCoeffs   >> obj.CameraPosOffset >>
                    obj.PositionInPicture >> obj.PositionInGantry >> obj.X_c >> obj.Y_c >> obj.Z_c;
}

QDataStream &operator<<(QDataStream &output, const HQmlCameraParameter &obj)
{
    return output << obj.Num << obj.Z_Focus << obj.Array_CameraAlgoris << obj.CameraAlgoris <<
                     obj.Array_ScanRectangle << obj.ChessboardPattern << obj.Array_Recognition <<
                     obj.QStrCameraMats << obj.QStrDistCoeffs   << obj.CameraPosOffset <<
                     obj.PositionInPicture << obj.PositionInGantry << obj.X_c << obj.Y_c << obj.Z_c;
}
