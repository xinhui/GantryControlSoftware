#include "hqmlmodelDispenseActionSetting.h"
#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QThread>
#include <iostream>
#include <string>
#include <sstream>

#include "hqmlmodelgluecontroller.h"
#include "hmotioncontroller.h"
#include "hqmlmodelio.h"
#include "QLog.h"

#define CMoveAbs(axis,pos,speed,acc,isWait) if(motion->moveAbs(axis, pos, speed, acc, isWait) == false) {return false;}

QMessageLogContext context_action;

HQmlModelDispenseActionSettings *HQmlModelDispenseActionSettings::DispensePtr = nullptr;

HQmlModelDispenseActionSettings::HQmlModelDispenseActionSettings(QObject *parent) : QObject(parent)
{   std::cout<<"construct the dispensing action setting"<<std::endl;
    QLog::LogMessageOutput(QtInfoMsg,context_action, "construct the dispensing action setting");
    loadConfig();
    updateUI();
    DispensePtr = this;
}

HQmlModelDispenseActionSettings::~HQmlModelDispenseActionSettings()
{
    saveConfig();
}

QStringList HQmlModelDispenseActionSettings::getOutputs()
{
    QLog::LogMessageOutput(QtInfoMsg,context_action, "get outputs");
    return HQmlModelIO::ioPtr->getOutputNames();
}

void HQmlModelDispenseActionSettings::addConfig(QString name)
{
    std::ostringstream oss;
    oss << "add configure " << name.toStdString();
    QLog::LogMessageOutput(QtInfoMsg,context_action, QString::fromStdString(oss.str()));
    if (m_actionNames.contains(name) || name.isEmpty()) {
        return;
    }
    m_actionNames << name;
    m_actionDatas << DispenseAction();
    m_currentActionName = name;
    updateUI();
}

void HQmlModelDispenseActionSettings::removeConfig(int index)
{
    std::ostringstream oss;
    oss << "removing configure " << index;
    QLog::LogMessageOutput(QtInfoMsg,context_action, QString::fromStdString(oss.str()));
    m_actionNames.removeAt(index);
    m_actionDatas.removeAt(index);
    m_currentActionName = m_actionNames.value(index);
    updateUI();
}

void HQmlModelDispenseActionSettings::setCurrentConfig(int index)
{
    std::ostringstream oss;
    oss << "set current configure dispensing action setting" << index;
    QLog::LogMessageOutput(QtInfoMsg,context_action, QString::fromStdString(oss.str()));
    if (index > -1 && index < m_actionNames.size()) {
        m_currentActionName = m_actionNames[index];
    }
    updateUI();
}

bool HQmlModelDispenseActionSettings::doTouchDown()
{
    QLog::LogMessageOutput(QtInfoMsg,context_action, "start doing touch down");
    HMotionController *motion = HMotionController::GetInstance();
    int cfgIndex = m_actionNames.contains(m_currentActionName) ? m_actionNames.indexOf(m_currentActionName) : 0;
    double waitingZ = m_actionDatas.value(cfgIndex).getStrs().value(1).toDouble();
    double gluingZ = m_actionDatas.value(cfgIndex).getStrs().value(2).toDouble();
    double fastSpeed = m_actionDatas.value(cfgIndex).getStrs().value(3).toDouble();
    double fastAcc = m_actionDatas.value(cfgIndex).getStrs().value(4).toDouble();
    double searchSpeed = 0.1;
    double searchForce = m_actionDatas.value(cfgIndex).getStrs().value(13).toDouble();
    QLog::LogMessageOutput(QtInfoMsg,context_action, "got all parameters for running touch-down");
    CMoveAbs(Axis_Z, waitingZ, fastSpeed, fastAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_action, tr("moved to waiting Z %1 ").arg(waitingZ));
    CMoveAbs(Axis_Z, gluingZ, fastSpeed, fastAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_action, tr("moved to gluing Z %1 ").arg(gluingZ));
    QThread::msleep(3000);
    HQmlModelIO::ioPtr->zeroForceSensor();
    QLog::LogMessageOutput(QtInfoMsg,context_action, "zeroed pressure sensor");
    CMoveAbs(Axis_Z, gluingZ-2., searchSpeed, fastAcc, false)
    QLog::LogMessageOutput(QtInfoMsg,context_action, "start moving z ");
    double force = 0;
    while (true) {
        if (!motion->isAxisMove(Axis_Z)) {
            goto ok;
        }
        if (!motion->getForceValue(&force)) {
            QLog::LogMessageOutput(QtInfoMsg,context_action, "fail to get force value");
            goto error;
        }
        if (force > searchForce) {
            motion->killAxis(Axis_Z);
            QThread::msleep(1000);
            goto ok;
        }
        if (QThread::currentThread()->isInterruptionRequested()) {
            goto error;
        }

    }

ok:
    QLog::LogMessageOutput(QtInfoMsg,context_action, "touch down ok");
    QThread::msleep(3000);
    m_touchDownHeight = motion->getAxisPosition(Axis_Z);
    QLog::LogMessageOutput(QtInfoMsg,context_action, tr("get the touch down z: %1 ").arg(m_touchDownHeight));
    CMoveAbs(Axis_Z, waitingZ, fastSpeed, fastAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_action, tr("back to waiting Z: %1 ").arg(waitingZ));
    return true;

error:
    QLog::LogMessageOutput(QtInfoMsg,context_action, "touch down error");
    motion->stopAxis(Axis_Z);
    CMoveAbs(Axis_Z, waitingZ, fastSpeed, fastAcc, true)
    QLog::LogMessageOutput(QtInfoMsg,context_action, tr("back to waiting Z: %1 ").arg(waitingZ));
    return false;
}

QString HQmlModelDispenseActionSettings::getTouchDownValue()
{
    QLog::LogMessageOutput(QtInfoMsg,context_action, "start getting touch down value");
    if (doTouchDown())
    {
        QLog::LogMessageOutput(QtInfoMsg,context_action, "get touch down value successfully");
        return QString::number(m_touchDownHeight,'f',4);
    }
    else
    {
        QLog::LogMessageOutput(QtInfoMsg,context_action, "failed to get touch down value (return 999)");
        return QString::number(999.,'f',4);
    }
}

void HQmlModelDispenseActionSettings::setConfigData(int index, const QString &value)
{
    std::ostringstream oss;
    oss << "set dispensing action configure data " << index;
    QLog::LogMessageOutput(QtInfoMsg,context_action, QString::fromStdString(oss.str()));
    int cindex = m_actionNames.indexOf(m_currentActionName);
    if (cindex != -1) {
        m_actionDatas[cindex].setValue(index, value.toDouble());
    }
}

void HQmlModelDispenseActionSettings::updateUI()
{
    QLog::LogMessageOutput(QtInfoMsg,context_action, "update dispensing action UI");
    int cfgIndex = m_actionNames.contains(m_currentActionName) ? m_actionNames.indexOf(m_currentActionName) : 0;
    emit updateControlData(cfgIndex, m_actionNames, m_actionDatas.value(cfgIndex).getStrs(), m_actionNames.indexOf(m_defaultAction),
                           m_actionNames.indexOf(m_defaultAction), m_touchDownHeight);
}

bool HQmlModelDispenseActionSettings::loadConfig()
{
    QLog::LogMessageOutput(QtInfoMsg,context_action, "start loading dispensing action setting");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/DispenseActionSettingsConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= ds >> *this;
    isok &= ds.atEnd();
    f.close();
    QLog::LogMessageOutput(QtInfoMsg,context_action, "loading dispensing action setting successfully");
    return isok;
}



bool HQmlModelDispenseActionSettings::saveConfig()
{

    QLog::LogMessageOutput(QtInfoMsg,context_action, "start saving dispensing action setting");
    QString configFile = QCoreApplication::applicationDirPath() + "/user/DispenseActionSettingsConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    QLog::LogMessageOutput(QtInfoMsg,context_action, "saving dispensing action setting successfully");
    return true;
}

bool operator>>(QDataStream &input, HQmlModelDispenseActionSettings &obj)
{
    input  >> obj.m_actionNames >> obj.m_currentActionName  >> obj.m_defaultAction >> obj.m_actionDatas >> obj.m_touchDownHeight;
    return true;
}

bool operator<<(QDataStream &output, const HQmlModelDispenseActionSettings &obj)
{
    output <<  obj.m_actionNames <<  obj.m_currentActionName << obj.m_defaultAction << obj.m_actionDatas << obj.m_touchDownHeight;
    return true;
}
