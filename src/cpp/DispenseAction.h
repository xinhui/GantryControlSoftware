#ifndef DISPENSEACTION_H
#define DISPENSEACTION_H

#include <QObject>
#include <QDataStream>
#include "hqmlmodelsettingcoord.h"
#include <iostream>

// define the dispensing action, move the tip to the given position and deposite the glue at given speed, time and pressure

class DispenseAction {
public:
    QStringList getStrs()
    {
        return QStringList{QString::number(m_actionVarObj.modeIndex), QString::number(m_actionVarObj.waitingz, 'f', 4), QString::number(m_actionVarObj.gluingz, 'f', 4),
                    QString::number(m_actionVarObj.movingSpeed, 'f', 4), QString::number(m_actionVarObj.movingAcc,'f',4),
                    QString::number(m_actionVarObj.gluingSpeed, 'f', 4), QString::number(m_actionVarObj.gluingAcc,'f',4),
                    QString::number(m_actionVarObj.gluingTime, 'f', 4), QString::number(m_actionVarObj.gluingPress,'f',4),
                    QString::number(m_actionVarObj.x1, 'f', 4), QString::number(m_actionVarObj.y1, 'f', 4), QString::number(m_actionVarObj.x2, 'f', 4), QString::number(m_actionVarObj.y2,'f',4),
                    QString::number(m_actionVarObj.protectionForce, 'f', 4)};
    }
    void setValue(int index, double value)
    {
        if (index > 0 && index <= 13) {
            double *p = (double *)this;
            p[index] = value;
        } else {
           m_actionVarObj.modeIndex = int(value);
        }
    }

    bool setDispenseSettings();
    bool runAction(MPointXYZT offset = MPointXYZT());

    friend bool operator>>(QDataStream &input, DispenseAction &obj)
    {
        input >> obj.m_actionVarObj.modeIndex >> obj.m_actionVarObj.waitingz >> obj.m_actionVarObj.gluingz >> obj.m_actionVarObj.movingSpeed >> obj.m_actionVarObj.movingAcc >> obj.m_actionVarObj.gluingSpeed >> obj.m_actionVarObj.gluingAcc
              >> obj.m_actionVarObj.gluingTime >> obj.m_actionVarObj.gluingPress >> obj.m_actionVarObj.x1 >> obj.m_actionVarObj.y1 >> obj.m_actionVarObj.x2 >> obj.m_actionVarObj.y2 >> obj.m_actionVarObj.protectionForce;
        return true;
    }

    friend bool operator<<(QDataStream &output, const DispenseAction &obj)
    {
        output << obj.m_actionVarObj.modeIndex << obj.m_actionVarObj.waitingz << obj.m_actionVarObj.gluingz << obj.m_actionVarObj.movingSpeed << obj.m_actionVarObj.movingAcc << obj.m_actionVarObj.gluingSpeed << obj.m_actionVarObj.gluingAcc
               << obj.m_actionVarObj.gluingTime << obj.m_actionVarObj.gluingPress << obj.m_actionVarObj.x1 << obj.m_actionVarObj.y1 << obj.m_actionVarObj.x2 << obj.m_actionVarObj.y2 << obj.m_actionVarObj.protectionForce;
        return true;
    }
private:
    struct ActionVars {
        ActionVars() {}
        int modeIndex = 0;
        double waitingz = 0.0;
        double gluingz = 0.0;
        double movingSpeed = 50;
        double movingAcc = 1000;
        double gluingSpeed = 5;
        double gluingAcc = 1000;
        double gluingTime = 1.0;
        double gluingPress = 5.0;
        double x1 = 2.0;
        double y1 = 1.0;
        double x2 = -2.0;
        double y2 = -1.0;
        double protectionForce = 10;

    };
    ActionVars m_actionVarObj;
};

#endif // DISPENSEACTION_H
