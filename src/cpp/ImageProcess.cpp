#include "ImageProcess.h"


namespace ImageProcess{
    double getImageMeanGradValue(cv::Mat src, int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType){
        cv::Mat dst_Sobel;
        // :: Tenegrad - mean gray value ::
        cv::Sobel(src, dst_Sobel, ddepth, dx, dy, ksize, scale, delta, borderType);
        double meanGrayValue = 0.0;
        meanGrayValue = cv::mean(dst_Sobel)[0];

        return meanGrayValue;
    };

    double getImageMeanGrayValue(cv::Mat src){
        cv::Mat meanValueImage;
        cv::Mat meanStdValueImage;
        cv::meanStdDev(src, meanValueImage, meanStdValueImage);
        double meanGrayValue = 0.0;
        meanGrayValue = cv::mean(meanValueImage)[0];

        return meanGrayValue;
    };

    double getImageMeanStdValue(cv::Mat src){
        cv::Mat meanValueImage;
        cv::Mat meanStdValueImage;
        cv::meanStdDev(src, meanValueImage, meanStdValueImage);
        double meanStdDev = 0.0;
        meanStdDev = cv::mean(meanStdValueImage)[0];

        return meanStdDev;
    };

    cv::Mat getBinaryImage(cv::Mat src, double thres_value, double maxval, int type){
        cv::Mat binaryGrayImage;
        cv::threshold(src, binaryGrayImage, thres_value, maxval, type);
        return binaryGrayImage;
    };

    cv::Mat getSpectrumDFT(cv::Mat src){
        cv::Mat dst = {}, src_Gray = {};
        if (src.type() >= 8){
            //src.convertTo(src, src.type()-8);
            std::cout<<"multi channel image, convert to gray image"<<std::endl;
            cv::cvtColor(src, src_Gray, cv::COLOR_RGB2GRAY);
        }
        else{
            std::cout<<"Gray image"<<std::endl;
            src_Gray = src;
        }
        cv::Mat transformMat = {}, imageSpectrum = {};

        cv::Mat planes[] = {cv::Mat_<float>(src_Gray), cv::Mat::zeros(src_Gray.size(), CV_32F)};
        cv::merge(planes, 2, transformMat);

        cv::dft(transformMat, transformMat);
        cv::split(transformMat, planes);
        cv::magnitude(planes[0], planes[1], imageSpectrum);
        imageSpectrum += cv::Scalar(1);
        log(imageSpectrum, imageSpectrum);
        cv::normalize(imageSpectrum, imageSpectrum, 0, 1, cv::NORM_MINMAX);
        imageSpectrum = imageSpectrum(cv::Rect(0, 0, imageSpectrum.cols & -2, imageSpectrum.rows & -2));
        int cx = imageSpectrum.cols / 2;
        int cy =imageSpectrum.rows / 2;
        cv::Mat q0(imageSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(imageSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(imageSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(imageSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);

        dst = imageSpectrum;
        return dst;
    };

    ComplexSpectrum getComplexSpectrum(cv::Mat src){
        ComplexSpectrum complexSpectrum;
        cv::Mat dst, src_Gray;
        if (src.type() >= 8){
            //src.convertTo(src, src.type()-8);
            std::cout<<"multi channel image, convert to gray image"<<std::endl;
            cv::cvtColor(src, src_Gray, cv::COLOR_RGB2GRAY);
        }
        else{
            std::cout<<"Gray image"<<std::endl;
            src_Gray = src;
        }

        cv::Mat transformMat = {}, imageSpectrum = {};
        cv::Mat planes[] = {cv::Mat_<float>(src_Gray), cv::Mat::zeros(src_Gray.size(), CV_32F)};
        cv::merge(planes, 2, transformMat);
        cv::dft(transformMat, transformMat);
        cv::split(transformMat, planes);

        complexSpectrum.transformMat = transformMat;
        complexSpectrum.x = planes[0];
        complexSpectrum.y = planes[1];
        cv::magnitude(planes[0], planes[1], complexSpectrum.magnitude);
        cv::phase(planes[0], planes[1], complexSpectrum.phase);

        cv::magnitude(planes[0], planes[1], imageSpectrum);
        imageSpectrum += cv::Scalar(1);
        log(imageSpectrum, imageSpectrum);
        cv::minMaxLoc(imageSpectrum, &complexSpectrum.mag_min, &complexSpectrum.mag_max);
        cv::normalize(imageSpectrum, imageSpectrum, 0, 1, cv::NORM_MINMAX);
        imageSpectrum = imageSpectrum(cv::Rect(0, 0, imageSpectrum.cols & -2, imageSpectrum.rows & -2));
        int cx = imageSpectrum.cols / 2;
        int cy =imageSpectrum.rows / 2;
        cv::Mat q0(imageSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(imageSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(imageSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(imageSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);
        dst = imageSpectrum;

        return complexSpectrum;
    };


    cv::Mat getSpectrumiDFT(cv::Mat srcSpectrum, ComplexSpectrum complexSpectrum, int nfactor){
        int cx = srcSpectrum.cols / 2.;
        int cy =srcSpectrum.rows / 2.;

        cv::Mat dstSpectrum, complexOutput;
        cv::Mat planes[] = {cv::Mat_<float>(srcSpectrum), cv::Mat::zeros(srcSpectrum.size(), CV_32F)};
        cv::Mat planes_[] = {cv::Mat_<float>(srcSpectrum), cv::Mat::zeros(srcSpectrum.size(), CV_32F)};

        cv::Mat q0(srcSpectrum, cv::Rect(0, 0, cx, cy));
        cv::Mat q1(srcSpectrum, cv::Rect(cx, 0, cx, cy));
        cv::Mat q2(srcSpectrum, cv::Rect(0, cy, cx, cy));
        cv::Mat q3(srcSpectrum, cv::Rect(cx, cy, cx, cy));
        cv::Mat tmp;
        q0.copyTo(tmp); q3.copyTo(q0); tmp.copyTo(q3);
        q1.copyTo(tmp); q2.copyTo(q1); tmp.copyTo(q2);

        cv::split(complexSpectrum.transformMat, planes);
        cv::split(srcSpectrum, planes_);

        cv::merge(planes, 2, complexOutput);
        cv::Mat dsttmp, dst1;
        cv::multiply(complexOutput, srcSpectrum, dstSpectrum);
        cv::split(dstSpectrum, planes);

      /*
        cv::magnitude(planes[0], planes[1], dst1);
        cv::Mat dst1q0(dst1, cv::Rect(0, 0, cx, cy));
        cv::Mat dst1q1(dst1, cv::Rect(cx, 0, cx, cy));
        cv::Mat dst1q2(dst1, cv::Rect(0, cy, cx, cy));
        cv::Mat dst1q3(dst1, cv::Rect(cx, cy, cx, cy));
        cv::Mat dst1tmp;
        dst1q0.copyTo(dst1tmp); dst1q3.copyTo(dst1q0); dst1tmp.copyTo(dst1q3);
        dst1q1.copyTo(dst1tmp); dst1q2.copyTo(dst1q1); dst1tmp.copyTo(dst1q2);


        dst1 += cv::Scalar(1);
        log(dst1, dst1);
        cv::normalize(dst1, dst1, 0, 1, cv::NORM_MINMAX);
        return dst1;
        */

        cv::Mat mag, ph;
        cv::magnitude(planes[0], planes[1], mag);
        cv::phase(planes[0], planes[1], ph);

        cv::Mat dst, complexIDFT, iDFToutput;
        cv::idft(dstSpectrum, complexIDFT);
        cv::split(complexIDFT, planes);
        cv::magnitude(planes[0], planes[1], dst);
        cv::normalize(dst, dst, 1, 0, cv::NORM_MINMAX);

        //std::cout<<"dst type: "<< dst.type()<<std::endl;
        return dst;
    };


    Corners getCornerFeatures(cv::Mat src, int maxCorners, double qualityLevel, double minDistance, cv::InputArray mask, int blockSize, bool useHarrisDetector, double k){
        Corners corners;
        cv::goodFeaturesToTrack(src, corners, maxCorners, qualityLevel, minDistance, mask, blockSize, useHarrisDetector, k);
        return corners;
    };

    Corners getCornerSubPix(cv::Mat src, Corners corners, cv::Size winSize, cv::Size zeroZone, cv::TermCriteria criteria){
        cv::cornerSubPix(src, corners, winSize, zeroZone, criteria);
        return corners;
    };

    cv::Mat getRectROI(cv::Mat src, Corner LeftUP, double width, double height){
        cv::Rect2f SearchArea(LeftUP.x-width/1., LeftUP.y-height/1., width*2., height*2.);

        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        cv::Mat dst=src(cv::Rect(SearchArea.x, SearchArea.y, SearchArea.width, SearchArea.height));

        return dst;
    };

    cv::Mat getRectROI(cv::Mat src, cv::Rect SearchArea){

        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        cv::Mat dst=src(cv::Rect(SearchArea.x, SearchArea.y, SearchArea.width, SearchArea.height));

        return dst;
    };

    cv::Rect RectROI(cv::Mat src, cv::Rect SearchArea){
        if (SearchArea.x < 0){
            SearchArea.x = 0;
        }
        else if (SearchArea.x + SearchArea.width > src.cols){
            SearchArea.width = src.cols - SearchArea.x;
        }
        if (SearchArea.y < 0) {
            SearchArea.y = 0;
        }
        else if (SearchArea.y + SearchArea.height > src.rows){
            SearchArea.height = src.rows - SearchArea.y;
        }
        return SearchArea;
    };

    cv::Mat DilateErode(cv::Mat src, std::vector<int> Mat_erode, std::vector<int> Mat_dilate){
        cv::Mat image_dilate, image_erode;
        cv::Mat element_erode, element_dilate;
        cv::Mat image = {};

        for (int i = 0; i < Mat_dilate.size(); i++){
            if (i == 0){
                image = src;
            }
            if (i != 0){
                image = image_dilate;
            }
            if (true){
                element_erode = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(Mat_erode.at(i),Mat_erode.at(i)));
                cv::erode(image, image_erode, element_erode);
                element_dilate = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(Mat_dilate.at(i),Mat_dilate.at(i)));
                cv::dilate(image_erode, image_dilate, element_dilate);
            }

            image = image_dilate;
            if (i == Mat_dilate.size()-1){
                return image;
            }
        }
    };


};

namespace ImageShow {
    void showImage(cv::Mat image, double scale, const std::string ImageName, bool TrackBar){
        //std::cout<<"image type: "<<image.type()<<std::endl;
        cv::Mat dst;
        if (image.type() < 8 || image.type() >= 16){
            dst = image;

        }

        else if (image.type() >= 8 && image.type() < 16){
            cv::Mat planes[] = {cv::Mat_<float>(image), cv::Mat::zeros(image.size(), CV_32F)};
            cv::split(image, planes);
            cv::magnitude(planes[0], planes[1], dst);
            dst += cv::Scalar(1);
            cv::log(dst, dst);
            cv::normalize(dst,dst,1,0,cv::NORM_MINMAX);
        }

        //std::cout<<"dst type: "<<dst.type() << ", channels: " << dst.channels() << " , rows: "<< dst.rows << " , cols: "<< dst.cols << std::endl;
        if (!TrackBar){
            cv::resize(dst, dst, cv::Size(dst.cols*scale, dst.rows*scale));
            cv::imshow(ImageName, dst);
        }

    };

};

namespace ManualOperation{

};

namespace Filter{
    cv::Mat GaussianBlur(cv::Mat src, cv::Size ksize, double sigmaX, double sigmaY, int borderType){
        cv::Mat dst;
        cv::GaussianBlur(src, dst, ksize, sigmaX, sigmaY, borderType);
        return dst;
    };


    cv::Mat GaussianBandFreqFilter(cv::Mat srcSpectrum, double sigmaX, double sigmaY, double centX, double centY){
        std::cout<<"Gaussian Band freq filter"<<std::endl;
        int width = srcSpectrum.cols;
        int height =srcSpectrum.rows;

        cv::Mat Gauss_band = cv::Mat::zeros(height, width, CV_32FC2);
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                double disX = (std::pow((j-centX),2));
                double disY = (std::pow((i-centY),2));
                Gauss_band.at<float>(i,2*j) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
                Gauss_band.at<float>(i,2*j+1) = 1 - std::expf(-0.5*disX/std::pow(sigmaX,2) + -0.5*disY/std::pow(sigmaY,2));
            }
        }

        return Gauss_band;

    };


    cv::Mat BandFreqFilter(cv::Mat srcSpectrum, double X, double Y, double width, double height){
        std::cout<<"Band freq filter"<<std::endl;
        cv::Mat band = cv::Mat::zeros(srcSpectrum.size(), CV_32FC2);
        if ( X + width >= srcSpectrum.cols){
            width = srcSpectrum.cols - X;
        }
        else if ( Y + height >= srcSpectrum.rows){
            height = srcSpectrum.rows - Y;
        }

        for (int i = 0; i < srcSpectrum.rows; i++){
            for (int j = 0; j < srcSpectrum.cols; j++){
                if ( X < j && j < X + width && Y < i && i < Y + height){
                    band.at<float>(i,2*j) = 0;
                    band.at<float>(i,2*j+1) = 0;
                }
                else{
                    band.at<float>(i,2*j) = 1;
                    band.at<float>(i,2*j+1) = 1;
                }
            }
        }

        return band;
    }

}

namespace Mouse{
    cv::Mat src, dst, dst_ZoomIn;
    cv::Point SelectedPoint = cv::Point(-999., -999.);
    bool isZoomed = true;
    int count = 0;

    static cv::Point pre_pt = cv::Point(-1, -1);
    static cv::Point cur_pt = cv::Point(-1, -1);
    static cv::Point rec_pt = cv::Point(-1, -1);

    void getsrcImage(std::string _str){
        src = cv::imread(_str);
    }

    void on_Mouse(int event, int x, int y, int flags, void* param) {
        cv::Mat tmp, img;
        count = 0;

        if (event == 1){		// Click left button
            pre_pt = cv::Point(x, y);
        }
        else if (event == cv::EVENT_MOUSEMOVE && flags == 1){
            src.copyTo(tmp);
            cv::resize(tmp, tmp, cv::Size(tmp.cols*0.25, tmp.rows*0.25));
            cur_pt = cv::Point(x, y);
            cv::rectangle(tmp, pre_pt, cur_pt, cv::Scalar(255, 255, 0, 0), 1, 8, 0);

            ImageShow::showImage(tmp, 1, "Source Image");
        }
        else if (event == 4){		// release left button
            src.copyTo(img);
            cv::resize(img, img, cv::Size(img.cols*0.25, img.rows*0.25));
            cur_pt = cv::Point(x, y);

            rec_pt = cv::Point(std::min(pre_pt.x, cur_pt.x), std::min(pre_pt.y, cur_pt.y));
            rectangle(img, pre_pt, cur_pt, cv::Scalar(0, 0, 255), 2, 8, 0);
            int width = abs(pre_pt.x - cur_pt.x);
            int height = abs(pre_pt.y - cur_pt.y);
            dst = src(cv::Rect(std::min(cur_pt.x * 4., pre_pt.x * 4.), std::min(cur_pt.y * 4., pre_pt.y * 4.), width * 4., height * 4.));
            dst.copyTo(dst_ZoomIn);
            isZoomed = true;
            std::cout<<pre_pt.x * 4.<<", "<<pre_pt.y * 4.<<std::endl;
            namedWindow("Zoom Image", cv::WINDOW_AUTOSIZE);

            if(isZoomed){
                cv::setMouseCallback("Zoom Image", on_Mouse_ZoomIn, 0);
            }
            ImageShow::showImage(dst, 1, "Zoom Image");
        }
    }

    void on_Mouse_ZoomIn(int event, int x, int y, int flags, void* param){
        cv::Mat tmp;

        static cv::Point cir_pt = cv::Point(-1, -1);
        static cv::Point search_pt = cv::Point(-1, -1);

        if(event == 0){
            dst_ZoomIn.copyTo(tmp);
            search_pt = cv::Point(x, y);

            cv::line(tmp, cv::Point(0, y), cv::Point(tmp.size().width, y), cv::Scalar(0, 0, 255), 1);
            cv::line(tmp, cv::Point(x, 0), cv::Point(x, tmp.size().height), cv::Scalar(0, 0, 255), 1);

            ImageShow::showImage(tmp, 1, "Zoom Image");

        }

        else if(event == 1){		// click left button @ Zoomed Image
            cir_pt = cv::Point(x, y);
            cv::line(dst_ZoomIn, cv::Point(0, y), cv::Point(dst_ZoomIn.size().width, y), cv::Scalar(0, 255, 0), 1);
            cv::line(dst_ZoomIn, cv::Point(x, 0), cv::Point(x, dst_ZoomIn.size().height), cv::Scalar(0, 255, 0), 1);

            ImageShow::showImage(dst_ZoomIn, 1, "Zoom Image");
        }

        else if(event == 4){
            cir_pt = cv::Point(x, y);
            SelectedPoint.x = cir_pt.x + rec_pt.x*4.;
            SelectedPoint.y = cir_pt.y + rec_pt.y*4.;
            std::cout<<"Pos in zoomed picture: " << SelectedPoint.x<<", "<< SelectedPoint.y<<std::endl;
            cv::line(dst_ZoomIn, cv::Point(0, y), cv::Point(dst_ZoomIn.size().width, y), cv::Scalar(255, 0, 255), 1);
            cv::line(dst_ZoomIn, cv::Point(x, 0), cv::Point(x, dst_ZoomIn.size().height), cv::Scalar(255, 0, 255), 1);
            cv::putText(dst_ZoomIn, std::to_string(count), cv::Point(x+10, y+20), cv::FONT_HERSHEY_PLAIN, 3, cv::Scalar(0, 255, 0), 2);
            count=count+1;

            ImageShow::showImage(dst_ZoomIn, 1, "Zoom Image");
        }
    }


    QPointF ClickPoint(){
        QPointF point;
        cv::namedWindow("Source Image", cv::WINDOW_AUTOSIZE);

        cv::setMouseCallback("Source Image", on_Mouse, 0);
        ImageShow::showImage(src, 0.25, "Source Image");

        cv::waitKey(0);
;
        return point;

    }

    QPointF getPointWithMouse(){
        QPointF selectedPoint;
        selectedPoint.setX(SelectedPoint.x);
        selectedPoint.setY(SelectedPoint.y);
        return selectedPoint;
    }
//end namespace Mouse
}

namespace Orb{
    cv::Point2f FindSensorCorner(std::string loc, int _pointsNum, int diskStrelRaius, int thres){
        cv::Mat srcImage = ImageLoading::readImage(loc);

        if (srcImage.empty()){
            std::cout << "File is empty!" << std::endl;
        }
        cv::Ptr<cv::FeatureDetector> featureDetector = cv::ORB::create(_pointsNum);
        std::vector<cv::KeyPoint> vecKeyPoints;
        featureDetector->cv::Feature2D::detect(srcImage, vecKeyPoints);

        //std::cout<< "points list size: " << vecKeyPoints.size() <<std::endl;
        cv::Mat mask = cv::Mat::zeros(srcImage.size(), CV_8UC1);
        for (int j = 0; j < vecKeyPoints.size(); j++){
            mask.at<uchar>(vecKeyPoints.at(j).pt.y, vecKeyPoints.at(j).pt.x) = 255;
        }
        //cv::imshow("origin image", srcImage);
        //cv::imshow("ORB corner points", mask);

        cv::Mat strel = diskStrel(diskStrelRaius);
        cv::Mat dstImage;
        cv::morphologyEx(mask, dstImage, cv::MORPH_CLOSE, strel);

        //cv::imshow("Useful mask", dstImage);

        cv::Mat canny;
        std::vector<cv::Vec4i> linesP1;
        cv::Canny(dstImage, canny, 100, 200);
        cv::HoughLinesP(canny, linesP1, 1, CV_PI / 180, thres);

        //cv::imshow("canny", canny);

        double DelAngle = 45;
        std::vector<cv::Vec4i> lines_horizon, lines_vertical;
        std::cout << linesP1.size()<<std::endl;
        for (int i = 0; i < linesP1.size(); ++i) {
            cv::Vec4i line_ = linesP1[i];
            if(abs(line_[3] - 0) < 100 || abs(line_[3] - 2040) < 100 ||
               abs(line_[1] - 0) < 100 || abs(line_[1] - 2040) < 100 ||
               abs(line_[2] - 0) < 100 || abs(line_[2] - 2432) < 100 ||
               abs(line_[0] - 0) < 100 || abs(line_[0] - 2432) < 100){
                continue;
               }
            double k = (double)(line_[3] - line_[1]) / (double)(line_[2] - line_[0]);
            double angle = atan(k) * 180 / CV_PI;

            if ((angle >= 0.-DelAngle && angle <= 0.+DelAngle) || (angle >= 360.-DelAngle && angle <= 360.+DelAngle)
            || (angle >= 180.-DelAngle && angle <= 180.+DelAngle) || (angle >= -180.-DelAngle && angle <= -180.+DelAngle)){
                lines_horizon.push_back(line_);
            }
            else if ((angle >= 90.-DelAngle && angle <= 90.+DelAngle) || (angle >= -90.-DelAngle && angle <= -90.+DelAngle)
            || (angle >= 270.-DelAngle && angle <= 270.+DelAngle) || (angle >= -270.-DelAngle && angle <= -270.+DelAngle)){
                lines_vertical.push_back(line_);
            }


        }

        std::cout<<"Horizontal: "<<lines_horizon.size()<<", Vertical: " << lines_vertical.size()<<std::endl;


        double s = 2000;
        std::vector<cv::Point2f> pointsFromHorizontalLine;
        std::vector<cv::Point2f> pointsFromVerticalLine;

        for(int i = 0; i < lines_horizon.size();i++){
            pointsFromHorizontalLine.push_back(cv::Point2f(lines_horizon[i][0], lines_horizon[i][1]));
            pointsFromHorizontalLine.push_back(cv::Point2f(lines_horizon[i][2], lines_horizon[i][3]));
            /*
            if( 1){
                double k_hor = (double)(lines_horizon[i][3] - lines_horizon[i][1]) / (double)(lines_horizon[i][2] - lines_horizon[i][0]);
                double angle_hor = atan(k_hor);
                cv::line(img1, cv::Point(lines_horizon[i][2]+s*cos(angle_hor), lines_horizon[i][3]+s*sin(angle_hor)), cv::Point(lines_horizon[i][2]-s*cos(angle_hor) , lines_horizon[i][3]-s*sin(angle_hor)), cv::Scalar(0, 255*(i/lines_horizon.size()), 255), 2);
                cv::circle(img1, cv::Point2f(lines_horizon[i][2], lines_horizon[i][3]), 5, cv::Scalar(255*(i/lines_horizon.size()), 255, 0), 2, 8, 0);
                cv::circle(img1, cv::Point2f(lines_horizon[i][0], lines_horizon[i][1]), 5, cv::Scalar(255*(i/lines_horizon.size()), 255, 0), 2, 8, 0);
            }
            */
        }

        for(int i = 0; i < lines_vertical.size();i++){
            pointsFromVerticalLine.push_back(cv::Point2f(lines_vertical[i][0], lines_vertical[i][1]));
            pointsFromVerticalLine.push_back(cv::Point2f(lines_vertical[i][2], lines_vertical[i][3]));
            /*
            if( 1){
                double k_ver = (double)(lines_vertical[i][3] - lines_vertical[i][1]) / (double)(lines_vertical[i][2] - lines_vertical[i][0]);
                double angle_ver = atan(k_ver);
                cv::line(img1, cv::Point(lines_vertical[i][2]+s*cos(angle_ver), lines_vertical[i][3]+s*sin(angle_ver)), cv::Point(lines_vertical[i][2]-s*cos(angle_ver), lines_vertical[i][3]-s*sin(angle_ver)), cv::Scalar(0, 255*(i/lines_vertical.size()), 255), 2);
                cv::circle(img1, cv::Point2f(lines_vertical[i][2], lines_vertical[i][3]), 5, cv::Scalar(255, 0, 255*(i/lines_vertical.size())), 2, 8, 0);
                cv::circle(img1, cv::Point2f(lines_vertical[i][0], lines_vertical[i][1]), 5, cv::Scalar(255, 0, 255*(i/lines_vertical.size())), 2, 8, 0);
            }
            */
        }
        cv::Vec4f HorizontalLine, VerticalLine;
        cv::fitLine(pointsFromHorizontalLine, HorizontalLine, cv::DIST_L2, 0, 0.01, 0.01);
        cv::fitLine(pointsFromVerticalLine, VerticalLine, cv::DIST_L2, 0, 0.01, 0.01);

        double k_hor = (double)HorizontalLine[1]/(double)HorizontalLine[0];
        double angle_hor = atan(k_hor);

        double k_ver = (double)VerticalLine[1]/(double)VerticalLine[0];
        double angle_ver = atan(k_ver);

        std::cout<<"k_hor: "<<k_hor <<", k_ver: "<<k_ver<<std::endl;

        double b_hor, b_ver;
        cv::Point2f corner;


        if(HorizontalLine[1] == 0 && VerticalLine[0] == 0){
            corner.x = VerticalLine[2];
            corner.y = HorizontalLine[3];
        }
        else if(HorizontalLine[1] == 0){
            b_ver = VerticalLine[3]-k_ver*VerticalLine[2];
            corner.x = (HorizontalLine[3]-b_ver)/(k_ver);
            corner.y = HorizontalLine[3];
        }
        else if(VerticalLine[0] == 0){
            b_hor = HorizontalLine[3]- k_hor*HorizontalLine[2];
            corner.x = VerticalLine[2];
            corner.y = k_hor*VerticalLine[2]+b_hor;
        }
        else{
            b_hor = HorizontalLine[3]- k_hor*HorizontalLine[2];
            b_ver = VerticalLine[3]-k_ver*VerticalLine[2];
            corner.x = (b_ver-b_hor)/(k_hor-k_ver);
            corner.y = (k_hor*b_ver-k_ver*b_hor)/(k_hor-k_ver);
        }

        return corner;
    }
}
