#include "QWriteElement.h"
#include <QFileInfo>
#include <QDir>
#include <QDebug>

QWriteElement::QWriteElement(QObject *parent) :
    QObject(parent),
    m_pLogfile(NULL)
{
    moveToThread(&m_thread);
    m_thread.start();

    connect(this, &QWriteElement::FileNameChanged, this, &QWriteElement::OnFileNameChanged);;
    connect(this, &QWriteElement::MessageReady, this, &QWriteElement::OnMessageReady);
}

void QWriteElement::SetFileName(const QString &fileName)
{
    emit FileNameChanged(fileName);
}

void QWriteElement::WriteMessage(const QString &message)
{
    emit MessageReady(message);
}

void QWriteElement::Close()
{
    m_thread.quit();
    m_thread.wait(2000);
    if (m_pLogfile && m_pLogfile->isOpen())
    {
        m_lock.lock();
        m_pLogfile->close();
        delete m_pLogfile;
        m_pLogfile = NULL;
        m_lock.unlock();
    }
}

qint64 QWriteElement::GetLogSize() const
{
    if (m_pLogfile)
    {
        return m_pLogfile->size();
    }
    return 0;
}

void QWriteElement::OnMessageReady(const QString &mesage)
{
    m_lock.lock();
    if (m_pLogfile && m_pLogfile->isOpen())
    {
        m_pLogfile->write(mesage.toUtf8());
        m_pLogfile->flush();
    }
    m_lock.unlock();
}

void QWriteElement::OnFileNameChanged(const QString &fileName)
{
    if (m_pLogfile)
    {
        QFileInfo current(*m_pLogfile);
        QFileInfo dest(fileName);
        if (current.absoluteFilePath() == dest.absoluteFilePath())
        {
            return;
        }
        m_pLogfile->close();
        m_pLogfile = NULL;
    }
    QFileInfo info(fileName);
    QDir dir = info.absoluteDir();
    if (!dir.exists())
    {
        QString path = dir.absolutePath();
        dir.mkpath(path);
    }
    m_pLogfile = new QFile(fileName);
    if (m_pLogfile->open(QIODevice::WriteOnly))
    {
        qDebug() << "create log file successfully" << fileName;
    }
    else
    {
        qDebug() << "failed to create log file" << fileName;
    }
}
