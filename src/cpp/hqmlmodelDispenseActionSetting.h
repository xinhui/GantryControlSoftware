#ifndef HQMLMODELDISPENSEACTIONSETTING_H
#define HQMLMODELDISPENSEACTIONSETTING_H

#include <QObject>
#include <QDataStream>

#include "src/cpp/DispenseAction.h"
#include "src/cpp/hqmlmodelsettingcoord.h"

class HMotionController;

class HQmlModelDispenseActionSettings : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelDispenseActionSettings(QObject *parent = nullptr);
    ~HQmlModelDispenseActionSettings();

    static HQmlModelDispenseActionSettings *DispensePtr;

    const QStringList &getAtcionNames() {return m_actionNames;}
    DispenseAction getAction(QString actionName) {return m_actionDatas[m_actionNames.indexOf(actionName)];}
    DispenseAction getCurrentAction(){return getAction(m_currentActionName);}
    bool doTouchDown();

signals:
    void updateControlData(int currentCfg, QStringList cfgNames, QStringList cfgDatas, int getter, int setter, double touchDownZ);

public slots:
    QStringList getOutputs();

    void addConfig(QString name);
    void removeConfig(int index);
    void setCurrentConfig(int index);
    void setConfigData(int index, const QString &value);

    void updateUI();

    bool setCurrentDispenseSettings() {return getCurrentAction().setDispenseSettings();};
    bool runCurrentAction() {return getCurrentAction().runAction();};
    QString getTouchDownValue();

protected:
    bool loadConfig();
    bool saveConfig();
    friend bool operator>>(QDataStream &input, HQmlModelDispenseActionSettings &obj);
    friend bool operator<<(QDataStream &output, const HQmlModelDispenseActionSettings &obj);


private:
    QStringList m_actionNames;
    QString m_currentActionName;
    QString m_defaultAction;
    double m_touchDownHeight;

    QVector<DispenseAction> m_actionDatas;

};




#endif // HQMLMODELDISPENSEACTIONSETTING_H
