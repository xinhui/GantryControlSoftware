#include "QLog.h"
#include "QLogWorker.h"
#include <QDateTime>

QMutex QLog::m_mutex;
QMutex QLog::m_useMutex;
QLog *QLog::m_pInstance = NULL;
QtMessageHandler QLog::gDefaultHandler = nullptr;

QLog::QLog(QObject *parent) :
    QObject(parent),
    m_pWorker(new QLogWorker(this))
{
}

QLogWorker *QLog::Worker() const
{
    return m_pWorker;
}

QLog *QLog::Instance()
{
    if (!m_pInstance)
    {
        m_mutex.lock();
        if (!m_pInstance)
        {
            m_pInstance = new QLog();
        }
        m_mutex.unlock();
    }
    return m_pInstance;
}

void QLog::Destroy()
{
    if (m_pInstance)
    {
        m_useMutex.lock();
        m_mutex.lock();
        delete m_pInstance;
        m_pInstance = NULL;
        m_mutex.unlock();
        m_useMutex.unlock();
    }
}

// Note that the log format recommends alignment
void QLog::LogMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    if (gDefaultHandler)
    {
        gDefaultHandler(type, context, msg);
    }
    QByteArray localMsg = msg.toLocal8Bit();
    QString log;
    eLogFlag flag;
    switch (type)
    {
    case QtDebugMsg:
        log.append(tr("[%1][Debug] %2\r\n").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(msg));
        flag = eLog_Debug;
        break;
    case QtInfoMsg:
        log.append(tr("[%1][Info] %2\r\n").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(msg));
        flag = eLog_Info;
        break;
    case QtWarningMsg:
        log.append(tr("[%1][Warning]] %2\r\n").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(msg));
        flag = eLog_Warning;
        break;
    case QtCriticalMsg:
        log.append(tr("[%1][Critical] %2\r\n").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(msg));
        flag = eLog_Critical;
        break;
    case QtFatalMsg:
        log.append(tr("[%1][Fatal] %2\r\n").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")).arg(msg));
        flag = eLog_Fatal;
        break;
    default:
        break;
    }
    if (!log.isEmpty())
    {
        m_useMutex.lock();
        QLog::Instance()->Worker()->WriteLog(flag, log);
        m_useMutex.unlock();
    }
}

void QLog::SetDefaultHandle(QtMessageHandler handler)
{
    gDefaultHandler = handler;
}

void QLog::SetLogFlags(eLogFlags flags)
{
    m_pWorker->SetLogFlags(flags);
}

void QLog::SetLogPath(const QString &path)
{
    m_pWorker->SetLogPath(path);
}

QList<QString> QLog::GetLogList()
{
    return m_pWorker->GetLogList();
}

QString QLog::GetLogDir()
{
    return m_pWorker->GetLogDir();
}

