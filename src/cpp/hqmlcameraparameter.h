#ifndef HQMLCAMARAPARAMETER_H
#define HQMLCAMARAPARAMETER_H

#include <QObject>
#include <QTransform>
#include <QDataStream>
#include "hqmlmodelsettingcoord.h"
#include <opencv2/opencv.hpp>

class HMotionController;

class HQmlCameraParameter : public QObject
{
    Q_OBJECT
public:
    explicit HQmlCameraParameter(QObject *parent = nullptr);
    ~HQmlCameraParameter();


    static HQmlCameraParameter *cameraPtr;

signals:
    void updateAllCameraData(const QString &num, const QString &zFocus,
                            const QString &cameraAli, const QStringList &scanRect,
                            const QStringList chessboardPattern, const QVector<quint16> &alis,
                            const QStringList &cameraMats, const QStringList &distCoeffs,
                            const QStringList &cameraPosOffset, const QStringList &positionInGantry,
                            const QStringList &positionInPicture);
    void updateAllCameraMatrixData(const QStringList &cameraMats);
    void updateAllPIGData(const QStringList &positionInGantry);
    void computeCompleted(bool isOk);

public slots:

    void setCameraAliArray(int index, const QString &text);
    void setCameraAli(const QString &text);

    void setFocusHeight(const QString &text);

    QStringList getAxisFPOS();
    bool moveZ(const QString &zFocus);
    bool moveXY(const QString &x, const QString &y);

    void setScanRangeData(int index,const QString axis, const QString &text);
    void setScanNum(const QString &text);

    void setChessboardPattern(const QString index, const QString axis, const QString &text);
    void computeInstrisincParMatrix();
    //void showInstrisincParMatrix(int row, int col);

    void setCameraOffset(const QString axis, const QString &text);
    void setPositionInGantry(const QString axis, const QString &text);
    void setPositionInPicture(const QString axis, const QString &text);

    void transPictureToGantry();
    QPointF transPicureToGantry(QPointF _pointInPicture);
    void updateCameraData();
    void updateCameraMatrixData();
    void updatePIGData();



protected:
    friend QDataStream &operator>>(QDataStream &input, HQmlCameraParameter &obj);

    friend QDataStream &operator<<(QDataStream &output, const HQmlCameraParameter &obj);

private:

    bool loadConfig();
    bool saveConfig();

    //QString getFilename =
    bool getCaliCameraPos(std::vector<cv::Point2f> &pixel, const QPointF &pos, quint32 num, quint16 ali);
    bool moveToZ(double pos, bool isWait = false);
    bool moveToXY(const QPointF &pos, bool isWait = false);
    bool moveToXYWait(const QPointF &pos);
    QStringList getStrs_XY(const QPointF &pos);
    QStringList getStrs_Mats(cv::Mat Mat);
    cv::Mat getMats_Strs(const QStringList &QStrMat, int col, int row);

    std::vector<cv::Point3f> BuildChessBoard(const QVector<QPointF> &ChessboardPattern);

    QVector<QPointF> CreateRandomCameraPos(int num, const QVector<QPointF> &array_ScanRectangle);

    cv::Mat getRotationMatrix3D(double theta, double phi, double omega);

private:

    bool m_isOK = false;


    quint32 Num;
    double Z_Focus, X_c, Y_c, Z_c;
    QVector<quint16> Array_CameraAlgoris;
    quint16 CameraAlgoris;

    QVector<QPointF> Array_ScanRectangle;
    QVector<QPointF> ChessboardPattern;
    QVector<QPointF> Array_Recognition;

    cv::Mat CameraMatrix, DistCoeffs;
    std::vector<cv::Mat> RotationMatrix, TransformationMatrix;

    QStringList QStrCameraMats;
    QStringList QStrDistCoeffs;

    QString m_getName, m_letName;

    QPointF CameraPosOffset;
    QPointF PositionInGantry;
    QPointF PositionInPicture;

    HMotionController *m_motion;

};

#endif // HQMLCAMERAPARAMETER_H
