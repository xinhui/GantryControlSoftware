#ifndef HQMLMODELDATAVISUAL_H
#define HQMLMODELDATAVISUAL_H

#include <QObject>

class HMotionController;
class HQmlModelDataVisual : public QObject
{
    Q_OBJECT
public:
    explicit HQmlModelDataVisual(QObject *parent = nullptr);

signals:

public slots:
    QVector<double> getDatas();

    void setSource(int index, bool check) {m_sources[index] = check;}
    bool getSource(int index) {return m_sources[index];}

    void setInterval(int value) {m_interval = value;}
    int getInterval() {return m_interval;}

    void setMaxAxisY(QString value) {m_maxAxisY = value.toDouble();}
    QString getMaxAxisY() {return QString::number(m_maxAxisY);}

    void setMinAxisY(QString value) {m_minAxisY = value.toDouble();}
    QString getMinAxisY() {return QString::number(m_minAxisY);}

    void setAutoAxisY(bool value) {m_autoAxisY = value;}
    bool getAutoAxisY() {return m_autoAxisY;}

    void setMinAxisX(QString value) {m_minAxisX = value.toInt();}
    QString getMinAxisX() {return QString::number(m_minAxisX);}

    void setMaxAxisX(QString value) {m_maxAxisX = value.toInt();}
    QString getMaxAxisX() {return QString::number(m_maxAxisX);}

    void setScrollAxisX(bool value) {m_scrollAxisX = value;}
    bool getScrollAxisX() {return m_scrollAxisX;}

private:
    HMotionController *m_motion;
    QVector<double> m_buffer;

    bool m_sources[5] = {0, 0, 0, 0, 1};
    int m_interval = 1000;
    double m_maxAxisY = 10;
    double m_minAxisY = -10;
    bool m_autoAxisY = true;
    int m_minAxisX = 0;
    int m_maxAxisX = 10000;
    bool m_scrollAxisX = true;
};

#endif // HQMLMODELDATAVISUAL_H
