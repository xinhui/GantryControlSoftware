#include "hqmlmodelflow.h"
#include <QCoreApplication>
#include <QFile>
#include <QThread>
#include <QtConcurrent>

#include "hqmlmodelCapture.h"
#include "hmotioncontroller.h"
#include "hqmlmodelcamera.h"
#include "hqmlmodelsettingcoord.h"
#include "hmathfunctions.h"
#include "hqmlPatternRecognition.h"
#include "hqmlmodelDispensePatternSetting.h"

#include <iostream>


HQmlModelFlow *HQmlModelFlow::flowPtr = nullptr;

HQmlModelFlow::HQmlModelFlow(QObject *parent) : QThread(parent)
{
    std::cout<<"Loading Model Flow"<<std::endl;

    // hard-coded parameters for assembling only one module
    if (!loadConfig()) {
        m_ToolingPos.resize(4);
        m_TargetPos.resize(4);
        m_VCPos.resize(3);
        m_grabs.resize(11);
        m_cameraAlis.fill(0, 8);
        std::cout<<"Loading Model Flow fail!"<<std::endl;
    }
    if (m_ToolingPos.size() != 4) m_ToolingPos.resize(4);
    if (m_TargetPos.size() != 4) m_TargetPos.resize(4);
    if (m_VCPos.size() != 3) m_VCPos.resize(3);
    if (m_grabs.size() != 11) m_grabs.resize(11);
    if (m_cameraAlis.size() != 8) m_cameraAlis.resize(8);
    flowPtr = this;
    m_motion = HMotionController::GetInstance();
    m_motion->addAxisAccVel(Axis_X, m_accVels[0], m_accVels[1]);
    m_motion->addAxisAccVel(Axis_Y, m_accVels[2], m_accVels[3]);
    m_motion->addAxisAccVel(Axis_Z, m_accVels[4], m_accVels[5]);
    m_motion->addAxisAccVel(Axis_T, m_accVels[6], m_accVels[7]);
    //connect the function callCamera and teeeeees
    connect(this, &HQmlModelFlow::callCamera, this, &HQmlModelFlow::teeeees, Qt::BlockingQueuedConnection);

    //connect(this, SIGNAL(HQmlModelFlow::updateGluingOffsetUI(MPointXYZT)), HQmlModelDispensePatternSettings::patternPtr, SLOT(HQmlModelDispensePatternSettings::updateGluingOffsetUI(MPointXYZT)), Qt::DirectConnection);
    connect(this, &HQmlModelFlow::receiveSignalForGluingOffset, HQmlModelDispensePatternSettings::patternPtr, &HQmlModelDispensePatternSettings::receiveSignalForGluingOffset, Qt::DirectConnection);

}

HQmlModelFlow::~HQmlModelFlow()
{
    std::cout<<"quit model flow"<<std::endl;
    saveConfig();
}


bool HQmlModelFlow::runFlow()
{
    if (isRunning()) {
        terminate();
        start(HighestPriority);
    } else {
        start(HighestPriority);
    }
    return true;
}

void HQmlModelFlow::stopFlow()
{
    requestInterruption();
}


QStringList HQmlModelFlow::getCaptureNames()
{
    return HQmlModelCapture::capturePtr->getConfigNames();
}


void HQmlModelFlow::setAccVels(int index, QString str)
{
    int axis = HAxises[index / 2];
    double value = str.toDouble();
    double acc, vel;
    if (index % 2) {
        acc = m_accVels[index - 1];
        vel = value;
    } else {
        acc = value;
        vel = m_accVels[index + 1];
    }
    m_motion->addAxisAccVel(axis, acc, vel);
    m_accVels[index] = str.toDouble();
}

void HQmlModelFlow::setToolingPos(int _index, QString _axis, QString _str)
{   if(_axis == "x"){
        m_ToolingPos[_index].x = _str.toDouble();
    }
    else if(_axis == "y"){
        m_ToolingPos[_index].y = _str.toDouble();
    }
    else if(_axis == "z"){
        m_ToolingPos[_index].z = _str.toDouble();
    }
    else if(_axis == "rot"){
        m_ToolingPos[_index].t = _str.toDouble();
    }
}

void HQmlModelFlow::setTargetPos(int _index, QString _axis, QString _str)
{   if(_axis == "x"){
        m_TargetPos[_index].x = _str.toDouble();
    }
    else if(_axis == "y"){
        m_TargetPos[_index].y = _str.toDouble();
    }
    else if(_axis == "z"){
        m_TargetPos[_index].z = _str.toDouble();
    }
    else if(_axis == "rot"){
        m_TargetPos[_index].t = _str.toDouble();
    }
}

void HQmlModelFlow::setVCPos(int _index, QString _axis, QString _str)
{   if(_axis == "x"){
        m_VCPos[_index].x = _str.toDouble();
    }
    else if(_axis == "y"){
        m_VCPos[_index].y = _str.toDouble();
    }
    else if(_axis == "z"){
        m_VCPos[_index].z = _str.toDouble();
    }
    else if(_axis == "rot"){
        m_VCPos[_index].t = _str.toDouble();
    }

    updateVCInfo();
    updateGluingOffsetInfo();
}

void HQmlModelFlow::setGrab(int index, int value)
{
    m_grabs[index] = HQmlModelCapture::capturePtr->getConfigNames().value(value);
}

void HQmlModelFlow::setCameraAli(int index, quint16 value)
{
    m_cameraAlis[index] = value;
}

int HQmlModelFlow::getGrab(int index)
{
    return HQmlModelCapture::capturePtr->getConfigNames().indexOf(m_grabs.value(index));
}

bool HQmlModelFlow::movePos(int _index, QString _type)
{
    if(_type == "Tooling"){
        return moveToXYZ(m_ToolingPos[_index]);
    }
    else if(_type == "Target"){
        return moveToXYZ(m_TargetPos[_index]);
    }
    else if(_type == "VC"){
        return moveToXYZ(m_VCPos[_index]);
    }
    return false;
}

bool HQmlModelFlow::moveZ(QString zFocus)
{
    return moveToZ(zFocus.toDouble());
}

bool HQmlModelFlow::moveZ(double zFocus)
{
    return moveToZ(zFocus);
}


bool HQmlModelFlow::moveXY(QPointF xyPoint){
    return moveToXY(xyPoint);
}

bool HQmlModelFlow::moveXY(QString x, QString y){
    QPointF PointXY;
    PointXY.setX(x.toDouble());
    PointXY.setY(y.toDouble());
    return moveToXY(PointXY);
}

bool HQmlModelFlow::homeT(){
    return moveToT(0.);
}

bool HQmlModelFlow::rotateT(int _index){
    return moveToT(m_TargetPos[_index].t);
}

bool HQmlModelFlow::rotateT(QString _angle){
    return moveToT(_angle.toDouble());
}

bool HQmlModelFlow::rotateClockwise(int _index){

    QString _type;
    if(_index == 0) {_type = "Sensor1";}
    if(_index == 1) {_type = "Sensor2";}
    if(_index == 2) {_type = "Flex";}
    if(_index == 3) {_type = "Module";}

    m_TargetPos[_index].t = abs(HQmlPatternRecognition::recognitionPtr->getRotation(_type));

    return moveT(0.-m_TargetPos[_index].t);
}

bool HQmlModelFlow::rotateCounterClockwise(int _index){
    QString _type;
    if(_index == 0) {_type = "Sensor1";}
    if(_index == 1) {_type = "Sensor2";}
    if(_index == 2) {_type = "Flex";}
    if(_index == 3) {_type = "Module";}

    m_TargetPos[_index].t = abs(HQmlPatternRecognition::recognitionPtr->getRotation(_type));

    return moveT(m_TargetPos[_index].t);
}

void HQmlModelFlow::getCurrentPos(int _index, QString _type)
{
    if(_type == "Tooling"){
        m_ToolingPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_ToolingPos[_index].y = m_motion->getAxisPosition(Axis_Y);
        m_ToolingPos[_index].z = m_motion->getAxisPosition(Axis_Z);
    }
    else if(_type == "Target"){
        m_TargetPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_TargetPos[_index].y = m_motion->getAxisPosition(Axis_Y);
        m_TargetPos[_index].z = m_motion->getAxisPosition(Axis_Z);
    }
    else if(_type == "VC"){
        m_VCPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_VCPos[_index].y = m_motion->getAxisPosition(Axis_Y);
        m_VCPos[_index].z = m_motion->getAxisPosition(Axis_Z);
    }
    updateCtrl();
}

void HQmlModelFlow::getCurrentXY(int _index, QString _type){
    if(_type == "Tooling"){
        m_ToolingPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_ToolingPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    else if(_type == "Target"){
        m_TargetPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_TargetPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    else if(_type == "VC"){
        m_VCPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_VCPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    updateCtrl();
}

void HQmlModelFlow::getCurrentZ(int _index, QString _type){
    if(_type == "Tooling"){
        m_ToolingPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_ToolingPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    else if(_type == "Target"){
        m_TargetPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_TargetPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    else if(_type == "VC"){
        m_VCPos[_index].x = m_motion->getAxisPosition(Axis_X);
        m_VCPos[_index].y = m_motion->getAxisPosition(Axis_Y);
    }
    updateCtrl();
}


void HQmlModelFlow::getPickingPos(int _index, QString _type, QString _axis){
    if(_axis == "x"){
        if(_type == "Flex"){
            m_TargetPos[_index].x = HQmlPatternRecognition::recognitionPtr->getCentre(_type).x() + pickFlexBiasX;
        }
        else if(_type == "Module"){
            m_TargetPos[_index].x = HQmlPatternRecognition::recognitionPtr->getCentre(_type).x() + pickModuleBiasX;
        }
        else{
            m_TargetPos[_index].x = HQmlPatternRecognition::recognitionPtr->getCentre(_type).x();
        }
    }
    else if(_axis == "y"){
        if(_type == "Flex"){
            m_TargetPos[_index].y = HQmlPatternRecognition::recognitionPtr->getCentre(_type).y() + pickFlexBiasY;
        }
        else if(_type == "Module"){
            m_TargetPos[_index].y = HQmlPatternRecognition::recognitionPtr->getCentre(_type).y() + pickModuleBiasY;
        }
        else{
            m_TargetPos[_index].y = HQmlPatternRecognition::recognitionPtr->getCentre(_type).y();
        }
    }
    else if(_axis == "z"){
        m_TargetPos[_index].z = 70.;
    }
    else if(_axis == "rot"){
        m_TargetPos[_index].t = abs(HQmlPatternRecognition::recognitionPtr->getRotation(_type));
    }

}

void HQmlModelFlow::updateCtrl()
{
    QStringList _strTooling, _strTarget, _strVC, _strRotAngle, _strGrabs, accVels;
    for (int k = 0; k < m_ToolingPos.size(); ++k) {
        _strTooling << m_ToolingPos[k].getStrs_XYZ();
    }
    for (int i = 0; i < m_TargetPos.size(); i++){
        _strTarget << m_TargetPos[i].getStrs_XYZT();
    }
    for (int i = 0; i < m_VCPos.size(); i++){
        _strVC << m_VCPos[i].getStrs_XYZ();
    }

    _strGrabs << m_grabs.toList();

    for (int k = 0; k < m_accVels.size(); ++k) {
        accVels << QString::number(m_accVels[k], 'f', 4);
    }

    auto cfgs = HQmlModelCapture::capturePtr->getConfigNames();
    QList<int> grabs;
    for (int k = 0; k < m_grabs.size(); ++k) {
        grabs << cfgs.indexOf(m_grabs.at(k));
    }
    emit updateUI(_strTooling, _strTarget, _strVC, _strGrabs, grabs, m_cameraAlis, accVels);
}

void HQmlModelFlow::updateTargetInfo(){
    QStringList _strTarget;
    QStringList typeList = {"Sensor1", "Sensor2", "Flex", "Module"};
    for(auto k:typeList){
        if(k == "Sensor1"){
            getPickingPos(0, k, "x");
            getPickingPos(0, k, "y");
            getPickingPos(0, k, "z");
            getPickingPos(0, k, "rot");
            continue;
        }
        if(k == "Sensor2"){
            getPickingPos(1, k, "x");
            getPickingPos(1, k, "y");
            getPickingPos(1, k, "z");
            getPickingPos(1, k, "rot");
            continue;
        }
        if(k == "Flex"){
            getPickingPos(2, k, "x");
            getPickingPos(2, k, "y");
            getPickingPos(2, k, "z");
            getPickingPos(2, k, "rot");
            continue;
        }
        if(k == "Module"){
            getPickingPos(3, k, "x");
            getPickingPos(3, k, "y");
            getPickingPos(3, k, "z");
            getPickingPos(3, k, "rot");
            continue;
        }
    }
    for (int i = 0; i < m_TargetPos.size(); i++){
        _strTarget << m_TargetPos[i].getStrs_XYZT();
    }
    saveConfig();

    emit updateTargetUI(_strTarget);
}

void HQmlModelFlow::updateVCInfo(){
    QStringList _strVC;

    m_VCPos[1].x = m_VCPos[0].x + gapDistance + HQmlPatternRecognition::recognitionPtr->getWidth("Sensor1")/2. + HQmlPatternRecognition::recognitionPtr->getWidth("Sensor2")/2.;
    m_VCPos[1].y = m_VCPos[0].y;
    m_VCPos[1].z = m_VCPos[0].z;

    m_VCPos[2].x = m_VCPos[0].x + disX_Sensor1FlexCentre + (HQmlPatternRecognition::recognitionPtr->getWidth("Sensor1") - 20.18)/2. + (gapDistance-0.15)/2. + biasX_Sensor1FlexCentre;
    m_VCPos[2].y = m_VCPos[0].y + disY_Sensor1FlexCentre + (HQmlPatternRecognition::recognitionPtr->getHeight("Sensor1") - 20.18)/2. + biasY_Sensor1FlexCentre;
    m_VCPos[2].z = m_VCPos[0].z;

    for (int i = 0; i < m_VCPos.size(); i++){
        _strVC << m_VCPos[i].getStrs_XYZ();
    }

    saveConfig();
    emit updateVCUI(_strVC);
}

void HQmlModelFlow::updateGluingOffsetInfo(){
    QStringList _strGluingOffset;
    MPointXYZT glueDot;

    glueDot.x = m_VCPos[0].x + disX_Sensor1CentreFlexHV1 - (HQmlPatternRecognition::recognitionPtr->getWidth("Sensor1") - 20.18)/2.;
    glueDot.y = m_VCPos[0].y + disY_Sensor1CentreFlexHV1 + (HQmlPatternRecognition::recognitionPtr->getHeight("Sensor1") - 20.18)/2.;
    glueDot.z = 30.;
    glueDot.t = 0.;

    std::cout<<"Send gluing offset signal"<<std::endl;
    emit receiveSignalForGluingOffset(glueDot);
}

bool HQmlModelFlow::teeeees(QStringList &result, quint16 fff)
{
    std::cout << "execute command: " << QString("PW,1,%1").arg(fff, 3, 10, QLatin1Char('0')).toStdString() << std::endl;
    qint32 execute_result = HQmlModelCamera::execCommond(QString("PW,1,%1").arg(fff, 3, 10, QLatin1Char('0')), result,50000);
    if (execute_result)
    {
        std::cout << "execut error" << execute_result << std::endl;
        return false;
    }
    if (result.size() != 1)
        return false;
    std::cout << "execute command: T1" << std::endl;
    if (HQmlModelCamera::execCommond("T1", result))
        return false;
    if (result.size() < 4)
        return false;
    if (!result.first().toInt())
        return false;
    return true;
}


bool HQmlModelFlow::loadConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/flowConfig.cfg";
    QFile f(configFile);
    if (!f.open(QIODevice::ReadOnly)) {
        return false;
    }

    bool isok = true;
    QDataStream ds(&f);
    isok &= (ds >> *this).atEnd();
    f.close();

    updateCtrl();

    return isok;
}

bool HQmlModelFlow::saveConfig()
{
    QString configFile = QCoreApplication::applicationDirPath() + "/user/flowConfig.cfg";
    QFile file(configFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream ds(&file);
    ds << *this ;
    file.close();
    return true;
}

bool HQmlModelFlow::moveT(const double &_angle, bool isWait){
    if (!m_motion->moveAbs(Axis_T, _angle, true))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::moveToZ(const double &pos, bool isWait)
{
    if (!m_motion->moveAbs(Axis_Z, pos, true))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::moveToT(const double &_angle, bool isWait){
    if (!m_motion->moveAbs(Axis_T, _angle, true))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::moveToXY(const QPointF &pos, bool isWait)
{
    if (!m_motion->moveAbs(Axis_X, pos.x()))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y()))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}
bool HQmlModelFlow::moveToXYZ(const MPointXYZT &pos, bool isWait)
{
    if (!m_motion->moveAbs(Axis_X, pos.x))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y))
        return false;
    if (!m_motion->moveAbs(Axis_Z, pos.z, true))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::moveToXYZT(const MPointXYZT &pos, bool isWait)
{
    //bool HMotionController::moveAbs(int axis, double pos, double vel, double acc, bool isWait)
    if (!m_motion->moveAbs(Axis_X, pos.x, m_accVels[1], m_accVels[0],true))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y, m_accVels[3], m_accVels[2],true))
        return false;
    if (!m_motion->moveAbs(Axis_Z, pos.z, m_accVels[5], m_accVels[4],false))
        return false;
    if (!m_motion->moveAbs(Axis_T, pos.t, m_accVels[7], m_accVels[6],false))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::moveToZXYT(const MPointXYZT &pos, bool isWait)
{
    //bool HMotionController::moveAbs(int axis, double pos, double vel, double acc, bool isWait)
    if (!m_motion->moveAbs(Axis_Z, pos.z, m_accVels[5], m_accVels[4],true))
        return false;
    if (!m_motion->moveAbs(Axis_X, pos.x, m_accVels[1], m_accVels[0],false))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pos.y, m_accVels[3], m_accVels[2],false))
        return false;
    if (!m_motion->moveAbs(Axis_T, pos.t, m_accVels[7], m_accVels[6],false))
        return false;
    if (isWait) {
        return m_motion->waitAllAxisStop();
    }
    return true;
}

bool HQmlModelFlow::getCameraPos(MPointXYZT &pos, const MPointXYZT &pt, quint16 ali, bool isresss)
{
    if (isresss) {
        if (!m_motion->moveAbs(Axis_Z, pt.z, true))
            return false;
    }
    if (!m_motion->moveAbs(Axis_X, pt.x))
        return false;
    if (!m_motion->moveAbs(Axis_Y, pt.y))
        return false;
    if (!m_motion->waitAllAxisStop())
        return false;
    if (!isresss) {
        if (!m_motion->moveAbs(Axis_Z, pt.z, true))
            return false;
    }
    QThread::msleep(500);
    QStringList cameraResult;
    if (!emit callCamera(cameraResult, ali)) {
        return false;
    }

    pos.x = cameraResult[1].toDouble() - 1216;
    pos.y = cameraResult[2].toDouble() - 1020;
    pos.z = 70.0;
    pos.t = cameraResult[3].toDouble();
    auto trans = HQmlModelSettingCoord::ptr->getCameraTrans();
    auto gggg = QPointF(pt.x, pt.y) - trans.map(pos.toPoint());
    pos.x = gggg.x();
    pos.y = gggg.y();
    return true;
}

QPointF HQmlModelFlow::getGlueDotPos(){
    QPointF GlueDotPos;
    GlueDotPos.setX(m_VCPos[0].x);
    GlueDotPos.setY(m_VCPos[0].y);

    return GlueDotPos;
}

void HQmlModelFlow::pickTooling(const QString &PickReleaseMode)
{   
    std::cout<< (PickReleaseMode).toStdString()<<std::endl;
    QThread *gthread = QThread::create([ = ]() {
        HQmlModelCapture::capturePtr->getFixture(PickReleaseMode);
    });
    gthread->start(QThread::TimeCriticalPriority);
}

void HQmlModelFlow::releaseTooling(const QString &PickReleaseMode)
{
    std::cout<< (PickReleaseMode).toStdString()<<std::endl;
    QThread *gthread = QThread::create([ = ]() {
        HQmlModelCapture::capturePtr->letFixture(PickReleaseMode);
    });
    gthread->start(QThread::TimeCriticalPriority);
}

QDataStream &operator>>(QDataStream &input, HQmlModelFlow &obj)
{
    return input >> obj.m_accVels >> obj.m_ToolingPos >> obj.m_TargetPos >> obj.m_VCPos >> obj.m_grabs >> obj.m_cameraAlis;
}

QDataStream &operator<<(QDataStream &output, const HQmlModelFlow &obj)
{
    return output << obj.m_accVels << obj.m_ToolingPos << obj.m_TargetPos << obj.m_VCPos << obj.m_grabs << obj.m_cameraAlis;
}
