QT += quick
QT += quickcontrols2
TEMPLATE = app
CONFIG += c++11
QT += widgets
QT += core
QT += concurrent
QT += qml
QT += charts
QT += serialport
#qtHaveModule(widgets): QT += widgets

DEFINES += QT_MESSAGELOGCONTEXT
QMAKE_LFLAGS_RELEASE = /INCREMENTAL:NO /DEBUG

QMAKE_LFLAGS += /MANIFESTUAC:\"level=\'requireAdministrator\' uiAccess=\'false\'\"

VERSION = 1.0.0.0

# 0x0004 mean zh
# for detail https://msdn.microsoft.com/en-us/library/dd318693%28vs.85%29.aspx
RC_LANG = 0x0409
RC_ICONS += logo.ico
#RC_ICONS += file.ico

QMAKE_TARGET_COMPANY = Coretech
QMAKE_TARGET_PRODUCT =  Chip Paster
QMAKE_TARGET_DESCRIPTION = Coretech
QMAKE_TARGET_COPYRIGHT = Copyright(C) 2021 Coretech


TARGET = ChipPaster
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0




SOURCES += \
    src/cpp/DispenseAction.cpp \
    src/cpp/DispensePattern.cpp \
    src/cpp/ImageLoading.cpp \
    src/cpp/ImageProcess.cpp \
    src/cpp/QLogWorker.cpp \
    src/cpp/QWriteElement.cpp \
    src/cpp/Qlog.cpp \
    src/cpp/hqmlLoading.cpp \
    src/cpp/hqmlLoadingCoordSetting.cpp \
    src/cpp/hqmlLoadingDispensingSetting.cpp \
    src/cpp/hqmlPatternRecognition.cpp \
    src/cpp/hqmlcameraparameter.cpp \
    src/cpp/hqmlmodelCapture.cpp \
    src/cpp/hqmlmodelflow.cpp \
    src/cpp/hqmlmodelDispenseActionSetting.cpp \
    src/cpp/hqmlmodelDispensePatternSetting.cpp \
    src/cpp/hqmlmodelcamera.cpp \
    src/cpp/grapimageprovider.cpp \
    src/cpp/hmotioncontroller.cpp \
    src/cpp/hnetworkcommond.cpp \
    src/cpp/hqmlmodeldatavisual.cpp \
    src/cpp/hqmlmodelgluecontroller.cpp \
    src/cpp/hqmlmodelio.cpp \
    src/cpp/hqmlmodelmotion.cpp \
    main.cpp \
    src/cpp/hqmlmodelsettingcoord.cpp \
    src/cpp/hqmlmodelsettingflex.cpp \
    src/cpp/hqmlmodeltoolbar.cpp \
    src/cpp/hqmlPatternRecognition.cpp \
    src/cpp/Algorithm.cpp \
    src/cpp/hqmlLoading.cpp \
    src/cpp/hqmlLoadingDispensingSetting.cpp \
    src/cpp/hqmlLoadingCoordSetting.cpp \


RESOURCES += \
    qml.qrc \
    icons.qrc\


HEADERS += \
    src/cpp/DispenseAction.h \
    src/cpp/DispensePattern.h \
    src/cpp/ImageLoading.h \
    src/cpp/ImageProcess.h \
    src/cpp/QLog.h \
    src/cpp/QLogWorker.h \
    src/cpp/QWriteElement.h \
    src/cpp/hqmlLoading.h \
    src/cpp/hqmlLoadingCoordSetting.h \
    src/cpp/hqmlLoadingDispensingSetting.h \
    src/cpp/hqmlPatternRecognition.h \
    src/cpp/hqmlcameraparameter.h \
    src/cpp/hqmlmodelCapture.h \
    src/cpp/hqmlmodelflow.h \
    src/cpp/hqmlmodelDispenseActionSetting.h \
    src/cpp/hqmlmodelDispensePatternSetting.h \
    src/cpp/hqmlmodelcamera.h \
    src/cpp/grapimageprovider.h \
    src/cpp/hmathfunctions.h \
    src/cpp/hmotioncontroller.h \
    src/cpp/hnetworkcommond.h \
    src/cpp/hqmlmodeldatavisual.h \
    src/cpp/hqmlmodelgluecontroller.h \
    src/cpp/hqmlmodelio.h \
    src/cpp/hqmlmodelmotion.h \
    src/cpp/hqmlmodelsettingcoord.h \
    src/cpp/hqmlmodelsettingflex.h \
    src/cpp/hqmlmodeltoolbar.h \
    src/cpp/hqmlPatternRecognition.h \
    src/cpp/Algorithm.h \
    src/cpp/hqmlLoading.h \
    src/cpp/hqmlLoadingDispensingSetting.h \
    src/cpp/hqmlLoadingCoordSetting.h \




win32: LIBS += -L$$PWD/../libs/acs/ -lACSCL_x64
LIBS += -lDbgHelp

CONFIG(debug, debug|release) {
      LIBS+="$$PWD/../libs/OpenCv/opencv4.1.1/x64/lib/opencv_world411d.lib"
}
else {
      LIBS+="$$PWD/../libs/OpenCv/opencv4.1.1/x64/lib/opencv_world411.lib"
}

INCLUDEPATH += $$PWD/../libs/acs\
               $$PWD/../libs/OpenCv/opencv4.1.1/x64/include\
#DEPENDPATH += $$PWD/libs/acs
